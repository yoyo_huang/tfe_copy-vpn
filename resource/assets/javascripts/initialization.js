'use strict';

(function() {
  //form actions
  $("body").on('click',".module-form-input", function(){
    $(this).parent().removeClass("is-error");
    $(this).parent().parent().removeClass("is-error");
  });

  $("body").on('touchstart',".module-form-input",function(e) {
      // e.preventDefault(); //try fix safari break
  });
  //index
  $(".btn-knowmore-clickable").click(function() {
      var nextscroll = $('#scene2').offset().top;
      $('html,body').animate({ scrollTop:( parseInt(nextscroll) )}, 1200);
  });
  $("body").on('click', 'i.btn-bars.icon-list', function() {
    // alert("click hamburg");
    $("body").find(".list-header,.list-header-bg,#header").addClass('is-on');
  });
  $("body").on('click', 'i.btn-bars.icon-x', function() {
    // alert("click hamburg");
    $("body").find(".list-header,.list-header-bg,#header").removeClass('is-on');
  });
  
  //custom select 
  $("body").on('click', '.module-customselect', function() {
    $(".module-customselect.is-on").not(this).removeClass('is-on');
    $(this).toggleClass('is-on');
  });
  $("body").on('click', '.module-customselect li', function() {
    var mytext = $(this).text();
    var myvalue = $(this).val();
    $(this).parent().parent().find('.module-customselect-selected').text(mytext).next("input").val(myvalue);
  });
  //CLOSE ALL SELECTS ON CLICKING ANYOTHERS
  $( "body" ).click(function( event ) {
    //whether himself
    if ( event.target.nodeName == "DIV" && event.target.className != "module-customselect" ){
      $("body").find(".module-customselect").removeClass('is-on');
    }
    //whether his child
    else if( event.target.nodeName == "UL" || event.target.nodeName == "LI" || event.target.nodeName == "SPAN" ){
      if ( ! $(event.target).parents(".module-customselect") ){
        $("body").find(".module-customselect").removeClass('is-on');
      }
    //any others
    }else if ( event.target.nodeName != "DIV" && event.target.nodeName != "UL" && event.target.nodeName != "LI" && event.target.nodeName != "SPAN" ){
      $("body").find(".module-customselect").removeClass('is-on');
    }
  });

  //product-detail
  $("body").on('click', '.switch-joiner', function() {
    $(this).closest('#joiner-result').toggleClass('is-off');
  });

  //product-list
  $("body").on('click', '.btn-accordion', function() {
    $(this).next('.module-accordion').toggleClass('is-off-m');
  });

  $("body").on('click', '.module-product-innerbox-top,.module-product-innerbox-bottom', function() {
    // window.location.href = 'product-detail.html';
  });

  //on cart
  $("body").on('click', '.btn-cart-checkall', function() {
	$("#allClick").attr("src","../resource/assets/images/icon-check-ob.png");
	$("#allCancel").attr("src","../resource/assets/images/icon-check-wb.png");
    $(".module-checkbox input").prop("checked",true);
  });
  $("body").on('click', '.btn-cart-discheckall', function() {
	$("#allClick").attr("src","../resource/assets/images/icon-check-wb.png");
	$("#allCancel").attr("src","../resource/assets/images/icon-check-ob.png");
    $(".module-checkbox input").prop("checked",false);
  });
  // 執行 FastClick
  FastClick.attach(document.body);
})();