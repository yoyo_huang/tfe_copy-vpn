function showMaxMin() {

	var $min = $('input[name="min"]');
	var $max = $('input[name="max"]');
	var $step = $('input[name="step"]');
	var $r = $('input[type=range]');

	$(".num-max").text($('input[name="max"]').val());
	$(".num-min").text($('input[name="min"]').val());
	var attributes = {
		min : $min[0].value,
		max : $max[0].value,
		step : $step[0].value
	};
	$r.attr(attributes);
	$r.rangeslider('update', true);
}
function bidderHighlight(myvalue) {
	$.each($(".bidder-profile"), function() {
		if (myvalue > parseInt($(this).attr("data-price"))) {
			$(this).addClass("style-highlight");
			$("body").find(".on-styling").removeClass('on-styling');
			$("body").find(".style-highlight").eq(0).addClass('on-styling');
		} else {
			$(this).removeClass("style-highlight");
		}
	});
}
function syncMobileInput() {
	var amoutVal = $("body").find(".js-amount-input").val();
	$(".js-amount-input-m").val(parseInt(amoutVal));
	//TODO:這一邊如果是作真實的拖拉效果。從這裡開始加
}
function  syncDeskInput(amountVal) {
	//var amountVal = $("body").find(".js-amount-input-m").val();
	$(".js-amount-input").val(parseInt(amountVal));
	//TODO:這一邊如果是作真實的拖拉效果。從這裡開始加
}

$(function() {
	var $document = $(document);
	var $r = $('input[type=range]');
	var $min = $('input[name="min"]');

	console.log('$min' + $min);
	console.log('$min.value()' + $min.val());

	var $max = $('input[name="max"]');
	var $step = $('input[name="step"]');
	var $amount = $('.js-amount-input');
	document.createElement('output');
	var output = document.querySelectorAll('output')[0];
	console.log("步驟一，如果有新增其他任何output元件，請在此新增var。");
	var output1 = 0;
	output1 = document.querySelectorAll('output')[1] ? document.querySelectorAll('output')[1] : 0;//如果有新增其他output元件，請在此新增一個var。
	var output2 = 0;
	output2 = document.querySelectorAll('output')[2] ? document.querySelectorAll('output')[2] : 0;//如果有新增其他output元件，請在此新增一個var。
	bidderHighlight(parseInt($('.js-amount-input').val()));
	// set initial output value
	console.log("步驟二，在此指定新增的output的初始值。");
//	output.innerHTML = $r[0].value;
	output1.innerHTML = 0;//在此指定第二個output的初始值
	output2.innerHTML = 0;//在此指定第三個output的初始值
	var userPayment = parseInt($('input[id="userPayment"]').val());
	var userTotalPeriods = parseInt($('input[id="userTotalPeriods"]').val());
	var userCurrentPeriods = parseInt($('input[id="userCurrentPeriods"]').val());
	var userBiddedCnt = parseInt($('input[id="userBiddedCnt"]').val());
	var userGuranCreditFee = parseInt($('input[id="userGuranCreditFee"]').val());
	// update output value
	$document.on('input', 'input[type="range"]', function(e) {
		output.innerHTML = ( ( (userPayment - e.currentTarget.value) * (userTotalPeriods - userCurrentPeriods) ) + ( userPayment * (userCurrentPeriods - 1) ) - ( userGuranCreditFee * userBiddedCnt * (userTotalPeriods - userCurrentPeriods) ))//math here
		console.log("步驟三，在此指定新增的output的連動值。");
		output1.innerHTML = e.currentTarget.value; //在此指定第二個output的連動值
		output2.innerHTML = e.currentTarget.value; //在此指定第三個output的連動值
		bidderHighlight(e.currentTarget.value);
	});
	// Initialize
	$r.rangeslider({
		polyfill : false,
	});
	var attributes = {
		min : $min[0].value,
		max : $max[0].value,
		step : $step[0].value
	};
	$r.attr(attributes);
	$r.rangeslider('update', true);

	$r.on('input', function() { /*拉霸改input*/
		//console.log("$r:"+this.value);
		$amount[0].value = this.value;
		syncMobileInput();
	});
	$amount.on('input', function() { /*input改拉霸*/
		if (isNaN(this.value)){
			//console.log("$amount.isNaN:"+this.value);
			return
		}else{
			//console.log("$amount:"+this.value);
			var value = this.value;
			if(value === ''){
				value = 0;
			}
			$r.val(value).change();
			syncMobileInput();
		}
	});

	// Example functionality to demonstrate programmatic attribute changes
	$document.on('change', 'input', function(e) { /*input重設max min step*/
		var attributes = {
			min : $min[0].value,
			max : $max[0].value,
			step : $step[0].value
		};
		$r.attr(attributes);
		$r.rangeslider('update', true);
		showMaxMin();
	});

	$document.on('change', 'input.js-amount-input-m', function(e) {/*同步手機版*/
		console.log("input.js-amount-input-m.change:"+this.value);
		if(isNaN(this.value)){
			//alert("請輸入數字1");
			//$("#bidAmount2").focus();
			return;
		}
		syncDeskInput(this.value);
		$r.val(this.value).change();
	});
	showMaxMin();
	syncMobileInput();
});//ver20161122