//未完成募集用,bidType: 1:check,2:joining,
function showBidshlv(controlID, shelID, crClass, bidTermM, bidTermC, bidIntendNumber, bidFulMember, isClosed, bidDate, bidType, hrefURL, addFunction)
{
	if(controlID)
	{
		var bidShlv = {shelID:shelID, crClass: crClass, bidTermM:bidTermM, bidTermC:bidTermC, bidIntendNumber:bidIntendNumber, bidFulMember:bidFulMember, isClosed:isClosed, bidDate:bidDate };
		
		var htmlcontent = getShowBidshlvString(bidShlv, bidType, hrefURL, addFunction);
		$("#" + controlID).append(htmlcontent);
	}
}

//完成募集用: bidType: 0:purchased, 1:notbid, 2:bidding
function showBidshlvForFullOrStart(controlID, shelID, crClass, bidTermM, bidTermC, bidIntendNumber, bidFulMember, isClosed, bidDate, bidType, currentTerm, hrefURL, addFunction, specialTagAndFuction)
{
	if(controlID)
	{
		var bidShlv = {shelID:shelID, crClass: crClass, bidTermM:bidTermM, bidTermC:bidTermC, bidIntendNumber:bidIntendNumber, bidFulMember:bidFulMember, isClosed:isClosed, bidDate:bidDate, currentTerm:currentTerm, isStart:true, specialTagAndFuction:specialTagAndFuction };
		
		var htmlcontent = getShowBidshlvString(bidShlv, bidType, hrefURL, addFunction);
		$("#" + controlID).append(htmlcontent);
	}
}

//isFullOrStart: bidType: 0:purchased, 1:notbid, 2:bidding
//others: bidType: 1:check,2:joining;
function getShowBidshlvString(bidShlv, bidType, hrefURL, addFunction) 
{
	var htmlcontent = '';
	if(bidShlv)
	{				
		var firstDiv = '<div class="module-product-box">';
		var isClosed = bidShlv.isClosed == undefined ? false : bidShlv.isClosed == 'Y';
		var isFullOrStart = bidShlv.isStart == undefined ? false : bidShlv.isStart;
		if(isClosed)
		{
			firstDiv = '<div class="module-product-box style-finishd">';		
		}
		else if(isFullOrStart)
		{
			if(bidType == 0)
			{
				firstDiv = '<div class="module-product-box style-start sale-status-purchased">';
			}
			else if(bidType == 1)
			{
				firstDiv = '<div class="module-product-box style-start sale-status-notbid">';
			}
			else if(bidType == 2)
			{
				firstDiv = '<div class="module-product-box style-start sale-status-bidding">';
			}			
			else
			{
				firstDiv = '<div class="module-product-box style-start">';
			}
		}
		else if(bidType == 1)
		{
			firstDiv = '<div class="module-product-box style-check">';
		}
		else if(bidType == 2)
		{
			firstDiv = '<div class="module-product-box style-joining">';
		}
		
		var Percentage = (bidShlv.bidIntendNumber!=undefined && bidShlv.bidFulMember != undefined && bidShlv.bidFulMember > 0) ?(bidShlv.bidIntendNumber/bidShlv.bidFulMember)*100 : 0; 
		htmlcontent = firstDiv +	
'<div class="module-product-inbox">' +
'	<div class="module-product-innerbox">' +
'		<div class="module-product-innerbox-top style-' + bidShlv.crClass.substring(0, 1) + '">' +
'			<span class="module-product-serialnumber">'+ bidShlv.shelID +'</span>' +												
'			<span class="module-product-class">' + bidShlv.crClass + '</span>' +
'		</div>' +
'		<div class="module-product-innerbox-bottom">'+
'			<p>每期<span class="dollarsign">NT$</span><b>' + numberWithCommas(bidShlv.bidTermM) + '</b></p>';
		if(bidShlv.currentTerm != undefined && bidShlv.currentTerm != '')
		{
			htmlcontent += 
'			<p>第' + bidShlv.currentTerm + '期&nbsp;/共' + bidShlv.bidTermC + '期</p>'; 
		}
		else
		{
			htmlcontent +=
'			<p>共<b>' + bidShlv.bidTermC + '</b>期</p>';
		}
		htmlcontent +=
'		</div>' + 
'		<div class="module-product-bar-containter">' +
'			<div class="module-product-bar-valueimg" style="width: ' + Percentage + '%;"></div>' +
'			<div class="module-product-bar-number">已募集' + bidShlv.bidIntendNumber + '人/共' + bidShlv.bidFulMember + '人</div>' +
'		</div>' +
'		<div class="module-product-innerbox-cover">' +
'			<a href="' + hrefURL + '"><i class="fa fa-search"></i>查看詳細</a>' +
'		</div>' +
'	</div>' +
'</div>';
		if(!isClosed && !isFullOrStart && bidType == 1)
		{
			htmlcontent += 
				('<label class="module-checkbox">' +
				 '<input type="checkbox" name="buy" value="' + bidShlv.shelID + '" onclick="' + addFunction + '">' +
				 '<i class="module-symbol"/></label>');			
		}
		else if(addFunction != null && addFunction != '')
		{
			htmlcontent += '<i class="icon-add" onclick="' + addFunction + '" ></i>';
		}
		else if (!isClosed && isFullOrStart && bidType < 4)
		{
			htmlcontent += '<i class="icon-add" ></i>';
		}
		if(bidShlv.specialTagAndFuction != undefined && bidShlv.specialTagAndFuction != '')
		{
			htmlcontent += bidShlv.specialTagAndFuction;			
		}
		if (!isClosed && bidShlv.bidDate != undefined && bidShlv.bidDate != '')
		{
			htmlcontent += '<br><p align="center">開標日:' + bidShlv.bidDate.substring(0 , 4) + ' / ' + bidShlv.bidDate.substring(4 , 6) + ' / ' + bidShlv.bidDate.substring(6 , 8) + '</p>';
		}		
		htmlcontent += '</div>';
		//console.log(htmlcontent);		
	}
	return htmlcontent;
}

//bidstatus: 1:bidding,2:purchased,others:notbid
function showBidSell(controlID, shelID, bidSellValue, payment, bidCurrentPeriod, periods, bidOrignValue, bidPrepayPeriod, bidstatus, hrefURL, addFunction) 
{
	if(controlID)
	{
		var bidSell = {shelID:shelID, bidSellValue: bidSellValue, payment:payment, bidCurrentPeriod:bidCurrentPeriod, periods:periods, bidOrignValue:bidOrignValue, bidPrepayPeriod:bidPrepayPeriod };
		
		var htmlcontent = getShowBidSellString(bidSell, bidstatus, hrefURL, addFunction);
		$("#" + controlID).append(htmlcontent);
	}
}

//bidstatus: 1:bidding,2:purchased,others:notbid
function getShowBidSellString(bidSell, bidstatus, hrefURL, addFunction) 
{
	var htmlcontent = '';
	if(bidSell)
	{
		var firstDiv = '<div class="module-product-box style-sale sale-status-notbid">';
		if(bidstatus == 1)
		{
			firstDiv = '<div class="module-product-box style-sale sale-status-bidding">';
		}
		else if(bidstatus == 2)
		{		
			firstDiv = '<div class="module-product-box style-sale sale-status-purchased">';
		}
										
		htmlcontent = firstDiv +
'<div class="module-product-inbox">' +
'	<div class="module-product-innerbox">' +
'		<div class="module-product-innerbox-top">' +
'			<span class="module-product-serialnumber">'+ bidSell.shelID +'</span>' +
'			<span class="module-product-saleinfo">12%off</span> ' +
'			<p class="module-product-saledollar">NT$</span><b>' + numberWithCommas(bidSell.bidSellValue) + '</b></p>' +
'	    </div>' +
'	    <div class="module-product-innerbox-bottom">' +
'	    	<p>每期<span class="dollarsign">NT$</span><b>' + numberWithCommas(bidSell.payment) + '</b></p>' +
'	    	<p>第' + bidSell.bidCurrentPeriod + '期/共' + bidSell.periods + '期</p>' +
'	    </div>' +
'	    <div class="module-product-bar-containter">' +
'	    	<div class="module-product-bar-valueimg" style="width:35%;" />' +
'	    	<div class="module-product-bar-number">' + (bidSell.bidOrignValue + (bidSell.bidPrepayPeriod * bidSell.payment) - bidSell.bidSellValue) / (bidSell.bidOrignValue + (bidSell.bidPrepayPeriod * bidSell.payment)) + '</div>' +
'	    </div>' +
'	    <div class="module-product-innerbox-cover">' +
'			<a href="' + hrefURL + '"><i class="fa fa-search"></i>查看詳細</a>' +
' 	</div>' +
'</div>';
if(addFunction == null || addFunction == '') 
	htmlcontent += '<i class="icon-add" />'; 
else 
	htmlcontent += '<i class="icon-add" onclick="' + addFunction + '" />';
htmlcontent += '</div>';
		//console.log(htmlcontent);				
	}
	return htmlcontent;
}

//給體驗區專用的,bidType:0:募集中,1:已成會,2:已成會,3:已加入
function showBidshlvForExperience(controlID, shelID, crClass, bidTermM, bidTermC, bidIntendNumber, bidFulMember, bidType, iscurrent, currentTerm, hrefURL)
{
	if(controlID)
	{
		var Percentage = (bidIntendNumber!=undefined && bidFulMember != undefined && bidFulMember > 0) ?(bidIntendNumber/bidFulMember)*100 : 0;
		var htmlcontent = '<div class="module-product-box">';
		if(bidType==1)
		{
			htmlcontent = '<div class="module-product-box style-start sale-status-notbid">';
		}
		else if(bidType==2)
		{
			htmlcontent = '<div class="module-product-box style-start sale-status-notbid hidd">';		
		}
		else if(bidType==3)
		{
			htmlcontent = '<div class="module-product-box style-start sale-status-purchased">';		
		}
		htmlcontent +=
'	<div class="module-product-inbox">';
		if(iscurrent)
		{
			htmlcontent += 
'		<div class="experience_cover experience_current">';
			if(bidType==0)
			{
				htmlcontent +=
'			<a href="' + hrefURL + '"></a>';				
			}
			else if(bidType==1)
			{
				htmlcontent +=
'			<a href="#col_box" class="do-popup-login cboxElement"></a>';
			}
		}
		else
		{
			htmlcontent +=
'		<div class="experience_cover">';	
		}
		htmlcontent +=
'		</div>' +
'		<div class="module-product-innerbox">'+
'			<div class="module-product-innerbox-top style-' + crClass.substring(0, 1) + '">' +
'				<span class="module-product-serialnumber">' + shelID + '</span>' +
'				<span class="module-product-class">' + crClass + '</span>' +
'			</div>' +
'			<div class="module-product-innerbox-bottom">' +
'				<p>每期<span class="dollarsign">NT$</span><b><font id="payment">' + numberWithCommas(bidTermM) + '</font></b>' +
'				</p>';
		if(currentTerm=='')
		{
			htmlcontent +=
'				<p>共<b>' + bidTermC + '</b>期</p>';
		}
		else
		{
			htmlcontent +=
'			<p>第' + currentTerm + '期&nbsp;/共' + bidTermC + '期</p>';

		}
		htmlcontent +=
'			</div>' +
'			<div class="module-product-bar-containter">' +
'				<div id="percentBar" class="module-product-bar-valueimg"style="width: ' + Percentage + '%;"></div>' +
'				<div class="module-product-bar-number">已募集' + bidIntendNumber + '人/共' + bidFulMember + '人</div>' +
'			</div>';
		if(bidType==3)
		{
			htmlcontent +=
'			<div class="module-product-innerbox-cover">' +
' 				<a href="product-detail.html">' +
'					<i class="fa fa-search"></i>' +
'					查看詳細' +
'				</a>' +
'			</div>';			
		}
		htmlcontent +=
'		</div>' +
'	</div>';
		if(bidType==1)
		{
			htmlcontent +=
'	<i class="icon-add" href="#popup_content"></i>';			
		}
		else
		{
			htmlcontent +=
'   <i class="icon-add"></i>';
		}
		htmlcontent +=
'</div>';
	
		//console.log(htmlcontent);	
		$("#" + controlID).append(htmlcontent);
	}
}


function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

