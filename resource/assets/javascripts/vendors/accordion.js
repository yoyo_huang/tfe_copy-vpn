'use strict';

(function() {
  $(".accordion-item").find(".accordion-item-title").on('click', function() {
    $(this).parent().toggleClass('on').find(".fa").toggleClass('fa-rotate-180');
  });

  $(".accordion-menu").on('click', function() {
    $(this).toggleClass('is-active');
  });


})()