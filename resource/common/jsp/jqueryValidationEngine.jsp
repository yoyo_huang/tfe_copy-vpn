<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	//********************************************************************
	// *** Set the path variable to the Context Path of the application ***
	// ********************************************************************
	String path = request.getContextPath();

%>
<script type="text/javascript" src="<%=path%>/resource/common/js/jquery-1.11.3.min.js"></script>
<link rel="stylesheet" href="<%=path%>/resource/common/css/validationEngine.jquery.css">
<script src="<%=path%>/resource/common/js/jquery.validationEngine-zh_CN.js"></script> 
<script src="<%=path%>/resource/common/js/jquery.validationEngine.js"></script> 
<!-- jquery.validationEngine-zh_CN.js 为配置文件，可根据需求自行调整或增加，也可以更换为其他语言配置文件 --> 
