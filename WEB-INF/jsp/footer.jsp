<footer id="footer">

	<div class="layout-mw-wrapper">
		<div class="row row1">
			<div class="col-left">
				<a href="${pageContext.request.contextPath}/qa">
					<span class="btn-circle">
						<i class="icon-qna"></i>
						<p>會員須知</p>
					</span>
				</a>
				<a href="${pageContext.request.contextPath}/statistics"> 
					<span class="btn-circle">
						<i class="icon-statistic"></i>
						<p>統計專區</p>
					</span>
				</a>
				<a href="${pageContext.request.contextPath}/blacklist">
					<span class="btn-circle">
						<i class="icon-blacklist"></i>
						<p>黑名單</p>
					</span>
				</a>
			</div>
			<div class="col-right">
				<p>
					<a href="${pageContext.request.contextPath}/news">最新消息</a>
					/
					<a href="${pageContext.request.contextPath}/media">媒體報導</a>
					/
					<a href="${pageContext.request.contextPath}/story">案例介紹</a>
					/
					<a href="https://www.youtube.com/channel/UCk0CzSupZWrcnaSXRmNXFyA/videos?view=0&sort=dd&shelf_id=0" target="_blank">影片分享</a>
				</p>
				<p>
					<a href="${pageContext.request.contextPath}/about">關於我們</a>
					/
					<a href="${pageContext.request.contextPath}/join">加入我們</a>
					/
					<a href="${pageContext.request.contextPath}/contact">聯絡我們</a>
					/
					<a href="https://www.facebook.com/shacomTFE/" target="_blank">
						<img src="${pageContext.request.contextPath}/resource/assets/images/fb_logo.jpg" style="width: 20px"> TFE粉絲團
					</a>
				</p>
			</div>
		</div>
		<div class="row row2">
			<p>
				<a href="${pageContext.request.contextPath}/termsOfService">服務條款</a>
				/
				<a href="${pageContext.request.contextPath}/policyOfPrivacy">隱私權政策</a>
				/
				<a href="${pageContext.request.contextPath}/patentStatement">專利聲明</a>
			</p>
			<p class="text-copycight">2015© Taiwan Fund Exchange. All rights reserved.</p>
		</div>
	</div>
</footer>
