<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%-- <%@ include file="/resource/common/jsp/changeDiv.jsp"%> --%>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/index.css" media="screen" rel="stylesheet" type="text/css" />
<style>
	#scene1{ 
		background:white; 
	}
	.coop-font{
		font-size: 103%;
	}
</style>
</head>
<body>
	<div class="layout-container">
		<div class="layout-view">
			<section id="scene1" style="background-image: url('');">
				<section id="scene1-inner">
				<div class="bannerswiper-outer">
					<div class="bannerswiper-container">
						<div class="swiper-wrapper" style="cursor: pointer;">
							<c:forEach items="${bcList}" var="eachBC">
								<c:forEach items="${eachBC.advertisementManges}" var="adver" varStatus="loop">
									<div class="advertisementdiv swiper-slide" onclick='jumpPage("${adver.isOut}","${adver.imageUrl}");return;'
										style="background-image: url('${adver.imagePath}');"></div>
								</c:forEach>
							</c:forEach>
						</div>
					</div>
				</div>
					<div class="bannerswiper-outer on-mobile">    
					    <div class="bannerswiper-container">
					        <div class="swiper-wrapper">
					            <c:forEach items="${bcList}" var="eachBC">
									<c:forEach items="${eachBC.advertisementManges}" var="adver" varStatus="loop">
										<div class="advertisementdiv swiper-slide" onclick='jumpPage("${adver.isOut}","${adver.imageUrl}");return;'
											style="background-image: url('${adver.imagePath}');"></div>
                    <!-- 請在background-image這裡置放相對應的手機版banner圖片連結 -->
									</c:forEach>
								</c:forEach>
					        </div>
					    </div>
					</div>

					<div class="scene1-bottom">
						<div class="btn-knowmore">
							<div class="bannerswiper-pagination"></div>
							<span class="btn-knowmore-clickable">
								<b>了解更多</b>
								<br />
								<img class="btn-knowmore-clickable on-desk" src="${pageContext.request.contextPath}/resource/assets/images/icon-arrow-down.png" alt="">
								<img class="btn-knowmore-clickable on-mobile" src="${pageContext.request.contextPath}/resource/assets/images/icon-arrow-down-mobile.png"
									alt="">
							</span>
						</div>
					</div>
				</section>
				<div class="bannerswiper-outer"></div>
			</section>
			<section id="scene2">
				<!-- Swiper -->
				<div class="swiper-outer">
					<div class="swiper-container on-desk">
						<div class="swiper-wrapper">
							<c:forEach items="${p2pList}" var="eachP2P">
								<c:forEach items="${eachP2P.advertisementManges}" var="adver">
									<div class="swiper-slide">
										<img width="768" height="500" src="${adver.imagePath}" />
									</div>
								</c:forEach>
							</c:forEach>
						</div>
						<div class="swiper-pagination"></div>
					</div>
					<div class="swiper-container on-mobile">
						<div class="swiper-wrapper">
							<c:forEach items="${p2pList}" var="eachP2P">
								<c:forEach items="${eachP2P.advertisementManges}" var="adver">
									<div class="swiper-slide">
										<img width="320" height="340" src="${adver.imagePath}" />
									</div>
								</c:forEach>
							</c:forEach>
						</div>
						<div class="swiper-pagination"></div>
					</div>
					<div class="swiper-button-prev"></div>
					<div class="swiper-button-next"></div>
				</div>
				<!-- END OF Swiper -->
			</section>
			<section id="scene3">
				<h2 class="scene-title">模擬體驗</h2>
				<i class="icon-underline"></i>
				<h3 class="scene-subtitle">請讓TFE了解你的金融期望</h3>
				<div class="choose-chara">
					<div class="choose-chara-center">
						<img src="${pageContext.request.contextPath}/resource/assets/images/icon-longarrow-left.png" class="arrow-horizonal" alt="">
						<img src="${pageContext.request.contextPath}/resource/assets/images/icon-chara-choose.svg" class="choose-chara-center-img btn-lr" alt="">
						<img src="${pageContext.request.contextPath}/resource/assets/images/icon-longarrow-right.png" class="arrow-horizonal" alt="">
						<p>請選擇金融角色</p>
					</div>
					<div class="choose-chara-circle circle-right"  onclick="window.open('experienceC1?semType=1','_self');return;">
						<div class="choose-chara-innercircle">
							<img class="circleimg" src="${pageContext.request.contextPath}/resource/assets/images/icon-chara2.svg" alt="">
							<span class="circletxt">我是貸款者</span>
						</div>
						<div class="choose-chara-innercircle-hover">
							<img class="circleimg" src="${pageContext.request.contextPath}/resource/assets/images/icon-chara2-hover.svg" alt="">
							<span class="circletxt">我是貸款者</span>
						</div>
					</div>
					<div class="choose-chara-circle circle-left"  onclick="window.open('experienceC1?semType=0','_self');return;">
						<div class="choose-chara-innercircle">
							<img class="circleimg" src="${pageContext.request.contextPath}/resource/assets/images/icon-chara1.svg" alt="">
							<span class="circletxt">我是投資者</span>
						</div>
						<div class="choose-chara-innercircle-hover">
							<img class="circleimg" src="${pageContext.request.contextPath}/resource/assets/images/icon-chara1-hover.svg" alt="">
							<span class="circletxt">我是投資者</span>
						</div>
					</div>
				</div>
			</section>
			<c:if test='${informationSetupList.get(3).IFSDisable eq 1}'>
				<section id="scene4">
					<c:if test='${not empty informationManageList.get(3)}'>
						<div class="col-left" style="background-image: url('${informationManageList.get(3).imageUrl}');"></div>
						<div class="col-right">
							<h2 class="scene-title">${informationManageList.get(3).infoTitle}</h2>
							<p class="story-content">${informationManageList.get(3).infoTitle1}</p>
							<div class="btn-rounded" onclick='jumpAnotherPage("story","${informationManageList.get(3).id.imid}")'>了解我的投資故事</div>
						</div>
					</c:if>
				</section>
			</c:if>
			<section id="coop-list" class="is-off">
				<div class="layout-mw-wrapper">
					<h3 class="scene-subtitle">合作夥伴</h3>
					<i class="icon-underline"></i>
					<ul class="clearfix">
						<li>
							<a href="http://www.davinci.idv.tw/" class="coop-inner">
								<span class="coop-font">法律諮詢</span>
								<img src="${pageContext.request.contextPath}/resource/assets/images/coop-2.jpg" alt="達文西法律事務所">
							</a>
						</li>
						<li>
							<a href="http://tayouan.weebly.com/" class="coop-inner">
								<span class="coop-font">法律諮詢</span>
								<img src="${pageContext.request.contextPath}/resource/assets/images/coop-3.jpg" alt="大員法律事務所">
							</a>
						</li>
						<li>
							<a href="http://www2.deloitte.com/tw/tc/legal/about-deloittetw.html" class="coop-inner">
								<span class="coop-font">會計簽證</span>
								<img src="${pageContext.request.contextPath}/resource/assets/images/coop-4.jpg" alt="勤業眾信會計師事務所">
							</a>
						</li>
						<li>
							<a href="http://www2.deloitte.com/tw/tc/legal/about-deloittetw.html" class="coop-inner">
								<span class="coop-font">ISO27001認證</span>
								<img src="${pageContext.request.contextPath}/resource/assets/images/coop-5.jpg" alt="勤業眾信會計師事務所">
							</a>
						</li>
					</ul>
					<span class="btn-more-coop" onclick="$(this).closest(&quot;#coop-list&quot;).toggleClass(&quot;is-off&quot;);">
						更多合作夥伴<i class="fa fa-angle-down" aria-hidden="true"></i>
					</span>
				</div>
			</section>
			<section id="scene5">
				<div class="layout-mw-wrapper">
				<c:forEach var="setup" items="${informationSetupList}" end="2" varStatus="status">
					<c:set var="ifsimid" scope="page" value="${setup.ifsimid}"/>
					<c:if test="${ifsimid eq 1}">
						<c:set var="pageLocation" scope="page" value="news"/>
					</c:if>
					<c:if test="${ifsimid eq 2}">
						<c:set var="pageLocation" scope="page" value="media"/>
					</c:if>
					<c:if test="${ifsimid eq 4}">
						<c:set var="pageLocation" scope="page" value="story"/>
					</c:if>
					<c:if test="${setup.IFSDisable eq 1}">
						<div class="module-news-block">
							<div onclick='jumpAnotherPage("${pageLocation}","${informationManageList.get(status.index).id.imid}")'  class="module-news-img btn-readmore" style="background-image: url('${informationManageList.get(status.index).imageUrl}');"></div>
							<div class="module-news-text">
								<h3>${fn:replace(fn:replace(informationManageList.get(status.index).infoTitle,"<p>",""),"</p>","")}</h3>
								<p>${fn:replace(fn:replace(informationManageList.get(status.index).infoContent,"<p>",""),"</p>","")}</p>
							</div>
						</div>
					</c:if>
				</c:forEach>
				</div>
				<div class="scene5-bottom">
					<div class="btn-readmore">
						<span>
							<font id="findMore">查看更多</font>
						</span>
					</div>
				</div>
			</section>
			<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o.svg" width="100%" style="margin-top: -1px;" alt="">
		</div>
	</div>
	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/swiper.js" type="text/javascript"></script>
	<script>
		var swipperTime=Number("${swipperTime}");
		var swiperBanner = new Swiper('.bannerswiper-container:not(.on-mobile)', {
			pagination : '.bannerswiper-pagination',
			paginationClickable : true,
			speed : 1000,
			spaceBetween : 0,
			centeredSlides : true,
			autoplay : true,
			autoplayDisableOnInteraction : false,
			autoplay: (swipperTime*1000),
			effect : 'fade'
		});

		var swiperBannerMobile = new Swiper('.bannerswiper-container.on-mobile', {
			pagination : '.bannerswiper-pagination',
			paginationClickable : true,
			speed : 1000,
			spaceBetween : 0,
			centeredSlides : true,
			autoplay : true,
			autoplayDisableOnInteraction : false,
			autoplay: (swipperTime*1000),
			effect : 'fade'
		});

		
	    var swiperDesk = new Swiper('.swiper-container.on-desk', {
	        pagination: '.swiper-container.on-desk .swiper-pagination',
	        nextButton: '.swiper-button-next',
	        prevButton: '.swiper-button-prev',
	        paginationClickable: true,
	        //speed: 1000,
	        spaceBetween: 0,
	        centeredSlides: true,
	        //autoplay: false,
	        autoplayDisableOnInteraction: false
	    });
	    
	    var swiperMobile = new Swiper('.swiper-container.on-mobile', {
	        pagination: '.swiper-container.on-mobile .swiper-pagination',
	        nextButton: '.swiper-button-next',
	        prevButton: '.swiper-button-prev',
	        paginationClickable: true,
	        //speed: 1000,
	        spaceBetween: 0,
	        centeredSlides: true,
	        //autoplay: false,
	        autoplayDisableOnInteraction: false
	    });
	    
	    
// 		var swiperDesk = new Swiper('.swiper-container.on-desk', {
// 			pagination : '.swiper-pagination',
// 			nextButton : '.swiper-button-next',
// 			prevButton : '.swiper-button-prev',
// 			paginationClickable : true,
// 			speed : 1000,
// 			spaceBetween : 0,
// 			centeredSlides : true,
// 			autoplay: false,
// 			autoplayDisableOnInteraction : false,
// 			autoplay : 3000,
// 			effect : 'fade'
// 		/*
// 		pagination: '.swiper-pagination',
// 		nextButton: '.swiper-button-next',
// 		prevButton: '.swiper-button-prev',
// 		paginationClickable: true,
// 		speed: 1000,
// 		spaceBetween: 0,
// 		centeredSlides: true,
// 		autoplay: false,
// 		autoplayDisableOnInteraction: false
// 		 */
// 		});
// 		var swiperMobile = new Swiper('.swiper-container.on-mobile', {
// 			pagination : '.swiper-pagination',
// 			nextButton : '.swiper-button-next',
// 			prevButton : '.swiper-button-prev',
// 			paginationClickable : true,
// 			speed : 1000,
// 			spaceBetween : 0,
// 			centeredSlides : true,
// 			//autoplay: false,
// 			autoplayDisableOnInteraction : false,
// 			autoplay : 3000,
// 			effect : 'fade'
// 		/*
// 		pagination: '.swiper-pagination',
// 		nextButton: '.swiper-button-next',
// 		prevButton: '.swiper-button-prev',
// 		paginationClickable: true,
// 		speed: 1000,
// 		spaceBetween: 0,
// 		centeredSlides: true,
// 		autoplay: false,
// 		autoplayDisableOnInteraction: false
// 		 */
// 		});
// 		swiperBanner.params.control = swiperDesk;
// 		swiperDesk.params.control = swiperBanner;
	</script>
	<script>
	
	function jumpPage(isOut,imageUrl){
		console.log("isOut:"+isOut);
		console.log("imageUrl"+imageUrl);
		if(imageUrl.length>0){
			if(isOut=='1'){
				window.open(imageUrl,'_blank');	
			}else{
				window.open(imageUrl,'_self');
			}
		}
	}
	$("#findMore").click(function(){
		window.open('${pageContext.request.contextPath}/news','_self');
	});
	
	function jumpAnotherPage(pageLocation,id) {
		//alert(pageLocation);
		window.open(pageLocation + 'Detail?id=' + id + '&page=' + '1', '_self');
	}
	
	
	</script>
</body>
</html>
