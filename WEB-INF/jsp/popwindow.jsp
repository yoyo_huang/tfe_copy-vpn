<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<div class="layout-container">
	<!-- Body Page -->
	<tiles:insertAttribute name="body" />
</div>
