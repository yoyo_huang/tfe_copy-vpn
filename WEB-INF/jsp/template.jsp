<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<tiles:importAttribute name="javascripts"/>

<%-- <%@ include file="/resource/common/jsp/changeDiv.jsp"%> --%>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <meta name="google-site-verification" content="cHjSzUT9MgIeazBlmq_l0mKzyFfAqEAArZHXcSYGgyY" />
    <meta charset="UTF-8">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="Title" content="TFE"/>
    <meta name="Author" content=""/>
    <meta name="Description"
          content="「台灣資金交易所」大幅改良流傳千年的民間「標會」機制，使用者在平台可以自主決定借款利率，解決了當今網路金融直接存借的問題。除此之外，使用者投資，建立信用等人生各階段金融需求皆可在平台上滿足，開創傳統銀行之外的另一個新選擇。"/>
    <meta name="keywords" content="台灣資金交易所"/>
    <!-- iOS -->
    <!-- Android -->
    <meta property="og:title" content="台灣資金交易所"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="http://www.taiwanfundexchange.com.tw/TFEFrontend/index"/>
    <meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/fb_title_image.jpg"/>
    <meta property="og:site_name" content="TFE"/>
    <meta property="og:description"
          content="「台灣資金交易所」大幅改良流傳千年的民間「標會」機制，使用者在平台可以自主決定借款利率，解決了當今網路金融直接存借的問題。除此之外，使用者投資，建立信用等人生各階段金融需求皆可在平台上滿足，開創傳統銀行之外的另一個新選擇。"/>
    <meta property="og:locality" content="Taipei"/>
    <meta property="og:country-name" content="Taiwan"/>

    <link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon"
          type="image/x-icon"/>
    <title>TFE台灣資金交易所 - 最聰明的網路借貸平台!</title>
    <script type="text/javascript">
        document.createElement('header');
        document.createElement('nav');
        document.createElement('menu');
        document.createElement('section');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('footer');
    </script>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
          media="screen" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/swiper.css" media="screen"
          rel="stylesheet" type="text/css"/>
</head>
<body>

<div class="layout-container">
    <!-- Header -->
    <tiles:insertAttribute name="header"/>

    <!-- Body Page -->
    <tiles:insertAttribute name="body"/>

    <!-- Footer Page -->
    <tiles:insertAttribute name="footer"/>
</div>

<!-- scripts -->
<c:forEach var="script" items="${javascripts}">
    <script src="<c:url value="${script}"/>"></script>
</c:forEach>
<!-- end scripts -->

</body>
</html>