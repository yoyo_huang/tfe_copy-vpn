<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script>
<style>

	.btn-rounded.style-thin {
		line-height: 2.4;
	}

	.btn-rounded.style-gray-thin {
		color: #4d4d4d;
		background-color: #dddddd;
		line-height: 2.4;
	}

</style>

	<header id="header">
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
						(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-76941557-1', 'auto');
			ga('send', 'pageview');

		</script>

		<div class="layout-mw-wrapper">
			<div class="header-logo">
				<a href="${pageContext.request.contextPath}/index">
					<img src="${pageContext.request.contextPath}/resource/assets/images/tfe_logo.svg" alt="" >
				</a>
				<!-- <i class="fa fa-bars on-mobile btn-bars"></i> -->
				<!-- <i class="fa fa-times btn-bars"></i> -->
				<i class="icon-list on-mobile btn-bars"></i>
				<i class="icon-x on-mobile btn-bars"></i>
			</div>
			<div class="header-right">
				<i class="header-icon icon-set"></i>
				<a class="link-name" href="javascript:void(0);" onclick="directTo('productList');return false;">投資 / 借貸</a>
				<i class="header-icon icon-idea"></i>
				<a class="link-name" href="${pageContext.request.contextPath}/qa">會員須知</a>
				<i class="header-icon icon-login"></i>
				<c:choose>
					<c:when test="${accountVO ne null}">
						<a class="link-name" href="${pageContext.request.contextPath}/logout">登出</a>
						<i class="header-icon icon-user"></i>
						<a class="link-name" href="${pageContext.request.contextPath}/mypage">會員專區</a>
					</c:when>
					<c:otherwise>
						<a class="link-name do-popup-login" href="#popup-login" onclick="clearInput();clearErrorMsg();replaceVerifyCode('login')">登入</a>
						<i class="header-icon icon-user"></i>
						<a class="link-name" href="${pageContext.request.contextPath}/register">註冊</a>
					</c:otherwise>
				</c:choose>
				<i class="header-icon icon-cart"></i>
				<a class="link-name" href="${pageContext.request.contextPath}/product/cart">追蹤清單</a>
			</div>
		</div>
	</header>
	<div class="on-mobile list-header-bg btn-bars"></div>
	<ul class="on-mobile list-header">
		<li class="list-header-item">
			<i class="header-icon icon-set"></i>
			<a class="link-name" href="javascript:void(0);" onclick="directTo('productList');return false;">投資 / 借貸</a>
		</li>
		<li class="list-header-item ">
			<i class="header-icon icon-idea"></i>
			<a class="link-name" href="${pageContext.request.contextPath}/qa">會員須知</a>
		</li>
		<li class="list-header-item ">
			<i class="header-icon icon-login"></i>
			<c:choose>
				<c:when test="${accountVO ne null}">
					<a class="" href="${pageContext.request.contextPath}/logout">登出</a>
				</c:when>
				<c:otherwise>
					<a class="link-name do-popup-login" href="#popup-login" onclick="clearInput();clearErrorMsg();replaceVerifyCode('login')">登入</a>
				</c:otherwise>
			</c:choose>
		</li>
		<li class="list-header-item ">
			<i class="header-icon icon-user"></i>
			<c:choose>
				<c:when test="${accountVO ne null}">
					<a class="link-name" href="${pageContext.request.contextPath}/mypage">會員專區</a>
				</c:when>
				<c:otherwise>
					<a class="link-name" href="${pageContext.request.contextPath}/register">註冊</a>
				</c:otherwise>
			</c:choose>
		</li>
		<li class="list-header-item">
			<i class="header-icon icon-cart"></i>
			<a class="link-name" href="${pageContext.request.contextPath}/product/cart">追蹤清單</a>
		</li>
		<form id="directForm" action="${pageContext.request.contextPath}/product/productList" method="post">
			<input type="hidden" name="identification" value="${accountVO.identification}">
		</form>
	</ul>
	<!-- 登入的選單畫面 -->
	<div class="hidden-popup-contents" style="display: none;">
		<div id="popup-login" class="module-popup">
			<h3>會員登入</h3>
			<form id="loginForm" action="${pageContext.request.contextPath}/userLogin" method="post">
				<div class="form-block">
					<label class="module-form-label" for="identityid">身分證字號</label>
					<input type="text" id="identityid" name="identityid" class="module-form-input" placeholder="" maxlength="10">
				</div>
				<div class="form-block">
					<label class="module-form-label" for="account">會員代號</label>
					<input type="text" id="loginAccount" name="loginAccount" class="module-form-input" placeholder="" maxlength="16">
				</div>
				<div class="form-block">
					<label class="module-form-label" for="password">使用者密碼</label>
					<input type="password" id="password" name="password" class="module-form-input" placeholder="" maxlength="12">
				</div>
				<div class="form-block style-verifycode">
					<label for="">驗證碼</label>
					<span class="verifycode-wrap">
						<input type="text" id="verifycode" name="verifycode" class="module-form-input style-rounded" placeholder="輸入驗證碼">
						<img id="verifyImg" src="" alt="">
						<i class="fa fa-refresh" onclick="replaceVerifyCode('login')"></i>
					</span>
				</div>
				<p id="loginErr" class="form-warning-text">您輸入的資料有誤，請重新確認。</p>
				<label class="module-checkbox">
					<input type="checkbox" id="rememberme" name="rememberme"/>
					<i class="module-symbol"></i>
					<span>記住身分證與會員代號</span>
				</label>
				<div class="btn-wrap style-center">
					<div id="loginBtn" class="btn-rounded style-thin" onclick="goTFESubmit()">登入</div>
				</div>
				<div class="btn-wrap style-center">
					<a class="do-popup-forgotpw style-normal" href="#popup-forgotpw" onclick="clearInput();clearErrorMsg();replaceVerifyCode('pw');">忘記密碼？</a>
					<a class="do-popup-forgotidpw style-normal" href="#checkSuccessMsg-idpw" onclick="clearInput();clearErrorMsg();">忘記帳號與密碼？</a>
				</div>
			</form>
			<p id="phoneCheck" class="alert-msg" style="display: none;">
				您的會員帳號尚未完成手機號碼驗證，請至
				<a href="${pageContext.request.contextPath}/registerPhoneService" class="btn-inlinelink">手機號碼驗證</a>
			</p>
			<p id="mailCheck" class="alert-msg" style="display: none;">
				您的會員帳號尚未完成電子郵件驗證，請至
				<a href="${pageContext.request.contextPath}/registerUser2" class="btn-inlinelink">電子信箱驗證</a>
			</p>
			<div class="btn-wrap">
				還未成為我們會員嗎？<div class="btn-rounded style-thin" onclick="goRegister()" >註冊</div>
			</div>
		</div>
		<div id="popup-forgotpw" class="module-popup">
			<h3>忘記密碼</h3>
			<form action="">
				<div class="form-block">
					<label class="module-form-label" for="identityid">身分證字號</label>
					<input type="text" id="pwIdentityid" name="identityid" class="module-form-input" placeholder="" maxlength="10">
				</div>
				<div class="form-block">
					<label class="module-form-label" for="account">會員代號</label>
					<input type="text" id="pwAccount" name="pwAccount" class="module-form-input" placeholder="" maxlength="16">
				</div>
				<div class="form-block style-verifycode">
					<label for="">驗證碼</label>
					<span class="verifycode-wrap">
						<input type="text" id="pwVerifycode" name="pwVerifycode" class="module-form-input style-rounded" placeholder="輸入驗證碼">
						<img id="pwVerifyImg" src="" alt="">
						<i class="fa fa-refresh" onclick="replaceVerifyCode('pw')"></i>
					</span>
				</div>
				<p id="findPassErr" class="form-warning-text">您輸入的資料有誤，請重新確認。</p>
				<div class="btn-wrap">
					<div class="btn-rounded style-thin" onclick="findPass()">找回密碼</div>
					<a class="do-popup-login style-normal" href="#popup-login" onclick="clearInput();clearErrorMsg();replaceVerifyCode('login');">返回會員登入</a>
				</div>
			</form>
		</div>
		<div id="checkSuccessMsg-pw" class="module-popup">
			<h3>忘記密碼</h3>
			系統將發出一組「臨時密碼」至您的手機簡訊，<br>
			請登入「台灣資金交易」更新您的密碼，謝謝。
		</div>
		<div id="checkSuccessMsg-idpw" class="module-popup">
			<h3>忘記帳號密碼</h3>
			請來電洽詢台灣資金交易所小組<br>
			0800-800-917<br>
			客服人員將依據您註冊所提供的<br>
			資料進行身分核對做後續處理。
		</div>
	</div>
	<script>
		$(document).ready(
			function() {
				//login popup and forgot pw popup
				$(".do-popup-login").colorbox({inline:true});
				$(".do-popup-forgotpw").colorbox({inline:true});
				$(".do-popup-forgotidpw").colorbox({inline:true});
				
				if ($.cookie("rememberme") == "1") {
					$("#rememberme").prop("checked", true)
					$("#identityid").val($.cookie("identityid"));
					$("#loginAccount").val($.cookie("acct"));
					$("#password").val($.cookie("pd"));
				}
			}
		);
		
		function replaceVerifyCode(doWhat){
			if(doWhat == 'login'){
				$("#verifyImg").attr("src", "${pageContext.request.contextPath}/userLogin/getCaptchaImage?" + (new Date().getTime()));
			}else if(doWhat == 'pw'){
				$("#pwVerifyImg").attr("src", "${pageContext.request.contextPath}/userLogin/getCaptchaImage?" + (new Date().getTime()));
			}
		}
		
		function goTFESubmit(){
			$.ajax({
				type:"POST",
				url: "<c:url value='/userLogin'/>",
				data:{
					identification: $("#identityid").val(),
					userId: $("#loginAccount").val(),
					password: $("#password").val(),
					verifycode: $("#verifycode").val()
				},
				cache:false, 
				async:false,
				success: function(backmsg){ //取得回傳訊息
					if (backmsg == "帳密錯誤"){
						$("#identityid").parent().addClass("is-error");
						$("#loginAccount").parent().addClass("is-error");
						$("#password").parent().addClass("is-error");
						$("#loginErr").html("您輸入的資料有誤，請重新確認。");
						$("#loginErr").show();
					}else if(backmsg == "圖形驗證碼錯誤"){
						$("#verifycode").parent().parent().addClass("is-error");
						$("#loginErr").html("您輸入的驗證碼有誤，請重新確認。");
						$("#loginErr").show();
					}else if(backmsg == "信箱尚未認證"){
						$("#mailCheck").show();
					}else if(backmsg == "手機尚未認證"){
						$("#phoneCheck").show();
					}else{
						saveUserInfo();
						location.replace("<c:url value='/mypage'/>");
					}
				}, 
				error: function(){
					$("#identityid").parent().addClass("is-error");
					$("#loginAccount").parent().addClass("is-error");
					$("#password").parent().addClass("is-error");
					$("#verifycode").parent().parent().addClass("is-error");
					$("#loginErr").html("未預期錯誤");
					$("#loginErr").show();
				}
			});
			$.colorbox.resize();
		}
		
		function idpwMessage(){
			$.colorbox({ inline: true, href: "#checkSuccessMsg-idpw" });
		}
		
		function goRegister(){
			window.open("${pageContext.request.contextPath}/register", "_self");
		}
		
		var findPassClick = 0;
		function findPass(){
			$("#mailTarget").val("pw");
			$.ajax({
				type:"POST",
				url: "<c:url value='/findPassValid'/>",
				data:{
					identification: $("#pwIdentityid").val(),
					userId: $("#pwAccount").val(),
					verifycode: $("#pwVerifycode").val(),
				},
				cache:false, 
				async:false,
				success: function(backmsg){ //取得回傳訊息
					if (backmsg == "帳密錯誤"){
						$("#pwIdentityid").parent().addClass("is-error");
						$("#pwAccount").parent().addClass("is-error");
						$("#findPassErr").html("您輸入的資料有誤，請重新確認。");
						$("#findPassErr").show();
						findPassClick = findPassClick + 1;
						if(findPassClick >= 3){
							findPassClick = 0;
							$.colorbox({ inline: true, href: "#popup-login" });
						}
					}else if(backmsg == "圖形驗證碼錯誤"){
						$("#pwVerifycode").parent().parent().addClass("is-error");
						$("#findPassErr").html("您輸入的驗證碼有誤，請重新確認。");
						$("#findPassErr").show();
					}else{
						findPassClick = 0;
						clearErrorMsg();
						$.colorbox({ inline: true, href: "#checkSuccessMsg-pw" });
					}
				},
				error: function(){
					$("#pwIdentityid").parent().addClass("is-error");
					$("#pwAccount").parent().addClass("is-error");
					$("#findPassErr").html("未預期錯誤");
					$("#findPassErr").show();
				}
			});
			$.colorbox.resize();
		}
		
		function clearInput(){
			$("#pwIdentityid").val("");
			$("#pwAccount").val("");
			$("#pwVerifycode").val("");
			$("#verifycode").val("");
			
		}
		
		function clearErrorMsg(){
			$("#mailCheck").hide();
			$("#phoneCheck").hide();
			$(".form-warning-text").hide();
			$(".is-error").each(function() {
				$(this).removeClass("is-error");
			});
		}
		//保存用戶信息 
		function saveUserInfo() {
			if ($("#rememberme").prop("checked")) {
				$.cookie("rememberme", "1", { expires : 7 });
				$.cookie("identityid", $("#identityid").val(), { expires : 7 });
				$.cookie("acct", $("#loginAccount").val(), { expires : 7 });
				$.cookie("pd", $("#password").val(), { expires : 7 });
			} else {
				$.cookie("rememberme", "0", { expires : 7 });
				$.cookie("identityid", "", { expires : -1 });
				$.cookie("acct", "", { expires : -1 });
				$.cookie("pd", "", { expires : -1 });
			}
		}
		
		function directTo(method){
			if(method == "productList"){
				$("#directForm").submit();
			}
		}
	</script>