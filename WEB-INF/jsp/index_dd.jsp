<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ include file="/resource/common/jsp/changeDiv.jsp"%>
<!DOCTYPE html>
<html lang="zh-Hant">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="Title" content="TFE" />
<meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
<meta name="Description" content="Description" />
<!-- iOS -->
<!-- Android -->
<meta property="og:title" content="Hconnect Mobile" />
<meta property="og:type" content="website" />
<meta property="og:url" content="" />
<meta property="og:image" content="resource/assets/images/share.jpg" />
<meta property="og:site_name" content="Hconnect" />
<meta property="og:description" content="Description" />
<meta property="og:locality" content="Taipei" />
<meta property="og:country-name" content="Taiwan" />
<link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
<title>TFE</title>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
	type="text/css" />
<link href="resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="resource/assets/stylesheets/vendors/swiper.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/index.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="layout-container">
		<jsp:include page="/WEB-INF/jsp/header.jsp"></jsp:include>
		<div class="layout-view">
			<section id="scene1" style="background-image: url('');">
				<div class="bannerswiper-outer">
					<div class="bannerswiper-container">
						<div class="swiper-wrapper">
							<c:forEach items="${bcList}" var="eachBC">
								<c:forEach items="${eachBC.advertisementManges}" var="adver">
									<div class="swiper-slide">
										<!--  
                <a href="${adver.imageUrl}" 
                <a href="${pageContext.request.contextPath}/${adver.imageUrl}"
                <a href="<c:choose><c:when test='${adver.isOut eq 1}'>${adver.imageUrl}</c:when><c:otherwise>${pageContext.request.contextPath}/imageArea/index?imageUrl=${adver.imageUrl}</c:otherwise></c:choose>"
                -->
										<a
											href="<c:choose><c:when test='${adver.isOut eq 1}'>${adver.imageUrl}</c:when><c:otherwise>${pageContext.request.contextPath}/${adver.imageUrl}</c:otherwise></c:choose>"
											target="<c:choose><c:when test='${adver.isOut eq 1}'>_blank</c:when><c:otherwise>_self</c:otherwise></c:choose>">
											<img class="btn-knowmore-clickable on-desk" src="${adver.imagePath}" />
										</a>
									</div>
								</c:forEach>
							</c:forEach>
							<!--  
            <div class="swiper-slide">
              <a href="http://tw.yahoo.com" target="_blank">            
                <img class="btn-knowmore-clickable on-desk" src="http://tfe-test.s3.hicloud.net.tw/image/Adve/%E5%BD%A2%E8%B1%A1%E5%8D%80/20151013122856284.jpg" alt="">
              </a>
            </div>
            
            <div class="swiper-slide">
              <a href="http://www.google.com" target="_blank">            
                <img class="btn-knowmore-clickable on-desk" src="http://tfe-test.s3.hicloud.net.tw/image/Adve/%E5%BD%A2%E8%B1%A1%E5%8D%80/20151013122914737.jpg" alt="">
              </a>
            </div>
            
            <div class="swiper-slide">
              <a href="http://www.shacom.com.tw" target="_blank">            
                <img class="btn-knowmore-clickable on-desk" src="http://tfe-test.s3.hicloud.net.tw/image/Adve/%E5%BD%A2%E8%B1%A1%E5%8D%80/20151013122932732.jpg" alt="">
              </a>
            </div>
         -->
							<!-- 
            <div class="swiper-slide" style="background-image: url('http://tfe-test.s3.hicloud.net.tw/image/Adve/%E5%BB%A3%E5%91%8AA/20151012144121570.jpg?AWSAccessKeyId=SE41NTAxNTA4ODE0MzUwNTMyMDM0OTI&Expires=1760251284&Signature=IbDaQtclgb295Vgj6S%2BhGvSKllM%3D');"></div>
            <div class="swiper-slide">
              <a href="http://www.google.com">abcde</a>
              
            </div>
            
            <font size="5" color="red">aa</font>
            
            <a href="http://www.google.com">
            
                <img class="btn-knowmore-clickable on-desk" src="http://tfe-test.s3.hicloud.net.tw/image/Adve/%E5%BD%A2%E8%B1%A1%E5%8D%80/20151012180532701.jpg?AWSAccessKeyId=SE41NTAxNTA4ODE0MzUwNTMyMDM0OTI&Expires=1760263536&Signature=nbw2K%2F1vySClkzaN4fQ0GI4Hdl0%3D" alt="">
              </a>
             -->
						</div>
					</div>
				</div>
				<section id="scene1-inner">
					<!--         <h1>存借共舞 收放自如</h1>
        <h2>跟著我們聰明打造</h2><h2>個人小金庫</h2> -->
					<div class="scene1-bottom">
						<div class="btn-knowmore">
							<div class="bannerswiper-pagination"></div>
							<span class="btn-knowmore-clickable">
								<b>了解更多</b>
								<br />
								<img class="btn-knowmore-clickable on-desk" src="${pageContext.request.contextPath}/resource/assets/images/icon-arrow-down.png" alt="">
								<img class="btn-knowmore-clickable on-mobile" src="${pageContext.request.contextPath}/resource/assets/images/icon-arrow-down-mobile.png"
									alt="">
							</span>
						</div>
					</div>
				</section>
			</section>
			<section id="scene2">
				<!--     <h2 class="scene-title"><img src="resource/assets/images/title-scene2.png" alt="P2P借貸時代已到來" title="P2P借貸時代已到來"></h2>
    <i class="icon-underline"></i> -->
				<!-- Swiper -->
				<div class="swiper-outer">
					<div class="swiper-container on-desk">
						<div class="swiper-wrapper">
							<c:forEach items="${p2pList}" var="eachP2P">
								<c:forEach items="${eachP2P.advertisementManges}" var="adver">
									<div class="swiper-slide">
										<img width="768" height="500" src="${adver.imagePath}" />
									</div>
								</c:forEach>
							</c:forEach>
							<!--     
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
            -->
						</div>
						<div class="swiper-pagination"></div>
					</div>
					<div class="swiper-container on-mobile">
						<div class="swiper-wrapper">
							<c:forEach items="${p2pList}" var="eachP2P">
								<c:forEach items="${eachP2P.advertisementManges}" var="adver">
									<div class="swiper-slide">
										<img width="320" height="340" src="${adver.imagePath}" />
									</div>
								</c:forEach>
							</c:forEach>
							<!--   
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
            -->
						</div>
						<div class="swiper-pagination"></div>
					</div>
					<div class="swiper-button-prev"></div>
					<div class="swiper-button-next"></div>
				</div>
				<!-- END OF Swiper -->
			</section>
			<section id="scene3">
				<h2 class="scene-title">模擬體驗</h2>
				<i class="icon-underline"></i>
				<h3 class="scene-subtitle">請讓TFE了解你的金融期望</h3>
				<div class="choose-chara">
					<div class="choose-chara-center">
						<img src="${pageContext.request.contextPath}/resource/assets/images/icon-longarrow-left.png" class="arrow-horizonal" alt="">
						<img src="${pageContext.request.contextPath}/resource/assets/images/icon-chara-choose.svg" class="choose-chara-center-img btn-lr" alt="">
						<img src="${pageContext.request.contextPath}/resource/assets/images/icon-longarrow-right.png" class="arrow-horizonal" alt="">
						<p>請選擇金融角色</p>
					</div>
					<div class="choose-chara-circle circle-right">
						<div class="choose-chara-innercircle">
							<img class="circleimg" src="resource/assets/images/icon-chara2.svg" alt="">
							<span class="circletxt">我是貸款者</span>
						</div>
						<div class="choose-chara-innercircle-hover">
							<img class="circleimg" src="resource/assets/images/icon-chara2-hover.svg" alt="">
							<span class="circletxt">我是貸款者</span>
						</div>
					</div>
					<div class="choose-chara-circle circle-left">
						<div class="choose-chara-innercircle">
							<img class="circleimg" src="resource/assets/images/icon-chara1.svg" alt="">
							<span class="circletxt">我是投資者</span>
						</div>
						<div class="choose-chara-innercircle-hover">
							<img class="circleimg" src="resource/assets/images/icon-chara1-hover.svg" alt="">
							<span class="circletxt">我是投資者</span>
						</div>
					</div>
				</div>
			</section>
			<section id="scene4">
				<%-- --%>
				<c:forEach items="${storyList}" var="eachStory">
					<c:forEach items="${eachStory.informationManages}" var="manage">
						<c:if test='${manage.showOrder eq 1}'>
							<div class="col-left" style="background-image: url('${manage.imageUrl}');"></div>
							<div class="col-right">
								<h2 class="scene-title">${manage.infoTitle}</h2>
								<i class="icon-underline"></i>
								<p class="story-content">${manage.infoContent}</p>
								<div class="btn-rounded" onclick="location.href='${manage.urlWay}'">了解我的投資故事</div>
							</div>
						</c:if>
					</c:forEach>
				</c:forEach>
				<!--   
        <div class="col-left" style="background-image: url('resource/assets/images/sample-story.jpg');">
        </div>
        <div class="col-right">
            <h2 class="scene-title">二次創業咖啡店</h2>
            <i class="icon-underline"></i>
            <p class="story-content">店裡溫馨的擺設、甜點蛋糕香氣的瀰漫，吸引著喜愛甜點的人們，在平常閒暇之餘來店裡坐坐，品嚐著我們親手做的甜品以及香濃的咖啡，人們透露出來的滿足就是</p>
            <div class="btn-rounded">了解我的投資故事</div>
        </div>
     -->
			</section>
			<section id="scene5">
				<div class="layout-mw-wrapper">
					<c:forEach items="${infoList}" var="eachInfo">
						<c:forEach items="${eachInfo.informationManages}" var="manage">
							<c:if test='${manage.showOrder eq 1}'>
								<div class="module-news-block">
									<div onclick="location.href='${manage.urlWay}'" class="module-news-img" style="background-image: url('${manage.imageUrl}');"></div>
									<div class="module-news-text">
										<h3>${manage.infoTitle}</h3>
										<p>${manage.infoContent}</p>
									</div>
								</div>
							</c:if>
						</c:forEach>
					</c:forEach>
					<%--    
            <div class="module-news-block">
                <div class="module-news-img" style="background-image: url('resource/assets/images/sample-newsimg.png');"><!--img src="resource/assets/images/sample-newsimg.png" width="100%" alt=""--></div>
                <div class="module-news-text">
                    <h3>關注的應用程序改善市場時機？</h3>
                    <p>最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些</p>
                </div>
            </div>
        
            <div class="module-news-block">
                <div class="module-news-img" style="background-image: url('resource/assets/images/sample-newsimg.png');"><!--img src="resource/assets/images/sample-newsimg.png" width="100%" alt=""--></div>
                <div class="module-news-text">
                    <h3>關注的應用程序改善市場時機？</h3>
                    <p>最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些</p>
                </div>
            </div>
        
            <div class="module-news-block">
                <div class="module-news-img" style="background-image: url('resource/assets/images/sample-newsimg.png');"><!--img src="resource/assets/images/sample-newsimg.png" width="100%" alt=""--></div>
                <div class="module-news-text">
                    <h3>關注的應用程序改善市場時機？</h3>
                    <p>最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些</p>
                </div>
            </div>
      --%>
				</div>
				<div class="scene5-bottom">
					<div class="btn-readmore">
						<span>
							查看更多
							<!-- <br/><img src="resource/assets/images/icon-arrow-down-orange-s.png" alt=""> -->
						</span>
					</div>
				</div>
			</section>
			<img src="resource/assets/images/bg-tri-twin-o.svg" width="100%" style="margin-top: -1px;" alt="">
		</div>
		<jsp:include page="/WEB-INF/jsp/footer.jsp"></jsp:include>
		</script>
		<script src="resource/assets/javascripts/vendors/fastclick.js" type="text/javascript"></script>
		<script src="resource/assets/javascripts/vendors/svg4everybody.min.js" type="text/javascript"></script>
		<script src="resource/assets/javascripts/initialization.js" type="text/javascript"></script>
		<script src="resource/assets/javascripts/vendors/swiper.js" type="text/javascript"></script>
		<script>
    var swiperBanner = new Swiper('.bannerswiper-container', {
        pagination: '.bannerswiper-pagination',
        paginationClickable: true,
        speed: 2000,
        spaceBetween: 0,
        centeredSlides: true,
        autoplay: true,
        autoplayDisableOnInteraction: false,
        //autoplay: 3000,
        effect: 'fade'
    });
    var swiperDesk = new Swiper('.swiper-container.on-desk', {
    	pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        speed: 1000,
        spaceBetween: 0,
        centeredSlides: true,
        //autoplay: false,
        autoplayDisableOnInteraction: false,
        autoplay: 3000,
        effect: 'fade'
        /*
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        speed: 1000,
        spaceBetween: 0,
        centeredSlides: true,
        autoplay: false,
        autoplayDisableOnInteraction: false
        */
    });
    var swiperMobile = new Swiper('.swiper-container.on-mobile', {
    	pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        speed: 1000,
        spaceBetween: 0,
        centeredSlides: true,
        //autoplay: false,
        autoplayDisableOnInteraction: false,
        autoplay: 3000,
        effect: 'fade'
    	/*
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        speed: 1000,
        spaceBetween: 0,
        centeredSlides: true,
        autoplay: false,
        autoplayDisableOnInteraction: false
    	*/
    });
    galleryTop.params.control = swiperDesk;
    galleryThumbs.params.control = swiperMobile;


</script>
	</div>
</body>
</html>