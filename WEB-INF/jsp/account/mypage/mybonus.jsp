﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1" />
<meta name="Title" content="TFE" />
<meta name="Author"
	content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
<meta name="Description" content="Description" />
<!-- iOS -->
<!-- Android -->
<meta property="og:title" content="Hconnect Mobile" />
<meta property="og:type" content="website" />
<meta property="og:url" content="" />
<meta property="og:image"
	content="${pageContext.request.contextPath}/resource/assets/images/share.jpg" />
<meta property="og:site_name" content="Hconnect" />
<meta property="og:description" content="Description" />
<meta property="og:locality" content="Taipei" />
<meta property="og:country-name" content="Taiwan" />
<link
	href="${pageContext.request.contextPath}/resource/assets/images/favicon.png"
	rel="shortcut icon" type="image/x-icon" />
<title>TFE</title>
<script type="text/javascript">
	document.createElement('header');
	document.createElement('nav');
	document.createElement('menu');
	document.createElement('section');
	document.createElement('article');
	document.createElement('aside');
	document.createElement('footer');
</script>
<style>
.pt3-scroll {
    max-height: 300px;
    overflow: scroll;
}
</style>
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css"
	media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="layout-view">
		<section class="section-banner" style="height: 0;"></section>
		<!--subsection start-->
		<section class="section-proflie-banner accordion-menu">
			<!--laptop start-->
			<div class="layout-mw-wrapper">
				<jsp:include page="/WEB-INF/jsp/account/mypage/sbheader.jsp"></jsp:include>
				<!--laptop end-->
			</div>
		</section>
		<!--subsection end-->
		<section class="section-content">
			<div class="layout-mw-wrapper">
				<!--PAGE CONTENT START-->
				<!--content start-->
				<div class="profile-item-title">
					<h2 class="profile-scene-title">通知區</h2>
					<i class="profile-icon-underline"></i>
				</div>
				<div class="corner2 layout-accordion">
					<div class="pt3-scroll">
						<ul class="accordion-list status-full">
							<c:forEach items="${appNotificationVOs}" var="appNotificationVO">					
								<li class="accordion-item">
									<div class="accordion-item-title">
										<span class="rule-num accordion-item-dot"> 
											<img src="${pageContext.request.contextPath}/resource/assets/images/picon-1.png" alt="">
										</span> 
										<span class="bar-text accordion-item-name"> 
										<span class="rule-num accordion-item-date">${appNotificationVO.scheduledTimeText}</span>
											${appNotificationVO.notificationDescription}
										</span> 
										<i role="img" class="fa fa-sort-desc"></i>
									</div>
									<div class="inner-content accordion-item-content">
										<div class="inner-content-article accordion-item-article">
											${appNotificationVO.notificationMessage}
										</div>
									</div>
								</li>					
							</c:forEach>
						</ul>
					</div>
				</div>
				<div class="profile-item-title">
					<h2 class="profile-scene-title">優惠區</h2>
					<i class="profile-icon-underline"></i>
				</div>
				<div class="corner2 layout-accordion corner2_ed">
					<div class="pt3-scroll">
						<table width="99%" border="0" class="pt3 pca">
							<tr class="table_header">
								<td align="LEFT">優惠名稱</td>
								<td align="LEFT">現金</td>
								<td align="LEFT">優惠券號</td>
								<td align="LEFT">種類</td>
								<td align="CENTER">優惠起始日</td>
								<td align="CENTER">優惠結束日</td>
								<td align="CENTER">狀態</td>
							</tr>
							
							<c:forEach items="${accountCouponVOs}" var="accountCouponVO">
								<tr>
									<td align="LEFT">${accountCouponVO.couponName}</td>
									<td align="LEFT">${accountCouponVO.discountPercent}</td>
									<td align="LEFT">${accountCouponVO.couponNo}</td>
									<td align="LEFT">${accountCouponVO.couponType}</td>
									<td align="CENTER">${accountCouponVO.couponStartDate}</td>
									<td align="CENTER">${accountCouponVO.couponEndDate}</td>
									<td align="CENTER">${accountCouponVO.couponStatus}</td>
								</tr>
							</c:forEach>
						</table>
					</div>
				</div>
				<!--content end-->
				<!--PAGE END-->
			</div>
		</section>
		<img
			src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg"
			width="100%" style="margin-top: -1px;" alt="">
	</div>
	<script>
		$('#link_mybonus').addClass("is-active");
		$(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
	</script>
</body>
</html>