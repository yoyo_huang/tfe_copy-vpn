﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="Title" content="TFE" />
<meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
<meta name="Description" content="Description" />
<!-- iOS -->
<!-- Android -->
<meta property="og:title" content="Hconnect Mobile" />
<meta property="og:type" content="website" />
<meta property="og:url" content="" />
<meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/share.jpg" />
<meta property="og:site_name" content="Hconnect" />
<meta property="og:description" content="Description" />
<meta property="og:locality" content="Taipei" />
<meta property="og:country-name" content="Taiwan" />
<link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
<title>TFE</title>
<script type="text/javascript">
	document.createElement('header');
	document.createElement('nav');
	document.createElement('menu');
	document.createElement('section');
	document.createElement('article');
	document.createElement('aside');
	document.createElement('footer');
	
	$(document).ready(function() {
		appendTitle();
		
		$('#searchButton').click( function() {
			$('#retureMessage').html('');
			$('#exportDiv').hide();
			if(!validateInput()){
				return;
			}
			
			var transDate = getTransDate();
		    $.ajax({
				type:"POST", 
				url: "<c:url value='/mypage/queryMemberStatement' />",
	            data: {
	            	'transDate': transDate
	            },
	            success: function(data){
	            	if(data.length == 0){
	            		showMessage("查無資料");
	            		appendTitle();
	            	}else{
		            	$('#exportDiv').show();
		            	appendTitle();
		            	$('#form1 input[name="transDate"]').val(transDate);
		                
		            	$.each(data, function(entryIndex, entry) {
	                		$('#memberStatementTable').append('<tr><td>' + entry.transDate + '</td><td>' 
	                				+ entry.transNo + '</td><td>' 
	                				+ entry.shelID + '</td><td>' 
	                				+ entry.periods + '</td><td>' 
	                				+ entry.bidType + '</td><td>'
	                				+ entry.payment + '</td><td>'
	                				+ entry.wonBidPrice + '</td><td>' 
	                				+ entry.avgPayment + '</td><td>'
	                				+ entry.guranCreditFee + '</td><td>' 
	                				+ entry.ioGuranCreditFee + '</td><td>' 
	                				+ entry.auctionIncome + '</td><td>' 
	                				+ entry.inOrPay + '</td><td>'
	                				+ entry.sysUseFee + '</td><td>' 
	                				+ entry.punish + '</td><td>' 
	                				+ entry.drawMoney + '</td><td>' 
	                				+ entry.payMoney + '</td><td>'
	                				+ entry.memberAccount + '</td></tr>');
	                		return;
		                });
	            	}
	            },
				error : function() {
					showMessage("查詢失敗");
				}
			});
		});
	});
	
	function showMessage(mesage){
		$('#retureMessage').html('');
		$('#retureMessage').append(mesage).css("color","red");
	}
	
	function appendTitle(){
		$('#memberStatementTable').html('');
		
    	$('#memberStatementTable').append('<tr><td>交易日期</td><td>交易單號</td>'
    		+'<td>組合編號</td>'
			+ '<td>組合期數</td>'
			+ '<td>類別</td>'
			+ '<td>每期會錢</td>'
			+ '<td>得標總會錢</td>'
			+ '<td>平均標金收入</td>'
			+ '<td>信用金收入</td>'
			+ '<td>信用金支出</td>'
			+ '<td>拍賣收入</td>'
			+ '<td>買入支出</td>'
			+ '<td>管理費</td>'
			+ '<td>其他</td>'
			+ '<td>取款</td>'
			+ '<td>入金</td>'
			+ '<td>帳戶餘額</td></tr>');
	}
	
	function validateInput(){
		var year = $('#year').val();
		var month= $('#month').val();
		
		if(0 == year || 0 == month){
			showMessage("請輸入對帳單查詢區間");
			return false;
		}
		return true;
	}
	
	function exportCSVFile(){
		if(!validateInput()){
			return false;
		}
		var transDate = getTransDate();
    	$('#form1').attr('action',"<c:url value='/mypage/printMemberStatementCSV' />");
		$('#form1').submit();
	}
	
	function exportPDFFile(){
		if(!validateInput()){
			return false;
		}		
		var transDate = getTransDate();
    	$('#form1').attr('action',"<c:url value='/mypage/printMemberStatementPDF' />");
		$('#form1').submit();
	}
	
	function getTransDate(){
		var year = $('#year').val();
		var month= $('#month').val();
		
		if(month < 10){
			month = '0' + month.trim();
		}
		var transDate = year.trim() + month.trim();
		return transDate;
	}
	
</script>
<style>
.pt3-scroll {
    max-height: 300px;
    overflow: scroll;
}
</style>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
	type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="layout-view">
		<section class="section-banner" style="height: 0;"></section>
		<!--subsection start-->
		<section class="section-proflie-banner accordion-menu">
			<!--laptop start-->
			<div class="layout-mw-wrapper">
				<jsp:include page="/WEB-INF/jsp/account/mypage/sbheader.jsp"></jsp:include>
				<!--laptop end-->
			</div>
		</section>
		<!--subsection end-->
		<section class="section-content">
			<div class="layout-mw-wrapper">
				<!--PAGE CONTENT START-->
				<section class="section-btnwrap">
					<a href="${pageContext.request.contextPath}/mypage/myrightInvoice">
				    	<span class="btn-rounded22">電子發票</span> 
				    </a>
					<a href="${pageContext.request.contextPath}/mypage/myrightWithdraw">
						<span class="btn-rounded22">取款</span>
					</a>
					<a href="${pageContext.request.contextPath}/mypage/myrightEstatement">
						<span class="btn-rounded22">對帳單查詢</span>
					</a>
				</section>
				<!--content start-->
				<div class="corner2">
					<div class="rt-list">
						<ul>
							<li class="title">對帳單查詢</li>
							<li>
								<div class="module-customselect style-xs">
									<span class="module-customselect-selected">年</span>
									<input type="hidden" value="0" name="year" id="year">
									<ul>
										<li value="2016">2016</li>
										<li value="2017">2017</li>
										<li value="2018">2018</li>
										<li value="2019">2019</li>
										<li value="2020">2020</li>
									</ul>
								</div>
							</li>
							<li>
								<div class="module-customselect style-xs">
									<span class="module-customselect-selected">月</span>
									<input type="hidden" value="0" name="month" id="month">
									<ul>
										<li value="01">1月</li>
										<li value="02">2月</li>
										<li value="03">3月</li>
										<li value="04">4月</li>
										<li value="05">5月</li>
										<li value="06">6月</li>
										<li value="07">7月</li>
										<li value="08">8月</li>
										<li value="09">9月</li>
										<li value="10">10月</li>
										<li value="11">11月</li>
										<li value="12">12月</li>
									</ul>
								</div>
							</li>
							<li class="gbtn" id="searchButton" name="searchButton">確定</li>
							<li id="retureMessage"></li>
							</li>
						</ul>
					</div>
					<div class="pt3-scroll">
						<table width="100%" border="0" class="pt3" name="memberStatementTable" id="memberStatementTable">
						</table>
					</div>
					<div class="corner-foot" name="exportDiv" id="exportDiv" hidden>
						請勾選需要的對帳單，並選擇檔案的儲存格式。
						<div>
							<span class="p-gbtn" onclick="exportCSVFile()">以CSV檔儲存</span>
							<span class="p-gbtn" onclick="exportPDFFile()">以PDF檔儲存</span>
						</div>
					</div>
					<!--content end-->
					<!--PAGE END-->
				</div>
		</section>
		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
	</div>
	<script>
		$('#link_myright').addClass("is-active");
		$(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
	</script>
	<form id="form1" name="form1" method="POST">
		<input type="hidden" name="transDate" value="">
	</form>
</body>
</html>