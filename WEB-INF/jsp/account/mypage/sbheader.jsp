<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
	<div class="sbheader-center">
		<div class="cb-list3">
			<ul>
				<li class="on-mobile">
					<a>&nbsp;&nbsp;&nbsp;&nbsp;</a>
				</li>
				<li>
					<a id="link_mypage" href="${pageContext.request.contextPath}/mypage/">我的帳戶</a>
				</li>
				<li>
					<a id="link_mycombination" href="${pageContext.request.contextPath}/mypage/mycombination">我的組合</a>
				</li>
				<li>
					<a id="link_mybonus" href="${pageContext.request.contextPath}/mypage/mybonus">優惠與通知</a>
				</li>
				<li>
					<a id="link_myright" href="${pageContext.request.contextPath}/mypage/myright">權益查詢</a>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>