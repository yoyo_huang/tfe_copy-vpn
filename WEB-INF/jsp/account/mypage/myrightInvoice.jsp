<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1"  />
<meta name="Title" content="TFE" />
<meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
<meta name="Description" content="Description" />
<!-- iOS -->
<!-- Android -->
<meta property="og:title" content="Hconnect Mobile" />
<meta property="og:type" content="website" />
<meta property="og:url" content="" />
<meta property="og:image" content="assets/images/share.jpg" />
<meta property="og:site_name" content="Hconnect" />
<meta property="og:description" content="Description" />
<meta property="og:locality" content="Taipei" />
<meta property="og:country-name" content="Taiwan" />
<link href="assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
<title>TFE</title>
<script type="text/javascript">
	document.createElement('header');
	document.createElement('nav');
	document.createElement('menu');
	document.createElement('section');
	document.createElement('article');
	document.createElement('aside');
	document.createElement('footer');
	
	$(document).ready(function() {
		appendTitle();
		
		$('#searchButton').click( function() {
			$('#retureMessage').html('');
			var year1 = $('#year1').val();
			var month1= $('#month1').val();
			var year2 = $('#year2').val();
			var month2 = $('#month2').val();
			
			if(0 == year1 || 0 == month1 || 0 == year2 || 0 == month2){
				showMessage("請輸入歷史查詢區間");
				return;
			}
			
			if(month1 < 10){
				month1 = '0' + month1.trim();
			}
			if(month2 < 10){
				month2 = '0' + month2.trim();
			}
			
			var iCreateDateStart = year1.trim() + month1.trim();
			var iCreateDateEnd = year2.trim() + month2.trim();
			
			if(iCreateDateStart > iCreateDateEnd){
				showMessage("結束時間不可大於起始時間");
				return;
			}
			
		    $.ajax({
				type:"POST", 
				url: "<c:url value='/mypage/queryMyrightInvoice' />",
	            data: {
	            	'iCreateDateStart': iCreateDateStart,
	            	'iCreateDateEnd': iCreateDateEnd
	            },
	            success: function(data){
	            	if(data.length == 0){
	            		showMessage("查無資料");
	            	}
	            	$('#einvoiceTable').html('');
	            	appendTitle();
                	
	                $.each(data, function(entryIndex, entry) {
                		$('#einvoiceTable').append('<tr><td>' + entry.iCreateDate + '</td><td>' + entry.invoiceNo + '</td><td>' 
                				+ entry.invoiceItem + '</td><td>' + entry.rate + '</td><td>' + entry.amount + '</td></tr>');
                		return;
	                });
	            },
				error : function() {
					showMessage("查詢失敗");
				}
			});
		});
	});
	
	function showMessage(mesage){
		$('#retureMessage').html('');
		$('#retureMessage').append(mesage).css("color","red");
	}
	
	function appendTitle(){
    	$('#einvoiceTable').append('<tr><td>開立日期</td><td>發票號碼</td>'
    			+ '<td>品項</td><td>應稅</td><td>總金額</td></tr>');
	}
	
</script>
<style>
.pt3-scroll {
    max-height: 300px;
    overflow: scroll;
}
</style>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
	type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="layout-container">
		<section class="section-banner" style="height: 0;"></section>
		<!--subsection start-->
		<section class="section-proflie-banner accordion-menu">
			<!--laptop start-->
			<div class="layout-mw-wrapper">
				<jsp:include page="/WEB-INF/jsp/account/mypage/sbheader.jsp"></jsp:include>
				<!--laptop end-->
			</div>
		</section>
		<!--subsection end-->

		<section class="section-content">
			<div class="layout-mw-wrapper">
		    	<!--PAGE CONTENT START-->
				<section class="section-btnwrap">
					<a href="${pageContext.request.contextPath}/mypage/myrightInvoice">
				    	<span class="btn-rounded22">電子發票</span> 
				    </a>
					<a href="${pageContext.request.contextPath}/mypage/myrightWithdraw">
						<span class="btn-rounded22">取款</span>
					</a>
					<a href="${pageContext.request.contextPath}/mypage/myrightEstatement">
						<span class="btn-rounded22">對帳單查詢</span>
					</a>
				</section>
		    <!--content start-->
		    <!--取款專區 start-->
		<!--     <div class="profile-title2">取款專區</div> -->
		 
		    	<!--content start-->
			    <div class="corner2">
					<div class="rt-list">
				    	<ul>
				        	<li class="title">歷史查詢</li>
					        <li>    
							  	<div class="module-customselect style-xs">
							    	<span class="module-customselect-selected">年</span>
							    	<input type="hidden" value="0" name="year1" id="year1">
								    <ul>
										<li value="2016">2016</li>
										<li value="2017">2017</li>
										<li value="2018">2018</li>
										<li value="2019">2019</li>
										<li value="2020">2020</li>
								    </ul>    
							  	</div>         
					        </li>
					        <li>    
								<div class="module-customselect style-xs">
									<span class="module-customselect-selected">月</span>
								  	<input type="hidden" value="0" name="month1" id="month1">
									<ul>
										<li value="01">1月</li>
										<li value="02">2月</li>
										<li value="03">3月</li>
										<li value="04">4月</li>
										<li value="05">5月</li>
										<li value="06">6月</li>
										<li value="07">7月</li>
										<li value="08">8月</li>
										<li value="09">9月</li>
										<li value="10">10月</li>
										<li value="11">11月</li>
										<li value="12">12月</li>
									</ul>    
								</div>        
					      	</li>
					      	<li>至</li>
					      	<li>    
								<div class="module-customselect style-xs">
							    	<span class="module-customselect-selected">年</span>
							    	<input type="hidden" value="0" name="year2" id="year2">
								    <ul>
										<li value="2016">2016</li>
										<li value="2017">2017</li>
										<li value="2018">2018</li>
										<li value="2019">2019</li>
										<li value="2020">2020</li>
								    </ul>    
							  	</div>         
					    	</li>
					    	<li>    
					  			<div class="module-customselect style-xs">
					    			<span class="module-customselect-selected">月</span>
					    			<input type="hidden" value="0" name="month2" id="month2">
								    <ul>
										<li value="01">1月</li>
										<li value="02">2月</li>
										<li value="03">3月</li>
										<li value="04">4月</li>
										<li value="05">5月</li>
										<li value="06">6月</li>
										<li value="07">7月</li>
										<li value="08">8月</li>
										<li value="09">9月</li>
										<li value="10">10月</li>
										<li value="11">11月</li>
										<li value="12">12月</li>
								    </ul>    
					  			</div>         
					  		</li>
				  			<li class="gbtn" id="searchButton" name="searchButton">確定</li>
				  			<li id="retureMessage"></li>
						</ul>
					</div>
			
					<div class="pt3-scroll">
						<table width="100%" border="0" class="pt3 style-tall-td" name="einvoiceTable" id="einvoiceTable">
						</table>
					</div>
				</div>

				<!--content end--> 
				<!--PAGE END-->
			</div>
		</section>
		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">

    </div>
	<script>
		$('#link_myright').addClass( "is-active" )  ;
		$(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
	</script>
  </body>
</html>