<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="Title" content="TFE"/>
    <meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited"/>
    <meta name="Description" content="Description"/>
    <!-- iOS -->
    <!-- Android -->
    <meta property="og:title" content="Hconnect Mobile"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content=""/>
    <meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/share.jpg"/>
    <meta property="og:site_name" content="Hconnect"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:locality" content="Taipei"/>
    <meta property="og:country-name" content="Taiwan"/>
    <link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon"
          type="image/x-icon"/>
    <title>TFE</title>
    <script type="text/javascript">
        document.createElement('header');
        document.createElement('nav');
        document.createElement('menu');
        document.createElement('section');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('footer');
    </script>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
          media="screen" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/member.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <style>
        .thumb {
        / / height: 110 px;
            width: 180px;
            border: 1px solid #000;
            margin: 10px 5px 0 0;
        }

        #cboxTitle {
            bottom: -19px;
            color: white;
        }

        .status_photo {
            width: 80%;
            margin: 0 auto;
            margin-top: 30px;
        }

        /* line 694, ../../sass/units/member.scss */
        .status_photo .p1 {
            border-right: 1px solid #ddd;
            width: 111px;
            color: #4d4d4d;
        }

        .status_photo .p2 {
            float: left;
            margin-left: 5%;
            width: 230px;
            color: #4d4d4d;
        }

        /* line 707, ../../sass/units/member.scss */
        .status_photo .p3 {
            border-right: 1px solid #ddd;
            width: 130px;
            color: #4d4d4d;
        }

        .status_photo .p4 {
            float: left;
            width: 15%;
            margin-left: 2%;
            color: #4d4d4d;
        }

        .email2_button {
            text-align: center;
            display: inline-block;
            line-height: 50px;
            padding-left: 1.5em;
            padding-right: 1.5em;
            color: #fff;
            background-color: #ed842f;
            cursor: pointer;
            border-radius: 25px;
            background-repeat: repeat-x;

            font-family: "Helvetica Neue", Helvetica, Arial, Chinese, "微軟正黑體", sans-serif;
        }

        .status3_button {
            width: 115px;
            height: 51px;
            font-size: 15px;
            margin-bottom: 10px;
        }

        .email3_button {
            text-align: center;
            display: inline-block;
            line-height: 50px;
            padding-left: 1.5em;
            padding-right: 1.5em;
            color: #fff;
            background-color: #fff;
            cursor: pointer;
            border-radius: 25px;
            background-repeat: repeat-x;

            font-family: "Helvetica Neue", Helvetica, Arial, Chinese, "微軟正黑體", sans-serif;
        }

        .register_status {
            width: 67%;
            margin: 0 auto;
            border-top: 1px solid #ccc;
            margin-top: 30px;
        }

    </style>
    <script type="text/javascript">
        var submitNotStart = true;
        function doSubmit() {
            if (submitNotStart) {
                submitNotStart = false;
                var question1Answer = $("#question1Answer").val();
                var question2Answer = $("#question2Answer").val();
                var question3Answer = $("#question3Answer").val();

                $('#' + 'depositBookInnerPage' + 'DeleteFileArray').val(depositBookInnerPageDeleteFileArray);
                $('#' + 'houseHoldregistrationTranscriptImage' + 'DeleteFileArray').val(houseHoldregistrationTranscriptImageDeleteFileArray);
                $('#' + 'personalProperty' + 'DeleteFileArray').val(personalPropertyDeleteFileArray);
                $('#' + 'realProperty' + 'DeleteFileArray').val(realPropertyDeleteFileArray);
                $('#' + 'anotherIdentifyDocument' + 'DeleteFileArray').val(anotherIdentifyDocumentDeleteFileArray);
                $('#' + 'studentIdentificationImage' + 'DeleteFileArray').val(studentIdentificationImageDeleteFileArray);
                $('#' + 'scoreImage' + 'DeleteFileArray').val(scoreImageDeleteFileArray);


                if (checkSubmitValue()) {
                    $('#file-form').submit();
                } else {
                    submitNotStart = true;
                }
            }
        }

        function checkSubmitValue() {
            //請選擇必要上傳檔案
            var question1Answer = $("#question1Answer").val();
            var question2Answer = $("#question2Answer").val();
            var question3Answer = $("#question3Answer").val();
            if (!question1Answer.trim() | !question2Answer.trim()) {
                alert("請選擇需要回答的問題");
                return false;
            } else {
                if (question1Answer == '1' | question1Answer == '2') {
                    if (!question3Answer.trim()) {
                        alert("請選擇需要回答的問題");
                        return false;
                    }
                }
            }

            var depositBookInnerPage = $('#' + 'depositBookInnerPage' + 'UploadButtonId').val();
            var houseHoldregistrationTranscriptImage = $('#' + 'houseHoldregistrationTranscriptImage' + 'UploadButtonId').val();
            var personalProperty = $('#' + 'personalProperty' + 'UploadButtonId').val();
            var realProperty = $('#' + 'realProperty' + 'UploadButtonId').val();
            var anotherIdentifyDocument = $('#' + 'anotherIdentifyDocument' + 'UploadButtonId').val();
            var studentIdentificationImage = $('#' + 'studentIdentificationImage' + 'UploadButtonId').val();
            var scoreImage = $('#' + 'scoreImage' + 'UploadButtonId').val();

            if (question1Answer == '1') {
                if (depositBookInnerPage == '') {
                    alert('請選擇必要上傳檔案' + ',' + '存摺內頁');
                    return false;
                }
            } else if (question1Answer == '2') {
                if (depositBookInnerPage == '') {
                    alert('請選擇必要上傳檔案' + ',' + '存摺內頁');
                    return false;
                }
            } else if (question1Answer == '3') {
                if (depositBookInnerPage == '') {
                    alert('請選擇必要上傳檔案' + ',' + '存摺內頁');
                    return false;
                }
                if (houseHoldregistrationTranscriptImage == '') {
                    alert('請選擇必要上傳檔案' + ',' + '戶口名簿影像');
                    return false;
                }
            } else if (question1Answer == '4') {
                if (depositBookInnerPage == '') {
                    alert('請選擇必要上傳檔案' + ',' + '存摺內頁');
                    return false;
                }
                if (studentIdentificationImage == '') {
                    alert('請選擇必要上傳檔案' + ',' + '學生證影像');
                    return false;
                }
                if (scoreImage == '') {
                    alert('請選擇必要上傳檔案' + ',' + '成績單影像');
                    return false;
                }
            }

            return true;

        }

        // 	function getFileExtension(filename) {
        // 		return filename.split('.').pop();
        // 	}
    </script>
    <fmt:parseNumber var="credStatus" type="number" value="${TFEACCCD.credStatus}"/>
    <fmt:parseNumber var="whvrStatus" type="number" value="${TFEACCCD.whvrStatus}"/>
    <fmt:parseNumber var="crrpeStatus" type="number" value="${TFEACCCD.crrpeStatus}"/>
    <fmt:parseNumber var="whvreStatus" type="number" value="${TFEACCCD.whvreStatus}"/>
    <fmt:parseNumber var="posinStatus" type="number" value="${TFEACCCD.posinStatus}"/>
    <fmt:parseNumber var="mopStatus" type="number" value="${TFEACCCD.mopStatus}"/>
    <fmt:parseNumber var="otpStatus" type="number" value="${TFEACCCD.otpStatus}"/>


</head>
<body>
<!-- 201*110 -->
<div class="layout-view">
    <section class="section-banner" style="height: 0;"></section>
    <!--subsection start-->
    <!--subsection end-->
    <section class="section-content">
        <div class="layout-mw-wrapper">
            <div class="register-content">
                <form method="POST" action="${pageContext.request.contextPath}/credit/uploadCreditFiles?id=${id}"
                      id="file-form"
                      name="FileForm" enctype="multipart/form-data">


                    <div class="re_status">
                        <div class="status_t1">麻煩回答幾個問題讓我們更了解您!</div>
                        <div class="status_brank">
                            <div class="clearfix b4_form">
                                <div class="module-customselect b4_select inp01 ">
                                    <span class="module-customselect-typename inp_left01">第一個問題 <font
                                            style="color:red;">*</font></span>
                                    <span class="module-customselect-selected inp_right01">-你的身分-</span>
                                    <input id="question1Answer" name="question1Answer" type="hidden" value="" name="">
                                    <ul id="question1ul">
                                        <li id="question1ul1" value="1">受薪</li>
                                        <li id="question1ul2" value="2">非受薪(攤商.自營工作室及店面等)</li>
                                        <li id="question1ul3" value="3">投資人(股票族.房東或家庭主婦等)</li>
                                        <li id="question1ul4" value="4">學生</li>
                                    </ul>
                                </div>
                            </div>
                            <div id="question2div" class="clearfix b4_form" style="display: none;">
                                <div class="module-customselect b4_select inp01">
                                    <span id="question2titlespan"
                                          class="module-customselect-typename inp_left01">第二個問題<font style="color:red;">*</font></span>
                                    <span id="question2selectText" class="module-customselect-selected inp_right01"
                                          style="background:none;">-請選擇-</span>
                                    <input id="question2Answer" name="question2Answer" type="hidden" value="" name="">
                                    <ul id="question2ul">
                                    </ul>
                                </div>
                            </div>
                            <div id="question3div" class="clearfix b4_form" style="display: none;">
                                <div class="module-customselect b4_select inp01">
                                    <span id="question3titlespan"
                                          class="module-customselect-typename inp_left01">第三個問題<font style="color:red;">*</font></span>
                                    <span id="question3selectText" class="module-customselect-selected inp_right01">-請選擇-</span>
                                    <input id="question3Answer" name="question3Answer" type="hidden" value="" name="">
                                    <ul id="question3ul">
                                    </ul>
                                </div>
                            </div>
                            <div class="clear"></div>


                            <div class="register_status2" style="display: none;">
                                <div class="clearfix b4_form">
                                    <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            市內電話</span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <input type="text" id="tel"
                                                   maxlength="15" placeholder="市內電話"
                                                   value='${account.tel != null?account.tel:""}'/>
                                        </span>
                                    </div>
                                </div>
                                <div class="clearfix b4_form">
                                    <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>聯絡人</span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <input class="text-input validate[required,custom[chinese]]"
                                                   type="text" id="contactName" maxlength="20"
                                                   value='${account.contactName != null?account.contactName:""}'/>
                                        </span>
                                    </div>
                                </div>
                                <div class="clearfix b4_form">
                                    <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>關係
                                        </span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <select id="realation" name="realation" class="validate[required]" style="width: 380px">
                                                <option value="" ${account.realation == null?"selected":""}>請選擇</option>
                                                <option value="1" ${account.realation==1?"selected":""}>親戚</option>
                                                <option value="2" ${account.realation==2?"selected":""}>同事</option>
                                                <option value="3" ${account.realation==3?"selected":""}>朋友</option>
                                                <option value="4" ${account.realation==4?"selected":""}>其他</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="clearfix b4_form">
                                    <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>聯絡人電話</span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <input type="text" id="contactTel" maxlength="15"
                                                   class="text-input validate[required]" placeholder="聯絡人電話"
                                                   value='${account.contactTel != null?account.contactTel:""}'/>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="register_status" style="display: none;">
                            <dl class="clearfix status_txt">
                                <dt>
                                    <img src="${pageContext.request.contextPath}/resource/assets/images/s2.png">
                                </dt>
                                <dd>
                                    <h1>請上傳以下文件電子檔以及任何足資證明您財務狀況的資料，檔案可以是手機拍攝的影像檔或掃描檔</h1>
                                    <h2>
                                        <span class="gray">● </span>
                                        <span class="c2">※ 以下欄位資訊皆可批次上傳多張圖片 </span><br>
                                        <span class="c2"><font style="color:red;"> (允許同時點選多張圖片上傳) </font></span>
                                    </h2>
                                </dd>
                            </dl>
                        </div>
                        <!-- 以下為上傳區   開始 -->
                        <div class="register_status" style="display: none;">

                            <div id="depositBookInnerPageDiv">
                                <ul class="clearfix status_photo">
                                    <li class="p1">
                                        存摺內頁<font style="color:red;">*</font>
                                        <div class="line W-hide"></div>
                                        <br><font style="color: red;" size="2px">(過去三個月資料或銀行對帳單)</font>
                                    </li>
                                    <li class="p2 ">
                                        <output id="adImg4List"></output>
                                    </li>
                                    <li class="p3">
                                        <div class="button">
                                            <input type="file" id="adImg4" accept="image/jpeg|image/png" name="adImg4"
                                                   multiple
                                                   style="display: none;"/>
                                            <input id="updateButton" class='email2_button status_button' type="button"
                                                   value="選擇檔案" onclick="$('input[id=adImg4]').click();"/>
                                        </div>
                                        <div class="button">
                                            <input id="updateButton" class='email2_button status_button' type="button"
                                                   value="檔案編輯" onclick='selectFile(4);'/>
                                        </div>
                                    </li>
                                    <li class="p4"><font color="red">${posinStatus!=2?TFEACCCD.posinRemark:""}</font>
                                    </li>
                                </ul>
                                <div id="houseHoldregistrationTranscriptImageDiv">
                                    <ul class="clearfix status_photo">
                                        <li class="p1">
                                            戶口名簿影像<font style="color:red;">*</font>
                                            <div class="line W-hide"></div>
                                        </li>
                                        <li class="p2 ">
                                            <output id="adImg10List"></output>
                                        </li>
                                        <li class="p3">
                                            <div class="button">
                                                <input type="file" id="adImg10" accept="image/jpeg|image/png"
                                                       name="adImg10" multiple
                                                       style="display: none;"/>
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="選擇檔案" onclick="$('input[id=adImg10]').click();"/>
                                            </div>
                                            <div class="button">
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="檔案編輯" onclick='selectFile(10);'/>
                                            </div>
                                        </li>
                                        <li class="p4"><font color="red"></font>
                                        </li>
                                    </ul>
                                </div>
                                <div id="personalPropertyDiv">
                                    <ul class="clearfix status_photo">
                                        <li class="p1">
                                            動產證明
                                            <div class="line W-hide"></div>
                                        </li>
                                        <li class="p2 ">
                                            <output id="adImg5List"></output>
                                        </li>
                                        <li class="p3">
                                            <div class="button">
                                                <input type="file" id="adImg5" accept="image/jpeg|image/png"
                                                       name="adImg5" multiple
                                                       style="display: none;"/>
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="選擇檔案" onclick="$('input[id=adImg5]').click();"/>
                                            </div>
                                            <div class="button">
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="檔案編輯" onclick='selectFile(5);'/>
                                            </div>
                                        </li>
                                        <li class="p4"><font color="red">${mopStatus!=2?TFEACCCD.mopRemark:""}</font>
                                        </li>
                                    </ul>
                                </div>
                                <div id="realPropertyDiv">
                                    <ul class="clearfix status_photo">
                                        <li class="p1">
                                            不動產證明
                                            <div class="line W-hide"></div>
                                        </li>
                                        <li class="p2 ">
                                            <output id="adImg13List"></output>
                                        </li>
                                        <li class="p3">
                                            <div class="button">
                                                <input type="file" id="adImg13" accept="image/jpeg|image/png"
                                                       name="adImg13" multiple
                                                       style="display: none;"/>
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="選擇檔案" onclick="$('input[id=adImg13]').click();"/>
                                            </div>
                                            <div class="button">
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="檔案編輯" onclick='selectFile(13);'/>
                                            </div>
                                        </li>
                                        <li class="p4"><font color="red">${mopStatus!=2?TFEACCCD.mopRemark:""}</font>
                                        </li>
                                    </ul>
                                </div>
                                <div id="studentIdentificationImageDiv">
                                    <ul class="clearfix status_photo">
                                        <li class="p1">
                                            學生證影像<font style="color:red;">*</font>
                                            <div class="line W-hide"></div>
                                        </li>
                                        <li class="p2 ">
                                            <output id="adImg11List"></output>
                                        </li>
                                        <li class="p3">
                                            <div class="button">
                                                <input type="file" id="adImg11" accept="image/jpeg|image/png"
                                                       name="adImg11" multiple
                                                       style="display: none;"/>
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="選擇檔案" onclick="$('input[id=adImg11]').click();"/>
                                            </div>
                                            <div class="button">
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="檔案編輯" onclick='selectFile(11);'/>
                                            </div>
                                        </li>
                                        <li class="p4"><font color="red"></font>
                                        </li>
                                    </ul>
                                </div>
                                <div id="scoreImageDiv">
                                    <ul class="clearfix status_photo">
                                        <li class="p1">
                                            成績單影像<font style="color:red;">*</font>
                                            <div class="line W-hide"></div>
                                        </li>
                                        <li class="p2 ">
                                            <output id="adImg12List"></output>
                                        </li>
                                        <li class="p3">
                                            <div class="button">
                                                <input type="file" id="adImg12" accept="image/jpeg|image/png"
                                                       name="adImg12" multiple
                                                       style="display: none;"/>
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="選擇檔案" onclick="$('input[id=adImg12]').click();"/>
                                            </div>
                                            <div class="button">
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="檔案編輯" onclick='selectFile(12);'/>
                                            </div>
                                        </li>
                                        <li class="p4"><font color="red"></font>
                                        </li>
                                    </ul>
                                </div>
                                <!--  -->
                                <div id="anotherIdentifyDocumentDiv">
                                    <ul class="clearfix status_photo">
                                        <li class="p1" id="otherId">
                                            其他證明文件
                                            <div class="line W-hide"></div>
                                            <br/><font style="color:red;" size="2px">(財產或所得清單)</font>
                                        </li>
                                        <li class="p2 ">
                                            <output id="adImg6List"></output>
                                        </li>
                                        <li class="p3">
                                            <div class="button">
                                                <input type="file" id="adImg6" accept="image/jpeg|image/png"
                                                       name="adImg6" multiple
                                                       style="display: none;"/>
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="選擇檔案" onclick="$('input[id=adImg6]').click();"/>
                                            </div>
                                            <div class="button">
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="檔案編輯" onclick='selectFile(6);'/>
                                            </div>
                                        </li>
                                        <li class="p4"><font color="red">${otpStatus!=2?TFEACCCD.otpRemark:""}</font>
                                        </li>
                                    </ul>
                                </div>

                                <!-- 以上為上傳區    結束 -->
                                <!-- 以下為宣導區    開始 -->
                                <!--
                                <div id="taxDeclarationDiv">
                                    <ul class="clearfix status_photo">
                                        <li class="p1">
                                            報稅單
                                            <div class="line W-hide"></div>
                                        </li>
                                        <li class="p2 M-hide">
                                            <p>請準備正本文件寄送至台灣資金交易所</p>
                                        </li>
                                        <li class="p3">
                                            <div class="button">
                                                <div class="email3_button status3_button">
                                                    <a ></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="p4"><font color="red"></font>
                                        </li>
                                    </ul>
                                </div>
                                -->
                                <div id="jcicReportDiv">
                                    <ul class="clearfix status_photo">
                                        <li class="p1">
                                            聯徵報告<font style="color:red;">*</font>
                                            <div class="line W-hide"></div>
                                        </li>
                                        <li class="p2">
                                            <p>請準備正本文件寄送至台灣資金交易所
                                                <a href="${pageContext.request.contextPath}/doc"
                                                   target="_blank">申請方式說明</a>
                                            </p>
                                        </li>
                                        <li class="p3">
                                            <div class="button">
                                                <div class="email3_button status3_button">
                                                    <a></a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="p4"><font
                                                color="red">${crrpeStatus!=2?TFEACCCD.crrpeRemark:""}</font>
                                        </li>
                                    </ul>
                                </div>
                                <!--
										<div id="theIncomeWithholdsTheVoucherDiv">
											<ul class="clearfix status_photo">
												<li class="p1">
													扣繳憑單<font style="color:red;">*</font><br/>
													<font style="color:red;" size="2px">(請提供近一年度扣繳憑單)</font>
													<div class="line W-hide"></div>
												</li>
												<li class="p2">
													<p>請準備正本文件寄送至台灣資金交易所</p>
												</li>
												<li class="p3">
													<div class="button">
														<div class="email3_button status3_button">
															<a ></a>
														</div>
													</div>
												</li>
												<li class="p4"><font color="red">${whvreStatus!=2?TFEACCCD.whvreRemark:""}</font>
												</li>
											</ul>
										</div>
										-->
                                <!-- 以下為宣傳區    結束 -->
                                <div class="status_btn2">
                                    <input id="updateButton" class="news_button" value="    取消    "
                                           onclick="window.location='/TFEFrontend/mypage';" type="button">
                                    <input id="updateButton" class="news_button" type="button"
                                           value="    修改    " onclick="uploadFile();"/>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--PAGE END-->
        </div>
    </section>
    <img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%"
         style="margin-top: -1px;" alt="">
</div>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js"
        type="text/javascript"></script>

<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script
        src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script
        src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
<script>
    $(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
</script>
<script>

    $(document).ready(function () {
        //console.log($("#question2div").val());
        //alert();

        $("#question1ul li").each(function () {
            //console.log($(this).val());
            $(this).click(function () {
                //console.log($(this).val());
                $("#question1Answer").val($(this).val());
                resetAnswerDeleteArrayAndUploadValue();
                refreshQustion2li($(this).val());

                if ($(this).val() == 1) {
                    $('#otherId').html('扣繳憑單<font style="color:red;">*</font><div class="line W-hide"></div><br/><font style="color:red;" size="2px">(請提供近一年度扣繳憑單)</font>')
                } else if ($(this).val() == 2) {
                    $('#otherId').html('報稅單<font style="color:red;"></font><div class="line W-hide"></div><br/><font style="color:red;" size="2px">(須提供近半年資料)</font>')
                } else {
                    $('#otherId').html('其他證明文件<br/><div class="line W-hide"></div><font style="color:red;" size="2px">(財產或所得清單)</font>')
                }
            });


            //console.log($("#question1ul li").val());
        });

        function resetAnswerDeleteArrayAndUploadValue() {

            $("#question1Answer").val('');
            $("#question2Answer").val('');
            $("#question3Answer").val('');

            depositBookInnerPageDeleteFileArray = [];
            houseHoldregistrationTranscriptImageDeleteFileArray = [];
            personalPropertyDeleteFileArray = [];
            realPropertyDeleteFileArray = [];
            anotherIdentifyDocumentDeleteFileArray = [];
            studentIdentificationImageDeleteFileArray = [];
            scoreImageDeleteFileArray = [];

            $('#' + 'depositBookInnerPage' + 'UploadButtonId').val("");
            $('#' + 'houseHoldregistrationTranscriptImage' + 'UploadButtonId').val("");
            $('#' + 'personalProperty' + 'UploadButtonId').val("");
            $('#' + 'realProperty' + 'UploadButtonId').val("");
            $('#' + 'anotherIdentifyDocument' + 'UploadButtonId').val("");
            $('#' + 'studentIdentificationImage' + 'UploadButtonId').val("");
            $('#' + 'scoreImage' + 'UploadButtonId').val("");

            $('#' + 'depositBookInnerPage' + 'DeleteFileArray').val("");
            $('#' + 'houseHoldregistrationTranscriptImage' + 'DeleteFileArray').val("");
            $('#' + 'personalProperty' + 'DeleteFileArray').val("");
            $('#' + 'realProperty' + 'DeleteFileArray').val("");
            $('#' + 'anotherIdentifyDocument' + 'DeleteFileArray').val("");
            $('#' + 'studentIdentificationImage' + 'DeleteFileArray').val("");
            $('#' + 'scoreImage' + 'DeleteFileArray').val("");

            $('#' + 'depositBookInnerPage' + 'ImageList').html('');
            $('#' + 'houseHoldregistrationTranscriptImage' + 'ImageList').html('');
            $('#' + 'personalProperty' + 'ImageList').html('');
            $('#' + 'realProperty' + 'ImageList').html('');
            $('#' + 'anotherIdentifyDocument' + 'ImageList').html('');
            $('#' + 'studentIdentificationImage' + 'ImageList').html('');
            $('#' + 'scoreImage' + 'ImageList').html('');

        }

        function refreshQustion2li(question1Val) {

            $('#question2selectText').css('background', 'url("${pageContext.request.contextPath}/resource/assets/images/icon-select.png") right center no-repeat white');

            $("#question2div").show();
            $("#question2ul").show();
            $("#question2ul").empty();
            $("#question3ul").empty();
            $("#question2selectText").text("-請選擇-");
            $("#question3selectText").text("-請選擇-");
            if (question1Val == 1) {

                <c:forEach items="${occupationType}" var="o" varStatus="loop">
                $("#question2ul").append('<li id="question2ul${o.code}" value="${o.code}">${o.description1}</li>');
                </c:forEach>

// 					$("#question2ul").append('<li value="1">專業人士</li>');
// 					$("#question2ul").append('<li value="2">公教正式編制人員</li>');
// 					$("#question2ul").append('<li value="3">績優公司主管</li>');
// 					$("#question2ul").append('<li value="4">一般企業白領主管</li>');
// 					$("#question2ul").append('<li value="5">軍警校級以上軍官</li>');
// 					$("#question2ul").append('<li value="6">公教藍領</li>');
// 					$("#question2ul").append('<li value="7">公教退休</li>');
// 					$("#question2ul").append('<li value="8">績優公司白領員工</li>');
// 					$("#question2ul").append('<li value="9">績優公司作業員</li>');
// 					$("#question2ul").append('<li value="10">績優公司藍領</li>');
// 					$("#question2ul").append('<li value="11">軍警普考以上警察</li>');
// 					$("#question2ul").append('<li value="12">公教兼任人員</li>');
// 					$("#question2ul").append('<li value="13">一般企業其他</li>');
// 					$("#question2ul").append('<li value="14">軍警士官長</li>');
// 					$("#question2ul").append('<li value="15">軍警基層員警</li>');
// 					$("#question2ul").append('<li value="16">軍警其他</li>');

                $("#question2selectText").text("-職業類型-");
            } else if (question1Val == 2) {
                //$("#question2ul" ).disabled();

                //style="display:none;"
                $("#question2ul").hide();
                $('#question2selectText').css('background', 'none');
                $("#question2selectText").html('<input type="text" id="questionCustomValue" value="" maxlength="3" onkeyup="value=value.replace(/[^\\d]/g,\'\') " placeholder="請輸入工作年資"/>');
                //$("#question2selectText").text("-累計工作年資-");
            } else if (question1Val == 3) {
                $("#question2ul").append('<li id="question2ul0" value="0">無</li>');
                $("#question2ul").append('<li id="question2ul1" value="1">一個</li>');
                $("#question2ul").append('<li id="question2ul2" value="2">兩個</li>');
                $("#question2ul").append('<li id="question2ul3" value="3">三個</li>');
                $("#question2ul").append('<li id="question2ul4" value="4">四個</li>');
                $("#question2ul").append('<li id="question2ul5" value="5">五個或五個以上</li>');
                $("#question2selectText").text("-小孩人數-");
            } else if (question1Val == 4) {
                <c:forEach items="${degreed}" var="o" varStatus="loop">
                $("#question2ul").append('<li id="question2ul${o.code}" value="${o.code}">${o.description1}</li>');
                </c:forEach>
// 					$("#question2ul").append('<li value="1">國立大學研究生</li>');
// 					$("#question2ul").append('<li value="2">國立大學</li>');
// 					$("#question2ul").append('<li value="3">私立大學研究生</li>');
// 					$("#question2ul").append('<li value="4">私立大學</li>');
// 					$("#question2ul").append('<li value="5">國立技職</li>');
// 					$("#question2ul").append('<li value="6">私立技職</li>');

                $("#question2selectText").text("-最高學歷-");
            }
            refreshQustion3li(question1Val);
        }

        function refreshQustion3li(question1Val) {

            $('#depositBookInnerPage' + 'Div').show();//存摺內頁
            $('#houseHoldregistrationTranscriptImage' + 'Div').show();//戶口名簿影像
            $('#personalProperty' + 'Div').show();//動產
            $('#realProperty' + 'Div').show();//不動產
            $('#anotherIdentifyDocument' + 'Div').show();//其他證明文件
            $('#studentIdentificationImage' + 'Div').show();//學生證影像
            $('#scoreImage' + 'Div').show();//成績單影像
            $('#jcicReport' + 'Div').show();//聯徵報告
            $('#theIncomeWithholdsTheVoucher' + 'Div').show();//扣繳憑單
            $('#taxDeclaration' + 'Div').show();//報稅單

            $("#question3ul").empty();
            if (question1Val == 1) {
                <c:forEach items="${jobType}" var="o" varStatus="loop">
                $("#question3ul").append('<li id="question3ul${o.code}" value="${o.code}">${o.description1}</li>');
                </c:forEach>
// 					$("#question3ul").append('<li value="1">高階主管</li>');
// 					$("#question3ul").append('<li value="2">初階主管</li>');
// 					$("#question3ul").append('<li value="3">資深</li>');
// 					$("#question3ul").append('<li value="4">退休</li>');
// 					$("#question3ul").append('<li value="5">新進</li>');
                $("#question3selectText").text("-工作類型-");
                $("#question3div").show();
                $('#houseHoldregistrationTranscriptImage' + 'Div').hide();
                $('#taxDeclaration' + 'Div').hide();
                $('#studentIdentificationImage' + 'Div').hide();//學生證影像
                $('#scoreImage' + 'Div').hide();//成績單影像

            } else if (question1Val == 2) {

                <c:forEach items="${nonHireType}" var="o" varStatus="loop">
                $("#question3ul").append('<li id="question3ul${o.code}" value="${o.code}">${o.description1}</li>');
                </c:forEach>
// 					$("#question3ul").append('<li value="1">網路商店</li>');
// 					$("#question3ul").append('<li value="2">自營店面</li>');
// 					$("#question3ul").append('<li value="3">流動攤販</li>');
// 					$("#question3ul").append('<li value="4">計程車司機</li>');
// 					$("#question3ul").append('<li value="5">自營工廠</li>');
// 					$("#question3ul").append('<li value="6">接案</li>');
// 					$("#question3ul").append('<li value="7">講師</li>');
// 					$("#question3ul").append('<li value="8">自營補習班</li>');
// 					$("#question3ul").append('<li value="9">家教</li>');
// 					$("#question3ul").append('<li value="10">作家</li>');
// 					$("#question3ul").append('<li value="11">音樂家</li>');
// 					$("#question3ul").append('<li value="12">畫家</li>');
// 					$("#question3ul").append('<li value="13">自營工作室</li>');
// 					$("#question3ul").append('<li value="14">其他</li>');
                $("#question3selectText").text("-現職行業-");
                $("#question3div").show();
                $('#houseHoldregistrationTranscriptImage' + 'Div').hide();

                $('#studentIdentificationImage' + 'Div').hide();//學生證影像
                $('#scoreImage' + 'Div').hide();//成績單影像
                $('#theIncomeWithholdsTheVoucher' + 'Div').hide();//扣繳憑單

            } else if (question1Val == 3) {
                $("#question3div").hide();
                $('#studentIdentificationImage' + 'Div').hide();//學生證影像
                $('#scoreImage' + 'Div').hide();//成績單影像
                $('#theIncomeWithholdsTheVoucher' + 'Div').hide();//扣繳憑單
                $('#taxDeclaration' + 'Div').hide();//報稅單

            } else if (question1Val == 4) {
                $('#personalProperty' + 'Div').hide();//動產
                $('#realProperty' + 'Div').hide();//不動產
                $('#houseHoldregistrationTranscriptImage' + 'Div').hide();
                $('#theIncomeWithholdsTheVoucher' + 'Div').hide();//扣繳憑單
                $('#taxDeclaration' + 'Div').hide();//報稅單
                $("#question3div").hide();

            }

            $(".register_status").each(function (index) {
                $(this).show();
            });

            $(".register_status2").each(function (index) {
                $(this).show();
            });

            if (question1Val == 2) {
                $('#questionCustomValue').change(function () {
                    //console.log("$('#questionCustomValue').val()"+$('#questionCustomValue').val());

                    $("#question2Answer").val($('#questionCustomValue').val());
                });
            } else {
                $("#question2ul li").each(function () {
                    $(this).click(function () {
                        //console.log($(this).val());
                        $("#question2Answer").val($(this).val());
                    });
                });
            }

            $("#question3ul li").each(function () {
                //console.log($(this).val());
                $(this).click(function () {
                    //console.log($(this).val());
                    $("#question3Answer").val($(this).val());
                });
                //console.log($("#question1ul li").val());
            });

        }

        // 			$('#depositBookInnerPage'+'Div').hide();//存摺內頁
        // 			$('#houseHoldregistrationTranscriptImage'+'Div').hide();//戶口名簿影像
        // 			$('#personalProperty'+'Div').hide();//動產
        // 			$('#realProperty'+'Div').hide();//不動產
        // 			$('#anotherIdentifyDocument'+'Div').hide();//其他證明文件
        // 			$('#studentIdentificationImage'+'Div').hide();//學生證影像
        // 			$('#scoreImage'+'Div').hide();//成績單影像
        // 			$('#jcicReport'+'Div').hide();//聯徵報告
        // 			$('#theIncomeWithholdsTheVoucher'+'Div').hide();//扣繳憑單
        // 			$('#taxDeclaration'+'Div').hide();//報稅單

        // 			存摺封面 depositBookCover
        // 			存摺內頁 depositBookInnerPage
        // 			戶口名簿影像 houseHoldregistrationTranscriptImage
        // 			動產 personalProperty
        // 			不動產 realProperty
        // 			其他證明文件 anotherIdentifyDocument
        // 			學生證影像 studentIdentificationImage
        // 			成績單影像 scoreImage
        // 			聯徵報告 jcicReport
        // 			扣繳憑單theIncomeWithholdsTheVoucher
        // 			報稅單taxDeclaration
        $("#adImg4Group").colorbox({rel: 'adImg4Group', photo: 'true', maxWidth: '70%', maxWidth: '70%'});
        $("#adImg1Group").colorbox({rel: 'adImg1Group', photo: 'true', maxWidth: '70%', maxWidth: '70%'});
        $("#adImg5Group").colorbox({rel: 'adImg5Group', photo: 'true', maxWidth: '70%', maxWidth: '70%'});
        $("#adImg13Group").colorbox({rel: 'adImg13Group', photo: 'true', maxWidth: '70%', maxWidth: '70%'});
        $("#adImg6Group").colorbox({rel: 'adImg6Group', photo: 'true', maxWidth: '70%', maxWidth: '70%'});
        $("#adImg11Group").colorbox({rel: 'adImg11Group', photo: 'true', maxWidth: '70%', maxWidth: '70%'});
        $("#adImg12Group").colorbox({rel: 'adImg12Group', photo: 'true', maxWidth: '70%', maxWidth: '70%'});
    });
    //取得初始圖片
    var adImg4Array = '${account.adImg4}'.split(';;');
    var adImg10Array = '${account.adImg10}'.split(';;');
    var adImg5Array = '${account.adImg5}'.split(';;');
    var adImg13Array = '${account.adImg13}'.split(';;');
    var adImg6Array = '${account.adImg6}'.split(';;');
    var adImg11Array = '${account.adImg11}'.split(';;');
    var adImg12Array = '${account.adImg12}'.split(';;');

    jQuery(window).load(function () {
        alert("Go Load!");
        var incomeSource = ${account.incomeSource};
        $("#question1ul" + incomeSource).trigger('click');

        switch (incomeSource) {
            case 1:
                $("#question2ul" + "${account.jobType}").trigger('click');

                $("#question3ul" + "${account.workLevel}").trigger('click');
                console.log("account.jobType" + "${account.jobType}");
                console.log("account.workLevel" + "${account.workLevel}");
                break;
            case 2:
                //$("#question2ul"+"${account.workingYear}" ).trigger('click');
                $("#questionCustomValue").val("${account.workingYear}");
                $("#question3ul" + "${account.workKind}").trigger('click');
                console.log("account.workingYear" + "${account.workingYear}");
                console.log("account.workKind" + "${account.workKind}");
                break;
            case 3:
                $("#question2ul" + "${account.children}").trigger('click');
                console.log("account.children" + "${account.children}");
                break;
            case 4:
                $("#question2ul" + "${account.educationLevel}").trigger('click');
                console.log("account.educationLevel" + "${account.educationLevel}");
                break;

        }
        jQuery("body").trigger("click");

        var adImg4Array = '${account.adImg4}'.split(';;');
        var adImg10Array = '${account.adImg10}'.split(';;');
        var adImg5Array = '${account.adImg5}'.split(';;');
        var adImg13Array = '${account.adImg13}'.split(';;');
        var adImg6Array;
        if (incomeSource == 1) {
            adImg6Array = '${account.adImg1}'.split(';;');
        } else {
            adImg6Array = '${account.adImg6}'.split(';;');
        }

        var adImg11Array = '${account.adImg11}'.split(';;');
        var adImg12Array = '${account.adImg12}'.split(';;');
        setImgURL("#adImg4", adImg4Array);
        setImgURL("#adImg10", adImg10Array);
        setImgURL("#adImg5", adImg5Array);
        setImgURL("#adImg13", adImg13Array);
        setImgURL("#adImg6", adImg6Array);
        setImgURL("#adImg11", adImg11Array);
        setImgURL("#adImg12", adImg12Array);
        //$("#question1ul"+"${account.accountPurpose}" ).trigger('click');

        tempCredStatus = '${credStatus}';
        /*****
         if(tempCredStatus > 2){
				$("#question1ul li").remove();
				$("#question2ul li").remove();
				$("#question3ul li").remove();
				$("#questionCustomValue").prop('disabled', true);
			}
         */

    })


    $("#adImg4").change(function () {
        readURL(this, "#adImg4", 110, 200);
    });
    $("#adImg10").change(function () {
        readURL(this, "#adImg10", 110, 200);
    });
    $("#adImg5").change(function () {
        readURL(this, "#adImg5", 110, 200);
    });
    $("#adImg13").change(function () {
        readURL(this, "#adImg13", 110, 200);
    });
    $("#adImg6").change(function () {
        readURL(this, "#adImg6", 110, 200);
    });
    $("#adImg11").change(function () {
        readURL(this, "#adImg11", 110, 200);
    });
    $("#adImg12").change(function () {
        readURL(this, "#adImg12", 110, 200);
    });
    function readURL(input, name, h, w) {

        if (input.files.length > 10) {
            alert("檔案數量已超過上限 請重新選擇");
            return;
        }
        if (input.files && input.files[0]) {
            //檢查檔案大小 & 格式
            for (var i = 0; i < input.files.length; i++) {
                var file = input.files[i];
                cheakStates = checkType(file, name);

                if (cheakStates == 1) {

                } else if (cheakStates == 2) {
                    alert("檔案太大 請重新選擇");
                    return;
                } else {
                    alert("檔案格式不正確 請重新選擇");
                    return;
                }


            }
            //產生預覽圖
            $(name + 'List').html('');
            for (var i = 0; i < input.files.length; i++) {
                var file = input.files[i];


                cheakStates = checkType(file, name);

                if (cheakStates == 1) {
                    url = loadImage.createObjectURL(file);
                    setURL(name, url);
                }
            }

        }
    }

    /****/


    function uploadFile() {
        var question1Answer = $("#question1Answer").val();
        var question2Answer = $("#question2Answer").val();
        var question3Answer = $("#question3Answer").val();
        if (question1Answer == '1' | question1Answer == '2') {
            if (question1Answer == '2') {
                if (!$("#questionCustomValue").val()) {
                    $("#questionCustomValue").val("0");
                    $("#question2Answer").val("0")
                }
            } else {
                if (!question2Answer.trim() | !question3Answer.trim()) {
                    alert("請選擇需要回答的問題");
                    return false;
                }
            }

        } else {
            if (!question1Answer.trim() | !question2Answer.trim()) {
                alert("請選擇需要回答的問題");
                return false;
            }
        }

        var dataArray = [4, 5, 6, 10, 11, 12, 13];
        for (var i = 0; i < dataArray.length; i++) {
            //file.type
            var fd = new FormData();
            var files = $("#adImg" + dataArray[i]).prop("files");
            if (files == undefined || files.length == 0) {

            } else if (files != undefined || files.length > 0) {
                for (var r = 0; r < files.length; r++) {
                    var file = files[i];
                    if (file != undefined) {
                        cheakStates = checkType(file, "#img-group-" + i);
                        if (cheakStates == 1) {

                        } else if (cheakStates == 2) {
                            alert("檔案太大 請重新選擇" + i);
                            return;
                        } else {
                            alert("檔案格式不正確 請重新選擇" + i);
                            return;
                        }
                    }
                }
            }

        }
        //changeStates = 0;
        //ShowProgressBar();
        //var file4states = uploadFileToServer(4);
        //var file5states = uploadFileToServer(5);
        //var file6states = uploadFileToServer(6);
        //var file10states = uploadFileToServer(10);
        //var file11states = uploadFileToServer(11);
        //var file12states = uploadFileToServer(12);
        //var file13states = uploadFileToServer(13);
        //var file5states = uploadFileToServer(5);
        //console.log("wait save file");
        uploadFileToServer();

    }

    function selectFile(i) {
        //alert("selectFile:"+i);
        //var adImg4List = $('#adImg'+i+'-group-1');


        /****/

        //取得寬度的語法
        var screenWidth = screen.width;
        //取得高度的語法
        var screenHeight = screen.height;
        //console.log ('The screen is '+screenWidth+'*'+screenHeight);
        var defualteValue = (screenWidth >= screenHeight ? screenHeight * 0.5 : screenWidth * 0.5);
        //console.log("defualteValue:"+defualteValue);
        //$.colorbox({ rel: 'colorbox-adImg'+i+'-group-1' });
        //$.colorbox({ rel: 'colorbox-adImg'+i+'-group-1' });
        //$(".colorbox-adImg"+i+"-group-1" ).trigger('click');

        $(".adImg" + i + "Group").colorbox({
            rel: 'adImg' + i + 'Group',
            photo: 'true',
            maxHeight: screenHeight * 0.5,
            maxWidth: screenWidth * 0.5,
            current: function () {
                return "<br><br><a id='closeSelected' class='popup-btn' onclick='$.fn.colorbox.close();'>結束預覽</a>";
            },
        });
        //$.colorbox({ rel: 'adImg'+i+'Group',photo : 'true',transition:"none" });
        //$(".adImg"+i+"Group").unbind();
        //$.colorbox({ rel: 'adImg'+i+'Group',photo : 'true', });

        $("#adImg" + i + "Group").trigger('click');
        //$("group1" ).trigger('click');


        //var files = $('#adImg'+i).prop("files");

    }


    function setImgURL(name, imgArray) {
        //console.log("imgArray.length:"+imgArray.length);
        if (imgArray.length > 1) {
            for (var i = 0; i < imgArray.length - 1; i++) {
                //console.log("name:"+name +" url:"+imgArray[i]);
                setURL(name, imgArray[i]);
            }
        }

    }

    function setURL(name, url) {
        var setName = name.replace('#', '');
        $(name + 'List').append('<img id="' + setName + 'Group" class="' + setName + 'Group" src="' + url + '" href="' + url + '"  width="200px" height="110px"><br/>');
    }

    function checkType(file, name) {

        var fileSize = 0;
        var checkStates = 0;
        //console.log("file.type:" + file.type);
        //console.log("name:" + name);
        //console.log("name==#video-group-1:" + (name.indexOf("video") > -1));
        if (file.size > 1024 * 1024) {
            fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100)
                    .toString();//+ 'MB';
            if ((name.indexOf("video") > -1)) {
                if (file.type.indexOf("video/mp4") > -1) {
                    if (fileSize <= 21) {

                        return 1;
                    } else {
                        return 2;
                    }
                } else {
                    return 0;
                }
            } else {
                if (file.type.indexOf("image/jpeg") > -1 || file.type.indexOf("image/png") > -1) {
//					if (file.type.indexOf("image/jpeg") > -1) {
                    if (fileSize <= 2) {
                        return 1;
                    } else {
                        return 2;
                    }
                } else {
                    return 0;
                }
            }

        } else {
            fileSize = (Math.round(file.size * 100 / 1024) / 100)
                    .toString();// + 'KB';
            if ((name.indexOf("video") > -1)) {
                if (file.type.indexOf("mp4") > -1) {
                    if (fileSize <= 21000) {

                        return 1;
                    } else {
                        return 2;
                    }
                } else {
                    return 0;
                }
            } else {
                if (file.type.indexOf("image/jpeg") > -1 || file.type.indexOf("image/png") > -1) {
//					if (file.type.indexOf("image/jpeg") > -1) {
                    if (fileSize <= 2100) {
                        return 1;
                    } else {
                        return 2;
                    }
                } else {
                    return 0;
                }
            }
        }
        //alert('Name: ' + file.name) ;
        //alert('Size: ' + fileSize) ;
        //alert('Type: ' + file.type) ;
        //alert("readImgURL:" + url);

    }

    function uploadFileToServer() {
        var fd = new FormData();
        var fileList = [4, 5, 6, 10, 11, 12, 13];

        ShowProgressBar();
        var typeArray = new Array();

        for (var i = 0; i < fileList.length; i++) {
            var files = $("#adImg" + fileList[i]).prop("files");
            var filesName = [];

            if (files == undefined || files.length == 0) {
                //changeStates = changeStates + 1;
                //setUrlData();
                console.log("file == undefined");
            } else {
                for (var r = 0; r < files.length; r++) {
                    filesName.push(fileList[i] + "|" + "file" + r, files[r]);
                    fd.append(fileList[i] + "|" + "file" + r, files[r]);
                    typeArray.push(fileList[i] + "|" + "file" + r);
                }

            }
        }
        console.log("typeArray:" + typeArray);
        fd.append("fileArrayName", typeArray);

        if (typeArray.length > 0) {
            $.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/credit/uploadCreditUpdataFiles",
                timeout: 50000,
                data: fd,
                contentType: false,
                processData: false,
                //contentType: "charset=UTF-8;",
                cache: false,
                dataType: "json",
                //async: false,
                /*beforeSend: function(xhr, settings) {
                 xhr.setRequestHeader("Content-Type", "multipart/form-data;boundary=gc0p4Jq0M2Yt08jU534c0p");
                 settings.data = {name: "file", file: inputElement.files[0]};
                 },*/
                success: function (result) {
                    //console.log(result);
                    //console.log("result.rs:"+result.rs);
                    if (result.rs == 0 || result.rs == 1) {
                        console.log(result);
                        //changeStates = changeStates + 1;
                        //window.location.href = "${pageContext.request.contextPath}/mypage";
                        //HideProgressBar();
                        uploadTextToServer();
                        return;
                    }

                },
                xhr: function () {//進度表
                    var xhr = $.ajaxSettings.xhr();
                    if (onprogress && xhr.upload) {
                        xhr.upload.addEventListener("progress", onprogress,
                                false);
                        return xhr;
                    }

                },
                complete: function () {
                    //HideProgressBar();
                },
            });
        } else {
            uploadTextToServer();
        }


    }
    ////
    /***
     function uploadFileToServer(i) {
			ShowProgressBar();
			
			var fd = new FormData();
			//var file = document.getElementById('adImg' + i).files[0];
			var files = $("#adImg"+i).prop("files");
			var filesName =[];

			if (files == undefined || files.length == 0) {
				changeStates = changeStates + 1;
				setUrlData();
			}else{
				//console.log("------- gg --------");
				for(var r=0; r<files.length; r++){
					filesName.push(files[r].name);
					fd.append("file"+r, files[r]);
				}
				fd.append("type", i);
				fd.append("fileName", filesName);
				ShowProgressBar();
				
				$.ajax({
					type : "POST",
					url : "${pageContext.request.contextPath}/credit/uploadCreditUpdataFiles",
					data : fd,
					contentType : false,
					processData : false,
					cache : false,
					//async: false,
					//beforeSend: function(xhr, settings) {
					//  xhr.setRequestHeader("Content-Type", "multipart/form-data;boundary=gc0p4Jq0M2Yt08jU534c0p");
					//  settings.data = {name: "file", file: inputElement.files[0]};                    
					//},
					success : function(result) {
						//alert(result);
						//console.log(result);
						changeStates = changeStates + 1;
						setUrlData();
						return;
					},
					error : function(result) {
						//console.log(result.responseText);
						changeStates = changeStates + 1;
						setUrlData();
						return;
					},
					xhr : function() {//進度表
						var xhr = $.ajaxSettings.xhr();
						if (onprogress && xhr.upload) {
							xhr.upload.addEventListener("progress", onprogress,
									false);
							return xhr;
						}

					},
					complete : function() {

					},
				});
			}

		}
     */
    function uploadTextToServer() {
        console.log("uploadTextToServer");
        var question1Answer = $("#question1Answer").val();
        var question2Answer = $("#question2Answer").val();
        var question3Answer = $("#question3Answer").val();

        if (question1Answer == "2") {
            question2Answer = $("#questionCustomValue").val();
        }

        $
                .ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/credit/updataCreditFileData",
                    data: "question1Answer=" + question1Answer + "&question2Answer="
                    + question2Answer + "&question3Answer=" + question3Answer,
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    processData: false,
                    cache: false,
                    success: function (result) {
                        //alert(result);
                        //console.log(result);
                        HideProgressBar();
                        window.location.href = "${pageContext.request.contextPath}/mypage";
                        return;
                    },
                    //error : function(result) {
                    //console.log(result);
                    //setTimeout('HideProgressBar()', 1000);
                    //return;
                    //}
                });
        HideProgressBar();
    }

    function setUrlData() {
        //console.log("setUrlData wait save file");
        if (changeStates != 7) {
            return;
        }
        ;

        //console.log("save file");
        setTimeout('uploadTextToServer()', 3000);
        changeStates = 0;
    }


    /**
     *    侦查附件上传情况    ,这个方法大概0.05-0.1秒执行一次
     */
    function onprogress(evt) {
        var loaded = evt.loaded; //已经上传大小情况
        var tot = evt.total; //附件总大小
        var per = Math.floor(100 * loaded / tot); //已经上传的百分比          $("#son").html( per +"%" );
        //$(".nav4").css("width" , per +"%");
        if (per > 99) {
            per = 99;
        }
        $(".nav5").text(per + "%");
        $(".nav5").css("width", per + "%");
        /**
         *    for(var i=value; value < per; value++){
			 *    		$(".nav5").css("width" , per +"%");
			 *    }
         */

    }

    function uploadProgress(evt) {
        if (evt.lengthComputable) {
            var percentComplete = Math.round(evt.loaded * 100 / evt.total);
            document.getElementById('progressNumber').innerHTML = percentComplete
                            .toString()
                    + '%';
        } else {
            document.getElementById('progressNumber').innerHTML = 'unable to compute';
        }
    }

    function uploadComplete(evt) {
        /* This event is raised when the server send back a response */
        //alert(evt.target.responseText);
    }

    function uploadFailed(evt) {
        alert("There was an error attempting to upload the file.");
    }

    function uploadCanceled(evt) {
        alert("The upload has been canceled by the user or the browser dropped the connection.");
    }

    // 顯示讀取遮罩
    function ShowProgressBar() {
        displayProgress();
        displayMaskFrame();
    }

    // 隱藏讀取遮罩
    function HideProgressBar() {
        var progress = $('#divProgress');
        var maskFrame = $("#divMaskFrame");
        progress.hide();
        maskFrame.hide();
    }
    // 顯示讀取畫面
    function displayProgress() {
        var w = $(document).width();
        var h = $(window).height();
        var progress = $('#divProgress');
        progress.css({
            "z-index": 999999,
            "top": (h / 2) - (progress.height() / 2),
            "left": (w / 2) - (progress.width() / 2)
        });
        progress.show();
    }
    // 顯示遮罩畫面
    function displayMaskFrame() {
        var w = $(window).width();
        var h = $(document).height();
        var maskFrame = $("#divMaskFrame");
        maskFrame.css({
            "z-index": 999998,
            "opacity": 0.7,
            "width": w,
            "height": h
        });
        maskFrame.show();
    }
    var changeStates = 0;
</script>
<div id="divProgress"
     style="text-align: center; display: none; position: fixed; top: 50%; left: 50%;">
    <img id="img-loading"
         src="${pageContext.request.contextPath}/resource/assets/images/loading.gif"/>
    <br/> <font color="#1B3563" size="2px">資料處理中</font>
    <div class="nav_rate">
        <div class="nav_rate2 nav5">&nbsp;</div>
    </div>
</div>
<div id="divMaskFrame"
     style="background-color: #F2F4F7; display: none; left: 0px; position: absolute; top: 0px;">
</div>

</body>
</html>