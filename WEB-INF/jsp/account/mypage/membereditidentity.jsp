<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<html lang="zh-tw">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport"
          content="width=device-width, initial-scale=1"/>
    <meta name="Title" content="TFE"/>
    <meta name="Author"
          content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited"/>
    <meta name="Description" content="Description"/>

    <link
            href="${pageContext.request.contextPath}/resource/assets/images/favicon.png"
            rel="shortcut icon" type="image/x-icon"/>
    <title>TFE</title>
    <link
            href="${pageContext.request.contextPath}/resource/assets/images/favicon.png"
            rel="shortcut icon" type="image/x-icon"/>

    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/member.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <style>
        select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            padding: 2px 30px 2px 2px;
            border: none;
        }

        .status_brank select {
            width: auto;
        }

        #cboxContent {
            padding-top: 0;
        }

        #cboxClose {
            display: none;
        }

        .status_btn2 {
            text-align: center;
        }

        .status_photo {
            width: 80%;
            margin: 0 auto;
            margin-top: 30px;
        }

        /* line 694, ../../sass/units/member.scss */
        .status_photo .p1 {
            border-right: 1px solid #ddd;
            width: 151px;
            color: #4d4d4d;
        }

        .status_photo .p2 {
            float: left;
            margin-left: 5%;
            width: 230px;
            color: #4d4d4d;
        }

        /* line 707, ../../sass/units/member.scss */
        .status_photo .p3 {
            border-right: 1px solid #ddd;
            width: 130px;
            color: #4d4d4d;
        }

        .status_photo .p4 {
            float: left;
            width: 15%;
            margin-left: 2%;
            color: #4d4d4d;
        }

        .email2_button {
            text-align: center;
            display: inline-block;
            line-height: 50px;
            padding-left: 1.5em;
            padding-right: 1.5em;
            color: #fff;
            background-color: #ed842f;
            cursor: pointer;
            border-radius: 25px;
            background-repeat: repeat-x;

            font-family: "Helvetica Neue", Helvetica, Arial, Chinese, "微軟正黑體", sans-serif;
        }


    </style>
    <script>

        var GO_DEBUG = false;
        function openDebug(obj, objName) {
            if (GO_DEBUG) {
//                alert("in openDebug");
                debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
                debugWindow.document.write("<HEAD><TITLE>Message window</TITLE></HEAD>");
                debugWindow.document.write("<Body><Table border='1'>" + dump_props(obj, objName) + "</Table></Body>");
//                alert("in openDebug out");
            }
        }

        function openDebugText(_text) {
            if (GO_DEBUG) { //alert("debug");
                debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
                debugWindow.document.write(_text);
            }
        }


        function openDebugTextNewLine(_text) {
            if (GO_DEBUG) { //alert("debug");
                debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
                var re = /\&/g;
                var result = _text.replace(re, '<br/>&');
                debugWindow.document.write(result + '<br/>');
            }
        }

        function dump_props(obj, objName) {
            var result = "";

            for (var i in obj) {
                try {
                    result += "<tr><td>" + objName + "." + i + "</td><td>" + obj[i] + "</td></tr>";
                }
                catch (err) {
                    result += "<tr><td>" + objName + "." + i + "</td><td>" + err + "</td></tr>";
                }
            }

            return result;
        }


        function getType() {
            var Sys = {};
            var ua = navigator.userAgent.toLowerCase();
            var s;
            (s = ua.match(/rv:([\d.]+)\) like gecko/)) ? Sys.ie = s[1] :
                    (s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] :
                            (s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] :
                                    (s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] :
                                            (s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] :
                                                    (s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;

            var type = 0;
            if (Sys.ie) type = 1;
            if (Sys.firefox) type = 2;
            if (Sys.chrome) type = 3;
            if (Sys.opera) type = 4;
            if (Sys.safari) type = 5;
            return type;

        }

        jQuery(window).load(function () {
            var isIE = getType();
            if (isIE == 1) {
                $("#bidNodeKind").css("padding-left", "11.5em");
                $("#bidNodeID").css("padding-left", "11.5em");
            }
        });
    </script>

    <fmt:parseNumber var="credStatus" type="number" value="${TFEACCCD.credStatus}"/>
    <fmt:parseNumber var="id1Status" type="number" value="${TFEACCCD.id1Status}"/>
    <fmt:parseNumber var="id2Status" type="number" value="${TFEACCCD.id2Status}"/>
    <fmt:parseNumber var="cardStatus" type="number" value="${TFEACCCD.cardStatus}"/>
    <fmt:parseNumber var="poscrStatus" type="number" value="${TFEACCCD.poscrStatus}"/>
</head>
<body>

<div class="layout-container">
    <div class="layout-view">
        <section class="section-banner" style="height: 0;"></section>
        <!--subsection start-->
        <!--subsection end-->

        <section class="section-content">
            <div class="layout-mw-wrapper">
                <!--PAGE CONTENT START-->

                <div class="register-content">
                    <form id="twid" class="twid" method="post" enctype="multipart/form-data"
                          action="${pageContext.request.contextPath}/registerFile">
                        <div class="register">
                            <div class="re_status">

                                <div class="register_status" style="border-top:0;">
                                    <dl class="clearfix status_txt">
                                        <dt>
                                            <img src="${pageContext.request.contextPath}/resource/assets/images/s3.png">
                                        </dt>
                                        <dd>
                                            <h1>使用者身份的真實性可確保大家的資金安全，因此我們需要您上傳雙證件電子檔，作為您本人意願及線上身份確認之用，請注意：</h1>
                                            <h2>
                                                <span class="gray">●</span> 第二證件可以是健保卡、駕照、護照等附照片之證件，
                                                <span class="gray">（上傳檔案可以是手機拍攝的影像檔或掃描檔）</span>
                                            </h2>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="register_status ">

                                    <ul class="clearfix status_photo">
                                        <li class="p1"><font color="#CC0000">*</font>身份證正面
                                            <div class="line W-hide"></div>
                                            <br/><font color="#CC0000">影像需清晰</font>
                                        </li>
                                        <li class="p2">
                                            <div id="files1" class="files1"></div>
                                            <c:if test="${not empty Account.adImg7}">
                                                <img id="img-group-1" class="img-group-1"
                                                     src='${fn:replace(Account.adImg7, ';;','')}'
                                                     width="200px" height="110px">
                                            </c:if>
                                            <c:if test="${empty Account.adImg7}">
                                                <img id="img-group-1" class="img-group-1"
                                                     src="${pageContext.request.contextPath}/resource/assets/images/p200x110.jpg"
                                                     width="200px" height="110px">
                                            </c:if>
                                        </li>

                                        <li class="p3">
                                            <div class="button" style="position: relative;">
                                                <input id="fileupload1" type="file" name="file"
                                                       <c:if test="${not empty Account.adImg7}">value="${fn:replace(Account.adImg7, ';;','')}"</c:if>
                                                       style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;z-index:3;" accept="image/jpeg|image/png">
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="選擇檔案" onclick="$('input[id=fileupload1]').click();"/>
                                            </div>
                                            <div class="button" style="position: relative;">
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="檔案編輯" onclick='selectFile(1);'/>
                                            </div>
                                        </li>
                                        <li class="p4"><font color="red">${id1Status!=2?TFEACCCD.id1Remark:""}</font>
                                        </li>
                                    </ul>

                                    <ul class="clearfix status_photo">
                                        <li class="p1"><font color="#CC0000">*</font>身份證反面
                                            <div class="line W-hide"></div>
                                            <br/><font color="#CC0000">影像需清晰</font>
                                        </li>
                                        <li class="p2">
                                            <div id="files2" class="files2"></div>
                                            <c:if test="${not empty Account.adImg8}">
                                                <img id="img-group-2" class="img-group-2"
                                                     src='${fn:replace(Account.adImg8, ';;','')}'
                                                     width="200px" height="110px">
                                            </c:if>
                                            <c:if test="${empty Account.adImg8}">
                                                <img id="img-group-2" class="img-group-2"
                                                     src="${pageContext.request.contextPath}/resource/assets/images/p200x110_2.jpg"
                                                     width="200px" height="110px">
                                            </c:if>
                                        </li>
                                        <li class="p3">
                                            <div class="button" style="position: relative;">
                                                <input id="fileupload2" type="file" name="file"
                                                       <c:if test="${not empty Account.adImg8}">value="${fn:replace(Account.adImg8, ';;','')}"</c:if>
                                                       style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;z-index:3;" accept="image/jpeg|image/png">
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="選擇檔案" onclick="$('input[id=fileupload2]').click();"/>
                                            </div>
                                            <div class="button" style="position: relative;">
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="檔案編輯" onclick='selectFile(2);'/>
                                            </div>
                                        </li>
                                        <li class="p4"><font color="red">${id2Status!=2?TFEACCCD.id2Remark:""}</font>
                                        </li>
                                    </ul>

                                    <ul class="clearfix status_photo">
                                        <li class="p1"><font color="#CC0000">*</font>第二證件
                                            <div class="line W-hide"></div>
                                            <br/><font color="#CC0000">影像需清晰</font>
                                        </li>
                                        <li class="p2">
                                            <div id="files3" class="files3"></div>
                                            <c:if test="${not empty Account.adImg9}">
                                                <img id="img-group-3" class="img-group-3"
                                                     src='${fn:replace(Account.adImg9, ';;','')}'
                                                     width="200px" height="110px">
                                            </c:if>
                                            <c:if test="${empty Account.adImg9}">
                                                <img id="img-group-3" class="img-group-3"
                                                     src="${pageContext.request.contextPath}/resource/assets/images/p3_200x110.jpg"
                                                     width="200px" height="110px">
                                            </c:if>
                                        </li>
                                        <li class="p3">
                                            <div class="button" style="position: relative;">
                                                <input id="fileupload3" type="file" name="file"
                                                       <c:if test="${not empty Account.adImg9}">value="${fn:replace(Account.adImg9, ';;','')}"</c:if>
                                                       style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;z-index:3;" accept="image/jpeg|image/png">
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="選擇檔案" onclick="$('input[id=fileupload3]').click();"/>
                                            </div>
                                            <div class="button" style="position: relative;">
                                                <input id="updateButton" class='email2_button status_button'
                                                       type="button"
                                                       value="檔案編輯" onclick='selectFile(3);'/>
                                            </div>
                                        </li>
                                        <li class="p4"><font color="red">${cardStatus!=2?TFEACCCD.cardRemark:""}</font>
                                        </li>
                                    </ul>

                                </div>
                            </div>

                            <div class="register_status">
                                <dl class="clearfix status_txt">
                                    <dt>
                                        <img src="${pageContext.request.contextPath}/resource/assets/images/s2.png">
                                    </dt>
                                    <dd>
                                        <h1>我們需要您提供銀行帳戶資料及存摺封面電子檔做平台資金確認之用， 請務必填寫本人帳戶。</h1>
                                        <h2><span class="gray">●</span> 銀行帳戶資料及存摺封面電子檔 </h2>
                                    </dd>
                                </dl>
                            </div>
                            <div class="register_status">
                                <div class="status_brank">

                                    <div class="clearfix b4_form">
                                        <div class="module-customselect b4_select inp01 ">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>金融機構類別
                                        </span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background:none;">
                                            <select id="bidNodeKind" name="bidNodeKind"
                                                    style="width: 380px;font-size:15px"
                                                    class="validate[required]">
                                                <option value="" ${Account.bidNodeKind == null?"selected":""}>請選擇
                                                </option>
                                                <option value="A" ${Account.bidNodeKind == "A"?"selected":""}>商業銀行
                                                </option>
                                                <option value="B" ${Account.bidNodeKind == "B"?"selected":""}>信用合作社
                                                </option>
                                                <option value="C" ${Account.bidNodeKind == "C"?"selected":""}>農漁會
                                                </option>
                                            </select>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="clearfix b4_form">

                                        <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>銀行名稱
                                        </span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <select id="bidNodeID" name="bidNodeID" style="width: 380px;font-size:15px"
                                                    class="validate[required]">
                                                <option value="" ${Account.bidNodeKind == null?"selected":""}>請選擇
                                                </option>
                                                <c:forEach var="codeVar" items="${code}" varStatus="loop">
                                                    <option value="${codeVar.id.code}" ${Account.bidNodeID == codeVar.id.code?"selected":""}>${codeVar.description1}</option>
                                                </c:forEach>
                                            </select>
                                        </span>
                                        </div>
                                    </div>

                                    <div class="clearfix b4_form">

                                        <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>銀行帳號</span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <input type="text" id="bidNodeAccount" maxlength="20" style="font-size:15px"
                                                   class="text-input validate[required,custom[number],minSize[8],maxSize[14]]"
                                                   value='${Account.bidNodeAccount != null?Account.bidNodeAccount:""}'/>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="clearfix b4_form">
                                        <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>銀行帳戶名稱
                                        </span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
											<input type="text" id="bidNodeUserName" maxlength="10" readonly="true"
                                                   style="font-size:15px"
                                                   value='${(not empty Account.bidNodeUserName)? Account.bidNodeUserName:Account.chineseName }'/>
                                        </span>
                                        </div>
                                    </div>

                                    <div class="clear"></div>
                                </div>


                                <ul class="clearfix status_photo">
                                    <li class="p1"><font color="#CC0000">*</font>存摺封面或提款卡
                                        <div class="line W-hide"></div>
                                        <br/><font color="#CC0000">影像需清晰</font>
                                    </li>
                                    <li class="p2">
                                        <div id="files4" class="files4"></div>
                                        <c:if test="${not empty Account.adImg3}">
                                            <img id="img-group-4" class="img-group-4"
                                                 src='${fn:replace(Account.adImg3, ';;','')}'
                                                 width="200px" height="110px">
                                        </c:if>
                                        <c:if test="${empty Account.adImg3}">
                                            <img id="img-group-4" class="img-group-4"
                                                 src="${pageContext.request.contextPath}/resource/assets/images/p7_200x110.jpg"
                                                 width="200px" height="110px">
                                        </c:if>
                                    </li>
                                    <li class="p3">
                                        <div class="button" style="position: relative;">
                                            <input id="fileupload4" type="file" name="file"
                                                   <c:if test="${not empty Account.adImg3}">value="${fn:replace(Account.adImg3, ';;','')}"</c:if>
                                                   style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;z-index:3;" accept="image/jpeg|image/png">
                                            <input id="updateButton" class='email2_button status_button' type="button"
                                                   value="選擇檔案" onclick="$('input[id=fileupload4]').click();"/>
                                        </div>
                                        <div class="button" style="position: relative;">
                                            <input id="updateButton" class='email2_button status_button' type="button"
                                                   value="檔案編輯" onclick='selectFile(4);'/>

                                        </div>
                                    </li>
                                    <li class="p4"><font color="red">${poscrStatus!=2?TFEACCCD.poscrRemark:""}</font>
                                    </li>
                                </ul>

                                <c:if test="${Account.accountPurpose==2}">
                                    <div class="register_status">
                                        <ul class="clearfix status_photo">
                                            <li class="p1"><font color="red">備註說明
                                                <div class="line W-hide"></div>
                                            </li>
                                            <li class="p4">
                                                <font color="red">${TFEACCCD2.checkRemark2}</font>
                                            </li>
                                        </ul>
                                    </div>
                                </c:if>


                            </div>

                            <div class="status_btn2">
                                <%--<input id="updateButton" class="news_button" type="button" value="    取消    "--%>
                                <%--onclick="window.location='${pageContext.request.contextPath}/mypage';"/>--%>
                                <%--<input id="updateButton" class="news_button" type="button" value="  儲存並離開  "--%>
                                <%--onclick="uploadFile();"/>--%>
                                <input id="updateButton" class="news_button" type="submit" value=" 儲存並離開 "/>

                            </div>

                        </div>

                    </form>
                </div>


            </div>
    </div>
</div>

<!--PAGE END-->
</div>
</section>

<img
        src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg"
        width="100%" style="margin-top: -1px;" alt="">
<script type="text/javascript"
        src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js"
        type="text/javascript"></script>


<!-- Generic page styles
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/common/css/style.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/common/css/jquery.fileupload.css">

-->

<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->

<script type="text/javascript"
        src="${pageContext.request.contextPath}/resource/common/js/jquery-1.11.3.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/common/css/validationEngine.jquery.css"/>
<script src="${pageContext.request.contextPath}/resource/common/js/jquery.validationEngine-zh_CN.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/common/js/jquery.validationEngine.js"></script>


<script>

    $('#twid').validationEngine({
        validationEventTriggers: "blur", //触发的事件  validationEventTriggers:"keyup blur",
        inlineValidation: true,//是否即时验证，false为提交表单时验证,默认true
        success: false,//为true时即使有不符合的也提交表单,false表示只有全部通过验证了才能提交表单,默认false
        promptPosition: "topRight",//提示所在的位置，topLeft, topRight, bottomLeft,  centerRight, bottomRight
        scroll: false,
        //ajax
        ajaxFormValidation: true,
        ajaxFormValidationMethod: 'post',
        onBeforeAjaxFormValidation: beforeCall,
    });

    function beforeCall(form, options) {
        if (!uploadFile()) {
            return false;
        }
//        alert("aaaa");
//        ShowProgressBar();
        if (window.console) {
            //console.log('Right before the AJAX form validation call');
        }
        ;
        return true;
    }
    ;

    var changeStates = 0;
    $(function () {

        /****
         $(".img-group-1")
         .colorbox(
         {
             rel : 'group1',
             current : "<a class='popup-btn'>檔案刪除</a> <a class='popup-btn'>結束預覽</a>"
         });


         */

    });

    function selectFile(i) {
        $
                .colorbox({
                    html: function () {
                        strSrcValue = $(".img-group-" + i).attr("src");
                        strHtml = getImgSize(strSrcValue, i);
                        //strHtml = "<div><img height='400' width='400' src='"+strSrcValue+"' style='display: block; margin: 0 auto;'></div><br><div  style='text-align:center;'><a class='popup-btn'  onclick='cancelFile("
                        //+ i
                        //+ ")'>檔案刪除</a> <a class='popup-btn' onclick='$.fn.colorbox.close()'>結束預覽</a></div>";
                        //在燈箱中要顯示的html字段
                        return strHtml;
                    },
                    //width:"90%",
                    //height:"90%",
                    //width : 700, //燈箱中間區塊的寬度
                    //height : 600, //燈箱中間區塊的高度
                    onComplete: function(){
                        $("#cboxWrapper,#cboxContent,#cboxLoadedContent").css("min-width","100%").css("max-width","100%");
                    },
                    onClosed: function () { //當燈箱關閉時的callback funtion
                        //alert('Lightbox is closed :'+$(".img-group-4").attr("src"));
                    },

                });
    }

    function getImgSize(imgSrc, i) {
        var newImg = new Image();
        newImg.src = imgSrc;
        var height = newImg.height;
        var width = newImg.width;
        //console.log ('The image size is '+width+'*'+height);
        //取得寬度的語法
        var screenWidth = screen.width;
        //取得高度的語法
        var screenHeight = screen.height;
        //console.log ('The screen is '+screenWidth+'*'+screenHeight);
        var defualteValue = (screenWidth >= screenHeight ? screenHeight * 0.5 : screenWidth * 0.5);

        var tempWidth;
        var tempHeight;
        var tempRate;
        if (height >= width) {
            tempRate = height / defualteValue;
        } else {
            tempRate = width / defualteValue;
        }
        tempHeight = height / tempRate;
        tempWidth = width / tempRate;
        //console.log ('The 2 image size is '+width+'*'+height);
        //console.log ('The 2 image size is tempWidth:'+tempWidth+'*tempHeight'+tempHeight);
        $("#cboxContent").css("background","transparent").css("text-align","center");
        
        return "<div><img height='" + tempHeight + "' width='" + tempWidth + "' src='" + strSrcValue + "'></div>"
                + "<br><div  style='text-align:center;'><a class='popup-btn'  onclick='cancelFile("
                + i
                + ")'>檔案刪除</a> <a class='popup-btn' onclick='$.fn.colorbox.close()'>結束預覽</a></div>";

    }

    $("#bidNodeKind").on('change', function () {
        $.ajax({
            type: "POST",
            url: "${pageContext.request.contextPath}/registerGetBankList",
            data: "type=" + $(this).val(),
            processData: false,
            cache: false,
            success: function (result) {
                $("#bidNodeID option").remove();
                $("#bidNodeID").append("<option value=''>請選擇 </option>");
                for (var i = 0; i < result.length; i++) {
                    var monthStr = "<option value=" + result[i].id.code + ">" + result[i].description1 + "</option>";
                    $("#bidNodeID").append(monthStr);
                    //<select id="bidNodeID" name="bidNodeID" class="module-customselect-selected inp_right01">
                    //<option value="" ${Account.bidNodeKind == null?"selected":""}>請選擇</option>
                }
                return;
            },
            error: function (result) {
                //console.log(result);

                return;
            }
        });
    });


    function cancelFile(num) {
        var fileName;
        var fileName2;
        var srcUrl;
        switch (num) {
            case 1:
                fileName = 'fileupload1';
                fileName2 = 'img-group-1';
                srcUrl = "${Account.adImg7 != null?Account.adImg7:'resource/assets/images/p200x110.jpg'}";
                break;
            case 2:
                fileName = 'fileupload2';
                fileName2 = 'img-group-2';
                srcUrl = "${Account.adImg8 != null?Account.adImg8:'resource/assets/images/p200x110.jpg'}";
                break;
            case 3:
                fileName = 'fileupload3';
                fileName2 = 'img-group-3';
                srcUrl = "${Account.adImg9 != null?Account.adImg9:'resource/assets/images/p3_200x110.jpg'}";
                break;
            case 4:
                fileName = 'fileupload4';
                fileName2 = 'img-group-4';
                srcUrl = "${Account.adImg3 != null?Account.adImg3:'resource/assets/images/p5_200x110.jpg'}";
                break;
        }
        $("#" + fileName).val('');
        $("." + fileName2).attr('src', srcUrl);
        $.fn.colorbox.close();
    }

    function readURL(input, name, h, w) {
        if (input.files && input.files[0]) {

            var file = input.files[0]

            //var fileSize = 0;
            //if (file.size > 1024 * 1024){
            //fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
            //}else{
            //fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
            //}
            //console.log('Name: ' + file.name) ;
            //console.log('Size: ' + fileSize) ;
            //console.log('Type: ' + file.type) ;
            //console.log("readImgURL:" + url);
            //console.log("cheakStates:" + cheakStates);

            cheakStates = checkType(file, name);

            if (cheakStates == 1) {
                url = loadImage.createObjectURL(file);
                $(name).attr('src', url);
                $(name).height(h);
                $(name).width(w);
            } else if (cheakStates == 2) {
                alert("檔案太大 請重新選擇");
                return;
            } else {
                alert("檔案格式不正確 請重新選擇");
                return;
            }

        }
    }

    function checkType(file, name) {

        var fileSize = 0;
        var checkStates = 0;
//        console.log("file.type:" + file.type);
//        console.log("name:" + name);
//        console.log("name==#video-group-1:" + (name.indexOf("video") > -1));
        if (file.size > 1024 * 1024) {
            fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100)
                    .toString();//+ 'MB';
//            alert(fileSize);
            if ((name.indexOf("video") > -1)) {
                if (file.type.indexOf("video/mp4") > -1) {
                    if (fileSize <= 21) {

                        return 1;
                    } else {
                        return 2;
                    }
                } else {
                    return 0;
                }
            } else {
                if (file.type.indexOf("image/jpeg") > -1 || file.type.indexOf("image/png") > -1) {
                    if (fileSize <= 5) {
                        return 1;
                    } else {
                        return 2;
                    }
                } else {
                    return 0;
                }
            }

        } else {
            fileSize = (Math.round(file.size * 100 / 1024) / 100)
                    .toString();// + 'KB';
            if ((name.indexOf("video") > -1)) {
                if (file.type.indexOf("mp4") > -1) {
                    if (fileSize <= 21000) {

                        return 1;
                    } else {
                        return 2;
                    }
                } else {
                    return 0;
                }
            } else {

                if (file.type.indexOf("image/jpeg") > -1 || file.type.indexOf("image/png") > -1) {
                    if (fileSize <= 2100) {
                        return 1;
                    } else {
                        return 2;
                    }
                } else {
                    return 0;
                }
            }
        }
        //alert('Name: ' + file.name) ;
        //alert('Size: ' + fileSize) ;
        //alert('Type: ' + file.type) ;
        //alert("readImgURL:" + url);

    }

    $("#fileupload1").change(function () {
        readURL(this, "#img-group-1", 110, 200);
    });
    $("#fileupload2").change(function () {
        readURL(this, "#img-group-2", 110, 200);
    });
    $("#fileupload3").change(function () {
        readURL(this, "#img-group-3", 110, 200);
    });
    $("#fileupload4").change(function () {
        readURL(this, "#img-group-4", 110, 200);
    });
    /**
     $("#fileupload5").change(function() {
			readURL(this, "#video-group-1", 500, 500);
		});
     */
    var totalUrl = ["", "", "", "", ""];
    var count = 0;
    function uploadFile() {
        var checkbidNodeKind = $("#bidNodeKind").val();
        var checkbidNodeID = $("#bidNodeID").val();
        var checkbidNodeName = $("#bidNodeID :selected").text();
        //var checkbidNodeBranchName = $("#bidNodeBranchName").val();
        var checkbidNodeAccount = $("#bidNodeAccount").val();
//        var checkbidNodeBranchID = $("#bidNodeBranchID").val();
        var checkbidNodeUserName = $("#bidNodeUserName").val();
//        if (checkbidNodeKind == "" || checkbidNodeID == "" || checkbidNodeAccount == "" || checkbidNodeBranchID == "" || checkbidNodeUserName == "") {
        if (checkbidNodeKind == "" || checkbidNodeID == "" || checkbidNodeAccount == "" || checkbidNodeUserName == "") {
            alert("請檢查銀行帳戶資料");
            return;
        }
//        if (checkbidNodeKind == undefined || checkbidNodeID == undefined || checkbidNodeAccount == undefined || checkbidNodeBranchID == undefined || checkbidNodeUserName == undefined) {
        if (checkbidNodeKind == undefined || checkbidNodeID == undefined || checkbidNodeAccount == undefined || checkbidNodeUserName == undefined) {
            alert("請檢查銀行帳戶資料");
            return;
        }
        if (isNaN(checkbidNodeAccount)) {
            alert("銀行帳號只能為數字");
            return;
        }

        if (checkbidNodeAccount.length < 8 || checkbidNodeAccount.length > 14) {
            alert("銀行帳號長度應為8~14碼");
            return;
        }
//        if (isNaN(checkbidNodeBranchID)) {
//            alert("分行代號只能為數字");
//            return;
//        }
        var imgNames = ["", "身份證正面", "身份證反面", "第二證件", "存摺封面或提款卡"];
        for (var i = 1; i <= 4; i++) {
            //file.type
            var fd = new FormData();
//            alert("aaa:"+document.getElementById('fileupload' + i));
//            openDebug(document.getElementById('fileupload' + i),imgNames[i] );
            openDebug(document.getElementById('fileupload' + i), imgNames[i] + ".files[0]");
            var file = document.getElementById('fileupload' + i).files[0];

            if (file == undefined) {
                if (document.getElementById('fileupload' + i).defaultValue == "") {
//                    alert(imgNames[i] + " 未選擇要上傳檔案");
                    alert('請選擇必要上傳檔案,' + imgNames[i]);
                    return;
                }
            } else {

                cheakStates = checkType(file, (i != 5) ? "#img-group-" + i
                        : "#video-group-1");
                if (cheakStates == 1) {

                } else if (cheakStates == 2) {
                    alert("檔案太大 請重新選擇" + i);
                    return;
                } else {
                    alert("檔案格式不正確 請重新選擇" + i);
                    return;
                }
            }

        }
        if (GO_DEBUG) {
            alert("STOP !! ......GO_DEBUG=" + GO_DEBUG);
            return;
        }
        changeStates = 0;
        ShowProgressBar();
        uploadFileToServer();
        //var file1states = uploadFileToServer();
        //var file2states = uploadFileToServer(2);
        //var file3states = uploadFileToServer(3);
        //var file4states = uploadFileToServer(4);
        //var file5states = uploadFileToServer(5);
        //console.log("wait save file");

    }

    function uploadFileToServer() {
        var fd = new FormData();
        //var bidNodeKind = $("#bidNodeKind").val();
        //var bidNodeID = $("#bidNodeID").val();
        //var bidNodeName = $('#bidNodeID :selected').text();
        //var bidNodeBranchName = $("#bidNodeBranchName").val();
        //var bidNodeAccount = $("#bidNodeAccount").val();

        //fd.append("bidNodeKind", bidNodeKind);
        //fd.append("bidNodeID", bidNodeID);
        //fd.append("bidNodeName", bidNodeName);
        //fd.append("bidNodeBranchName", bidNodeBranchName);
        //fd.append("bidNodeAccount", bidNodeAccount);

        ShowProgressBar();
        var typeArray = new Array();

        for (var i = 1; i <= 4; i++) {
            var file = document.getElementById('fileupload' + i).files[0];
            if (file == undefined) {
                console.log("file == undefined");
                //changeStates = changeStates + 1;
                //setUrlData();
            } else {
                fd.append("file" + i, file);
                typeArray.push(i);
            }
        }
        //console.log("typeArray:"+typeArray);
        fd.append("fileArrayName", typeArray);

        if (typeArray.length > 0) {
            $.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/updataFileList",
                timeout: 50000,
                data: fd,
                contentType: false,
                processData: false,
                //contentType: "charset=UTF-8;",
                cache: false,
                //async: false,
                /*beforeSend: function(xhr, settings) {
                 xhr.setRequestHeader("Content-Type", "multipart/form-data;boundary=gc0p4Jq0M2Yt08jU534c0p");
                 settings.data = {name: "file", file: inputElement.files[0]};
                 },*/
                success: function (result) {
                    //console.log(result);
                    if (result == 0 || result == 1) {
                        //console.log(result);
                        changeStates = changeStates + 1;
                        uploadTextToServer();
                        //window.location.href = "${pageContext.request.contextPath}/mypage";
                        //HideProgressBar();
                        return;
                    }

                },
                xhr: function () {//進度表
                    var xhr = $.ajaxSettings.xhr();
                    if (onprogress && xhr.upload) {
                        xhr.upload.addEventListener("progress", onprogress,
                                false);
                        return xhr;
                    }

                },
                complete: function () {
                    HideProgressBar();
                },
            });
        } else {
            uploadTextToServer();
        }


    }

    function uploadTextToServer() {
        var bidNodeKind = $("#bidNodeKind").val();
        var bidNodeID = $("#bidNodeID").val();
        var bidNodeName = $('#bidNodeID :selected').text();
        //var bidNodeBranchName = $("#bidNodeBranchName").val();
        var bidNodeAccount = $("#bidNodeAccount").val();
//        var bidNodeBranchID = $("#bidNodeBranchID").val();
        var bidNodeUserName = $("#bidNodeUserName").val();
        $.ajax({
            type: "POST",
            url: "${pageContext.request.contextPath}/updataFileData",
            data: "bidNodeKind=" + bidNodeKind + "&bidNodeID="
            + bidNodeID + "&bidNodeName=" + bidNodeName
                //+ "&bidNodeBranchName=" + bidNodeBranchName
            + "&bidNodeAccount=" + bidNodeAccount
//            + "&bidNodeBranchID=" + bidNodeBranchID
            + "&bidNodeUserName=" + bidNodeUserName,
            processData: false,
            cache: false,
            success: function (result) {
                //alert(result);
                //console.log(result);
                HideProgressBar();
                window.location.href = "${pageContext.request.contextPath}/mypage";
                return;
            }
        });
    }

    function setUrlData() {
        //console.log("setUrlData wait save file");
        if (changeStates != 5) {
            return;
        }

        //console.log("save file:"+changeStates);
        setTimeout('uploadTextToServer()', 1500);
        changeStates = 0;
    }


    /**
     *    侦查附件上传情况    ,这个方法大概0.05-0.1秒执行一次
     */
    function onprogress(evt) {
        var loaded = evt.loaded; //已经上传大小情况
        var tot = evt.total; //附件总大小
        var per = Math.floor(100 * loaded / tot); //已经上传的百分比          $("#son").html( per +"%" );
        //$(".nav4").css("width" , per +"%");
        if (per > 99) {
            per = 99;
        }
        $(".nav5").text(per + "%");
        $(".nav5").css("width", per + "%");
        /**
         *    for(var i=value; value < per; value++){
			 *    		$(".nav5").css("width" , per +"%");
			 *    }
         */

    }

    function uploadProgress(evt) {
        if (evt.lengthComputable) {
            var percentComplete = Math.round(evt.loaded * 100 / evt.total);
            document.getElementById('progressNumber').innerHTML = percentComplete
                            .toString()
                    + '%';
        } else {
            document.getElementById('progressNumber').innerHTML = 'unable to compute';
        }
    }

    function uploadComplete(evt) {
        /* This event is raised when the server send back a response */
        //alert(evt.target.responseText);
    }

    function uploadFailed(evt) {
        alert("There was an error attempting to upload the file.");
    }

    function uploadCanceled(evt) {
        alert("The upload has been canceled by the user or the browser dropped the connection.");
    }

    // 顯示讀取遮罩
    function ShowProgressBar() {
        displayProgress();
        displayMaskFrame();
    }

    // 隱藏讀取遮罩
    function HideProgressBar() {
        var progress = $('#divProgress');
        var maskFrame = $("#divMaskFrame");
        progress.hide();
        maskFrame.hide();
    }
    // 顯示讀取畫面
    function displayProgress() {
        var w = $(document).width();
        var h = $(window).height();
        var progress = $('#divProgress');
        progress.css({
            "z-index": 999999,
            "top": (h / 2) - (progress.height() / 2),
            "left": (w / 2) - (progress.width() / 2)
        });
        progress.show();
    }
    // 顯示遮罩畫面
    function displayMaskFrame() {
        var w = $(window).width();
        var h = $(document).height();
        var maskFrame = $("#divMaskFrame");
        maskFrame.css({
            "z-index": 999998,
            "opacity": 0.7,
            "width": w,
            "height": h
        });
        maskFrame.show();
    }
</script>
<!--
<button id="demo8">Run</button>
-->
<div id="divProgress"
     style="text-align: center; display: none; position: fixed; top: 50%; left: 50%;">
    <img id="img-loading"
         src="${pageContext.request.contextPath}/resource/assets/images/loading.gif"/>
    <br/> <font color="#1B3563" size="2px">資料處理中</font>
    <div class="nav_rate">
        <div class="nav_rate2 nav5">&nbsp;</div>
    </div>
</div>
<div id="divMaskFrame"
     style="background-color: #F2F4F7; display: none; left: 0px; position: absolute; top: 0px;">
</div>

</body>
</html>