﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="Title" content="TFE" />
<meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
<meta name="Description" content="Description" />
<!-- iOS -->
<!-- Android -->
<meta property="og:title" content="Hconnect Mobile" />
<meta property="og:type" content="website" />
<meta property="og:url" content="" />
<meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/share.jpg" />
<meta property="og:site_name" content="Hconnect" />
<meta property="og:description" content="Description" />
<meta property="og:locality" content="Taipei" />
<meta property="og:country-name" content="Taiwan" />
<link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
<title>TFE</title>
<script type="text/javascript">
	document.createElement('header');
	document.createElement('nav');
	document.createElement('menu');
	document.createElement('section');
	document.createElement('article');
	document.createElement('aside');
	document.createElement('footer');
	
	$(document).ready(function() {
		appendTitle();
		// 取款歷史查詢
		$('#searchButton').click( function() {
			$('#retureMessage2').html('');
			var year1 = $('#year1').val();
			var month1= $('#month1').val();
			var year2 = $('#year2').val();
			var month2 = $('#month2').val();
			
			if(0 == year1 || 0 == month1 || 0 == year2 || 0 == month2){
				showMessageTable("請輸入歷史查詢區間");
				return;
			}
			
			if(month1 < 10){
				month1 = '0' + month1.trim();
			}
			if(month2 < 10){
				month2 = '0' + month2.trim();
			}
			
			var dateStart = year1.trim() + month1.trim();
			var dateEnd = year2.trim() + month2.trim();
			
			if(dateStart > dateEnd){
				showMessageTable("結束時間不可大於起始時間");
				return;
			}
			
		    $.ajax({
				type:"POST", 
				url: "<c:url value='/mypage/queryAccountPaymentHistory' />",
	            data: {
	            	'dateStart': dateStart,
	            	'dateEnd': dateEnd
	            },
	            success: function(data){
	            	if(data.length == 0){
	            		showMessageTable("查無資料");
	            	}
	            	$('#bidPaymentTable').html('');
	            	appendTitle();
                	
	                $.each(data, function(entryIndex, entry) {
                		$('#bidPaymentTable').append('<tr><td>' + entry.transNo + '</td><td>' + entry.paymentDateM + '</td><td>' 
                				+ entry.paymentTimeM + '</td><td>' + entry.payment + '</td><td>' + entry.status + '</td></tr>');
                		return;
	                });
	            },
				error : function() {
					showMessageTable("查詢失敗");
				}
			});
		});
		
		// 會員取款
		$('#paymentButton').click( function() {
			$('#retureMessage1').html('');
			$("#successMsg").hide();
			var tempAmount = $('#tempAmount').val();
			var amountStr = $('#amount').val();
			
			if(!amountStr.trim()){
				showMessage("請輸入取款金額");
				return;
			}
			
			if(isNaN(amountStr)){
				showMessage('請輸入數字金額');
				return;
			}
			
			var amount = parseInt(amountStr, 10);
			if(1 > amount){
				showMessage("取款金額不可為0");
				return;
			}
			
			if(parseInt(amount) > parseInt(tempAmount)){
				showMessage("您輸入的取款金額不可大於專屬帳戶餘額");
				return;
			}
			$.colorbox({ inline: true, href: "#popup-html" });
		});
	});
	
	// 取款錯誤訊息
	function showMessage(mesage){
		$("#retureMessage1").show();
		$('#retureMessage1').html('');
		$('#retureMessage1').append(mesage);
	}
	
	// 取款歷史查詢錯誤訊息
	function showMessageTable(mesage){
		$('#retureMessage2').html('');
		$('#retureMessage2').append(mesage).css("color","red");
	}
	
	function appendTitle(){
    	$('#bidPaymentTable').append('<tr><td>申請序號</td><td>申請日期</td>'
    			+ '<td>申請時間</td><td>取款金額</td><td>狀態</td></tr>');
	}

	// 取得取款手續費
	function goSubmit(){
		$.colorbox.close();
		$('#retureMessage1').html('');
		var amount= $('#amount').val();
		var tempAmount = $('#tempAmount').val();
		var bidNodeID = '${accountPaymentVO.bidNodeID}';
		if('' == bidNodeID){
			alert('銀行代號錯誤，請更新銀行資料!');
			return;
		}else{
		    $.ajax({
				type:"POST", 
				url: "<c:url value='/mypage/getCommission' />",
	            data: {
	            	'amount': amount,
	            	'bidNodeID': bidNodeID
	            },
	            success: function(data){
	            	if(0 == data.totalAmount){
	            		showMessage("取款失敗");
	            	}
	    			if(parseInt(data.totalAmount) > parseInt(tempAmount)){
	    				showMessage("您輸入的(取款金額:"+ data.amount + " + 手續費:" + data.rate + "元)大於專屬帳戶餘額");
	    				return;
	    			}else{
	    				// 取款驗證
	    				validateAccountPayment(amount);
	    			}
	            },
				error : function() {
					showMessage("取款失敗");
				}
			});
		}
	}
	
	// 取款驗證
	function validateAccountPayment(amount){
		$.ajax({
			type:"POST",
			url: "<c:url value='/mypage/validateAccountPayment'/>",
			success: function(backmsg){ //取得回傳訊息
				if (backmsg.success){
					// 執行會員取款
    				accountPayment(amount);
				}else{
					showMessage(backmsg.msg);
				}
			}, 
			error: function(e){
				showMessage("取款失敗，資料錯誤");
			}
		});
	}
	
	// 執行會員取款
	function accountPayment(amount){
		showProgressBar();
	    $.ajax({
			type:"POST",
			url: "<c:url value='/mypage/accountPayment' />",
            data: {
            	'amount': amount
            },
            success: function(data){
            	if("" == data){
            		showMessage("取款失敗");
            	}else{
            		if("" != data.pAmountNewStr && "" != data.pAmountNew && "" != data.payment){
                		$('#amount').val('');
                		$('#availableAmount').text(data.pAmountNewStr);
                		$('#tempAmount').val(data.pAmountNew);
                		$("#successMsg").show();
                		$("#successMsg").html('你的取款金額' + data.payment +'元(外加手續費'+ data.payRate +' 元)，'
                				+'可於下方查詢取款歷史記錄，'
                				+'若有問題請<a href="${pageContext.request.contextPath}/contact">與我們聯繫</a>');
            		}else{
            			showMessage("取款失敗");
            		}
            	}
            	hideProgressBar();
            },
			error : function() {
				showMessage("取款失敗");
				hideProgressBar();
			}
		});
	}
	
	// 顯示讀取遮罩
	function showProgressBar() {
	    displayProgress();
	    displayMaskFrame();
	}

	// 隱藏讀取遮罩
	function hideProgressBar() {
	    var progress = $('#divProgress');
	    var maskFrame = $("#divMaskFrame");
	    progress.hide();
	    maskFrame.hide();
	}
	// 顯示讀取畫面
	function displayProgress() {
	    var w = $(document).width();
	    var h = $(window).height();
	    var progress = $('#divProgress');
	    progress.css({ "z-index": 999999, "top": (h / 2) - (progress.height() / 2), "left": (w / 2) - (progress.width() / 2) });
	    progress.show();
	}
	// 顯示遮罩畫面
	function displayMaskFrame() {
	    var w = $(window).width();
	    var h = $(document).height();
	    var maskFrame = $("#divMaskFrame");
	    maskFrame.css({ "z-index": 999998, "opacity": 0.7, "width": w, "height": h });
	    maskFrame.show();
	}	
</script>
<style>
.pt3-scroll {
    max-height: 300px;
    overflow: scroll;
}
</style>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
	type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="layout-view">
		<section class="section-banner" style="height: 0;"></section>
		<!--subsection start-->
		<section class="section-proflie-banner accordion-menu">
			<!--laptop start-->
			<div class="layout-mw-wrapper">
				<jsp:include page="/WEB-INF/jsp/account/mypage/sbheader.jsp"></jsp:include>
				<!--laptop end-->
			</div>
		</section>
		<!--subsection end-->
		<section class="section-content">
			<div class="layout-mw-wrapper">
				<!--PAGE CONTENT START-->
				<section class="section-btnwrap">
					<a href="${pageContext.request.contextPath}/mypage/myrightInvoice">
				    	<span class="btn-rounded22">電子發票</span> 
				    </a>
					<a href="${pageContext.request.contextPath}/mypage/myrightWithdraw">
						<span class="btn-rounded22">取款</span>
					</a>
					<a href="${pageContext.request.contextPath}/mypage/myrightEstatement">
						<span class="btn-rounded22">對帳單查詢</span>
					</a>
				</section>
				<!--content start-->
				<!--取款專區 start-->
				<div class="profile-title2">取款專區</div>
				<div class="basic-info-box rts">
					姓名：${accountPaymentVO.chineseName}
					<br>
					帳號：${accountPaymentVO.userId}
					<br>
					約定銀行資訊：${accountPaymentVO.conventionsBankInformation}
					<br>
					可取款金額：<span ame="availableAmount" id="availableAmount">${accountPaymentVO.amount}</span>
					<br>
					<!--取款button start-->
					<div class="rights-gc">
						<div class="rights-gc-cont">
							<div class="rights-gc-cont-l">取款金額</div>
							<div class="rights-gc-cont-l2">
								<input id="amount" name="amount" type="text" value="">
							</div>
							<div class="rights-gc-cont-r">
								<div class="rights-obtn" id="paymentButton" name="paymentButton">確認</div>
							</div>
						</div>
					</div>
					<!--取款button end-->
					<!--alert start-->
					<p class="tr" name="successMsg" id="successMsg" hidden>
					</p>
					<p class="tr" id="retureMessage1" hidden></p>					
					<!--alert end-->
				</div>
				<br>
				<!--content start-->
				<div class="corner2">
					<div class="rt-list">
						<ul>
							<li class="title">取款歷史查詢</li>
							<li>
								<div class="module-customselect style-xs">
									<span class="module-customselect-selected">年</span>
									<input type="hidden" value="0" name="year1" id="year1">
									<ul>
										<li value="2016">2016</li>
										<li value="2017">2017</li>
										<li value="2018">2018</li>
										<li value="2019">2019</li>
										<li value="2020">2020</li>
									</ul>
								</div>
							</li>
							<li>
								<div class="module-customselect style-xs">
									<span class="module-customselect-selected">月</span>
									<input type="hidden" value="0" name="month1" id="month1">
									<ul>
										<li value="01">1月</li>
										<li value="02">2月</li>
										<li value="03">3月</li>
										<li value="04">4月</li>
										<li value="05">5月</li>
										<li value="06">6月</li>
										<li value="07">7月</li>
										<li value="08">8月</li>
										<li value="09">9月</li>
										<li value="10">10月</li>
										<li value="11">11月</li>
										<li value="12">12月</li>
									</ul>
								</div>
							</li>
							<li>至</li>
							<li>
								<div class="module-customselect style-xs">
									<span class="module-customselect-selected">年</span>
									<input type="hidden" value="0" name="year2" id="year2">
									<ul>
										<li value="2016">2016</li>
										<li value="2017">2017</li>
										<li value="2018">2018</li>
										<li value="2019">2019</li>
										<li value="2020">2020</li>
									</ul>
								</div>
							</li>
							<li>
								<div class="module-customselect style-xs">
									<span class="module-customselect-selected">月</span>
									<input type="hidden" value="0" name="month2" id="month2">
									<ul>
										<li value="01">1月</li>
										<li value="02">2月</li>
										<li value="03">3月</li>
										<li value="04">4月</li>
										<li value="05">5月</li>
										<li value="06">6月</li>
										<li value="07">7月</li>
										<li value="08">8月</li>
										<li value="09">9月</li>
										<li value="10">10月</li>
										<li value="11">11月</li>
										<li value="12">12月</li>
									</ul>
								</div>
							</li>
							<li class="gbtn" id="searchButton" name="searchButton">確定</li>
							<li id="retureMessage2"></li>
						</ul>
					</div>
					<div class="pt3-scroll">
						<table width="100%" border="0" class="pt3" name="bidPaymentTable" id="bidPaymentTable">
						</table>
						<input type="hidden" id="tempAmount" name="tempAmount" value="${accountPaymentVO.tempAmount}">
					</div>
				</div>
				<!--content end-->
				<!--PAGE END-->
			</div>
		</section>
		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
	</div>
	<!-- 確認取款的畫面 -->
	<div class="hidden-popup-contents" style="display: none;">
		<div id="popup-html" class="module-popup">
			<h3>確認取款</h3>
			<div class="btn-wrap style-center">
				<div id="submitBtn" class="btn-rounded style-thin" onclick="goSubmit()">確定</div>
				<div id="cleanBtn" class="btn-rounded style-thin" onclick="$.colorbox.close()">取消</div>
			</div>
		</div>
	</div>
	<div id="divProgress" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;" >
	    <img id="img-loading" src="${pageContext.request.contextPath}/resource/assets/images/loading.gif" />
	    <br />
	    <font color="#1B3563" size="2px">資料處理中</font>
	    <div class="nav_rate">
			<div class="nav_rate2 nav5">&nbsp;</div>
		</div>
	</div>
	<div id="divMaskFrame" style="background-color: #F2F4F7; display: none; left: 0px;
	    position: absolute; top: 0px;">
	</div>
	<script>
		$('#link_myright').addClass( "is-active" )  ;
		$(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
	</script>
</body>
</html>