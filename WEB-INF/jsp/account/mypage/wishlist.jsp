﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css" />

	<div class="layout-view">
		<section class="section-banner" style="height: 0;"></section>
		<!--subsection start-->
		<section class="section-proflie-banner accordion-menu">
			<!--laptop start-->
			<div class="layout-mw-wrapper">
				<jsp:include page="/WEB-INF/jsp/account/mypage/sbheader.jsp"></jsp:include>
				<!--laptop end-->
			</div>
		</section>
		<!--subsection end-->
		<section class="section-content">
			<div class="layout-mw-wrapper">
				<div class="profile-item-title">
					<h2 class="profile-scene-title">募集中</h2>
					<i class="profile-icon-underline"></i>
				</div>
				<div class="cb-products" style="padding-top: 60px;">
					<ul class="products">
						<c:forEach var="track" items="${trackList}" varStatus="status">
							<li id="track${track.shelID}">
								<div class="module-product-box">
									<div class="module-product-inbox">
										<div class="module-product-innerbox">
											<div class="module-product-innerbox-top">
												<span class="module-product-serialnumber">${track.shelID}</span>
												<span class="module-product-class style-${fn:substring(track.crClass, 0, 1)}">${track.crClass}</span>
												<span class="module-product-percent">${track.credit}%</span>
											</div>
											<div class="module-product-innerbox-bottom">
												<p>第0期&nbsp;/共${track.bidTermC}期</p>
												<p>
													<span class="dollarsign">每期NT$</span>
													<fmt:formatNumber value="${track.bidTermM}" pattern="###,###,###"/>
												</p>
											</div>
											<div class="module-product-bar-containter">
												<div class="module-product-bar-valueimg" style="width: ${track.bidIntendNumber/track.bidFulMember * 100}%;"></div>
												<div class="module-product-bar-number">目前${track.bidIntendNumber}人/共${track.bidFulMember}人</div>
											</div>
											<div class="module-product-innerbox-cover">
												<a href="${pageContext.request.contextPath}/product/detail?from=track&shelId=${track.shelID}">
													<i class="fa fa-search"></i>查看詳細
												</a>
											</div>
										</div>
									</div>
									<i class="icon-add" onclick="addToCart(${track.shelID})"></i>
								</div>
							</li>
						</c:forEach>
					</ul>
				</div>
				<div class="profile-item-title">
					<h2 class="profile-scene-title">已開始</h2>
					<i class="profile-icon-underline"></i>
				</div>
				<div class="cb-products" style="padding-top: 60px;">
					<ul class="products">
						<c:forEach var="track" items="${trackCloseList}" varStatus="status">
							<li>
								<div class="module-product-box style-finishd">
									<div class="module-product-inbox">
										<div class="module-product-innerbox">
											<div class="module-product-innerbox-top">
												<span class="module-product-serialnumber">${track.shelID}</span>
												<span class="module-product-class style-${fn:substring(track.crClass, 0, 1)}">${track.crClass}</span>
												<span class="module-product-percent">${track.credit}%</span>
											</div>
											<div class="module-product-innerbox-bottom">
												<p>第${shelIdMap.get(track.shelID).substring(1)}期&nbsp;/共${track.bidTermC}期</p>
												<p>
													<span class="dollarsign">每期NT$</span>
													<fmt:formatNumber value="${track.bidTermM}" pattern="###,###,###"/>
												</p>
											</div>
											<div class="module-product-bar-containter">
												<div class="module-product-bar-valueimg" style="width: ${track.bidIntendNumber/track.bidFulMember * 100}%;"></div>
												<div class="module-product-bar-number">目前${track.bidIntendNumber}人/共${track.bidFulMember}人</div>
											</div>
											<div class="module-product-innerbox-cover">
												<a href="${pageContext.request.contextPath}/product/detail?from=track&shelId=${track.shelID}">
													<i class="fa fa-search"></i>查看詳細
												</a>
											</div>
										</div>
									</div>
									<i class="icon-add"></i>
								</div>
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</section>
		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
	</div>

<script>
	$('#link_wishlist').addClass( "is-active" )  ;
	$(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
	
	function addToCart(shelId){
		$.ajax({
			type:"POST",
			url: "<c:url value='/product/addBid'/>",
			data:{
				shelId: shelId,
			},
			cache:false, 
			async:false,
			success: function(backmsg){ //取得回傳訊息
				if (backmsg == "Success"){
					$("#track"+shelId).hide();
					alert('您所選擇競標組合已成功加入追蹤清單,請您至追蹤清單查詢,若確定要加入此競標組合,請勾選確定加入,即可成功加入競標組合');
				}else{
					alert("追蹤清單筆數已超過上限,無法加入");
				}
			}, 
			error: function(){
			}
		});
	}
</script>