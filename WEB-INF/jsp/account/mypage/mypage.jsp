﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <!-- VER 20161117 - fix Nov editlist no.10 & no.11 -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="Title" content="TFE"/>
    <meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited"/>
    <meta name="Description" content="Description"/>
    <!-- iOS -->
    <!-- Android -->
    <meta property="og:title" content="Hconnect Mobile"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content=""/>
    <meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/share.jpg"/>
    <meta property="og:site_name" content="Hconnect"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:locality" content="Taipei"/>
    <meta property="og:country-name" content="Taiwan"/>
    <link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon"
          type="image/x-icon"/>
    <title>TFE</title>
    <script type="text/javascript">
        document.createElement('header');
        document.createElement('nav');
        document.createElement('menu');
        document.createElement('section');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('footer');
    </script>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
          media="screen" rel="stylesheet"
          type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <style type="text/css">
        .profile-obtn {
            padding-left: 2em;
            padding-right: 2em;
        }
    </style>
</head>
<body>
<div class="layout-view">
    <section class="section-banner" style="height: 0;"></section>
    <!--subsection start-->
    <section class="section-proflie-banner accordion-menu">
        <!--laptop start-->
        <div class="layout-mw-wrapper">
            <jsp:include page="/WEB-INF/jsp/account/mypage/sbheader.jsp"></jsp:include>
            <!--laptop end-->
        </div>
    </section>
    <!--subsection end-->
    <section class="section-content">
        <div class="layout-mw-wrapper">
            <!--PAGE CONTENT START-->

            <!--帳戶開始-->
            <div class="profile-cont">
                <%--<div class="txt-alert">提示訊息：完成基本資料填寫經審核即可加入競標組合，享有投資權利。</div>--%>
                <div class="txt-alert"><%--HHH${account.credStatus1}HH--%>
                    <c:if test="${account.credStatus1 != 4 }"> 提示訊息：完成基本資料填寫經審核即可加入競標組合，享有投資權利。 </c:if>
                    <%--<c:if test="${account.credStatus1 == 4 }"> 提示訊息：參與標會須先匯入首期會錢至您的專屬帳號。</c:if>--%>
                </div>
                <!--left start-->
                <div class="profile-cont-l">
                    <!--**************************************************個資 START**************************************************-->
                    <div class="profile-title">會員基本資料</div>
                    <div class="basic-info-box txtcenter">
                        <div class="txt-alert">
                            基本資料審核狀態:${account.credStatus1==0? "未審核":account.credStatus1==4? "審核完成":"審核中"} </div>
                    </div>
                    <div class="basic-info-box">
                        <table width="100%" border="1">
                            <c:if test="${(account.accountPurpose != 3) }">
                            <tr>
                                <td colspan="2" nowrap>使用者代號：${account.userId}</td>
                            </tr>
                            </c:if>
                            <tr>
                                <td colspan="2" nowrap>${(account.accountPurpose == 3? "公司名稱":"姓名")}：${account.chineseName}</td>
                            </tr>
                            <tr>
                                <td colspan="2" nowrap>${(account.accountPurpose == 3? "公司統編":"身分證字號")}：${account.identification}</td>
                            </tr>
                            <c:if test="${(account.accountPurpose == 3) }">
                                <tr>
                                    <td colspan="2" nowrap>負責人姓名：${account.principal}</td>
                                </tr>
                            </c:if>
                            <c:if test="${(account.accountPurpose != 3) }">
                            <tr>
                                <td colspan="2" nowrap>性別：${account.gender==1?"男":account.gender==2? "女":""}</td>
                            </tr>
                            <tr>
                                <td colspan="2" nowrap>
                                    <c:if test="${not empty account.birthdate}">
                                        生日：${fn:substring(account.birthdate, 0, 4)}年${fn:substring(account.birthdate, 5, 7)}月${fn:substring(account.birthdate, 8, 11)}日
                                    </c:if>
                                </td>
                                <!-- 19XX年XX月XX日 -->
                            </tr>
                            <tr>
                                <td colspan="2" nowrap>
                                    婚姻：${account.maritalStatus==1?"未婚":""}${account.maritalStatus==2?"已婚":""}${account.maritalStatus==3?"其他":""}</td>
                            </tr>
                            </c:if>
                            <tr>
                                <td width="20%" nowrap>電子郵件：</td>
                                <td width="80%">${account.email}(${account.emailStatus=="2"? "已":account.emailStatus=="3"? "重新":"待"}認證)</td>
                                <!-- ryan@shacom.com.tw -->
                            </tr>
                            <tr>
                                <td nowrap>手機號碼：</td>
                                <td>${account.mobile}(${account.mobileStatus=="2"? "已":account.mobileStatus=="3"? "重新":"待"}認證)</td>
                                <!-- 0952 XXX XXX -->
                            </tr>

                            <tr>
                                <td nowrap>居住地址：</td>
                                <td>${account.city}${account.area}${account.address}</td>
                                <!-- 台北市忠孝東路六段21號12樓-7 -->
                            </tr>
                            <%--<tr>--%>
                            <%--<td nowrap>銀行資訊：</td>--%>
                            <!-- <td>兆豐銀行 南港分行 0000 0000 0000</td> -->
                            <%--<td>${account.bidNodeName} ${account.bidNodeBranchName} ${account.bidNodeAccount}</td>--%>
                            <%--</tr>--%>
                            <tr>
                                <td nowrap>專屬帳號：</td>
                                <td><c:if test="${account.credStatus1 == 4}">
                                    中國信託 敦南分行 ${account.memberVirtualAccount}</c:if>
                                    <c:if test="${account.credStatus1 != 4}">-</c:if>
                                </td>
                            </tr>
                            <c:if test="${((account.memberType == 'A')&&(account.credStatus1 == 4)) || (account.accountPurpose == 3) }">
                                <tr>
                                    <td colspan="2">推薦網址：</td>
                                </tr>
                                <tr>
                                    <td colspan="2">http://${header['host']}${pageContext.request.contextPath}/register?Reference=${encodeUserId}&IsNode=${(account.accountPurpose == 3)? 1:0}</td>
                                </tr>
                            </c:if>

                            <c:if test="${not empty account.creditMark}">
                                <tr>
                                    <td nowrap>
                                        <div class="style-blue" style="font-weight: bold;">個資審核備註：</div>
                                    </td>
                                    <td>
                                        <div class="style-blue" style="font-weight: bold;">${account.creditMark}</div>
                                    </td>
                                </tr>
                            </c:if>
                        </table>
                    </div>
                    <div class="basic-info-box txtcenter">
                        <div class="profile-obtn"></div>
                        <%--<input class="profile-obtn" type="button" value="${account.credStatus1==4? "修改":"編輯"}"--%>
                        <%--onclick="window.location='${pageContext.request.contextPath}/mypage/memberedit';">--%>
                        <c:choose>
                            <c:when test="${ (account.dmStatus==0 || account.dmStatus==4) && (account.credStatus2 == 0 || account.credStatus2 == 4) && (account.accountPurpose != 3)}">
                                <input class='profile-obtn' type="button"
                                       value="${account.credStatus1==4? "修改":"編輯"}"
                                       onclick="window.location='${pageContext.request.contextPath}/mypage/memberedit';"
                                >
                            </c:when>
                            <c:otherwise>
                                <input class='profile-obtn style-gray' type="button"
                                       value="${account.credStatus1==4? "修改":"編輯"}"
                                       disabled="disabled"
                                >
                            </c:otherwise>
                        </c:choose>

                    </div>
                    <!--**************************************************個資 END**************************************************-->

                    <!--**************************************************出金 START**************************************************-->
                    <c:if test="${(account.accountPurpose==2)||(not empty account.memberType ) || (account.accountPurpose == 3)}">
                        <div class="basic-info-box">
                            <table width="100%" border="1">
                                <tr>
                                    <td colspan="2" nowrap>
                                        <div class="txt-alert">
                                            取款資料審核狀態：${account.dmStatus==4?"審核完成":(account.dmStatus<4 && account.dmStatus!=0)?"審核中":"未審核"}</div>
                                    </td>
                                </tr>
                                <c:if test="${account.dmStatus!=4}">
                                    <tr>
                                        <td colspan="2">
                                            <div class="txt-alert">取款前您必須提供身分證件照片,第二證件照片及帳戶資料照片以及存摺封面或提款卡。</div>
                                        </td>
                                    </tr>
                                </c:if>
                                <c:if test="${not empty account.firstCheck}">
                                    <tr>
                                        <td nowrap>
                                            <div class="txt-alert" style="font-weight: bold;">取款審查備註：</div>
                                        </td>
                                        <td>
                                            <div class="txt-alert"
                                                 style="font-weight: bold;">${account.firstCheck}</div>
                                        </td>
                                    </tr>
                                </c:if>
                            </table>
                        </div>
                        <div class="basic-info-box txtcenter">
                            <div class="profile-obtn"></div>
                                <%--<input class="profile-obtn" type="button" value="上傳帳戶資料"--%>
                                <%--onclick="window.location='${pageContext.request.contextPath}/mypage/membereditidentity';">--%>
                            <c:choose>
                                <c:when test="${(account.authenticateStatus>=4) && (account.credStatus1 == 0 || account.credStatus1 == 4)}">
                                    <input class='profile-obtn' type="button"
                                           value="${account.dmStatus==4? "修改":"上傳"}帳戶資料"
                                           onclick="window.location='${pageContext.request.contextPath}/mypage/membereditidentity';"
                                    >
                                </c:when>
                                <c:otherwise>
                                    <input class='profile-obtn style-gray' type="button"
                                           value="${account.dmStatus==4? "修改":"上傳"}帳戶資料"
                                           disabled="disabled"
                                    >
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </c:if>
                </div>
                <!--**************************************************出金 END**************************************************-->


                <!--right start-->
                <div class="profile-cont-r">
                    <!--**************************************************信用額度資料START**************************************************-->
                    <div class="profile-title">信用額度資料</div>
                    <div class="basic-info-box">
                        <table width="100%" border="1">
                            <tr>
                                <td colspan="2">
                                    信用額度申請狀況：${account.credStatus2==4?"審核完成":(account.credStatus2<4 && account.credStatus2!=0)?"審核中":"尚未申請"}</td>
                            </tr>
                            <%--<c:if test="${account.credStatus>=4}">--%>
                            <tr>
                                <td colspan="2">信用評等：${account.credStatus2!=4? "-":TFECredit}</td>
                            </tr>
                            <tr>
                                <td colspan="2">信用額度：${account.credStatus2!=4? "-":GuranCredit}</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    收入狀況：${account.credStatus2!=4? "-":account.incomeSource==1?"受薪":account.incomeSource==2?"非受薪":account.incomeSource==3?"無業":account.incomeSource==4?"學生":"" }
                                </td>
                            </tr>
                            <c:if test="${account.credStatus2!=0}">
                                <tr>
                                    <td width="145" nowrap="true" rowspan="8">已上傳/寄送資料</td>
                                    <td>  　  
										<img src="${pageContext.request.contextPath}/resource/assets/images/icon-check-ob.png" alt="" align="top" style="opacity:0;"><span>　</span>
                                    </td>
                                </tr>
                                <c:if test="${not empty account.adImg4}">
                                    <tr style="min-height: 37px;">
                                        <td>
                                            <img src="${pageContext.request.contextPath}/resource/assets/images/icon-check-ob.png"
                                                 alt="" align="top">
                                            存摺內頁
                                        </td>
                                    </tr>
                                </c:if>
                                <c:if test="${not empty account.adImg5}">
                                    <tr style="min-height: 37px;">
                                        <td>
                                            <img src="${pageContext.request.contextPath}/resource/assets/images/icon-check-ob.png"
                                                 alt="" align="top">
                                            動產
                                        </td>
                                    </tr>
                                </c:if>
                                <c:if test="${not empty account.adImg6}">
                                    <tr style="min-height: 37px;">
                                        <td>
                                            <img src="${pageContext.request.contextPath}/resource/assets/images/icon-check-ob.png"
                                                 alt="" align="top">
                                            其他財力證明
                                        </td>
                                    </tr>
                                </c:if>
                                <c:if test="${not empty account.adImg10}">
                                    <tr style="min-height: 37px;">
                                        <td>
                                            <img src="${pageContext.request.contextPath}/resource/assets/images/icon-check-ob.png"
                                                 alt="" align="top">
                                            戶籍謄本含記事
                                        </td>
                                    </tr>
                                </c:if>
                                <c:if test="${not empty account.adImg11}">
                                    <tr style="min-height: 37px;">
                                        <td>
                                            <img src="${pageContext.request.contextPath}/resource/assets/images/icon-check-ob.png"
                                                 alt="" align="top">
                                            學生證
                                        </td>
                                    </tr>
                                </c:if>
                                <c:if test="${not empty account.adImg12}">
                                    <tr style="min-height: 37px;">
                                        <td>
                                            <img src="${pageContext.request.contextPath}/resource/assets/images/icon-check-ob.png"
                                                 alt="" align="top">
                                            成績單
                                        </td>
                                    </tr>
                                </c:if>
                                <c:if test="${not empty account.adImg13}">
                                    <tr style="min-height: 37px;">
                                        <td>
                                            <img src="${pageContext.request.contextPath}/resource/assets/images/icon-check-ob.png"
                                                 alt="" align="top">
                                            不動產
                                        </td>
                                    </tr>
                                </c:if>
                            </c:if>


                            <%--</c:if>--%>

                        </table>
                        <c:if test="${!(account.accountPurpose==1 && (account.credStatus2>0)) }">
                            <p class="style-blue">是否申請信用額度？</p>
                            <p class="style-blue">
                                我們根據您的個人資料及信用狀況，經過嚴謹詳實的徵信流程與最新的動態信用風險評估模型，決定您的信用評等及信用額度，並在您參加的每個不同競標組合中決定您的信用金。</p>
                            <p>申請信用額度可以讓您在台灣資金交易所：</p>
                            <p class="style-orange">
                                1. 擁有借款的權利。
                                <br>
                                2. 進行更靈活的資金操作。
                                <br>
                                3. 累積個人的行為資料，提升未來個人信用品質。
                            </p>
                            <div class="txt-alert">※ 強烈建議您進行申請，取得完整使用平台服務。</div>
                        </c:if>
                    </div>
                    <br>
                    <br>
                    <br>
                    <div class="basic-info-box txtcenter">

                        <c:choose>
                            <c:when test="${((account.credStatus1 == 4) && (account.dmStatus == 0 || account.dmStatus == 4))
                                        && (account.credStatus2 != 0 ) && (account.authenticateStatus>=4) && (account.accountPurpose != 3)}">
                                <input class='profile-obtn' type="button"
                                       value="修改信用資料"
                                       onclick="window.location='${pageContext.request.contextPath}/credit';"
                                >
                            </c:when>
                            <c:otherwise>
                                <input class='profile-obtn style-gray' type="button"
                                       value="修改信用資料"
                                       disabled="disabled"
                                >
                            </c:otherwise>
                        </c:choose>


                        <%--<c:choose>--%>
                        <%--<c:when test="${account.accountPurpose == 1}">--%>
                        <%--<input ${account.accountPurpose == 1?"class='profile-obtn'":"class='profile-obtn style-gray'"}--%>
                        <%--type="button" value="修改信用資料"--%>
                        <%--onclick="window.location='${pageContext.request.contextPath}/mypage/membereditcredit';">--%>
                        <%--</c:when>--%>
                        <%--<c:otherwise>--%>
                        <%--<input ${account.accountPurpose == 1?"class='profile-obtn'":"class='profile-obtn style-gray'"}--%>
                        <%--type="button" value="修改信用資料" disabled="disabled">--%>
                        <%--</c:otherwise>--%>
                        <%--</c:choose>--%>

                        <c:choose>
                            <c:when test="${(( account.credStatus1 == 4) && (account.dmStatus == 0 || account.dmStatus == 4))
                                        && (account.credStatus2 == 0 ) && (account.authenticateStatus>=4) && (account.accountPurpose != 3)}">
                                <input class='profile-obtn' type="button"
                                       value="立即申請"
                                       onclick="window.location='${pageContext.request.contextPath}/credit';"
                                >
                            </c:when>
                            <c:otherwise>
                                <input class='profile-obtn style-gray' type="button"
                                       value="立即申請"
                                       disabled="disabled"
                                >
                            </c:otherwise>
                        </c:choose>

                        <%--<c:choose>--%>
                        <%--<c:when test="${account.accountPurpose == 2}">--%>
                        <%--<input ${account.accountPurpose == 2?"class='profile-obtn'":"class='profile-obtn style-gray'"}--%>
                        <%--type="button" value="立即申請"--%>
                        <%--onclick="window.location='${pageContext.request.contextPath}/credit';">--%>
                        <%--</c:when>--%>
                        <%--<c:otherwise>--%>
                        <%--<input ${account.accountPurpose == 2?"class='profile-obtn'":"class='profile-obtn style-gray'"}--%>
                        <%--type="button" value="立即申請" disabled="disabled">--%>
                        <%--</c:otherwise>--%>
                        <%--</c:choose>--%>

                    </div>
                    <!--**************************************************信用額度資料 END**************************************************-->
                </div>
            </div>
            <!--帳戶結束-->
            <!--PAGE END-->
        </div>
    </section>
    <img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%"
         style="margin-top: -1px;" alt="">
</div>
<script>
    $('#link_mypage').addClass("is-active");
    $(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());


    <c:if test="${param.ACStatus=='1' }">
    alert("感謝您使用台灣資金交易所，平台會依您年資及繳費狀況給予額度及評等，詳細資料請查詢會員專區信用額度資料。若欲取得更高的信用額度，請點選修改信用資料按鍵提出申請。");
    </c:if>

    <c:if test="${(isBidJoined=='False') && (account.authenticateStatus>=4) && (account.credStatus1 == 4 || account.credStatus2 == 4)}">
    alert("1. 恭喜您，您已取得供競標使用之專屬帳號\n2. 參與標會需先確認帳號餘額大於首期會錢");
    </c:if>

</script>
</body>
</html>