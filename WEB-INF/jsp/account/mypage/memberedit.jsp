<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="Title" content="TFE"/>
    <meta name="Author" content="TFE"/>
    <meta name="Description" content="Description"/>

    <link
            href="${pageContext.request.contextPath}/resource/assets/images/favicon.png"
            rel="shortcut icon" type="image/x-icon"/>
    <title>TFE</title>

    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/member.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css"
            media="screen" rel="stylesheet" type="text/css"/>

</head>
<body>
<div class="layout-container">

    <div class="layout-view">
        <section class="section-banner" style="height: 0;"></section>
        <!--subsection start-->
        <!--subsection end-->
        <section class="section-content">
            <div class="layout-mw-wrapper">
                <!--PAGE CONTENT START-->
                <div class="register-content">
                    <center>
                        <table width="0" border="0" align="center">
                            <tr>
                                <td><img src="${pageContext.request.contextPath}/resource/assets/images/memberedit_03.gif" alt="" width="500" height="100"></td>
                            </tr>
                        </table>
                    </center>
                    <form id="user_form" class="user_form formular" method="post"
                          action="${pageContext.request.contextPath}/updateUserData">
                        <div class="register">
                            <div class="register_t2 " style="border-top: 0;">
                                <div class="register_column2">
                                    <h1>請務必填寫真實資料以便審查</h1>
                                    <dl class="w1">
                                        <dt>姓名</dt>
                                        <dd>
                                            <input class="text-input validate[required,custom[chinese]]"
                                                   maxlength="20" id="user" name="user" type="text"
                                                   value="${account.chineseName}"
                                                   autocomplete="off" placeholder="XXXX">
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>
                                    <dl class="clearfix w1">
                                        <dt>身分證字號</dt>
                                        <dd>
                                            <input
                                                    class="text-input validate[required,funcCall[checkTwId],custom[twId]]"
                                                    id="uid" name="uid" maxlength="10" type="text"
                                                    value="${account.identification}"
                                                    autocomplete="off" placeholder="x123456789" disabled="disabled">
                                        </dd>
                                    </dl>

                                    <dl class="clearfix w2">
                                        <dt>會員代號</dt>
                                        <dd>
                                            <input id="account" name="account"
                                                   class="text-input validate[required,funcCall[checkAccountSize],funcCall[checkAccount]]"
                                                   type="text" value="${account.userId}" autocomplete="off"
                                                   placeholder="會員代號" readonly="true"
                                                   maxlength="16">
                                        </dd>
                                    </dl>
                                    <div class="pass_tip">&nbsp;&nbsp;&nbsp;請於下方填入生日</div>

                                    <div class="select w_y b1_2_form" id="webview">
                                        <div class="module-customselect">
                                            <dl class="month">
                                                <dd>
                                                    <div class="time">
                                                        <select id="sel_year" name="sel_year"
                                                                class="validate[required]"
                                                                rel="${fn:substring(account.birthdate, 0, 4)}"></select>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="module-customselect">
                                            <dl class="month">
                                                <dd>
                                                    <div class="time">
                                                        <select id="sel_month" name="sel_month"
                                                                class="validate[required]"
                                                                rel="${fn:substring(account.birthdate, 5, 7)}"></select>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="module-customselect">
                                            <dl class="month">
                                                <dd>
                                                    <div class="time">
                                                        <select id="sel_day" name="sel_day"
                                                                class="validate[required]"
                                                                rel="${fn:substring(account.birthdate, 8, 11)}"></select>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>

                                    <%--<div class="pass_tip">&nbsp;&nbsp;&nbsp;請於下方填入身分證發證日期</div>--%>
                                    <div class=" b1_2_mobile">
                                        <dl class="year">
                                            <dt>出生年</dt>
                                        </dl>
                                        <dl class="month">
                                            <dd>
                                                <select id="sel_year2" name="sel_year"
                                                        class="validate[required]"
                                                        rel="${fn:substring(account.birthdate, 0, 4)}">
                                                </select>
                                            </dd>

                                            <dd>
                                                <select id="sel_month2" name="sel_month"
                                                        class="validate[required]"
                                                        rel="${fn:substring(account.birthdate, 5, 7)}">
                                                </select>
                                            </dd>
                                            <dd>
                                                <select id="sel_day2" name="sel_day"
                                                        class="validate[required]"
                                                        rel="${fn:substring(account.birthdate, 8, 11)}">
                                                </select>
                                            </dd>
                                        </dl>
                                    </div>

                                    <div class="clear"></div>

                                    <%--發證資訊--%>
                                    <%--<div class="select w_y b1_2_form" id="webview">
                                        <div class="module-customselect">
                                            <dl class="month">
                                                <dd>
                                                    <div class="time">
                                                        <select id="idApply_year" name="idApply_year"
                                                                class="validate[required]"
                                                                rel="${fn:substring(account.idApplyDate, 0, 4)}"></select>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="module-customselect">
                                            <dl class="month">
                                                <dd>
                                                    <div class="time">
                                                        <select id="idApply_month" name="idApply_month"
                                                                class="validate[required]"
                                                                rel="${fn:substring(account.idApplyDate, 5, 7)}"></select>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="module-customselect">
                                            <dl class="month">
                                                <dd>
                                                    <div class="time">
                                                        <select id="idApply_day" name="idApply_day"
                                                                class="validate[required]"
                                                                rel="${fn:substring(account.idApplyDate, 8, 11)}"></select>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>

                                    <div class=" b1_2_mobile">
                                        <dl class="year">
                                            <dt>發證</dt>
                                        </dl>
                                        <dl class="month">
                                            <dd>
                                                <select id="idApply_year2" name="idApply_year"
                                                        class="validate[required]"
                                                        rel="${fn:substring(account.idApplyDate, 0, 4)}">
                                                </select>
                                            </dd>

                                            <dd>
                                                <select id="idApply_month2" name="idApply_month"
                                                        class="validate[required]"
                                                        rel="${fn:substring(account.idApplyDate, 5, 7)}">
                                                </select>
                                            </dd>
                                            <dd>
                                                <select id="idApply_day2" name="idApply_day"
                                                        class="validate[required]"
                                                        rel="${fn:substring(account.idApplyDate, 8, 11)}">
                                                </select>
                                            </dd>
                                        </dl>
                                    </div>

                                    <div class="clear"></div>

                                    <div class="clearfix b1_2_form" id="webview">
                                        <div class="module-customselect" id="address-zone3">
                                            <dl class="clearfix w1">
                                                <dt>發證地</dt>
                                                <dd>
                                                    <select id="idApplyCity" name="idApplyCity"
                                                            class="validate[required]">
                                                        <option value="">發證地</option>
                                                    </select>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>

                                    <div class="select b1_2_mobile" id="address-zone4">
                                        <dl class="area clearfix">
                                            <dt>發證地</dt>
                                            <dd>
                                                <select id="idApplyCity" name="idApplyCity" class="validate[required]">
                                                    <option value="">發證地</option>
                                                </select>
                                            </dd>
                                        </dl>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="clear"></div>

                                    <div class="clearfix b1_2_form" id="webview">
                                        <div class="module-customselect">
                                            <dl class="clearfix w1">
                                                <dt>領補換類別</dt>
                                                <dd>
                                                    <select id="idApplyType" name="idApplyType"
                                                            class="validate[required]">
                                                        <option value="">領補換類別</option>
                                                        <option value="1" ${account.idApplyType==1?"selected":""}>領發
                                                        </option>
                                                        <option value="2" ${account.idApplyType==2?"selected":""}>補發
                                                        </option>
                                                        <option value="3" ${account.idApplyType==3?"selected":""}>換發
                                                        </option>
                                                    </select>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>

                                    <div class="select b1_2_mobile">
                                        <dl>
                                            <dt>領補換類別</dt>
                                            <dd>
                                                <select id="idApplyType" name="idApplyType" class="validate[required]">
                                                    <option value="1" ${account.idApplyType==1?"selected":""}>領發
                                                    </option>
                                                    <option value="2" ${account.idApplyType==2?"selected":""}>補發
                                                    </option>
                                                    <option value="3" ${account.idApplyType==3?"selected":""}>換發
                                                    </option>
                                                </select>
                                            </dd>
                                        </dl>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear"></div>--%>
                                    <%--發證資訊 END--%>
                                </div>
                            </div>
                            <div class="register_t2 ">
                                <div class="register_column2">
                                    <div class="clearfix b1_2_form" id="webview">
                                        <div class="module-customselect">
                                            <dl class="clearfix w1">
                                                <dt>性別</dt>
                                                <dd>
                                                    <select id="sex" name="sex" class="validate[required]">
                                                        <option value="">請選擇性別</option>
                                                        <option value="1" ${account.gender==1?"selected":""}>男</option>
                                                        <option value="2" ${account.gender==2?"selected":""}>女</option>
                                                    </select>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>

                                    <div class="select b1_2_mobile">
                                        <dl>
                                            <dt>姓別</dt>
                                            <dd>
                                                <select id="sex" name="sex" class="validate[required]">
                                                    <option value="">請選擇性別</option>
                                                    <option value="1" ${account.gender==1?"selected":""}>男</option>
                                                    <option value="2" ${account.gender==2?"selected":""}>女</option>
                                                </select>
                                            </dd>
                                        </dl>
                                        <div class="clear"></div>
                                    </div>

                                    <div class="clearfix b1_2_form" id="webview">

                                        <div class="module-customselect">

                                            <dl class="clearfix w1">
                                                <dt>婚姻狀態</dt>
                                                <dd>
                                                    <select id="marriage" name="marriage"
                                                            class="validate[required]">
                                                        <option value="">婚姻狀態</option>
                                                        <option value="1" ${account.maritalStatus==1?"selected":""}>未婚
                                                        </option>
                                                        <option value="2" ${account.maritalStatus==2?"selected":""}>已婚
                                                        </option>
                                                        <option value="3" ${account.maritalStatus==3?"selected":""}>其他
                                                        </option>
                                                    </select>
                                                </dd>
                                            </dl>
                                        </div>
                                    </div>
                                    <div class="select b1_2_mobile">
                                        <dl>
                                            <dt>婚姻狀態</dt>
                                            <dd>
                                                <select id="marriage" name="marriage"
                                                        class="validate[required]">
                                                    <option value="">婚姻狀態</option>
                                                    <option value="1" ${account.maritalStatus==1?"selected":""}>未婚
                                                    </option>
                                                    <option value="2" ${account.maritalStatus==2?"selected":""}>已婚
                                                    </option>
                                                    <option value="3" ${account.maritalStatus==3?"selected":""}>其他
                                                    </option>
                                                </select>
                                            </dd>
                                        </dl>
                                        <div class="clear"></div>
                                    </div>

                                    <dl class="clearfix w1">
                                        <dt>手機號碼</dt>
                                        <dd>
                                            <input type="text" id="phone" name="phone" value="${account.mobile}"
                                                   class="text-input validate[required,custom[phone],funcCall[checkMoblie]]">
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>
                                    <dl class="clearfix w2">
                                        <dt>電子信箱</dt>
                                        <dd>
                                            <input type="text" id="email" name="email"
                                                   value="${account.email}"
                                                   class="text-input validate[required,funcCall[checkMail],custom[email]]">
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>

                                    <div class="select w_y b1_2_form" id="address-zone">

                                        <div class="module-customselect" id="webview">
                                            <dl class="month">
                                                <dd>
                                                    <div class="time">
                                                        <select id="city" name="city" class="validate[required]">
                                                            <option value="">居住縣市</option>
                                                        </select>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                        <div class="module-customselect" id="webview">
                                            <dl class="month">
                                                <dd>
                                                    <div class="time">
                                                        <select id="county" name="county" class="validate[required]">
                                                            <option value="">區域</option>
                                                        </select>
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>

                                    </div>
                                    <div class="select b1_2_mobile" id="address-zone2">
                                        <dl class="area clearfix">
                                            <dt>居住縣市</dt>
                                            <dd>
                                                <select id="city" name="city" class="validate[required]">
                                                    <option value="">居住縣市</option>
                                                </select>
                                            </dd>
                                            <div class="clear"></div>
                                        </dl>

                                        <dl class="area clearfix">
                                            <dt>區域</dt>
                                            <dd>
                                                <select id="county" name="county"
                                                        class="validate[required]">
                                                    <option value="">區域</option>
                                                </select>
                                            </dd>
                                        </dl>
                                        <div class="clear"></div>
                                    </div>

                                    <dl class="clearfix w2">
                                        <dt>完整地址</dt>
                                        <dd>
                                            <input class="validate[required]" id="add" name="add" placeholder="完整地址"
                                                   type="text" value="${account.address}" maxlength="60">
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>
                                    <div class="t5">
                                        <dl class="clearfix w2">
                                            <dt>&nbsp;</dt>
                                            <dd>※ 地址須填入實際居住地，非戶籍地址</dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>

                            <div class="register_t2">
                                <div class="btn-wrap re_btn">
                                    <input id="changPWDButton" class="news_button" type="button" onclick="javascript:changePWDdisplay();" value="修改密碼"/>
                                </div>

                                <div class="register_column2" id="pwdArea" style="display:none;">
                                    <dl class="clearfix w2">
                                        <dt>原始密碼</dt>
                                        <dd>
                                            <input type="password" id="origPwd" name="origPwd"
                                                   class="text-input validate[funcCall[checkOrigPassword]]"
                                                   placeholder="請輸入原始密碼"
                                                   maxlength="10" autocomplete="off">
                                        </dd>
                                    </dl>
                                    <dl class="clearfix w2">
                                        <dt>修改密碼</dt>
                                        <dd>
                                            <input type="password" value="" id="pwd" name="password"
                                                   placeholder="請輸入新密碼"
                                                   class="text-input validate[funcCall[checkPasswordSize],funcCall[checkPasswordRule],funcCall[checkNewPassword]]"
                                                   maxlength="10" autocomplete="off">
                                        </dd>
                                    </dl>
                                    <div class="pass_tip">※ 密碼需由英文、數字、特殊符號組成(1.介於6到10個字元2.不可與會員代號相同. 3.例如:@bcd1234)
                                    </div>

                                    <dl class="clearfix w2">
                                        <dt>確認密碼</dt>
                                        <dd>
                                            <input id="password2" name="password2" type="password" value=""
                                                   class="text-input validate[equals[pwd]]" placeholder="再次確認密碼"
                                                   maxlength="10" autocomplete="off">
                                        </dd>
                                    </dl>
                                    <div class="pass_tip">※ 請輸入相同密碼</div>
                                </div>
                            </div>

                            <div class="register_t2 ">
                                <div class="statement">
                                    <label class="module-checkbox style-block"><input
                                            id="checkbox1" type="checkbox" name="checkbox1" value="0"
                                            class="validate[required]" checked="checked"><i
                                            class="module-symbol"></i><span>本人特此聲明，我已閱讀、瞭解並同意遵守<a
                                            id="url-1" onClick="showView('11')">服務條款</a>及<a id="url-2"
                                                                                            onClick="showView('1')">隱私權政策</a></span>
                                    </label> <label
                                        class="module-checkbox style-block"> <input
                                        id="checkbox2" type="checkbox" name="checkbox2" value="0"><i
                                        class="module-symbol"></i><span>我想獲得台灣資金交易所的最新消息、廣告及優惠通知</span>
                                </label>
                                    <div class="btn-wrap re_btn clearfix"></div>
                                    <div class="btn-wrap re_btn">
                                        <!--
                                        <div class="news_button">
                                            <a href="mypage.html"></a>
                                        </div>
                                        -->
                                        <input id="sendButton" class="news_button" type="submit" value="確定"/>
                                        <%--<input id="updateButton" class="news_button" type="button"
                                               value="修改個人身分資料" onclick="redirectPage();"/>--%>
                                        <!--
										<div class="news_button">
											<a href="${pageContext.request.contextPath}/mypage/membereditidentity">修改個人身分資料</a>
										</div>
										-->
                                    </div>
                                </div>
                            </div>


                        </div>
                    </form>
                </div>

                <!--PAGE END-->
            </div>
        </section>
        <img
                src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg"
                width="100%" style="margin-top: -1px;" alt="">
    </div>
</div>
<link
        href="${pageContext.request.contextPath}/resource/assets/images/favicon.png"
        rel="shortcut icon" type="image/x-icon"/>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/index.css" media="screen"
      rel="stylesheet" type="text/css"/>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/member.css" media="screen"
      rel="stylesheet" type="text/css"/>

<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js"
        type="text/javascript"></script>
<script

        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js"
        type="text/javascript"></script>

<!-- 註冊 -->
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resource/common/js/jquery-1.11.3.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/common/css/validationEngine.jquery.css"/>
<script src="${pageContext.request.contextPath}/resource/common/js/jquery.validationEngine-zh_CN.js"></script>
<script src="${pageContext.request.contextPath}/resource/common/js/jquery.validationEngine.js"></script>
<script src="${pageContext.request.contextPath}/resource/common/js/birthday.js"></script>
<script src="${pageContext.request.contextPath}/resource/common/js/aj-address.js"></script>
<script type="text/javascript">
    var originalEMail = "${account.email}";
    var originalMoblie = "${account.mobile}";
    function redirectPage() {

        window.location.replace("${pageContext.request.contextPath}/mypage/membereditidentity");
    }
    $('#user_form').validationEngine({
        validationEventTriggers: "blur", //触发的事件  validationEventTriggers:"keyup blur",
        inlineValidation: true,//是否即时验证，false为提交表单时验证,默认true
        success: false,//为true时即使有不符合的也提交表单,false表示只有全部通过验证了才能提交表单,默认false
        promptPosition: "topRight",//提示所在的位置，topLeft, topRight, bottomLeft,  centerRight, bottomRight
        scroll: false,
        //ajax
        ajaxFormValidation: true,
        ajaxFormValidationMethod: 'post',
        onAjaxFormComplete: ajaxValidationCallback,
        onBeforeAjaxFormValidation: beforeCall,
        custom_error_messages: {
            // Custom Error Messages for Validation Types
            '#user': {
                'custom[chinese]': {
                    'message': '姓名格式錯誤'
                },
            },
            '#checkbox1': {
                'required': {
                    'message': '選項服務條款及隱私權政策未同意無法進行後續作業 '
                }
            },
        },

    });

    //This method is called right before the ajax form validation request
    //it is typically used to setup some visuals ("Please wait...");
    //you may return a false to stop the request
    function beforeCall(form, options) {
        ShowProgressBar();
        if (window.console) {
            //console.log('Right before the AJAX form validation call');
        }
        ;
        return true;
    }
    ;

    //Called once the server replies to the ajax form validation request
    function ajaxValidationCallback(status, form, json, options) {
        //console.log(status);
        //console.log(form);
        //console.log(json);
        //console.log(options);
        //if (window.console) {
        //console.log(status);
        //}
        //;

        if (status === true) {
            //alert('the form is valid!');
            if (json.rs == true) {
                //alert("成功");
                //$.post("${pageContext.request.contextPath}"+"/account/register/Register_B2",function(data){

                //$(".layout-container").html(data);

                //alert("a........href:"+data);
                //});
                if (json.mail == true) {
                    alert("系統已發送認證信至您修改後信箱,請您至信箱收取認證信進行確認,信箱將在您確認後進行更新");
                }
                HideProgressBar();
                window.location.replace("${pageContext.request.contextPath}" + json.view);
            } else {
                alert("失敗");
            }
        } else {
            alert("error");
        }
        HideProgressBar();

    }
    ;
    //${account.birthdate}

    $('#address-zone').ajaddress({city: "${account.city}", county: "${account.area}"});
    $('#address-zone2').ajaddress({city: "${account.city}", county: "${account.area}"});

    $('#address-zone3').applyAddress({applyCity: "${account.idApplyCity}"});
    $('#address-zone4').applyAddress({applyCity: "${account.idApplyCity}"});

    $.ms_DatePicker({
        YearSelector: "#idApply_year",
        MonthSelector: "#idApply_month",
        DaySelector: "#idApply_day",
        FirstText: "發證"
    });
    $.ms_DatePicker({
        YearSelector: "#idApply_year2",
        MonthSelector: "#idApply_month2",
        DaySelector: "#idApply_day2",
        FirstText: "發證"
    });

    $.ms_DatePicker({
        YearSelector: "#sel_year",
        MonthSelector: "#sel_month",
        DaySelector: "#sel_day"
    });
    $.ms_DatePicker({
        YearSelector: "#sel_year2",
        MonthSelector: "#sel_month2",
        DaySelector: "#sel_day2"
    });

    function showDiv() {
        //alert("show");
        $("#regbutton").hide();
        $(".register_t2 ").show();

    }

    //    function checkPassword(field, rules, i, options) {
    //
    //        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
    //
    //        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
    //        var password = field.val();
    //        if (password.length < 1) {
    //            return false;
    //        }
    //        //利用match函數去比較密碼是否符合指定條件：最少一個數字，最少一個小階英文，最少一個大階英文，長度限制為10-100。
    //        var chkPwdStength = password.length;
    //        var maxSize = 12;
    //        var checkVal = passwordGrade(password);
    //        //console.log("checkVal:"+checkVal);
    //        //var size = (chkPwdStength / maxSize) * 100;
    //        var size = checkVal;
    //
    //        if (size > 99) {
    //            size = 99;
    //        } else if (size < 0) {
    //            size = 0;
    //        }
    //
    //        if (size < 40) {
    //            $(".pass_s3").text("弱");
    //            //$(".pass_rate2").css('background',"#FF0000");
    //        } else if (size < 70) {
    //            $(".pass_s3").text("中");
    //            //$(".pass_rate2").css('background',"#FFFF00");
    //        } else if (size >= 70) {
    //
    //            $(".pass_s3").text("強");
    //            //$(".pass_rate2").css('background',"#70CBFF");
    //        }
    //
    //        $(".pass_rate2").css('width', size + "%");
    //
    //        return true;
    //    }

    function checkPasswordSize(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        if (value.length < 1) {
            return false;
        } else if (value.length >= 1 && value.length < 6) {
            return "密碼為6~12個字元";
        } else if (value.length > 12) {
            return "密碼為6~12個字元";
        }
        return false;
    }

    function checkPasswordRule(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        var accountValue = $("#account").val();
        if (value != accountValue) {
            return false;
        } else {
            return "不可與會員代號相同";
        }

        return false;
    }

    /**
     * 確認user 輸入原始密碼是否正確
     * @returns {*}
     */
    function checkOrigPassword(field, rules, i, options) {
        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        if (value.length < 1) {
            return false;
        }
        var checkPWStates = false;
        $.ajax({
            type: "POST",
            url: "${pageContext.request.contextPath}/register/checkSPassWord?"
            + "pw=" + value,
            dataType: "json",
            contentType: "application/json; charset=UTF-8",
            cache: false,
            async: false,
            success: function (backmsg) { //取得回傳訊息
                if (backmsg.rs) {
                    checkPWStates = true;
                } else {
//                    alert("IN false:"+backmsg.errorMsg);
                    if(backmsg.errorMsg!=undefined && backmsg.errorMsg!="")
                            alert(backmsg.errorMsg);
                    return false;
                }
            },
            error: function () {
                //alert("新增失敗" );
                //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");
                return "";
            }
        });

        //return false;
        if (checkPWStates) {
            return false;
        } else {
            return "原始密碼錯誤";
//            return "原始密碼錯誤";
        }

    }

    /**
     * 確認user 輸入新密碼是否等於原始密碼
     * @returns {*}
     */
    function checkNewPassword(field, rules, i, options) {
        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        if (value.length < 1) {
            return false;
        }
        var checkPWStates = false;
        $.ajax({
            type: "POST",
            url: "${pageContext.request.contextPath}/register/checkSPassWord?"
            + "pw=" + value,
            //data: JSON.stringify({
            //account: value,
            //}),
            dataType: "json",
            contentType: "application/json; charset=UTF-8",
            cache: false,
            async: false,
            success: function (backmsg) { //取得回傳訊息
                if (backmsg.rs) {
                    checkPWStates = true;
                } else {
                    return false;
                }
            },
            error: function () {
                //alert("新增失敗" );
                //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");
                return "";
            }
        });

        //return false;
        if (checkPWStates) {
            return "更新密碼不可與原始密碼相同";
        } else {
            return false;
        }

    }

    function changePWDdisplay() {

        var pwDivObj = $("#pwdArea");
//        openDebug(pwDivObj, "pwDivObj");
//        openDebug(pwDivObj.css, "pwDivObj.css");

        if (pwDivObj.css('display') == 'none') {
//            alert("show");
            pwDivObj.show();
        } else {
//            alert("hide");
//            pwDivObj.hide();
//            $("#origPwd").val("");
//            $("#pwd").val("");
//            $("#password2").val("");
        }

    }

    function checkPasswordValue(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        //alert("pwd2:"+$("#pwd2").val() + ",password:"+field.val());
        pwValue = $("#pwd2").val();
        var value = field.val();
        if (value == pwValue) {
            return false;
        } else {
            return "兩次輸入密碼不一致";
        }

    }

    function checkAccountSize(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        if (value.length < 1) {
            return false;
        } else if (value.length >= 1 && value.length < 6) {
            return "代號為6~16個字元";
        } else if (value.length > 16) {
            return "代號為6~16個字元";
        }
        return false;
    }

    function checkAccount(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        if (sUserId == value) {
            $(".tip").html("");
            return false;
        }

        if (value.length < 1) {
            $(".tip").html("");
            return false;
        } else if (value.length >= 1 && value.length < 6) {
            $(".tip").html("");
            return false;
        } else if (value.length > 10) {
            $(".tip").html("");
            return false;
        }

        var accountVar = false;
        $
                .ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/register/checkUserId?"
                    + "userId=" + value,
                    //data: JSON.stringify({
                    //account: value,
                    //}),
                    dataType: "json",
                    contentType: "application/json; charset=UTF-8",
                    cache: false,
                    async: false,
                    success: function (backmsg) { //取得回傳訊息
                        //console.log(backmsg);
                        //alert(backmsg);
                        if (backmsg.rs == true) {
                            //alert("新增成功" );
                            $(".tip")
                                    .html(
                                            "<img src='${pageContext.request.contextPath}/resource/assets/images/tip_ok.jpg' >此帳號可使用!");
                            return false;
                        } else {
                            $(".tip")
                                    .html(
                                            "<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");
                            accountVar = true;
                        }

                    }
                });

        if (accountVar) {
            return options.allrules.checkAccount.alertText;
        } else {
            return false;
        }

    }
    ;

    function checkTwId(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        if (value.length < 10) {
            return options.allrules.checkTwId.alertText;
        }
        var twidVar = false;
        $
                .ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/register/checkAccount?"
                    + "account=" + value,
                    //data: JSON.stringify({
                    //account: value,
                    //}),
                    dataType: "json",
                    contentType: "application/json; charset=UTF-8",
                    cache: false,
                    async: false,
                    success: function (backmsg) { //取得回傳訊息
                        //console.log(backmsg);
                        //alert(backmsg);
                        if (backmsg.rs == true) {
                            //alert("新增成功" );
                            //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip_ok.jpg' >此帳號可使用!");
                            twidVar = true;
                            return;
                        } else {
                            return options.allrules.checkTwId.alertText;
                        }
                        //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");

                    },
                    error: function () {
                        //alert("新增失敗" );
                        //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");
                        return options.allrules.checkTwId.alertText;
                    }
                });
        //return options.allrules.checkTwId.alertText;
        if (twidVar != true) {
            return options.allrules.checkTwId.alertText;
        }
    }
    ;

    function checkMail(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        if (originalEMail != value) {
            var mailVar = false;
            $
                    .ajax({
                        type: "POST",
                        url: "${pageContext.request.contextPath}/register/checkMail?"
                        + "mail=" + value,
                        //data: JSON.stringify({
                        //account: value,
                        //}),
                        dataType: "json",
                        contentType: "application/json; charset=UTF-8",
                        cache: false,
                        async: false,
                        success: function (backmsg) { //取得回傳訊息
                            //console.log(backmsg);
                            //alert(backmsg);
                            if (backmsg.rs == true) {
                                //alert("新增成功" );
                                //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip_ok.jpg' >此帳號可使用!");
                                mailVar = true;
                                return;
                            } else {
                                return options.allrules.checkMail.alertText;
                            }
                            //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");

                        },
                        error: function () {
                            //alert("新增失敗" );
                            //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");
                            return options.allrules.checkMail.alertText;
                        }
                    });
            if (mailVar != true) {
                return options.allrules.checkMail.alertText;
            }
        } else {
            return;
        }


    }
    ;

    function checkMoblie(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        if (originalMoblie != value) {
            var moblieVar = false;
            $
                    .ajax({
                        type: "POST",
                        url: "${pageContext.request.contextPath}/register/checkMoblie?"
                        + "moblie=" + value,
                        //data: JSON.stringify({
                        //account: value,
                        //}),
                        dataType: "json",
                        contentType: "application/json; charset=UTF-8",
                        cache: false,
                        async: false,
                        success: function (backmsg) { //取得回傳訊息
                            //console.log(backmsg);
                            //alert(backmsg);
                            if (backmsg.rs == true) {
                                //alert("新增成功" );
                                //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip_ok.jpg' >此帳號可使用!");
                                moblieVar = true;
                                return;
                            } else {
                                return options.allrules.checkMail.alertText;
                            }
                            //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");

                        },
                        error: function () {
                            //alert("新增失敗" );
                            //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");
                            return options.allrules.checkMail.alertText;
                        }
                    });
            if (moblieVar != true) {
                return options.allrules.checkMail.alertText;
            }
        } else {
            return;
        }


    }
    ;

</script>

<script>
    <%--function registerAgainMail() {--%>

    <%--//透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。--%>
    <%--var mail = $("#mail").val();--%>

    <%--$.ajax({--%>
    <%--type: "POST",--%>
    <%--url: "${pageContext.request.contextPath}/resendAuthMail?"--%>
    <%--+ "email=" + mail,--%>
    <%--//data: JSON.stringify({--%>
    <%--//account: value,--%>
    <%--//}),--%>
    <%--dataType: "json",--%>
    <%--contentType: "application/json; charset=UTF-8",--%>
    <%--cache: false,--%>
    <%--async: false,--%>
    <%--success: function (backmsg) { //取得回傳訊息--%>
    <%--//console.log(backmsg);--%>
    <%--//alert(backmsg);//0:發送失敗 1:成功發送--%>
    <%--if (backmsg.rs == true) {--%>
    <%--alert("成功發送 ");--%>
    <%--return true;--%>
    <%--} else {--%>
    <%--alert("發送失敗");--%>
    <%--}--%>
    <%--return false;--%>
    <%--},--%>
    <%--error: function () {--%>
    <%--alert("發送失敗");--%>
    <%--return false;--%>
    <%--}--%>
    <%--});--%>
    <%--return false;--%>
    <%--}--%>
    ;


    // 顯示讀取遮罩
    function ShowProgressBar() {
        displayProgress();
        displayMaskFrame();
    }

    // 隱藏讀取遮罩
    function HideProgressBar() {
        var progress = $('#divProgress');
        var maskFrame = $("#divMaskFrame");
        progress.hide();
        maskFrame.hide();
    }
    // 顯示讀取畫面
    function displayProgress() {
        var w = $(document).width();
        var h = $(window).height();
        var progress = $('#divProgress');
        progress.css({
            "z-index": 999999,
            "top": (h / 2) - (progress.height() / 2),
            "left": (w / 2) - (progress.width() / 2)
        });
        progress.show();
    }
    // 顯示遮罩畫面
    function displayMaskFrame() {
        var w = $(window).width();
        var h = $(document).height();
        var maskFrame = $("#divMaskFrame");
        maskFrame.css({"z-index": 999998, "opacity": 0.7, "width": w, "height": h});
        maskFrame.show();
    }


    function onLink(value) {
        $.colorbox({href: value, top: true, escKey: false, overlayClose: false});
        //$.colorbox({href:value+" .layout-view",scrolling:true});
    }

    var sUserId = "${account.userId}";

    function passwordGrade(pwd) {

        var score = 0;

        var regexArr = ['[0-9]', '[a-z]', '[A-Z]', '[\\W_]'];

        var repeatCount = 0;

        var prevChar = '';

        //check length

        var len = pwd.length;

        score += len > 12 ? 12 : len;

        //check type
        for (var i = 0, num = regexArr.length; i < num; i++) {
            if (eval('/' + regexArr[i] + '/').test(pwd)) score += 1;
        }

        //bonus point
        for (var i = 0, num = regexArr.length; i < num; i++) {
            if (pwd.match(eval('/' + regexArr[i] + '/g')) && pwd.match(eval('/' + regexArr[i] + '/g')).length >= 2) score += 2;
            if (pwd.match(eval('/' + regexArr[i] + '/g')) && pwd.match(eval('/' + regexArr[i] + '/g')).length >= 5) score += 2;
        }

        //deduction

        for (var i = 0, num = pwd.length; i < num; i++) {
            if (pwd.charAt(i) == prevChar) repeatCount++;
            else prevChar = pwd.charAt(i);
        }
        score -= repeatCount * 1;
        if (score <= 6) {
            return score * 1.5;
        } else if (score > 6 && score <= 10) {
            return score * 2;
        } else if (score > 10 && score <= 15) {
            return score * 2.5;
        } else if (score > 15 && score <= 20) {
            return score * 3.3;
        } else if (score > 20 && score <= 25) {
            return score * 4;
        } else if (score > 25) {
            return score * 5;
        }
    }

    function showView(value) {
        if (value == 1) {
            $.colorbox({
                html: function () {
                    //在燈箱中要顯示的html字段
                    return $("#divPrivacy").html();
                },
                width: 700, //燈箱中間區塊的寬度
                //height : 600, //燈箱中間區塊的高度
                onClosed: function () { //當燈箱關閉時的callback funtion
                    //alert('Lightbox is closed :'+$(".img-group-1").attr("src"));
                }
            })
        } else {
            $.colorbox({
                html: function () {
                    //在燈箱中要顯示的html字段
                    return $("#divService").html();
                },
                width: 700, //燈箱中間區塊的寬度
                //height : 600, //燈箱中間區塊的高度
                onClosed: function () { //當燈箱關閉時的callback funtion
                    //alert('Lightbox is closed :'+$(".img-group-1").attr("src"));
                }
            })
        }


    }
</script>
<script type="text/javascript">

    function dump_props(obj, objName) {
        var result = "";

        for (var i in obj) {
            try {
                result += "<tr><td>" + objName + "." + i + "</td><td>" + obj[i] + "</td></tr>";
            }
            catch (err) {
                result += "<tr><td>" + objName + "." + i + "</td><td>" + err + "</td></tr>";
            }
        }

        return result;
    }

    var GO_DEBUG = false;
    function openDebug(obj, objName) {
        if (GO_DEBUG) {
            debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
            debugWindow.document.write("<HEAD><TITLE>Message window</TITLE></HEAD>");
            debugWindow.document.write("<Body><Table border='1'>" + dump_props(obj, objName) + "</Table></Body>");
        }
    }

    function openDebugText(_text) {
        if (GO_DEBUG) { //alert("debug");
            debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
            debugWindow.document.write(_text);
        }
    }


    function openDebugTextNewLine(_text) {
        if (GO_DEBUG) { //alert("debug");
            debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
            var re = /\&/g;
            var result = _text.replace(re, '<br/>&');
            debugWindow.document.write(result + '<br/>');
        }
    }
</script>

<style>
    select {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        padding: 2px 30px 2px 2px;
        border: none;
    }

    .register_t2 csdd {
        width: 180px;
        padding-top: 12px;
        padding-bottom: 12px;
        height: 54px;
        line-height: 35px;
        border-radius: 30px;
        margin-bottom: 6px;
        color: #4D4D4D;
    }

    #webview select {
        width: 138px;
        background: #FFF url("${pageContext.request.contextPath}/resource/assets/images/g-drap.png") no-repeat scroll right 1rem center;
        border: 0px none;
        font-size: 14px;
        font-family: "Helvetica Neue", Helvetica, Arial, Chinese, "微軟正黑體",
        sans-serif;
        color: #4D4D4D;
        padding: 0px 10px;
    }

    .module-customselect .time {
        font-size: 14px;
        padding-left: 0px;
        text-indent: 10px;
    }

    .register_t2 .select dl {
        margin-left: 0px;
        width: 162px;
    }

    .register_t2 .b1_2_mobile .year {
        float: left;
        margin: 0px;
        width: 86px;
    }
</style>

<div id="divProgressLink" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;">

</div>
<div id="divMaskFrameLink" style="background-color: #F2F4F7; display: none; left: 0px;
    position: absolute; top: 0px;">
</div>

<div id="divProgress" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;">
    <img id="img-loading" src="${pageContext.request.contextPath}/resource/assets/images/loading.gif"/>
    <br/>
    <font color="#1B3563" size="2px">資料處理中</font>
    <div class="nav_rate">
        <div class="nav_rate2 nav5">&nbsp;</div>
    </div>
</div>
<div id="divMaskFrame" style="background-color: #F2F4F7; display: none; left: 0px;
    position: absolute; top: 0px;">
</div>
<div id="divPrivacy" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;">
    ${privacy}
</div>
<div id="divService" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;">
    ${service}
</div>
</body>
</html>