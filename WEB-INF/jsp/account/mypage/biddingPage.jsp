﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/rangeslider.css" media="screen" rel="stylesheet" type="text/css" />
<style>
#cboxWrapper,#cboxContent,#cboxLoadedContent{
  height: initial !important;
}
#cboxContent{
	padding-bottom: 20px;
}
#cboxLoadedContent{
	width: initial !important;
}
</style>	
		<section class="bidpopup" id="popup_content">
			<h3 class="on-mobile-block">下標金額拖拉</h3>
			<dl class="module-dl style-rounded on-mobile">
				<dt>下標金額</dt>
				<dd>
					<input type="text" id="bidAmount2" class="js-amount-input-m" value="${userBidSetDetail.bid}" onkeydown="return blockChar(this,value)">
				</dd>
			</dl>
			<div class="col-left">
				<h3 class="on-desk-block">下標金額拖拉</h3>
				<p class="num-max">Max</p>
				<div class="col-left-bar">
					<input type="range" data-orientation="vertical" value="${userBidSetDetail.bid}">
				</div>
				<div class="col-left-bidder">
					<c:forEach items="${bidSetDetailList}" var="bidSetDetail">
						<c:choose>
							<c:when test="${ bidSetDetail ne null }">
								<div class="per-bidder" style="margin-bottom: 5px;">
									<span class="bidder-price">${bidSetDetail.bid}</span>
									<span class="bidder-profile" data-price="${bidSetDetail.bid}">
										信用金${bidSetDetail.guranCreditFee}
										<i>/</i>
										<c:forEach items="${accountList}" var="account">
											<c:if test="${bidSetDetail.id.identification eq account.identification }">${account.userId}</c:if>
										</c:forEach>
									</span>
								</div>
							</c:when>
							<c:otherwise>
								<div class="per-bidder" style="visibility: hidden;margin-bottom: 5px;">
									<span class="bidder-price">0</span>
									<span class="bidder-profile" data-price="">
										信用金<i>/</i>no data
									</span>
								</div>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</div>
				<p class="num-min">min</p>
			</div>

			<div class="col-right">
				<dl class="module-dl style-rounded on-desk">
					<dt>下標金額</dt>
					<dd>
						<input type="text" id="bidAmount" class="js-amount-input" value="${userBidSetDetail.bid}" onkeydown="return blockChar(event)">
					</dd>
				</dl>
				<dl class="module-dl bidpopup-table">
					<dt><span>開標日期</span></dt>
					<dd><span>${fn:substring(userBidSetDetail.bidDate, 0 , 4)} / ${fn:substring(userBidSetDetail.bidDate, 4 , 6)} / ${fn:substring(userBidSetDetail.bidDate, 6 , 8)}</span></dd>
					
					<dt><span>現在期數/總期數</span></dt>
					<dd><span>${userBidSetDetail.id.periods} / ${userBidSetDetail.totalPeriods}</span></dd>
					<dt><span>可得標人數</span></dt>
					<dd><span>${userBidSetDetail.biddedCnt}</span></dd>
					<dt><span>剩餘額度</span></dt>
					<dd><span>${bidAccount.workSelfAccuCredit + bidAccount.workGurranCredit + bidAccount.workReturnCredit - bidAccount.riskCred}</span></dd>
					<dt><span>得標總資金</span></dt>
					<fmt:parseNumber var="totalPeriods" type="number" value="${userBidSetDetail.totalPeriods}" />
					<fmt:parseNumber var="currentPeriods" type="number" value="${userBidSetDetail.id.periods}" />
					<fmt:parseNumber var="biddedCnt" type="number" value="${userBidSetDetail.biddedCnt}" />
					<input type="hidden" id="userPayment" value="${userBidSetDetail.payment}" >
					<input type="hidden" id="userTotalPeriods" value="${totalPeriods}" >
					<input type="hidden" id="userCurrentPeriods" value="${currentPeriods}" >
					<input type="hidden" id="userBiddedCnt" value="${biddedCnt}" >
					<input type="hidden" id="userGuranCreditFee" value="${userBidSetDetail.guranCreditFee}" >
					<dd><span><output>${( (userBidSetDetail.payment - userBidSetDetail.bid) * (totalPeriods - currentPeriods) ) + ( userBidSetDetail.payment * (currentPeriods - 1) ) - ( userBidSetDetail.guranCreditFee * biddedCnt * (totalPeriods - currentPeriods) )}</output></span></dd>
					<dt><span>信用金</span></dt>
					<dd><span>${userBidSetDetail.guranCreditFee}</span></dd>
					<dt><span>管理費</span></dt>
					<dd><span>${userBidSetDetail.sysUseFee}</span></dd>
					<dt></dt>
					<dd class="bidpopup-selectcoupon">
						<div class="module-customselect">
							<c:choose>
								<c:when test="${usedAccountCoupon ne null}">
									<span class="module-customselect-selected">${usedAccountCoupon.couponName}</span>
									<input type="hidden" value="${usedAccountCoupon.id.couponNo}" name="" id="couponNo">
								</c:when>
								<c:otherwise>
									<span class="module-customselect-selected">不使用COUPON</span>
									<input type="hidden" value="0" name="" id="couponNo">
								</c:otherwise>
							</c:choose>
							<ul>
								<li value="0">不使用Coupon</li>
								<c:if test="${usedAccountCoupon ne null}">
									<li value="${usedAccountCoupon.id.couponNo}">${usedAccountCoupon.couponName}</li>
								</c:if>
								<c:forEach items="${accountCouponList}" var="accountCoupon">
									<li value="${accountCoupon.id.couponNo}">${accountCoupon.couponName}</li>
								</c:forEach>
							</ul>
						</div>
					</dd>
				</dl>
				<div class="btn-wrap">
					<div class="btn-rounded style-gray"  onclick="validCancelBidding(${userBidSetDetail.bid})">取消下標</div>
					<div class="btn-rounded" onclick="validBidding()">確認下標</div>
				</div>
				<input type="hidden" name="min" value="0">
				<br />
				<input type="hidden" id="maxBid" name="max" value="${maxBid}">
				<br />
				<input type="hidden" name="step" value="1">
			</div>
		</section>
	
		<section class="bidpopup" id="passCheckPage" style=" width: 100%;max-width: 400px;margin:0 auto;display: none;">
			<div >
				<div class="tip">
					<div id="ckTitle" class="c4_txt">確認下標</div>
				</div>
				<dl class="module-dl style-rounded">
					<dt>請輸入密碼</dt>
					<dd>
						<input type="password" id="passDCheck" value="">
					</dd>
				</dl>
				<div class="btn-wrap">
					<div class="btn-rounded style-gray" onclick="jQuery('#cboxClose').click();">取消</div>
					<div id="cfButton" class="btn-rounded" onclick="bidding()">確認</div>
				</div>
			</div>
		</section>
		
		<section class="bidpopup" id="messagePage" style=" width: 100%;max-width: 400px;margin:0 auto;display: none;">
			<div >
				<div class="tip">
					<div id="finishMsg" class="c4_txt" align="center" style="height: 100px;"></div>
				</div>
				<div class="btn-wrap">
					<div id="mpButton" class="btn-rounded" onclick="finishBid()">確認</div>
				</div>
			</div>
		</section>
		
	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/bidpopup.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/rangeslider.js" type="text/javascript"></script>
	<script>
		function validBidding(){
//			console.log("$(#bidAmount).css('display') : "+$("#bidAmount").css('display'));
//			console.log("$(#bidAmount2).css('display') : "+$("#bidAmount2").css('display'));
//			console.log("$(.module-dl.style-rounded.on-mobile).css('display') : "+$(".module-dl.style-rounded.on-mobile").css('display'));
//			console.log("$(.module-dl.style-rounded.on-desk).css('display') : "+$(".module-dl.style-rounded.on-desk").css('display'));

			var isOnMobile =($(".module-dl.style-rounded.on-desk").css('display')=="none")? true:false;
//			console.log("isOnMobile:"+isOnMobile);
			var amount = (isOnMobile)? $("#bidAmount2").val():$("#bidAmount").val();
//			console.log("amount:"+amount);

			$("body").find("#colorbox").animate({ scrollTop:( 0 )}, 300);
			if(!isNaN(amount)){
				var bid = parseInt(amount);
				var totalCredit = ${bidAccount.workSelfAccuCredit + bidAccount.workGurranCredit + bidAccount.workReturnCredit};
				var totalBid = ${userBidSetDetail.payment * (totalPeriods - 1) + bidAccount.riskCred};
				if(bid <= $("#maxBid").val()){
					if((totalBid <= totalCredit) || "${userBidSetDetail.bid}" > 0){
						$("#ckTitle").text("確認下標");
						$("#cfButton").attr("onclick","").click( function(){ bidding() });

						$("#popup_content").hide();
						$("#passCheckPage").show();
						$("#cboxContent").css("text-align","center");
						$("#cboxContent").height(280);
					}else{
						alert("剩餘額度不足，需大於" + totalBid);
					}
				}else{
					alert("下標金額不能大於當期最高下標金額");
				}
			}else{
				alert("請輸入數字");
				$("#bidAmount").focus();
			}
		}
		
		function bidding(){
			var bid = parseInt($("#bidAmount").val());
			var shelId = "${userBidSetDetail.id.shelID}";
			var currentPeriod = "${userBidSetDetail.id.periods}";
			var couponNo = $("#couponNo").val();
			var password = $("#passDCheck").val();
			$.ajax({
				type:"POST",
				url: "<c:url value='/mypage/bidding'/>",
				data:{
					shelId: shelId,
					currentPeriod: currentPeriod,
					bid: bid,
					couponNo: couponNo,
					password: password
				},
				cache:false, 
				async:false,
				success: function(backmsg){ //取得回傳訊息
					$("#passCheckPage").hide();
					$("#messagePage").show();
					if (backmsg.success){
						var guarPeriod = "${guarPeriod}";
						if(currentPeriod <= guarPeriod){
							$("#finishMsg").html("下標成功，<br>於當期得標者之會員需提供本票!!!");
							$("#cboxContent").css("text-align","center");
						}else{
							$("#finishMsg").html("下標成功");
							$("#cboxContent").css("text-align","center");
						}
					}else{
						$("#finishMsg").html(backmsg.msg);
					}
				}, 
				error: function(){
					$("#passCheckPage").hide();
					$("#messagePage").show();
					$("#finishMsg").html("未預期錯誤");
					$("#cboxContent").css("text-align","center");
				}
			});
		}
		
		function validCancelBidding(bid){
			$("body").find("#colorbox").animate({ scrollTop:( 0 )}, 300);
			if(bid > 0){
				$("#ckTitle").text("取消下標");
				$("#cfButton").attr("onclick","").click( function(){ cancelBidding() });
				$("#popup_content").hide();
				$("#passCheckPage").show();
				// $("#cboxContent").width(400);
				$("#cboxContent").css("text-align","center");
				$("#cboxContent").height(280);
			}else{
				alert("尚未下標");
			}
		}
		
		function cancelBidding(){
			$("body").find("#colorbox").animate({ scrollTop:( 0 )}, 300);
			var shelId = "${userBidSetDetail.id.shelID}";
			var currentPeriod = "${userBidSetDetail.id.periods}";
			var password = $("#passDCheck").val();
			$.ajax({
				type:"POST",
				url: "<c:url value='/mypage/cancelBidding'/>",
				data:{
					shelId: shelId,
					currentPeriod: currentPeriod,
					password: password
				},
				cache:false, 
				async:false,
				success: function(backmsg){ //取得回傳訊息
					$("#passCheckPage").hide();
					$("#messagePage").show();
					if (backmsg.success){
						$("#finishMsg").html("取消下標成功");
						$("#cboxContent").css("text-align","center");
					}else{
						$("#finishMsg").html(backmsg.msg);
					}
				}, 
				error: function(e){
					$("#passCheckPage").hide();
					$("#messagePage").show();
					$("#finishMsg").html("未預期錯誤");
					$("#cboxContent").css("text-align","center");
				}
			});
		}
		function finishBid(){
			jQuery('#cboxClose').click();
			window.top.location.href = "<c:url value='/mypage/mycombination'/>";
		}
		function blockChar(e){
//			var key = window.event ? e.keyCode : e.which;
//			if(8==key || 46==key){//8:backspace 46:delete
//				return true;
//			}
//			var keychar = String.fromCharCode(key);
//			reg = /\d/;
//			return reg.test(keychar);
		}
	</script>
