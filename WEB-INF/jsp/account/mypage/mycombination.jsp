﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/rangeslider.css" media="screen" rel="stylesheet" type="text/css" />
<style>
  .forBid,.icon-add{
    cursor: pointer !important;
  }
</style>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/bidShlvAndSell.js" type="text/javascript"></script>	
	<div class="layout-view">
		<section class="section-banner" style="height: 0;"></section>
		<!--subsection start-->
		<section class="section-proflie-banner accordion-menu">
			<!--laptop start-->
			<div class="layout-mw-wrapper">
				<jsp:include page="/WEB-INF/jsp/account/mypage/sbheader.jsp"></jsp:include>
				<!--laptop end-->
			</div>
		</section>
		<!--subsection end-->
		<section class="section-content">
			<div class="layout-mw-wrapper">
				<!--PAGE CONTENT START-->
				<div class="profile-item-title">
					<h2 class="profile-scene-title">已開始</h2>
					<i class="profile-icon-underline"></i>
				</div>
				<div class="cb-products" style="padding-top: 60px;">
					<ul class="products">
						<c:forEach var="joiningBid" items="${joiningBidList}" varStatus="status">
							<!-- PER ITEM -->
							<li id='${joiningBid.shelID}li' />
							<script type="text/javascript">							
								var bidDate = "";
								<c:forEach var="bidSetDetail" items="${bidSetDetailList}" varStatus="status">
									<c:if test="${joiningBid.shelID eq bidSetDetail.id.shelID}">
										bidDate = "${bidSetDetail.bidDate}";										
									</c:if>
								</c:forEach>
								<c:choose>
									<c:when test="${shelIdMap.get(joiningBid.shelID).substring(0, 1) eq 0}">
										showBidshlvForFullOrStart("${joiningBid.shelID}li", "${joiningBid.shelID}", "${joiningBid.crClass}", "${joiningBid.bidTermM}","${joiningBid.bidTermC}","${joiningBid.bidIntendNumber}","${joiningBid.bidFulMember}", "", bidDate, "${shelIdMap.get(joiningBid.shelID).substring(0, 1)}", "${shelIdMap.get(joiningBid.shelID).substring(1)}", "${pageContext.request.contextPath}/product/detail?detail?from=join&shelId=${joiningBid.shelID}", "", "");
									</c:when>
									<c:otherwise>
										showBidshlvForFullOrStart("${joiningBid.shelID}li", "${joiningBid.shelID}", "${joiningBid.crClass}", "${joiningBid.bidTermM}","${joiningBid.bidTermC}","${joiningBid.bidIntendNumber}","${joiningBid.bidFulMember}", "", bidDate, "${shelIdMap.get(joiningBid.shelID).substring(0, 1)}", "${shelIdMap.get(joiningBid.shelID).substring(1)}", "${pageContext.request.contextPath}/product/detail?detail?from=join&shelId=${joiningBid.shelID}", "checkAllowBid(${joiningBid.shelID}, ${allowBidMap.get(joiningBid.shelID)}, ${shelIdMap.get(joiningBid.shelID).substring(1) eq joiningBid.bidTermC})", '<i class=\"forBid\" id=\"forBid${joiningBid.shelID}\" href=\"${pageContext.request.contextPath}/mypage/biddingPage?shelId=${joiningBid.shelID}&currentPeriod=${shelIdMap.get(joiningBid.shelID).substring(1)}\" style=\"display: none;\"></i>');
									</c:otherwise>
								</c:choose>																
							</script>																			
							<!--END OF PER ITEM -->						
						</c:forEach>
					</ul>
				</div>
				<div class="profile-item-title">
					<h2 class="profile-scene-title">募集中</h2>
					<i class="profile-icon-underline"></i>
				</div>
				<div class="cb-products" style="padding-top: 60px;">
					<ul class="products">
						<c:forEach var="intendBidShlv" items="${intendBidShlvList}" varStatus="status">
							<li id="intend${intendBidShlv.shelID}" />							
							<script type="text/javascript">
								showBidshlv("intend${intendBidShlv.shelID}", "${intendBidShlv.shelID}", "${intendBidShlv.crClass}", "${intendBidShlv.bidTermM}","${intendBidShlv.bidTermC}","${intendBidShlv.bidIntendNumber}","${intendBidShlv.bidFulMember}", "", "", 2, "${pageContext.request.contextPath}/product/detail?from=join&shelId=${intendBidShlv.shelID}", "abortBid(${intendBidShlv.shelID}, ${intendBidShlv.bidTermM})");									
							</script>
						</c:forEach>
					</ul>
				</div>
				<div class="profile-item-title">
					<h2 class="profile-scene-title">已結束</h2>
					<i class="profile-icon-underline"></i>
				</div>
				<div class="cb-products" style="padding-top: 60px;">
					<ul class="products">
						<c:forEach var="finishBid" items="${finishBidList}" varStatus="status">
							<!-- PER ITEM -->							
							<li id="finish${finishBid.shelID}" />
							<script type="text/javascript">
								showBidshlvForFullOrStart("finish${finishBid.shelID}", "${finishBid.shelID}", "${finishBid.crClass}", "${finishBid.bidTermM}","${finishBid.bidTermC}","${finishBid.bidIntendNumber}","${finishBid.bidFulMember}","Y","", 4, "${finishBid.bidTermC}", "${pageContext.request.contextPath}/product/detail?from=join&shelId=${finishBid.shelID}", "", "");									
							</script>
							<!--END OF PER ITEM -->
						</c:forEach>
					</ul>
				</div>
				<!--PAGE END-->


<!-- TEST START -->
<div class="profile-item-title">
  <h2 class="profile-scene-title">TEST</h2>
  <i class="profile-icon-underline"></i>
</div>
<div class="cb-products clearfix" style="padding-top: 60px;">
  <ul class="products">
    <!--TEST ITEM --> 
    <li id="1609299512li">
      <div class="module-product-box style-start sale-status-notbid">
        <div class="module-product-inbox">  
          <div class="module-product-innerbox">   
            <div class="module-product-innerbox-top style-B">     
              <span class="module-product-serialnumber">TEST
              </span>     
              <span class="module-product-class">TEST
              </span>    
            </div>    
            <div class="module-product-innerbox-bottom">      
              <p>每期
                <span class="dollarsign">NT$
                </span>
                <b>1,000
                </b>
              </p>      
              <p>第2期&nbsp;/共4期
              </p>    
            </div>    
            <div class="module-product-bar-containter">     
              <div class="module-product-bar-valueimg" style="width: 100%;">
              </div>      
              <div class="module-product-bar-number">已募集4人/共4人
              </div>    
            </div>    
            <div class="module-product-innerbox-cover">     
              <a href="/TFEFrontend/product/detail?detail?from=join&amp;shelId=1609299512">
                <i class="fa fa-search">
                </i>查看詳細
              </a>   
            </div>  
          </div>
        </div>
        <i class="icon-add for-test" onclick=" //$.colorbox({html:'<h1>TEST</h1>'}); //checkAllowBid(1609299512, 1, false)">
        </i>
        <i class="forBid cboxElement" id="forBid1609299512" href="/TFEFrontend/mypage/biddingPage?shelId=1609299512&amp;currentPeriod=2" style="display: none;">
        </i>
        <br>
        <p align="center">開標日:2016 / 12 / 17
        </p>
      </div>                                      
    </li>
    <!--END OF TEST ITEM -->           
  </ul>
</div>
<!-- TEST END -->

			</div>
		</section>

		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
	</div>
	
	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js" type="text/javascript">
	
	</script>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/bidpopup.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/rangeslider.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js" type="text/javascript"></script> --%>
	<script>
  $("body").on('click', '.forBid', function(event) {
    // event.preventDefault();
    /* Act on the event */
    // alert("clicking .forBid!!");
  });

  $("body").on('click', '.icon-add', function(event) {
    // event.preventDefault();
    /* Act on the event */
    // alert("clicking .icon-add!!");
  });


    $(".module-product-box.style-sale:not('.sale-status-purchased') .forBid, .module-product-box.style-start:not('.sale-status-purchased') .forBid ").colorbox({ });


		$(".for-test").colorbox({
      html:'<div style="padding: 40px 120px;"><img src="/TFEFrontend/resource/assets/images/loading.gif"></img></div>',
      onComplete: function(){
        // alert("load popup complete");
        checkAllowBid(1609299512, 1, false)
      }
    });
 		
		$('#link_mycombination').addClass("is-active");
		$(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
		
		function checkAllowBid(shelId, allowBid, isLastPeriod){
			console.log(isLastPeriod);
			if(allowBid == 0){
				alert("親愛的會員您好，您的標會款項尚未繳清，\n請儘速繳納，以免影響到您下標的權益，謝謝。");
			}else if(isLastPeriod){
				alert("親愛的會員您好，您的標會組合為最後一期，\n所以不需下標，謝謝!!!");
			}else{
				jQuery("#forBid" + shelId).click(); 
        console.log("請修正此處：詳見此行下方註解內容");

//請修正此處：目前貴公司針對此功能串接的做法為：
//
//-->1.貴司將敝公司切版所提供的錘子的icon（<i class="icon-add">）行內註冊onclick事件
//-->2.使用者點擊上述的錘子icon（<i class="icon-add" onclick="checkAllowBid(ID, 1, false)"></i>）時，執行點擊事件做檢查---->
//-->3.checkAllowBid此函式檢查通過後、使用jQuery語法(此處)間接執行一次一個貴公司所新增的隱藏元件"#forBid" + shelId這一個<i>元件的點擊事件，並藉此連結至下標popup畫面的href進而開啟下標畫面。
//
//但因iOS安全性限制，間接點擊的開啟手法並不盡然適用於手機上，請修改此處成：只有一個<i>元件，且不使用display:none的狀況下，在同一個元件直接點擊並做判斷開啟與否。（即不要使用任何.click();來進行任何的事件初始化。）
//
//Thanks!

			}
		}
		
		function abortBid(shelId, payment){
			$.ajax({
				type:"POST",
				url: "<c:url value='/mypage/abortBid'/>",
				data:{
					shelId: shelId,
					payment: payment,
				},
				cache:false,
				async:false,
				success: function(backmsg){ //取得回傳訊息
					if (backmsg.success){
						$("#intend"+shelId).hide();
						if(backmsg.bidStatus == 1){
							alert("為維護其他會員權益，標會組合僅提供３次退出權利，超過３次則不得再加入此標會組合。");
						}else if(backmsg.bidStatus == 2){
							alert("您已退出此標會組合２次，剩餘１次。");
						}else if(backmsg.bidStatus == 3){
							alert("您已退出此標會組合３次，不得再加入，請參考其他標會組合。");
						}
					}
				}, 
				error: function(){
				}
			});
		}
	</script>
