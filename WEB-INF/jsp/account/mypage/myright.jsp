﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="Title" content="TFE" />
<meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
<meta name="Description" content="Description" />
<!-- iOS -->
<!-- Android -->
<meta property="og:title" content="Hconnect Mobile" />
<meta property="og:type" content="website" />
<meta property="og:url" content="" />
<meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/share.jpg" />
<meta property="og:site_name" content="Hconnect" />
<meta property="og:description" content="Description" />
<meta property="og:locality" content="Taipei" />
<meta property="og:country-name" content="Taiwan" />
<link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
<title>TFE</title>
<script type="text/javascript">
	document.createElement('header');
	document.createElement('nav');
	document.createElement('menu');
	document.createElement('section');
	document.createElement('article');
	document.createElement('aside');
	document.createElement('footer');
	
	$(document).ready(function() {
		$('td').each(function() {
		    var cellvalue = $(this).html();
		    var str = cellvalue.indexOf("-");
		    if (str > -1) {
		        $(this).css("color","red");
		    }
		});                     
	});	
</script>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
	type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="layout-view">
		<section class="section-banner" style="height: 0;"></section>
		<!--subsection start-->
		<section class="section-proflie-banner accordion-menu">
			<!--laptop start-->
			<div class="layout-mw-wrapper">
				<jsp:include page="/WEB-INF/jsp/account/mypage/sbheader.jsp"></jsp:include>
				<!--laptop end-->
			</div>
		</section>
		<!--subsection end-->
		<section class="section-content">
			<div class="layout-mw-wrapper">
				<!--PAGE CONTENT START-->
				<section class="section-btnwrap">
					<a href="${pageContext.request.contextPath}/mypage/myrightInvoice">
				    	<span class="btn-rounded22">電子發票</span> 
				    </a>
					<a href="${pageContext.request.contextPath}/mypage/myrightWithdraw">
						<span class="btn-rounded22">取款</span>
					</a>
					<a href="${pageContext.request.contextPath}/mypage/myrightEstatement">
						<span class="btn-rounded22">對帳單查詢</span>
					</a>
				</section>
				<!--content start-->
				<div class="corner2">
					<table width="100%" border="0" class="pt3">
						<caption>額度資訊</caption>
						<tr>
							<td width="50%">自然額度</td>
							<td>
								${memberOfRightVO.workSelfAccuCredit}
							</td>
						</tr>
						<tr>
							<td>剩餘信用額度</td>
							<td>
								${memberOfRightVO.workGurranCredit}
							</td>
						</tr>
						<tr>
							<td>剩餘額度</td>
							<td>
								${memberOfRightVO.credit}
							</td>
						</tr>
						<tr>
							<th colspan="2">權益資訊</th>
						</tr>
						<tr>
							<td>帳戶價值</td>
							<td>
								${memberOfRightVO.amount}
							</td>
						</tr>
						<!--
						<tr>
							<td>投入金額</td>
							<td>
								${memberOfRightVO.inCome}
							</td>
						</tr>
						<tr>
							<td>未實現投資利息</td>
							<td>
								${memberOfRightVO.unrealizedinterest}
							</td>
						</tr>
						<tr>
							<td>已實現投資利息</td>
							<td>
								${memberOfRightVO.realizedinterest}
							</td>
						</tr>
						<tr>
							<td>未實現投資損失</td>
							<td>
								${memberOfRightVO.unrealizedLost}
							</td>
						</tr>
						<tr>
							<td>已實現投資損失</td>
							<td>
								${memberOfRightVO.realizedLost}
							</td>
						</tr>
						-->
						<tr>
							<td>累積得標金額</td>
							<td>
								${memberOfRightVO.accumulateCash}
							</td>
						</tr>
						<tr>
							<td>待還款金額</td>
							<td>
								${memberOfRightVO.repayment}
							</td>
						</tr>
						<tr>
							<th colspan="2">組合資訊</th>
						</tr>
						<tr>
							<td>已完成組合數</td>
							<td>${memberOfRightVO.joinToFinish}</td>
						</tr>
						<tr>
							<td>參加中組合數</td>
							<td>${memberOfRightVO.joinBid}</td>
						</tr>
						<!--
						<tr>
							<td>您已幫助了</td>
							<td>
								${memberOfRightVO.joinToHelp}
							</td>
						</tr>
						-->
					</table>
				</div>
				<!--content end-->
				<!--PAGE END-->
			</div>
		</section>
		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
	</div>
	<script>
		$('#link_myright').addClass("is-active");
		$(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
	</script>
</body>
</html>