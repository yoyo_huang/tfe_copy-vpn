<%-- <%@ include file="/resource/common/jsp/changeDiv.jsp"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-tw">

<body>
  <div class="layout-container">

    <div class="layout-view">
      <section class="section-banner" style="height: 0;"></section>
      <!--subsection start-->
      <!--subsection end-->
      <section class="section-content">
        <div class="layout-mw-wrapper">


          <!-- 尚無頁面內容 -->
          <div class="login-form-wrap">
            <h2 class="title-large">會員登入</h2>

            <form id="loginForm" action="/TFEFrontend/userLogin" method="post">
              <div class="form-block">
                <label class="module-form-label" for="identityid">身分證字號</label>
                <input type="text" id="identityid" name="identityid" class="module-form-input" placeholder="" maxlength="10">
              </div>
              <div class="form-block">
                <label class="module-form-label" for="account">會員代號</label>
                <input type="text" id="loginAccount" name="loginAccount" class="module-form-input" placeholder="" maxlength="16">
              </div>
              <div class="form-block">
                <label class="module-form-label" for="password">使用者密碼</label>
                <input type="password" id="password" name="password" class="module-form-input" placeholder="" maxlength="12">
              </div>
              <div class="form-block style-verifycode">
                <label for="">驗證碼</label>
                <span class="verifycode-wrap">
                  <input type="text" id="verifycode" name="verifycode" class="module-form-input style-rounded" placeholder="輸入驗證碼">
                  <img id="verifyImg" src="/TFEFrontend/userLogin/getCaptchaImage?1480664353120" alt="">
                  <i class="fa fa-refresh" onclick="replaceVerifyCode('login')"></i>
                </span>
              </div>
              <p id="loginErr" class="form-warning-text" style="display: none;">您輸入的資料有誤，請重新確認。</p>
              <label class="module-checkbox">
                <input type="checkbox" id="rememberme" name="rememberme">
                <i class="module-symbol"></i>
                <span>記住身分證與會員代號</span>
              </label>
              <div class="btn-wrap style-center">
                <div id="loginBtn" class="btn-rounded" onclick="goTFESubmit()">登入</div>
              </div>
              <div class="btn-wrap style-center">
              <a class="style-normal" href="/TFEFrontend/pwdrecovery" onclick="clearInput();clearErrorMsg();replaceVerifyCode('pw');">忘記密碼？</a><br/><br/>
                <a class="style-normal" href="/TFEFrontend/idpwdrecovery" onclick="clearInput();clearErrorMsg();">忘記帳號與密碼？</a>
              </div>
            </form>


          </div>



        </div>
        <!--PAGE END-->
      </section>
    </div>
    <img
    src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg"
    width="100%" style="margin-top: -1px;" alt="">
  </div>
</div>

</body>
</html>