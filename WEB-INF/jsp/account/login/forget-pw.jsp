<%-- <%@ include file="/resource/common/jsp/changeDiv.jsp"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-tw">

<body>
    <div class="layout-container">

        <div class="layout-view">
            <section class="section-banner" style="height: 0;"></section>
            <!--subsection start-->
            <!--subsection end-->
            <section class="section-content">
                <div class="layout-mw-wrapper">


                  <div class="login-form-wrap">
                    <h2 class="title-large">忘記密碼</h2>

                    <form action="">
                      <div class="form-block">
                        <label class="module-form-label" for="identityid">身分證字號</label>
                        <input type="text" id="pwIdentityid" name="identityid" class="module-form-input" placeholder="" maxlength="10">
                      </div>
                      <div class="form-block">
                        <label class="module-form-label" for="account">會員代號</label>
                        <input type="text" id="pwAccount" name="pwAccount" class="module-form-input" placeholder="" maxlength="16">
                      </div>
                      <div class="form-block style-verifycode">
                        <label for="">驗證碼</label>
                        <span class="verifycode-wrap">
                          <input type="text" id="pwVerifycode" name="pwVerifycode" class="module-form-input style-rounded" placeholder="輸入驗證碼">
                          <img id="pwVerifyImg" src="/TFEFrontend/userLogin/getCaptchaImage?1480665510165" alt="">
                          <i class="fa fa-refresh" onclick="replaceVerifyCode('pw')"></i>
                        </span>
                      </div>
                      <p id="findPassErr" class="form-warning-text" style="display: none;">您輸入的資料有誤，請重新確認。</p>
                      <div class="btn-wrap">
                        <div class="btn-rounded" onclick="findPass()">找回密碼</div>
                        <a class="style-normal" href="/TFEFrontend/Servicelogin" onclick="clearInput();clearErrorMsg();replaceVerifyCode('login');">返回會員登入</a>
                      </div>
                    </form>

                  </div>



            </div>
            <!--PAGE END-->
        </section>
    </div>
    <img
    src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg"
    width="100%" style="margin-top: -1px;" alt="">
</div>
</div>

</body>
</html>