<%-- <%@ include file="/resource/common/jsp/changeDiv.jsp"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-tw">

<body>
  <div class="layout-container">

    <div class="layout-view">
      <section class="section-banner" style="height: 0;"></section>
      <!--subsection start-->
      <!--subsection end-->
      <section class="section-content">
        <div class="layout-mw-wrapper">



          <div class="login-form-wrap">
            <h2 class="title-large">忘記帳號密碼</h2>

            <p>請來電洽詢台灣資金交易所小組<br>
              <tel>0800-800-917</tel><br>
              客服人員將依據您註冊所提供的資料進行身分核對做後續處理。
            </p>

          </div>


        </div>
        <!--PAGE END-->
      </section>
    </div>
    <img
    src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg"
    width="100%" style="margin-top: -1px;" alt="">
  </div>
</div>

</body>
</html>