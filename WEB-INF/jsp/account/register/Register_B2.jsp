<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link
        href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
        media="screen" rel="stylesheet" type="text/css"/>
<link
        href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css"
        media="screen" rel="stylesheet" type="text/css"/>
<link
        href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css"
        media="screen" rel="stylesheet" type="text/css"/>
<link
        href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/member.css"
        media="screen" rel="stylesheet" type="text/css"/>
<div class="layout-container">
    <div class="layout-view">
        <section class="section-banner" style="height: 0;"></section>
        <!--subsection start-->
        <!--subsection end-->

        <section class="section-content">
            <div class="layout-mw-wrapper">
                <!--PAGE CONTENT START-->
                <div class="register-content">
                    <div class="register">
                        <script type="text/javascript"
                                src="${pageContext.request.contextPath}/resource/common/js/jquery-1.11.3.min.js"></script>
                        <script type="text/javascript">


                            function registerAgainMail() {

                                //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
                                var mail = $("#mail").val();
                                ShowProgressBar();
                                $.ajax({
                                    type: "POST",
                                    url: "${pageContext.request.contextPath}/resendAuthMail?"
                                    + "email=" + mail,
                                    //data: JSON.stringify({
                                    //account: value,
                                    //}),
                                    dataType: "json",
                                    contentType: "application/json; charset=UTF-8",
                                    cache: false,
                                    async: false,
                                    success: function (backmsg) { //取得回傳訊息
                                        //console.log(backmsg);
                                        //alert(backmsg);//0:發送失敗 1:成功發送
                                        if (backmsg.rs == true) {
                                            alert("成功發送 ");
                                            HideProgressBar();
                                            //return true;
                                        } else {
                                            alert("發送失敗");
                                            HideProgressBar();
                                        }
                                        //return false;
                                    },
                                    error: function () {
                                        alert("發送失敗");
                                        HideProgressBar();
                                        return false;
                                    }
                                });
                                return false;
                            }
                            ;

                            // 顯示讀取遮罩
                            function ShowProgressBar() {
                                displayProgress();
                                displayMaskFrame();
                            }

                            // 隱藏讀取遮罩
                            function HideProgressBar() {
                                var progress = $('#divProgress');
                                var maskFrame = $("#divMaskFrame");
                                progress.hide();
                                maskFrame.hide();
                            }
                            // 顯示讀取畫面
                            function displayProgress() {
                                var w = $(document).width();
                                var h = $(window).height();
                                var progress = $('#divProgress');
                                progress.css({
                                    "z-index": 999999,
                                    "top": (h / 2) - (progress.height() / 2),
                                    "left": (w / 2) - (progress.width() / 2)
                                });
                                progress.show();
                            }
                            // 顯示遮罩畫面
                            function displayMaskFrame() {
                                var w = $(window).width();
                                var h = $(document).height();
                                var maskFrame = $("#divMaskFrame");
                                maskFrame.css({"z-index": 999998, "opacity": 0.7, "width": w, "height": h});
                                maskFrame.show();
                            }

                        </script>
                        <div class="email_Verify">
                            <h1>我們已收到您的資料，請您至電子郵件信箱收取認證信，並點選認證網址，謝謝。</h1>
                            <h2>仍未收到認證信嗎？ 請再輸入您的電子郵件信箱，我們會再次寄送驗證信至您的信箱</h2>
                            <div class="email_style">
                                <dl class="email clearfix" style="margin-bottom: 0.5em;">
                                    <dt>電子信箱</dt>
                                    <dd>
                                        <input type="text" id="mail" name="mail" value="" class="email_b">
                                    </dd>
                                </dl>

                                <div class="clearfix">
                                    <div class="email_button m_btn">
                                        <a onclick="registerAgainMail();">寄送驗證信</a>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <!--PAGE END-->
            </div>
        </section>

    </div>
</div>


<div id="divProgressLink" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;">

</div>
<div id="divMaskFrameLink" style="background-color: #F2F4F7; display: none; left: 0px;
    position: absolute; top: 0px;">
</div>

<div id="divProgress" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;">
    <img id="img-loading" src="${pageContext.request.contextPath}/resource/assets/images/loading.gif"/>
    <br/>
    <font color="#1B3563" size="2px">資料處理中</font>
    <div class="nav_rate">
        <div class="nav_rate2 nav5">&nbsp;</div>
    </div>
</div>
<div id="divMaskFrame" style="background-color: #F2F4F7; display: none; left: 0px;
    position: absolute; top: 0px;">
</div>

<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js"
        type="text/javascript"></script>
