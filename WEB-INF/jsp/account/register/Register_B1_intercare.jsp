<%-- <%@ include file="/resource/common/jsp/changeDiv.jsp"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-tw">

<body>
<div class="layout-container">

    <div class="layout-view">
        <section class="section-banner" style="height: 0;"></section>
        <!--subsection start-->
        <!--subsection end-->
        <section class="section-content">
            <div class="layout-mw-wrapper">
                <center>
                    <table width="0" border="0" align="center">
                        <tr>
                            <td><img src="${pageContext.request.contextPath}/resource/assets/images/Register_B1_02.gif"
                                     alt="" width="400" height="100"></td>
                        </tr>
                    </table>
                </center>
                <form id="user_form" class="user_form formular" method="post"
                      action="${pageContext.request.contextPath}/registerUserForIntercare">
                    <!--PAGE CONTENT START-->

                    <div class="register-content">
                        <div class="register">
                            <div class="register_t2 ">
                                <div class="register_column2">
                                    <h1>首次登入請設定您的會員代號及密碼，並確認您的手機號碼</h1>
                                    <input type="hidden" id="FBID" value='' name="FBID">
                                    <input id="reference" name="reference" type="hidden" value="${param.Reference}">
                                    <dl class="clearfix w1">
                                        <dt>身分證字號</dt>
                                        <dd>
                                            <input
                                                    class="text-input "
                                                    id="uid" name="uid" maxlength="10" type="text"
                                                    value="${account.identification}"
                                                    autocomplete="off" readonly>
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>

                                    <dl class="clearfix w1">
                                        <dt>會員代號</dt>
                                        <dd>
                                            <input id="account" name="account"
                                                   class="text-input validate[required,funcCall[checkAccountSize],funcCall[checkAccount]]"
                                                   type="text" value="${account.userId}" autocomplete="off"
                                                   maxlength="16">
                                        </dd>
                                    </dl>
                                    <div class="pass_tip">※ 會員代號須為英數字(1.長度為6~16碼 2. 請區分英文字大寫或小寫 3.例如：david1234)</div>
                                    <div class="clear"></div>

                                    <dl class="clearfix w1">
                                        <dt>密碼</dt>
                                        <dd>
                                            <input type="password" value="" id="pwd" name="password" placeholder="會員密碼"
                                                   class="text-input validate[funcCall[checkPasswordSize],funcCall[checkPasswordRule],${(account.accountPurpose==3)? "required,":"" }funcCall[checkPassword]]"
                                                   maxlength="10" autocomplete="off">
                                        </dd>
                                    </dl>
                                    <div class="pass_tip">※ 密碼需由英文、數字、特殊符號組成(1.介於6到10個字元2.不可與會員代號相同. 3.例如:@bcd1234)
                                    </div>
                                    <div class="clear"></div>

                                    <dl class="clearfix w1">
                                        <dt>確認密碼</dt>
                                        <dd>
                                            <input id="password2" name="password2" type="password" placeholder="再次確認密碼"
                                                   value="" class="text-input validate[${(account.accountPurpose==3)? "required,":"" }equals[pwd]]"
                                                   maxlength="10" autocomplete="off">
                                        </dd>
                                    </dl>
                                    <div class="pass_tip">※ 請輸入相同密碼</div>
                                    <div class="clear"></div>

                                    <dl class="clearfix w1">
                                        <dt>手機號碼</dt>
                                        <dd>
                                            <input type="text" id="phone" name="phone" placeholder="手機號碼"
                                                   value="${(account.mobile=="0965175995")? '':account.mobile}"
                                                   class="text-input validate[required,custom[phone],funcCall[checkMoblie]]">
                                        </dd>
                                    </dl>
                                    <div class="clear"></div>

                                </div>
                            </div>
                            <%--<div class="register_t2 " style="display: none;"></div>--%>

                            <div class="register_t2 ">
                                <div class="statement">
                                    <p>TEST</p>
                                    <div class="btn-wrap re_btn clearfix">
                                        <input id="sendButton" class="news_button" type="submit" value="確定"/>
                                    </div>


                                </div>
                            </div>
                        </div>


                    </div>
                </form>
            </div>
            <!--PAGE END-->
        </section>
    </div>
    <img
            src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg"
            width="100%" style="margin-top: -1px;" alt="">
</div>
</div>

<link
        href="${pageContext.request.contextPath}/resource/assets/images/favicon.png"
        rel="shortcut icon" type="image/x-icon"/>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/index.css" media="screen"
      rel="stylesheet" type="text/css"/>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/member.css" media="screen"
      rel="stylesheet" type="text/css"/>

<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js"
        type="text/javascript"></script>
<script

        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js"
        type="text/javascript"></script>

<!-- 註冊 -->
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resource/common/js/jquery-1.11.3.min.js"></script>
<link rel="stylesheet"
      href="${pageContext.request.contextPath}/resource/common/css/validationEngine.jquery.css"/>
<script
        src="${pageContext.request.contextPath}/resource/common/js/jquery.validationEngine-zh_CN.js"></script>
<script
        src="${pageContext.request.contextPath}/resource/common/js/jquery.validationEngine.js"></script>
<script
        src="${pageContext.request.contextPath}/resource/common/js/birthday.js"></script>
<script
        src="${pageContext.request.contextPath}/resource/common/js/aj-address.js"></script>
<script type="text/javascript">

    $('#user_form').validationEngine({
        validationEventTriggers: "blur", //触发的事件  validationEventTriggers:"keyup blur",
        inlineValidation: true,//是否即时验证，false为提交表单时验证,默认true
        success: false,//为true时即使有不符合的也提交表单,false表示只有全部通过验证了才能提交表单,默认false
        promptPosition: "topRight",//提示所在的位置，topLeft, topRight, bottomLeft,  centerRight, bottomRight
        scroll: false,
        //ajax
        ajaxFormValidation: true,
        ajaxFormValidationMethod: 'post',
        onAjaxFormComplete: ajaxValidationCallback,
        onBeforeAjaxFormValidation: beforeCall,
        custom_error_messages: {
            // Custom Error Messages for Validation Types
            '#user': {
                'custom[chinese]': {
                    'message': '姓名格式錯誤'
                },
            },
            '#checkbox1': {
                'required': {
                    'message': '選項服務條款及隱私權政策未同意無法進行後續作業 '
                }
            },
        },

    });

    //This method is called right before the ajax form validation request
    //it is typically used to setup some visuals ("Please wait...");
    //you may return a false to stop the request
    function beforeCall(form, options) {
        ShowProgressBar();
        if (window.console) {
            //console.log('Right before the AJAX form validation call');
        }
        ;
        return true;
    }
    ;

    //Called once the server replies to the ajax form validation request
    function ajaxValidationCallback(status, form, json, options) {
        //console.log(status);
        //console.log(form);
        //console.log(json);
        //console.log(options);
        if (window.console) {
            //console.log(status);
        }
        ;

        if (status === true) {
            //alert('the form is valid!');
            if (json.rs == 1) {
                console.log(json);

                HideProgressBar();

                showResultColorBox();
                <%--window.location.href = "${pageContext.request.contextPath}" + json.view;// 前往電話認證--%>

//                $(".register_t2 ").hide();
            } else if (json.rs == 2) {
                alert("會員資料設定成功");
                location.replace("<c:url value='/'/>"+json.view);
            } else {
                alert("會員資料設定失敗");
            }
        } else {
            alert("會員資料設定時發生未預期錯誤");
        }
        HideProgressBar();

    }
    ;

    function showResultColorBox() {
        $.colorbox({
            inline: true,
            onCleanup: function () {
//                alert("trigger onCleanup");
                location.replace("<c:url value='/logout'/>");
            },
            href: "#popup-updateOK"
        });
    }
    function showDiv() {
        //alert("show");
//        $(".register_t2 ").show();
        var mobileStatus = "${account.mobileStatus}";
        if (mobileStatus == "3") { // 3.重新認證
//            console.log($.colorbox);
            if($.colorbox){
                showResultColorBox();
                clearInterval(myShowDiv);
            }else{
                console.log("go setInterval"+Date());
            }
        }

    }

    function checkPasswordSize(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        if (value.length < 1) {
            return false;
        } else if (value.length >= 1 && value.length < 6) {
            return "密碼為6~10個字元";
        } else if (value.length > 12) {
            return "密碼為6~10個字元";
        }
        return false;
    }

    function checkPasswordRule(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        var accountValue = $("#account").val();
        if (value != accountValue) {
            return false;
        } else {
            return "不可與會員代號相同";
        }

        return false;
    }

    function checkPassword(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var password = field.val();
        if (password.length < 1) {
            return false;
        }
        //alert("password:"+password);
        //利用match函數去比較密碼是否符合指定條件：最少一個數字，最少一個小階英文，最少一個大階英文，長度限制為10-100。
//        var chkPwdStength = password.length;
//        var maxSize = 12;
//        var checkVal = passwordGrade(password);
        //console.log("checkVal:"+checkVal);
        //var size = (chkPwdStength / maxSize) * 100;
//        var size = checkVal;
//        if (size > 99) {
//            size = 99;
//        } else if (size < 0) {
//            size = 0;
//        }

//        if (size < 40) {
//            $(".pass_s3").text("弱");
//            //$(".pass_rate2").css('background',"#FF0000");
//        } else if (size < 70) {
//            $(".pass_s3").text("中");
//            //$(".pass_rate2").css('background',"#FFFF00");
//        } else if (size >= 70) {
//
//            $(".pass_s3").text("強");
//            //$(".pass_rate2").css('background',"#70CBFF");
//        }
//
//        $(".pass_rate2").css('width', size + "%");

        return true;
    }

    function checkPasswordValue(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        //alert("password:"+$("#password").val());
        pwValue = $("#pwd2").val();
        var value = field.val();
        if (value == pwValue) {
            return false;
        } else {
            return "兩次輸入密碼不一致";
        }

    }

    function checkAccountSize(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        if (value.length < 1) {
            return false;
        } else if (value.length >= 1 && value.length < 6) {
            return "代號為6~16個字元";
        } else if (value.length > 16) {
            return "代號為6~16個字元";
        }
        return false;
    }

    function checkAccount(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        if (value.length < 1) {
            $(".tip").html("");
            return false;
        }

//        if (value.length < 1) {
//            $(".tip").html("");
//            return false;
//        } else if (value.length >= 1 && value.length < 6) {
//            $(".tip").html("");
//            return false;
//        } else if (value.length > 10) {
//            $(".tip").html("");
//            return false;
//        }

        if($("#uid").val()==value){
           return "身份證字號與會員代號欄位的值不可重複。";
        }

        <c:if test="${account.accountPurpose==3}">
        var origUserId ="${account.userId}";
        if(origUserId==value){
            return "會員代號欄位需做修正。";
        }
        </c:if>


        var accountVar = false;
        $.ajax({
            type: "POST",
            url: "${pageContext.request.contextPath}/register/checkUserId?"
            + "userId=" + value + "&identification=" + $("#uid").val(),
            //data: JSON.stringify({
            //account: value,
            //}),
            dataType: "json",
            contentType: "application/json; charset=UTF-8",
            cache: false,
            async: false,
            success: function (backmsg) { //取得回傳訊息
                //console.log(backmsg);
                //alert(backmsg);
                if (backmsg.rs == true) {
                    //alert("新增成功" );
                    $(".tip")
                            .html(
                                    "<img src='${pageContext.request.contextPath}/resource/assets/images/tip_ok.jpg' >此帳號可使用!");
                    return;
                } else {
                    $(".tip")
                            .html(
                                    "<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");
                    accountVar = true;
                }

            }
        });

        if (accountVar) {
            return options.allrules.checkAccount.alertText;
        }
        return false;
    }
    ;



</script>

<script>

    function goSuccess() {
        location.replace("<c:url value='/logout'/>");
    }

    function resendSMS() {
        $.ajax({
            type: "POST",
            url: "<c:url value='/resendSMS4Intercare'/>",
            data: {
                verifycode: "D45477"
            },
            cache: false,
            async: false,
            success: function (backmsg) { //取得回傳訊息
                if (backmsg == "帳密錯誤") {
                    $("#identityid").parent().addClass("is-error");
                    $("#loginAccount").parent().addClass("is-error");
                    $("#password").parent().addClass("is-error");
                    $("#loginErr").html("您輸入的資料有誤，請重新確認。");
                    $("#loginErr").show();
                } else if (backmsg == "圖形驗證碼錯誤") {
                    $("#verifycode").parent().parent().addClass("is-error");
                    $("#loginErr").html("您輸入的驗證碼有誤，請重新確認。");
                    $("#loginErr").show();
                } else if (backmsg == "信箱尚未認證") {
                    $("#mailCheck").show();
                } else if (backmsg == "手機尚未認證") {
                    $("#phoneCheck").show();
                } else {
                    alert("發送成功! ");
                    location.replace("<c:url value='/logout'/>");
                }
            },
            error: function () {
                $("#identityid").parent().addClass("is-error");
                $("#loginAccount").parent().addClass("is-error");
                $("#password").parent().addClass("is-error");
                $("#verifycode").parent().parent().addClass("is-error");
                $("#loginErr").html("未預期錯誤");
                $("#loginErr").show();
            }
        });
        $.colorbox.resize();
    }

    // 顯示讀取遮罩
    function ShowProgressBar() {
        displayProgress();
        displayMaskFrame();
    }

    // 隱藏讀取遮罩
    function HideProgressBar() {
        var progress = $('#divProgress');
        var maskFrame = $("#divMaskFrame");
        progress.hide();
        maskFrame.hide();
    }
    // 顯示讀取畫面
    function displayProgress() {
        var w = $(document).width();
        var h = $(window).height();
        var progress = $('#divProgress');
        progress.css({
            "z-index": 999999,
            "top": (h / 2) - (progress.height() / 2),
            "left": (w / 2) - (progress.width() / 2)
        });
        progress.show();
    }
    // 顯示遮罩畫面
    function displayMaskFrame() {
        var w = $(window).width();
        var h = $(document).height();
        var maskFrame = $("#divMaskFrame");
        maskFrame.css({"z-index": 999998, "opacity": 0.7, "width": w, "height": h});
        maskFrame.show();
    }

    function checkMoblie(field, rules, i, options) {

        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var value = field.val();
        var value2 = '${sysStatus}';
        console.log("value2:" + value2);
        if (value2 == 0) {
            return false;
        }
        //if(originalMoblie != value){
        var moblieVar = false;
        console.log("value2:" + value2);

        $.ajax({
            type: "POST",
            url: "${pageContext.request.contextPath}/register/checkMoblie?"
            + "moblie=" + value,
            //data: JSON.stringify({
            //account: value,
            //}),
            dataType: "json",
            contentType: "application/json; charset=UTF-8",
            cache: false,
            async: false,
            success: function (backmsg) { //取得回傳訊息
                //console.log(backmsg);
                //alert(backmsg);
                if (backmsg.rs == true) {
                    //alert("新增成功" );
                    //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip_ok.jpg' >此帳號可使用!");
                    moblieVar = true;
                    return;
                } else {
                    return options.allrules.checkMail.alertText;
                }
                //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");

            },
            error: function () {
                //alert("新增失敗" );
                //$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");
                return options.allrules.checkMail.alertText;
            }
        });
        if (moblieVar != true) {
            return options.allrules.checkMail.alertText;
        }
        //}else{
        //return;
        //}


    }
    ;

</script>


<style>
    .register_t2 .w1{
      margin-left: auto;
      margin-right: auto;
    }

    select {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        padding: 2px 30px 2px 2px;
        border: none;
    }

    .register_t2 csdd {
        width: 180px;
        padding-top: 12px;
        padding-bottom: 12px;
        height: 54px;
        line-height: 35px;
        border-radius: 30px;
        margin-bottom: 6px;
        color: #4D4D4D;
    }

    #webview select {
        width: 138px;
        background: #FFF url("${pageContext.request.contextPath}/resource/assets/images/g-drap.png") no-repeat scroll right 1rem center;
        border: 0px none;
        font-size: 14px;
        font-family: "Helvetica Neue", Helvetica, Arial, Chinese, "微軟正黑體",
        sans-serif;
        color: #4D4D4D;
        padding: 0px 10px;
    }

    .module-customselect .time {
        font-size: 14px;
        padding-left: 0px;
        text-indent: 10px;
    }

    .register_t2 .select dl {
        margin-left: 0px;
        width: 162px;
    }

    .register_t2 .b1_2_mobile .year {
        float: left;
        margin: 0px;
        width: 86px;
    }
</style>

<div id="divProgressLink" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;">

</div>
<div id="divMaskFrameLink" style="background-color: #F2F4F7; display: none; left: 0px;
    position: absolute; top: 0px;">
</div>

<div id="divProgress" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;">
    <img id="img-loading" src="${pageContext.request.contextPath}/resource/assets/images/loading.gif"/>
    <br/>
    <font color="#1B3563" size="2px">資料處理中</font>
    <div class="nav_rate">
        <div class="nav_rate2 nav5">&nbsp;</div>
    </div>
</div>
<div id="divMaskFrame" style="background-color: #F2F4F7; display: none; left: 0px;
    position: absolute; top: 0px;">
</div>

<div id="divPrivacy" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;">
    ${privacy}
</div>
<div id="divService" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;">
    ${service}
</div>

<div class="hidden-popup-contents" style="display: none;">
    <div id="popup-updateOK" class="module-popup">
        <h3 style="text-align: left;">親愛的會員您好，<BR>請至手機簡訊區點選手機驗證網址後再請登入台灣資金交易所網站，感謝您!!</h3>
        <form action="">
            <div class="btn-wrap">
                <div class="btn-rounded style-thin" onclick="resendSMS()">重新發送</div>
                <div class="btn-rounded style-thin" onclick="goSuccess()">確定</div>
            </div>
        </form>
    </div>
</div>
<script>
    var myShowDiv;
    $(document).ready(
        myShowDiv = setInterval(function(){ showDiv() }, 200)
    );


</script>
</body>
</html>