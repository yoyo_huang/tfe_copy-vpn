<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link
	href="${pageContext.request.contextPath}/resource/assets/images/favicon.png"
	rel="shortcut icon" type="image/x-icon" />

<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/member.css"
	media="screen" rel="stylesheet" type="text/css" />
<div class="layout-view">
	<section class="section-banner" style="height: 0;"></section>
	<!--subsection start-->
	<!--subsection end-->

	<section class="section-content">
		<div class="layout-mw-wrapper">
			<!--PAGE CONTENT START-->

			<div class="register-content">
				<div class="register">
					<div class="register-nav">
						<ul class="clearfix">
							<li class="hidd">會員資料註冊</li>
							<li class="hidd">電子信箱驗證</li>
							<li class="hidd">手機號碼驗證</li>
							<li class="hidd">個人身分資料</li>
							<li class="current_btn current">會員註冊完成</li>
						</ul>
						<div class="nav_rate">
							<div class="nav_rate2 nav5">&nbsp;</div>
						</div>
					</div>
					<div class="re_status">
						<div class="status_t1">恭喜！手機號碼也通過認證了。</div>
						<div class="register_status">
							<dl class="clearfix status_txt">
								<dt>
									<img
										src="${pageContext.request.contextPath}/resource/assets/images/s5.jpg">
								</dt>
								<dd>
									<h1>是否申請信用額度？</h1>
									<h3>我們根據您的個人資料及信用狀況，經過嚴謹詳實的徵信流程與最新的動態信用風險評估模型，
										決定您的信用評等及信用額度，並在您參加的每個不同競標組合中決定您的信用金。</h3>
									<h4>
										申請信用額度可以讓您在台灣資金交易所：<br> <span class="c1">1.
											擁有借款的權利。<br> 2. 進行更靈活的資金操作。<br> 3.
											累積個人的行為資料，提升未來個人信用品質。
										</span><br> <span class="c2">※ 我們強烈建議您進行申請，取得完整使用平台服務的機會！</span>
									</h4>
								</dd>
							</dl>
						</div>
						<div class="home_btn">
							<div class="news_button">
								<a href="credit">申請</a>
							</div>
							<div class="news_button">
								<a href="index">暫不申請/返回首頁</a>
							</div>
							<!--  
							<div class="news_button style-gray">
								<a href="">了解更多</a>
							</div>
							-->
							<div class="clear"></div>
						</div>
					</div>
				</div>


			</div>

			<!--PAGE END-->
		</div>
	</section>
	<img
		src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg"
		width="100%" style="margin-top: -1px;" alt="">
</div>


</div>
<script
	src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js"
	type="text/javascript"></script>
<script
	src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js"
	type="text/javascript"></script>
</body>
</html>