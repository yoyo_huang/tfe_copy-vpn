<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>

    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/member.css" media="screen"
          rel="stylesheet" type="text/css"/>

    <script type="text/javascript">

        function dump_props(obj, objName) {
            var result = "";

            for (var i in obj) {
                try {
                    result += "<tr><td>" + objName + "." + i + "</td><td>" + obj[i] + "</td></tr>";
                }
                catch (err) {
                    result += "<tr><td>" + objName + "." + i + "</td><td>" + err + "</td></tr>";
                }
            }

            return result;
        }

        var GO_DEBUG = false;
        function openDebug(obj, objName) {
            if (GO_DEBUG) {
                debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
                debugWindow.document.write("<HEAD><TITLE>Message window</TITLE></HEAD>");
                debugWindow.document.write("<Body><Table border='1'>" + dump_props(obj, objName) + "</Table></Body>");
            }
        }

        function openDebugText(_text) {
            if (GO_DEBUG) { //alert("debug");
                debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
                debugWindow.document.write(_text);
            }
        }


        function openDebugTextNewLine(_text) {
            if (GO_DEBUG) { //alert("debug");
                debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
                var re = /\&/g;
                var result = _text.replace(re, '<br/>&');
                debugWindow.document.write(result + '<br/>');
            }
        }


        var submitNotStart = true;

        /********************************progress bar start*******************************************/
        // 顯示讀取遮罩
        function ShowProgressBar() {
            displayProgress();
            displayMaskFrame();
        }

        // 隱藏讀取遮罩
        function HideProgressBar() {
            var progress = $('#divProgress');
            var maskFrame = $("#divMaskFrame");
            progress.hide();
            maskFrame.hide();
        }
        // 顯示讀取畫面
        function displayProgress() {
            var w = $(document).width();
            var h = $(window).height();
            var progress = $('#divProgress');
            progress.css({
                "z-index": 999999,
                "top": (h / 2) - (progress.height() / 2),
                "left": (w / 2) - (progress.width() / 2)
            });
            progress.show();
        }
        // 顯示遮罩畫面
        function displayMaskFrame() {
            var w = $(window).width();
            var h = $(document).height();
            var maskFrame = $("#divMaskFrame");
            maskFrame.css({"z-index": 999998, "opacity": 0.7, "width": w, "height": h});
            maskFrame.show();
        }
        /********************************progress bar end*********************************************/
        function doSubmit() {
//            alert("bbtest==>"+submitNotStart);
            if (submitNotStart) {
                submitNotStart = false;
                var question1Answer = $("#question1Answer").val();
                var question2Answer = $("#question2Answer").val();
                var question3Answer = $("#question3Answer").val();
                
                console.log('question1 answer:' + $("#question1Answer").val());
                console.log('question2 answer:' + $("#question2Answer").val());
                console.log('question3 answer:' + $("#question3Answer").val());
//                alert(checkSubmitValue());
                if (checkSubmitValue()) {
                    ShowProgressBar();
                    $('#file-form').submit();
                } else {
                    submitNotStart = true;
                    return false;
                }
            }
            return false;
        }

        function checkSubmitValue() {

        	var question1Answer = $("#question1Answer").val().trim();
            var question2Answer = $("#question2Answer").val().trim();
            var question3Answer = $("#question3Answer").val().trim();
			var workingYearAnswer = $("#workingYear").val().trim();
			var HasNotAnswer = false;
            if (question1Answer == '1' || question1Answer == '3') 
            {               	         
				if (question2Answer == '' || question3Answer == '') 
				{				    
				    HasNotAnswer = true;				    
				}
            } 
        	else if (question1Answer == '' || question2Answer == '')
            {                 
				HasNotAnswer = true;                
            }
            
            if(!HasNotAnswer && 
            	(question1Answer == '1' || question1Answer == '2') && workingYearAnswer == '')
           	{
            	HasNotAnswer = true;            	
            }
        	
            if(HasNotAnswer)
           	{
            	alert("請選擇需要回答的問題");
                return false;           
           	}
            return true;

        }

    </script>

</head>
<body>
<!-- 201*110 -->
<div class="layout-view">
    <section class="section-banner" style="height: 0;"></section>
    <!--subsection start-->
    <!--subsection end-->
    <section class="section-content">
        <div class="layout-mw-wrapper">
            <div class="register-content">
                <center>
                    <table width="0" border="0" align="center">
                        <tr>
                            <td><img src="${pageContext.request.contextPath}/resource/assets/images/submit_02.gif"
                                     alt="" width="500" height="100"></td>
                        </tr>
                    </table>
                </center>

                <form method="POST" action="${pageContext.request.contextPath}/creditForIntercare"
                      id="file-form" name="FileForm" >

                    <div class="register">
                        <div class="re_status">
                            <div class="status_t1">請選擇您的身份</div>
                            <div class="status_brank">
                                <div class="clearfix b4_form">
                                    <div class="module-customselect b4_select inp01 ">
											<span class="module-customselect-typename inp_left01">
												<font style="color: red;">*</font>
												第一個問題												
											</span>
                                        <span class="module-customselect-selected inp_right01">-你的身分-</span>
                                        <input id="question1Answer" name="question1Answer" type="hidden" value="">
                                        <ul id="question1ul">
                                            <li id="question1ul1" value="1">受薪</li>
                                            <li id="question1ul2" value="2">非受薪(攤商.自營工作室及店面等)</li>
                                            <li id="question1ul3" value="3">其他投資人(股票族.房東或家庭主婦等)</li>
                                            <li id="question1ul4" value="4">學生</li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="question2div" class="clearfix b4_form" style="display: none;">
                                    <div class="module-customselect b4_select inp01">
											<span id="question2titlespan" class="module-customselect-typename inp_left01">
											    <font style="color: red;">*</font>
												第二個問題												
											</span>
                                        <span id="question2selectText" class="module-customselect-selected inp_right01"
                                              style="background: none;">-請選擇-</span>
                                        <input id="question2Answer" style="font-size:15px" name="question2Answer"
                                               type="hidden" value="">
                                        <ul id="question2ul" style="overflow: auto" />                                        
                                    </div>
                                </div>
                                <div id="question3div" class="clearfix b4_form" style="display: none;">
                                    <div class="module-customselect b4_select inp01">
											<span id="question3titlespan" class="module-customselect-typename inp_left01">
												<font style="color: red;">*</font>
												第三個問題												
											</span>
                                        <span id="question3selectText" class="module-customselect-selected inp_right01">-請選擇-</span>
                                        <input id="question3Answer" name="question3Answer" type="hidden" value="">
                                        <ul id="question3ul" style="overflow: auto" />                                        
                                    </div>
                                </div>
                                <div id="workingYeardiv" class="clearfix b4_form" style="display: none;">
                                    <div class="module-customselect b4_select inp01">
											<span id="workingYeartitlespan" class="module-customselect-typename inp_left01">
												<font style="color: red;">*</font>
												  年資												
											</span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <input type="text" id="workingYear" name="workingYear"
                                                   maxlength="3" placeholder="年資" style="font-size:15px"                                                   
                                                   value='${account.workingYear != null?account.workingYear:""}'/>
                                        </span>
                                    </div>
                                </div>
                                <div class="clear" />
                            </div>

                            <!-- 以下為上傳區   開始 -->
                            <div class="register_status" style="display: none;">

                                <div id="depositBookInnerPageDiv">
                                    <div class="status_btn b139">
                                        <div class="button">
                                            <input id="sendButton" class="news_button" type="submit" value="確定"/>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--PAGE END-->
        </div>


    </section>
    <img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%"
         style="margin-top: -1px;" alt="">
</div>
<div id="divProgressLink" style="text-align: center; display: none; position: fixed; top: 50%; left: 50%;"></div>
<div id="divMaskFrameLink"
     style="background-color: #F2F4F7; display: none; left: 0px; position: absolute; top: 0px;"></div>
<div id="divProgress" style="text-align: center; display: none; position: fixed; top: 50%; left: 50%;">
    <img id="img-loading" src="${pageContext.request.contextPath}/resource/assets/images/loading.gif"/>
    <br/>
    <font color="#1B3563" size="2px">資料處理中</font>
    <div class="nav_rate">
        <div class="nav_rate2 nav5">&nbsp;</div>
    </div>
</div>
<div id="divMaskFrame" style="background-color: #F2F4F7; display: none; left: 0px; position: absolute; top: 0px;"></div>

<%--<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/common/css/validationEngine.jquery.css"/>--%>
<script src="${pageContext.request.contextPath}/resource/common/js/jquery.validationEngine-zh_CN.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/common/js/jquery.validationEngine.js"></script>

<script>
    $(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());

    $('#file-form').validationEngine({
        validationEventTriggers: "blur", //触发的事件  validationEventTriggers:"keyup blur",
        inlineValidation: true,//是否即时验证，false为提交表单时验证,默认true
        success: false,//为true时即使有不符合的也提交表单,false表示只有全部通过验证了才能提交表单,默认false
        promptPosition: "topRight",//提示所在的位置，topLeft, topRight, bottomLeft,  centerRight, bottomRight
        scroll: false,
        //ajax
        ajaxFormValidation: true,
        ajaxFormValidationMethod: 'post',
        onAjaxFormComplete: ajaxValidationCallback,
        onBeforeAjaxFormValidation: beforeCall,
        custom_error_messages: {
            // Custom Error Messages for Validation Types
            '#contactName': {
                'custom[chinese]': {
                    'message': '姓名格式錯誤'
                },
            },
//            '#checkbox1': {
//                'required': {
//                    'message': '選項服務條款及隱私權政策未同意無法進行後續作業 '
//                }
//            },
        },

    });
    //This method is called right before the ajax form validation request
    //it is typically used to setup some visuals ("Please wait...");
    //you may return a false to stop the request
    function beforeCall(form, options) {
        if (!doSubmit()) {
            return false;
        }
//        alert("aaaa");
        ShowProgressBar();
        if (window.console) {
            //console.log('Right before the AJAX form validation call');
        }
        ;
        return true;
    }
    ;

    //Called once the server replies to the ajax form validation request
    function ajaxValidationCallback(status, form, json, options) {

        if (status === true) {
            //alert('the form is valid!');
            if (json.rs == true) {
                //alert("成功");
                //$.post("${pageContext.request.contextPath}"+"/account/register/Register_B2",function(data){

                //$(".layout-container").html(data);

                //alert("a........href:"+data);
                //});
                if (json.mail == true) {
                    alert("系統已發送認證信至您修改後信箱,請您至信箱收取認證信進行確認,信箱將在您確認後進行更新");
                }
                HideProgressBar();
                alert("${pageContext.request.contextPath}" + json.view);
                window.location.replace("${pageContext.request.contextPath}" + json.view);
            } else {
                alert("失敗");
            }
        } else {
            alert("error");
        }
        HideProgressBar();

    }
    ;
</script>
<script>

    $(document).ready(function () {
        //console.log($("#question2div").val());
        //alert();

        $("#question1ul").change(function () {
            console.log($("#question1ul li").val());
        });

        $("#question1ul li").each(function () {
            //console.log($(this).val());
            $(this).click(function () {
                console.log($(this).val());
                $("#question1Answer").val($(this).val());
                resetAnswerDeleteArrayAndUploadValue();
                refreshQustion2li($(this).val());
            });
            //console.log($("#question1ul li").val());
        });

        $("#workingYeardiv").keydown(function (e) {
        	var keyCode = (window.event) ? e.which : e.keyCode;	        
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                 // Allow: home, end, left, right, down, up
                (keyCode >= 35 && keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (keyCode < 48 || keyCode > 57)) && (keyCode < 96 || keyCode > 105)) {
                e.preventDefault();
            }
        });
        
        function resetAnswerDeleteArrayAndUploadValue() {

            $("#question1Answer").val('');
            $("#question2Answer").val('');
            $("#question3Answer").val('');

        }

        function refreshQustion2li(question1Val) {

            $('#question2selectText').css('background', 'url("${pageContext.request.contextPath}/resource/assets/images/icon-select.png") right center no-repeat white');

            $("#question2div").show();
            $("#question2ul").show();
            $("#question2ul").empty();
            $("#question3ul").empty();
            $("#question2selectText").text("-請選擇-");
            $("#question3selectText").text("-請選擇-");
            if (question1Val == 1) {
            	<c:forEach items="${occupationType}" var="o" varStatus="count">                	              
            		$("#question2ul").append('<li id="question2ul${o.key}" value="${count.count+1}">${o.key}</li>');
            	</c:forEach>

            	$("#question2selectText").text("-職業類型-");
            	$("#question2ul li").each(function () {                    
                	$(this).click(function () {
                    	console.log(" question2ul li:" + $(this).text());
                    	refreshQustion3li(question1Val, $(this).text());
                	});                    
            	});
            } else if (question1Val == 2) {
            	<c:forEach items="${nonHireType}" var="o">                
	            	$("#question2ul").append('<li id="question2ul${o.code}" value="${o.code}">${o.description1}</li>');
	            </c:forEach>               
	            $("#question2selectText").text("-現職行業-");
	            $("#question2div").show();  
            } else if (question1Val == 3) {
                $("#question2ul").append('<li id="question2ul0" value="0">無</li>');
                $("#question2ul").append('<li id="question2ul1" value="1">一個</li>');
                $("#question2ul").append('<li id="question2ul2" value="2">兩個</li>');
                $("#question2ul").append('<li id="question2ul3" value="3">三個</li>');
                $("#question2ul").append('<li id="question2ul4" value="4">四個</li>');
                $("#question2ul").append('<li id="question2ul5" value="5">五個或五個以上</li>');
                $("#question2selectText").text("-小孩人數-");
            } else if (question1Val == 4) {
                <c:forEach items="${degreed}" var="o">
                	$("#question2ul").append('<li id="question2ul${o.code}" value="${o.code}">${o.description1}</li>');
                </c:forEach>
                $("#question2selectText").text("-最高學歷-");
            }
            refreshQustion3li(question1Val,'');
        }

        function refreshQustion3li(question1Val, question2Val) {
        	$("#question3div").hide();			
            $("#question3ul").empty();
            $("#workingYeardiv").hide();
            
            if (question1Val == 1) {
            	var hasOccupationTypeDetail = false;
            	<c:forEach items="${occupationTypeDetail}" var="od">                	 
                	if(question2Val == '${od.description1}')
                	{         
                		if('${od.description2}' != '')
                		{
                			hasOccupationTypeDetail = true;
                			$("#question3ul").append('<li id="question3ul${od.code}" value="${od.code}">${od.description2}</li>');
                   		}
                   		else
                   		{                			
                			$("#question3ul").append('<li id="question3ul${od.code}" value="${od.code}">${od.description1}</li>');
                			$("#question3Answer").val('${od.code}');
                		}
                	};
    			</c:forEach>
                
                if(hasOccupationTypeDetail)
               	{
                	$("#question3selectText").text("-職業名稱-");
                	$("#question3div").show();
               	}               
               	$("#workingYear").attr("placeholder","請輸入現職年資");
                $("#workingYeardiv").show();
            } else if (question1Val == 2) {            	 
                 $("#workingYear").attr("placeholder","請輸入累計工作年資");
                 $("#workingYeardiv").show();
            } else if (question1Val == 3) {
            	<c:forEach items="${othersJobType}" var="ojt">                
	            	$("#question3ul").append('<li id="question3ul${ojt.code}" value="${ojt.code}">${ojt.description1}</li>');
	            </c:forEach>
	        	
	        	$("#question3selectText").text("-現職行業-");
	            $("#question3div").show();
            } else if (question1Val == 4) {
                $("#question3div").hide();
            }

            $(".register_status").each(function (index) {
                $(this).show();
            });

            $(".register_status2").each(function (index) {
                $(this).show();
            });

            $("#question2ul li").each(function () {
                $(this).click(function () {
                    console.log("question2ul li : " + $(this).val());
                    $("#question2Answer").val($(this).val());
                });
            });

            $("#question3ul li").each(function () {
                //console.log($(this).val());
                $(this).click(function () {
                    console.log($(this).val());
                    $("#question3Answer").val($(this).val());
                });
                //console.log($("#question1ul li").val());
            });
            initData();
        }



        function initData() {
//            alert("go initData");
            if (IS_LOAD_INIT_DONT == false) {
            }

            IS_LOAD_INIT_DONT = true;
        }


    });

    jQuery(window).load(function () {
//        alert("Go Load!");
        var incomeSource = ${empty account.incomeSource? 0:account.incomeSource};

        $("#question1ul" + incomeSource).trigger('click');

        switch (incomeSource) {
            case 1:
            	<c:forEach items="${occupationTypeDetail}" var="od">    
	        		<c:if test="${account.jobType==od.code}">          	                	        
	            		$("#question2ul li:contains('${od.description1}')").trigger('click');                	
	            	</c:if>
				</c:forEach>
				                
	            $("#question3ul" + "${account.jobType}").trigger('click');
	            console.log("account.jobType" + "${account.jobType}");
                //console.log("account.workLevel" + "${account.workLevel}");
                break;
            case 2:
            	$("#question2ul" + "${account.workKind}").trigger('click');
                console.log("account.workingYear" + "${account.workingYear}");
                console.log("account.workKind" + "${account.workKind}");
                break;
            case 3:
            	$("#question2ul" + "${account.children}").trigger('click');
                $("#question3ul" + "${account.jobType}").trigger('click');
                console.log("account.children" + "${account.children}");
                break;
            case 4:
                $("#question2ul" + "${account.educationLevel}").trigger('click');
                console.log("account.educationLevel" + "${account.educationLevel}");
                break;

        }
        jQuery("body").trigger("click");
    });

    var IS_LOAD_INIT_DONT = false;

</script>
</body>
</html>