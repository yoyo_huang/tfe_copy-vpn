<%-- <%@ include file="/resource/common/jsp/changeDiv.jsp"%> --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<head>
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="0">
    <link
            href="${pageContext.request.contextPath}/resource/assets/images/favicon.png"
            rel="shortcut icon" type="image/x-icon"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/member.css"
            media="screen" rel="stylesheet" type="text/css"/>

</head>
<body onload="startTime()">
<div class="layout-container">

    <div class="layout-view">
        <section class="section-banner" style="height: 0;"></section>
        <!--subsection start-->
        <!--subsection end-->

        <section class="section-content">
            <div class="layout-mw-wrapper">
                <!--PAGE CONTENT START-->

                <div class="register-content">
                    <div class="register">
                        <div class="phone_Verify">
                            <div class="button">
                                <div class="news_button b168">

                                    <a onclick="registerAgainGetCodeNotPhone();">寄送認證碼</a>
                                </div>
                            </div>

                            <div class="clearfix phone_num">
                                <div class="phone_input">
                                    <input name="phonecode" id="phonecode" type="text" value=""
                                           autocomplete="off" placeholder="輸入手機認證碼">

                                </div>
                                <!--
                                <div class="erro">
                                    認證碼錯誤，<br> 請按下重寄認證碼，重新操作一次
                                </div>
                                 -->
                                <div class="clear"></div>
                            </div>
                            <div class="style-aligncenter">
                                <div class="form-block style-verifycode is-error">
                                    <label for="">驗證碼</label> <span class="verifycode-wrap">
											<input type="text" id="enterVerifyCode" name="verifycode"
                                                   class="module-form-input style-rounded" placeholder="輸入驗證碼">
											<img class="ccode" id="verifyImg" src="">
											<i class="fa fa-refresh" onclick="exchangeVerifyCode()"></i>
										</span>
                                </div>
                                <p class="form-warning-text"></p>
                                <div id="codeMsg"></div>
                            </div>
                            <div class="button" id="registerCode">
                                <div class="news_button b168">
                                    <a onclick="registerCheckCode();">確定</a>
                                </div>
                            </div>

                            <div class="phone_time">
                                <div class="f1">
                                    認證時間倒數 <span id="txt">10：00</span>
                                </div>
                                <div class="f2">※ 請在時間內輸入認證碼，若超過時間認證碼將失效</div>
                            </div>
                            <div class="phone_repeat">
                                <div class="n1">仍沒收到手機認證碼簡訊嗎？</div>
                                <div class="n2">請在下面輸入您的手機號碼，並按下寄送更新並寄送認證碼，
                                    我們將更新您註冊的手機號碼並重新寄送認證碼。
                                </div>
                                <div class="clearfix phone_num">
                                    <div class="phone_input">
                                        <input id="phone" name="phone" type="text" value=""
                                               autocomplete="off" placeholder="輸入手機號碼">

                                    </div>
                                    <div class="up">
                                        <div class="email_button email_b">
                                            <a onclick="registerAgainGetCode();">更新並寄送認證碼</a>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--PAGE END-->
            </div>
        </section>
        <img
                src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg"
                width="100%" style="margin-top: -1px;" alt="">
    </div>
</div>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js"
        type="text/javascript"></script>

<div id="divProgressLink" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;">

</div>
<div id="divMaskFrameLink" style="background-color: #F2F4F7; display: none; left: 0px;
    position: absolute; top: 0px;">
</div>

<div id="divProgress" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;">
    <img id="img-loading" src="${pageContext.request.contextPath}/resource/assets/images/loading.gif"/>
    <br/>
    <font color="#1B3563" size="2px">資料處理中</font>
    <div class="nav_rate">
        <div class="nav_rate2 nav5">&nbsp;</div>
    </div>
</div>
<div id="divMaskFrame" style="background-color: #F2F4F7; display: none; left: 0px;
    position: absolute; top: 0px;">
</div>
<script type="text/javascript">

    $(window).load(function () {
        exchangeVerifyCode();
    });
    var totalTime = 60 * 10;
    var t;
    function startTime() {

        // add a zero in front of numbers<10
        var m = totalTime / 60;
        var s = totalTime % 60;
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('txt').innerHTML = Math.floor(m) + ":" + s;
        t = setTimeout('startTime()', 1000);
        if (totalTime > 0) {
            totalTime = totalTime - 1;
        } else {
            clearTimeout(t);
            $("#registerCode").hide();
        }

    }


    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        }
        return i
    }

    function registerAgainGetCodeNotPhone() {
        ShowProgressBar();
        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。

        $
                .ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/registerNotPhoneServiceAgain?",
                    //data: JSON.stringify({
                    //account: value,
                    //}),
                    dataType: "json",
                    contentType: "application/json; charset=UTF-8",
                    cache: false,
                    async: false,
                    success: function (backmsg) { //取得回傳訊息
                        //console.log(backmsg);
                        //alert(backmsg);//0:發送失敗 1:成功發送 2:發送次數已達上限 3:格式錯誤
                        if (backmsg.rs == 1) {
                            totalTime = 60 * 10;
                            alert("成功發送 ");
                            $("#registerCode").show();
                            setTimeout("HideProgressBar()", 1000);
                            return true;
                        } else if (backmsg.rs == 2) {
                            alert("發送次數已達上限");
                        } else {
                            alert("發送失敗");
                        }
                        setTimeout("HideProgressBar()", 1000);
                        return false;
                    },
                    error: function () {
                        alert("發送失敗");
                        setTimeout("HideProgressBar()", 1000);
                        return false;
                    }
                });
        return false;
    }
    ;

    function registerAgainGetCode() {
        ShowProgressBar();
        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var phone = $("#phone").val();

        $
                .ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/registerUserServiceAgain?"
                    + "phone=" + phone,
                    //data: JSON.stringify({
                    //account: value,
                    //}),
                    dataType: "json",
                    contentType: "application/json; charset=UTF-8",
                    cache: false,
                    async: false,
                    success: function (backmsg) { //取得回傳訊息
                        //console.log(backmsg);
                        //alert(backmsg);//0:發送失敗 1:成功發送 2:發送次數已達上限 3:格式錯誤
                        if (backmsg.rs == 1) {
                            totalTime = 60 * 10;
                            $("#registerCode").show();
                            alert("成功發送 ");
                            setTimeout("HideProgressBar()", 1000);
                            return true;
                        } else if (backmsg.rs == 2) {
                            alert("發送次數已達上限");
                        } else if (backmsg.rs == 3) {
                            alert("格式錯誤");
                        } else {
                            alert("發送失敗");
                        }
                        setTimeout("HideProgressBar()", 1000);
                        return false;
                    },
                    error: function () {
                        alert("發送失敗");
                        setTimeout("HideProgressBar()", 1000);
                        return false;
                    }
                });
        return false;
    }
    ;

    function registerCheckCodeStates() {

        var phonecode = $("#phonecode").val();
        $.ajax({
            type: "POST",
            url: "${pageContext.request.contextPath}/registerCheckCode?"
            + "code=" + phonecode,
            //data: JSON.stringify({
            //account: value,
            //}),
            dataType: "json",
            contentType: "application/json; charset=UTF-8",
            cache: false,
            async: false,
            success: function (backmsg) { //取得回傳訊息
                //console.log(backmsg);
                if (backmsg.rs == true) {
                    HideProgressBar();
                    //$(".register").load("${pageContext.request.contextPath}" + backmsg.view+ " .register-content");
                    window.location.href = "${pageContext.request.contextPath}"
                            + backmsg.view;
                    clearTimeout(t);
                    setTimeout("HideProgressBar()", 1000);
                    return true;
                } else {
                    alert("認證失敗");
                    setTimeout("HideProgressBar()", 1000);
                }
            },
            error: function () {
                //alert("新增失敗" );
                alert("認證失敗");
                setTimeout("HideProgressBar()", 1000);
            }
        });


    }
    ;

    function registerCheckCode() {
        ShowProgressBar();
        //透過getElementById，從ID為password的輸入框裡獲取用戶輸入的密碼。
        var isboolean = checkVerifyCode();
        if (isboolean) {
            setTimeout("registerCheckCodeStates()", 1000);
        } else {
            setTimeout("HideProgressBar()", 1000);
        }

    }
    ;


    function exchangeVerifyCode() {
        $('.ccode').attr('src', '${pageContext.request.contextPath}/userLogin/getCaptchaImage' + "?" + (new Date().getTime()));
    }


    function checkVerifyCode() {
        $("#codeMsg").html();
        var checkType = false;
        $.ajax({
            type: "POST",
            url: "${pageContext.request.contextPath}/registerCheckVerifyCode?"
            + "verifycode=" + $("#enterVerifyCode").val() + "",
            //data: JSON.stringify({
            //account: value,
            //}),
            dataType: "json",
            contentType: "application/json; charset=UTF-8",
            cache: false,
            async: false,
            success: function (backmsg) { //取得回傳訊息
                //console.log(backmsg);
                //alert(backmsg);
                if (backmsg.rs == true) {
                    $("#codeMsg").html();
                    checkType = true;
                } else {
                    $("#codeMsg").html("驗證碼輸入錯誤");
                }
            },
            error: function () {
                $("#codeMsg").html("驗證碼輸入錯誤");
            }
        });


        return checkType;
    }
    ;

    // 顯示讀取遮罩
    function ShowProgressBar() {
        displayProgress();
        displayMaskFrame();
    }

    // 隱藏讀取遮罩
    function HideProgressBar() {
        var progress = $('#divProgress');
        var maskFrame = $("#divMaskFrame");
        progress.hide();
        maskFrame.hide();
    }
    // 顯示讀取畫面
    function displayProgress() {
        var w = $(document).width();
        var h = $(window).height();
        var progress = $('#divProgress');
        progress.css({
            "z-index": 999999,
            "top": (h / 2) - (progress.height() / 2),
            "left": (w / 2) - (progress.width() / 2)
        });
        progress.show();
    }
    // 顯示遮罩畫面
    function displayMaskFrame() {
        var w = $(window).width();
        var h = $(document).height();
        var maskFrame = $("#divMaskFrame");
        maskFrame.css({"z-index": 999998, "opacity": 0.7, "width": w, "height": h});
        maskFrame.show();
    }

</script>
</body>
</html>