<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="utf-8">
<meta name="description"
	content="File Upload widget with multiple file selection, drag&amp;drop support, progress bar, validation and preview images, audio and video for jQuery. Supports cross-domain, chunked and resumable file uploads. Works with any server-side platform (Google App Engine, PHP, Python, Ruby on Rails, Java, etc.) that supports standard HTML form file uploads.">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link
	href="${pageContext.request.contextPath}/resource/assets/images/favicon.png"
	rel="shortcut icon" type="image/x-icon" />

<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/member.css"
	media="screen" rel="stylesheet" type="text/css" />
<style>
select {
	-webkit-appearance: none;
	-moz-appearance: none;
	appearance: none;
	padding: 2px 30px 2px 2px;
	border: none;
}
.status_brank select {
    width: auto;
 }
 
#cboxContent {
	padding-top: 0;
}

#cboxClose {
	display: none;
}

.module-customselect-selected input {
    width: 50%;
    font-size: 15px;
}

.email2_button {
    text-align: center;
    display: inline-block;
    line-height: 50px;
    padding-left: 1.5em;
    padding-right: 1.5em;
    color: #fff;
    background-color: #ed842f;
    cursor: pointer;
    border-radius: 25px;
    background-repeat: repeat-x;
   
    font-family: "Helvetica Neue", Helvetica, Arial, Chinese, "微軟正黑體", sans-serif;
}
</style>
<script>
function getType() {
	var Sys = {};
    var ua = navigator.userAgent.toLowerCase();
    var s;
    (s = ua.match(/rv:([\d.]+)\) like gecko/)) ? Sys.ie = s[1] :
    (s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] :
    (s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] :
    (s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] :
    (s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] :
    (s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0;
    
    var type = 0;
    if (Sys.ie) type = 1;
    if (Sys.firefox) type = 2;
    if (Sys.chrome) type = 3;
    if (Sys.opera) type = 4;
    if (Sys.safari) type = 5;
    return type;
   
}	 

jQuery(window).load(function() {
	var isIE = getType(); 
	if(isIE == 1){
		$("#bidNodeKind").css( "padding-left", "11.5em" );
		$("#bidNodeID").css( "padding-left", "11.5em" );
	}
});
</script>
</head>
<body>
	<div class="layout-container">
		<div class="layout-view">
			<section class="section-banner" style="height: 0;"></section>
			<!--subsection start-->
			<!--subsection end-->

			<section class="section-content">
				<div class="layout-mw-wrapper">
					<!--PAGE CONTENT START-->

					<div class="register-content">
						<div class="register">
							<div class="register-nav">
								<ul class="clearfix">
									<li class="hidd">會員資料註冊</li>
									<li class="hidd">電子信箱驗證</li>
									<li class="hidd">手機號碼驗證</li>
									<li class="current_btn current">個人身分資料</li>
									<li class="hidd">會員註冊完成</li>
								</ul>
								<div class="nav_rate">
									<div class="nav_rate2 nav4">&nbsp;</div>
								</div>
							</div>
							<div class="re_status">
								<div class="status_t1">恭喜！手機號碼也通過認證了。</div>

								<div class="register_status">
									<dl class="clearfix status_txt">
										<dt>
											<img
												src="${pageContext.request.contextPath}/resource/assets/images/s3.png">
										</dt>
										<dd>
											<h1>使用者身份的真實性可確保大家的資金安全，因此我們需要您上傳雙證件電子檔，作為您本人意願及線上身份確認之用，請注意：</h1>
											<h2>
												<span class="gray">●</span> 第二證件可以是健保卡、駕照、護照等附照片之證件，<span
													class="gray">（上傳檔案可以是手機拍攝的影像檔或掃描檔）</span>
											</h2>
										</dd>
									</dl>
								</div>
								<div class="register_status ">
									<form id="twid" class="twid" method="post"
										action="${pageContext.request.contextPath}/registerFile"
										enctype="multipart/form-data">
										<ul class="clearfix status_photo">
											<li class="p1"><font color="#CC0000">*</font>身份證正面
												<div class="line W-hide"></div><br/><font color="#CC0000">影像需清晰</font>
											</li>
											<li class="p2"><div id="files1" class="files1"></div> <img
												id="img-group-1" class="img-group-1"
												src='${Account.adImg7 != null && Account.adImg7 !="" ?Account.adImg7:"resource/assets/images/p200x110.jpg"}' width="200px" height="110px" >

											</li>

											<li class="p3">
												<div class="button">
													<input id="fileupload1" type="file" name="file" multiple
															style="display: none;" accept="image/jpeg|image/png">
													<input id="updateButton" class="email2_button status_button" type="button"
																value="選擇檔案" onclick="$('input[id=fileupload1]').click();" />	
												</div>
												<div class="button">
													<input id="updateButton" class="email2_button status_button" type="button"
																value="檔案編輯" onclick="cancelFile(1);" />
												</div>
											</li>
											<div class="clear"></div>
										</ul>
									</form>
									<ul class="clearfix status_photo">
										<li class="p1"><font color="#CC0000">*</font>身份證反面
											<div class="line W-hide"></div><br/><font color="#CC0000">影像需清晰</font>
										</li>
										<li class="p2"><div id="files2" class="files2"></div> <img
											id="img-group-2" class="img-group-2"
											src='${Account.adImg8 != null?Account.adImg8:"resource/assets/images/p200x110.jpg"}' width="200px" height="110px">
										</li>
										<li class="p3">
											<div class="button">
												<input id="fileupload2" type="file" name="file" multiple
															style="display: none;" accept="image/jpeg|image/png">
												<input id="updateButton" class="email2_button status_button" type="button"
																value="選擇檔案" onclick="$('input[id=fileupload2]').click();" />	
											</div>
											<div class="button">
												<input id="updateButton" class="email2_button status_button" type="button"
																value="檔案編輯" onclick="cancelFile(2);" />
											</div>
										</li>
										<div class="clear"></div>
									</ul>

									<ul class="clearfix status_photo">
										<li class="p1"><font color="#CC0000">*</font>第二證件
											<div class="line W-hide"></div><br/><font color="#CC0000">影像需清晰</font>
										</li>
										<li class="p2"><div id="files3" class="files3"></div> <img
											id="img-group-3" class="img-group-3"
											src='${Account.adImg9 != null?Account.adImg9:"resource/assets/images/p3_200x110.jpg"}' width="200px" height="110px">
										</li>
										<li class="p3">
											<div class="button">
												<input id="fileupload3" type="file" name="file" multiple
															style="display: none;" accept="image/jpeg|image/png">
												<input id="updateButton" class="email2_button status_button" type="button"
																value="選擇檔案" onclick="$('input[id=fileupload3]').click();" />	
											</div>
											<div class="button">
												<input id="updateButton" class="email2_button status_button" type="button"
																value="檔案編輯" onclick="cancelFile(3);" />
											</div>
										</li>
										<div class="clear"></div>
									</ul>


								</div>

							</div>
							<!-- 
							<div class="register_status">
								<dl class="clearfix status_txt">
									<dt>
										<img
											src="${pageContext.request.contextPath}/resource/assets/images/s4.jpg">
									</dt>
									<dd>
										<h1>使用者身份的真實性可確保大家的資金安全，因此我們需要您上傳雙證件電子檔，
											並拍攝一段本人影片，作為您本人意願及線上身份確認之用，請注意：</h1>
										<h2>
											<span class="gray">●</span> 影片拍攝時須完整頭部及手持身份證正面入鏡，並朗讀以下文字：</span>
										</h2>
										<h2>
											<span class="gray">「我是（您的大名），身分證字號是（您的身分證字號），我提供給台灣資金交易所的資料都是真實有效的。<br>
												我願意遵守台灣資金交易所的服務條款與隱私權政策。若借款後未按時還款，<br>
												我同意台灣資金交易所公開我的個人資料。」
											</span>
										</h2>
									</dd>
								</dl>
							</div>
							-->
							<div class="register_status ">
								<!--  
								<div class="status_vedio">
									<div class="vedio_name">請參考示範影片：</div>
									<div class="vedio_play" id="videogroup6"
										style="text-align: center;">
										<video id="video-group-1" src='${Account.introV1 != null?Account.introV1:""}' width="500px" height="500px" controls>您的瀏覽器不支援影片的撥放</video>
									</div>

									<ul class="clearfix status_photo">
										<li class="p1"><font color="#CC0000">*</font>影片上傳
											<div class="line W-hide"></div>
										</li>
										<li class="p2">
										<input id="fileupload5" type="file" name="file" multiple
													style="display: none;" accept="video/mp4"> 
										<input id="updateButton" class="email2_button status_button" type="button"
																value="選擇檔案" onclick="$('input[id=fileupload5]').click();" />
										<font color="red"><br/>&nbsp;&nbsp;&nbsp;影片支援格式：MP4</font>
										</li>
									</ul>
								</div>
								-->
								<div class="register_status">
									<dl class="clearfix status_txt">
										<dt>
											<img
												src="${pageContext.request.contextPath}/resource/assets/images/s2.png" >
										</dt>
										<dd>
											<h1>我們需要您提供銀行帳戶資料及存摺封面電子檔做平台資金確認之用， 請務必填寫本人帳戶。</h1>
											<h2>
												<span class="gray">●</span> 銀行帳戶資料及存摺封面電子檔
											</h2>
										</dd>
									</dl>
								</div>
								<div class="register_status">
									<div class="status_brank">

										<div class="clearfix b4_form">
											<div class="module-customselect b4_select inp01 ">
												<span class="module-customselect-typename inp_left01"><font color="#CC0000">*</font>金融機構類別</span>
												<span class="module-customselect-selected inp_right01" style="background: none;">
												<select id="bidNodeKind" name="bidNodeKind" >
													<option value="" ${Account.bidNodeKind == null?"selected":""}>請選擇</option>
													<option value="A" ${Account.bidNodeKind == "A"?"selected":""}>商業銀行</option>
													<option value="B" ${Account.bidNodeKind == "B"?"selected":""}>信用合作社</option>
													<option value="C" ${Account.bidNodeKind == "C"?"selected":""}>農漁會</option>
												</select>
												</span>
											</div>
										</div>
										<div class="clearfix b4_form">

											<div class="module-customselect b4_select inp01">
												<span class="module-customselect-typename inp_left01"><font color="#CC0000">*</font>銀行名稱</span>
												<span class="module-customselect-selected inp_right01" style="background: none;">
												<select id="bidNodeID" name="bidNodeID" >
													<option value="" ${Account.bidNodeKind == null?"selected":""}>請選擇</option>
													<c:forEach var="codeVar" items="${code}" varStatus="loop">
														<option value="${codeVar.id.code}" ${Account.bidNodeID == codeVar.id.code?"selected":""}>${codeVar.description1}</option>
													</c:forEach>
												</select>
												</span>
											</div>
										</div>
										<!-- 
										<div class="clearfix b4_form">

											<div class="module-customselect b4_select inp01">
												<span class="module-customselect-typename inp_left01"><font color="#CC0000">*</font>分行名稱</span>
												<span class="module-customselect-selected inp_right01" style="background: none;">
												<input type="text" id="bidNodeBranchName" value='${Account.bidNodeBranchName != null?Account.bidNodeBranchName:""}' maxlength="20">
												<a href="http://www.fisc.com.tw/tc/service/branch.aspx" target="_blank">[查詢分行]</a>
												</span>
											</div>
										</div>	
										 -->								
										<div class="clearfix b4_form">
											<div class="module-customselect b4_select inp01">
												<span class="module-customselect-typename inp_left01"><font color="#CC0000">*</font>分行代號</span>
												<span class="module-customselect-selected inp_right01" style="background: none;">
												<input type="text" id="bidNodeBranchID" value='${Account.bidNodeBranchID != null?Account.bidNodeBranchID:""}' maxlength="4">

												</span>

											</div>
										</div>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="http://www.fisc.com.tw/tc/service/branch.aspx" target="_blank">[查詢分行]</a>
										<div class="clearfix b4_form">
											<div class="module-customselect b4_select inp01">
												<span class="module-customselect-typename inp_left01"><font color="#CC0000">*</font>銀行帳戶名稱</span>
												<span class="module-customselect-selected inp_right01" style="background: none;">
												<input type="hidden" id="bidNodeUserName" value='${Account.chineseName}' maxlength="20" >${Account.chineseName}
												</span>
	
											</div>
										</div>
										<div class="clearfix b4_form">

											<div class="module-customselect b4_select inp01">
												<span class="module-customselect-typename inp_left01"><font color="#CC0000">*</font>銀行帳號</span>
												<span class="module-customselect-selected inp_right01" style="background: none;">
												<input type="text" id="bidNodeAccount" value='${Account.bidNodeAccount != null?Account.bidNodeAccount:""}' maxlength="20">
											</div>
										</div>

										<div class="clear"></div>
									</div>


									<ul class="clearfix status_photo">
										<li class="p1"><font color="#CC0000">*</font>存摺封面或提款卡
											<div class="line W-hide"></div><br/><font color="#CC0000">影像需清晰</font>
										</li>
										<li class="p2"><div id="files4" class="files4"></div> <img
											id="img-group-4" class="img-group-4"
											src='${Account.adImg3 != null?Account.adImg3:"resource/assets/images/p7_200x110.png"}' width="200px" height="110px">
										</li>
										<li class="p3">
											<div class="button">
												<input id="fileupload4" type="file" name="file" multiple
															style="display: none;" accept="image/jpeg|image/png">
												<input id="updateButton" class="email2_button status_button" type="button"
																value="選擇檔案" onclick="$('input[id=fileupload4]').click();" />	
											</div>
											<div class="button">
												<input id="updateButton" class="email2_button status_button" type="button"
																value="檔案編輯" onclick="cancelFile(4);" />
											</div>
										</li>
										<div class="clear"></div>
									</ul>

								</div>
								<div class="status_btn2">
									<!--  
									<div class="button btn1 fl">
										<div class="news_button btn_w2">
											<a onClick="uploadDataToServer();">儲存並離開</a>
										</div>
									</div>
									-->
									<!-- 
									<div class="button btn1 fl">
										<input id="sendButton" class="news_button" type="button" value="送出" onClick="uploadFile();" />
									</div>
									 -->

									<div class="status_btn2">
										<input id="updateButton" class="news_button" type="button"
														value="    送 出    " onclick="uploadFile();" />
										
									</div>
									<div class="clear"></div>
									<div id="progressNumber"></div>
									<div id="fileName"></div>
									<div id="fileSize"></div>
									<div id="fileType"></div>
								</div>

							</div>


						</div>


					</div>
				</div>
		</div>

		<!--PAGE END-->
	</div>
	</section>
	
	<img
		src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg"
		width="100%" style="margin-top: -1px;" alt="">
	<script type="text/javascript"
		src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js"
		type="text/javascript"></script>


	<!-- Generic page styles 
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/common/css/style.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/common/css/jquery.fileupload.css">

-->
	
	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script
		src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script
		src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
	<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
	
	
	<script>
	
		$(function() {
			
			/****
			$(".img-group-1")
			.colorbox(
					{
						rel : 'group1',
						current : "<a class='popup-btn'>檔案刪除</a> <a class='popup-btn'>結束預覽</a>"
					});
			
			
			 */
			 
			$(".img-group-1").colorbox({
				html : function() {
					strSrcValue = $(".img-group-1").attr("src");
					strHtml = '<img src='+strSrcValue+'>';
					//在燈箱中要顯示的html字段
					return strHtml;
				},

				//width : 700, //燈箱中間區塊的寬度
				//height : 600, //燈箱中間區塊的高度
				onClosed : function() { //當燈箱關閉時的callback funtion
					//alert('Lightbox is closed :'+$(".img-group-1").attr("src"));
				}
			});
			$(".img-group-2").colorbox({
				html : function() {
					strSrcValue = $(".img-group-2").attr("src");
					strHtml = '<img src='+strSrcValue+'>';
					//在燈箱中要顯示的html字段
					return strHtml;
				},

				//width : 700, //燈箱中間區塊的寬度
				//height : 600, //燈箱中間區塊的高度
				onClosed : function() { //當燈箱關閉時的callback funtion
					//alert('Lightbox is closed :'+$(".img-group-2").attr("src"));
				}
			});
			$(".img-group-3").colorbox({
				html : function() {
					strSrcValue = $(".img-group-3").attr("src");
					strHtml = '<img src='+strSrcValue+'>';
					//在燈箱中要顯示的html字段
					return strHtml;
				},

				//width : 700, //燈箱中間區塊的寬度
				//height : 600, //燈箱中間區塊的高度
				onClosed : function() { //當燈箱關閉時的callback funtion
					//alert('Lightbox is closed :'+$(".img-group-3").attr("src"));
				}
			});
			$(".img-group-4").colorbox({
				html : function() {
					strSrcValue = $(".img-group-4").attr("src");
					strHtml = '<img src='+strSrcValue+'>';
					//在燈箱中要顯示的html字段
					return strHtml;
				},

				//width : 700, //燈箱中間區塊的寬度
				//height : 600, //燈箱中間區塊的高度
				onClosed : function() { //當燈箱關閉時的callback funtion
					//alert('Lightbox is closed :'+$(".img-group-4").attr("src"));
				}
			});

		});

		function cancelFile(i) {
			$
					.colorbox({
						html : function() {
							strSrcValue = $(".img-group-" + i).attr("src");
							strHtml = getImgSize(strSrcValue, i);
							
							//在燈箱中要顯示的html字段
							return strHtml;
						},

						//width : 700, //燈箱中間區塊的寬度
						//height : 600, //燈箱中間區塊的高度
						onClosed : function() { //當燈箱關閉時的callback funtion
							//alert('Lightbox is closed :'+$(".img-group-4").attr("src"));
						}
					});
		}
		
		function getImgSize(imgSrc, i)
		{
			var newImg = new Image();
			newImg.src = imgSrc;
			var height = newImg.height;
			var width = newImg.width;
			//console.log ('The image size is '+width+'*'+height);
			//取得寬度的語法
			var screenWidth = screen.width;
			//取得高度的語法
			var screenHeight = screen.height;
			//console.log ('The screen is '+screenWidth+'*'+screenHeight);
			var defualteValue = (screenWidth>=screenHeight?screenHeight*0.5:screenWidth*0.5);
			
			var tempWidth;
			var tempHeight;
			var tempRate;
			if(height >= width){
				tempRate = height / defualteValue;
			}else{
				tempRate = width / defualteValue;
			}
			tempHeight = height / tempRate;
			tempWidth = width / tempRate;
			//console.log ('The 2 image size is '+width+'*'+height);
			//console.log ('The 2 image size is tempWidth:'+tempWidth+'*tempHeight'+tempHeight);
			return "<div><img height='"+tempHeight+"' width='"+tempWidth+"' src='"+strSrcValue+"'></div>"
			+"<br><div  style='text-align:center;'><a class='popup-btn'  onclick='cancelFile2("
			+ i
			+ ")'>檔案刪除</a> <a class='popup-btn' onclick='$.fn.colorbox.close()'>結束預覽</a></div>";
			
		}
		
		function cancelFile2(num) {
			var fileName;
			var fileName2;
			var srcUrl;
			switch (num) {
			case 1:
				fileName = 'fileupload1';
				fileName2 = 'img-group-1';
				srcUrl = "${pageContext.request.contextPath}/resource/assets/images/p200x110.jpg";
				break;
			case 2:
				fileName = 'fileupload2';
				fileName2 = 'img-group-2';
				srcUrl = "${pageContext.request.contextPath}/resource/assets/images/p200x110.jpg";
				break;
			case 3:
				fileName = 'fileupload3';
				fileName2 = 'img-group-3';
				srcUrl = "${pageContext.request.contextPath}/resource/assets/images/p3_200x110.jpg";
				break;
			case 4:
				fileName = 'fileupload4';
				fileName2 = 'img-group-4';
				srcUrl = "${pageContext.request.contextPath}/resource/assets/images/p5_200x110.jpg";
				break;
			}
			$("#" + fileName).val('');
			$("." + fileName2).attr('src', srcUrl);
			$.fn.colorbox.close();

		}

		function readURL(input, name, h, w) {
			if (input.files && input.files[0]) {

				var file = input.files[0]
				cheakStates = checkType(file, name);

				if (cheakStates == 1) {
					url = loadImage.createObjectURL(file);
					$(name).attr('src', url);
					$(name).height(h);
					$(name).width(w);
				} else if (cheakStates == 2) {
					alert("檔案太大 請重新選擇");
					return;
				} else {
					alert("檔案格式不正確 請重新選擇");
					return;
				}
				
				

				//var fileSize = 0;
				//if (file.size > 1024 * 1024){
				//fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100).toString() + 'MB';
				//}else{
				//fileSize = (Math.round(file.size * 100 / 1024) / 100).toString() + 'KB';
				//}
				//console.log('Name: ' + file.name) ;
				//console.log('Size: ' + fileSize) ;
				//console.log('Type: ' + file.type) ;
				//console.log("readImgURL:" + url);
				//console.log("cheakStates:" + cheakStates);

				
			}
		}

		function checkType(file, name) {

			var fileSize = 0;
			var checkStates = 0;
			console.log("file.type:" + file.type);
			console.log("name:" + name);
			console.log("name==#video-group-1:" + (name.indexOf("video") > -1));
			if (file.size > 1024 * 1024) {
				fileSize = (Math.round(file.size * 100 / (1024 * 1024)) / 100)
						.toString();//+ 'MB';
				if ((name.indexOf("video") > -1)) {
					if (file.type.indexOf("video/mp4") > -1) {
						if (fileSize <= 21) {
							
							return 1;
						} else {
							return 2;
						}
					} else {
						return 0;
					}
				} else {
					if (file.type.indexOf("image/jpeg") > -1 || file.type.indexOf("image/png") >-1) {
//					if (file.type.indexOf("image/jpeg") > -1) {
						if (fileSize <= 2100) {
							return 1;
						} else {
							return 2;
						}
					} else {
						return 0;
					}
				}

			} else {
				fileSize = (Math.round(file.size * 100 / 1024) / 100)
						.toString();// + 'KB';
				if ((name.indexOf("video") > -1)) {
					if (file.type.indexOf("mp4") > -1) {
						if (fileSize <= 2100) {
							
							return 1;
						} else {
							return 2;
						}
					} else {
						return 0;
					}
				} else {
					if (file.type.indexOf("image/jpeg") > -1 || file.type.indexOf("image/png") >-1) {
//					if (file.type.indexOf("image/jpeg") > -1) {
						if (fileSize <= 2100) {
							return 1;
						} else {
							return 2;
						}
					} else {
						return 0;
					}
				}
			}
			//alert('Name: ' + file.name) ;
			//alert('Size: ' + fileSize) ;
			//alert('Type: ' + file.type) ;
			//alert("readImgURL:" + url);

		}

		$("#fileupload1").change(function() {
			readURL(this, "#img-group-1", 110, 200);
		});
		$("#fileupload2").change(function() {
			readURL(this, "#img-group-2", 110, 200);
		});
		$("#fileupload3").change(function() {
			readURL(this, "#img-group-3", 110, 200);
		});
		$("#fileupload4").change(function() {
			readURL(this, "#img-group-4", 110, 200);
		});
		$("#fileupload5").change(function() {
			readURL(this, "#video-group-1", 500, 500);
		});
	
	var totalUrl = ["", "", "", "", ""];
	var count=0;
		function uploadFile() {
			
			var checkbidNodeKind = $("#bidNodeKind").val();
			var checkbidNodeID = $("#bidNodeID").val();
			var checkbidNodeName = $("#bidNodeID :selected").text();
			//var checkbidNodeBranchName = $("#bidNodeBranchName").val();
			var checkbidNodeAccount = $("#bidNodeAccount").val();
			var checkbidNodeBranchID = $("#bidNodeBranchID").val();
			var checkbidNodeUserName = $("#bidNodeUserName").val();
			
			if(checkbidNodeKind=="" || checkbidNodeID=="" || checkbidNodeAccount=="" || checkbidNodeBranchID=="" || checkbidNodeUserName==""){
				alert("請檢查銀行帳戶資料");
				return;
			}
			
			if(checkbidNodeKind == undefined || checkbidNodeID == undefined || checkbidNodeAccount == undefined || checkbidNodeBranchID == undefined || checkbidNodeUserName == undefined){
				alert("請檢查銀行帳戶資料");
				return;
			}

			if(isNaN(checkbidNodeAccount)){
				alert("銀行帳號只能為數字");
				return;
			}
			if(isNaN(checkbidNodeBranchID)){
				alert("分行代號只能為數字");
				return;
			}
						
			for (var i = 1; i <= 4; i++) {
				//file.type
				var fd = new FormData();
				var file = document.getElementById('fileupload' + i).files[0];

				if (file == undefined) {
					alert("未選擇要上傳檔案");
					return;
				} else {

					cheakStates = checkType(file, (i != 5) ? "#img-group-" + i
							: "#video-group-1");

					if (cheakStates == 1) {

					} else if (cheakStates == 2) {
						alert("檔案太大 請重新選擇" + i);
						return;
					} else {
						alert("檔案格式不正確 請重新選擇" + i);
						return;
					}
				}

			}
			
			uploadFileToServer(1);
			//uploadFileToServer(2);
			//uploadFileToServer(3);
			//uploadFileToServer(4);
			//uploadFileToServer(5);
			//alert("end");

		}

		function uploadFileToServer(i) {
			var fd = new FormData();
			var file = document.getElementById('fileupload' + i).files[0];
			var file2 = document.getElementById('fileupload' + 2).files[0];
			var file3 = document.getElementById('fileupload' + 3).files[0];
			var file4 = document.getElementById('fileupload' + 4).files[0];
			//var file5 = document.getElementById('fileupload' + 5).files[0];
			
			var bidNodeKind = $("#bidNodeKind").val();
			var bidNodeID = $("#bidNodeID").val();
			var bidNodeName = $("#bidNodeID :selected").text();
			//var bidNodeBranchName = $("#bidNodeBranchName").val();
			var bidNodeBranchID = $("#bidNodeBranchID").val();			
			var bidNodeAccount = $("#bidNodeAccount").val();
			var bidNodeUserName = $("#bidNodeUserName").val();
			
			fd.append("file1", file);
			fd.append("file2", file2);
			fd.append("file3", file3);
			fd.append("file4", file4);
			//fd.append("file5", file5);
			
			//$.colorbox({title: "Loading, Please wait", width: "100%", height: "100%", html: "&nbsp;", fastIframe:false });
			ShowProgressBar();
			$.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/registerFile",
                data: fd,
                contentType: false,
                processData: false,
                cache: false,
                //async: false,
                /*beforeSend: function(xhr, settings) {
                    xhr.setRequestHeader("Content-Type", "multipart/form-data;boundary=gc0p4Jq0M2Yt08jU534c0p");
                    settings.data = {name: "file", file: inputElement.files[0]};                    
                },*/
                success: function (result) {  
                	//alert(result);
                	//console.log(bidNodeName);
                	//console.log(result);
                	$.ajax({
                        type: "POST",
                        url: "${pageContext.request.contextPath}/registerData",
                        data: 
                        	"bidNodeKind="+bidNodeKind+"&bidNodeID="+bidNodeID+"&bidNodeName="+bidNodeName+"&bidNodeAccount="+bidNodeAccount+"&bidNodeBranchID="+bidNodeBranchID+"&bidNodeUserName="+bidNodeUserName,                        		
                        	contentType: "application/x-www-form-urlencoded; charset=UTF-8", 
                        processData: false,
                        cache: false,
                        success: function (result) {  
                        	//alert(result);
                        	//console.log(result);
                        	HideProgressBar(); 
                        	$(".register")
							.load(
									"${pageContext.request.contextPath}/registerUser4 .register-content");
                            return ;
                        },
                        error: function (result) {
                            //console.log(result);
                            HideProgressBar(); 
                            return ;
                        } 
                    });
                	
                    
                    return ;
                },
                error: function (result) {
                    console.log(result.responseText);
                    HideProgressBar(); 
                    return ;
                },
                xhr: function(){//進度表
                    var xhr = $.ajaxSettings.xhr();
                    if(onprogress && xhr.upload) {
                        xhr.upload.addEventListener("progress" , onprogress, false);
                        return xhr;
                    }
                    
                },complete:function(){
                	HideProgressBar(); 
                }, 
            });
				
			/***
			fd.append("file", file);
			//fd.append("fileName", file.name);
			//fd.append("fileupload", i);
			var xhr = new XMLHttpRequest();
			xhr.upload.addEventListener("progress", uploadProgress, false);
			xhr.addEventListener("load", uploadComplete, false);
			xhr.addEventListener("error", uploadFailed, false);
			xhr.addEventListener("abort", uploadCanceled, false);
			xhr.open("POST", "${pageContext.request.contextPath}/registerFile");
			xhr.onreadystatechange = function() {
				if (this.readyState == 4) {
					//直接接收是有規律的字符串
					alert(this.responseText);
					//var strValue = this.responseText.split("?");
					totalUrl[i] = this.responseText;
					console.log(totalUrl);
					//return this.responseText;
					//var json=eval('('+this.responseText+')');
					count++;
					console.log("count:"+count);
					if(count==5){
						console.log("end count:"+count);
					}
				}
			}
			xhr.send(fd);
			*/
		}

		$("#bidNodeKind").on('change',function(){
		     $.ajax({
					type : "POST",
					url : "${pageContext.request.contextPath}/registerGetBankList",
					data : "type=" + $(this).val(),
					processData : false,
					cache : false,
					success : function(result) {
						$("#bidNodeID option").remove();
						$("#bidNodeID").append("<option value=''>請選擇 </option>");
						for(var i=0; i<result.length; i++){						
							var monthStr = "<option value="+result[i].id.code+">" + result[i].description1 + "</option>";
			                $("#bidNodeID").append(monthStr);
							//<select id="bidNodeID" name="bidNodeID" class="module-customselect-selected inp_right01">
							//<option value="" ${Account.bidNodeKind == null?"selected":""}>請選擇</option>
						}
						return;
					},
					error : function(result) {
						//console.log(result);
						
						return;
					}
				});
		});

		 
		function uploadDataToServer(i) {
			var file1 = $("#img-group-1").attr("src");
			var file2 = $("#img-group-2").attr("src");
			var file3 = $("#img-group-3").attr("src");
			var file4 = $("#img-group-4").attr("src");
			//var file5 = $("#video-group-1").attr("src");
			
			//console.log("file1:"+file1);
			//console.log("file2:"+file2);
			//console.log("file3:"+file3);
			//console.log("file4:"+file4);
			//console.log("file5:"+file5);
			var bidNodeKind = $("#bidNodeKind").val();
			var bidNodeID = $("#bidNodeID").val();
			var bidNodeName = $("#bidNodeName").text();
			//var bidNodeBranchName = $("#bidNodeBranchName").val();
			var bidNodeAccount = $("#bidNodeAccount").val();
			var bidNodeBranchID = $("#bidNodeBranchID").val();
			var bidNodeUserName = $("#bidNodeUserName").val();
			//$.colorbox({title: "Loading, Please wait", width: "100%", height: "100%", html: "&nbsp;", fastIframe:false });
			ShowProgressBar();
			$.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/registerSave",
                data: "file1="+file1+"&file2="+file2+"&file3="+file3+"&file4="+file4+"&file5="+file5+"&bidNodeKind="+bidNodeKind+"&bidNodeID="+bidNodeID+"&bidNodeName="+bidNodeName+"&bidNodeAccount="+bidNodeAccount+"&bidNodeUserName="+bidNodeUserName,                        		
            	contentType: "application/x-www-form-urlencoded; charset=UTF-8", 
                processData: false,
                cache: false,
                //async: false,
                /*beforeSend: function(xhr, settings) {
                    xhr.setRequestHeader("Content-Type", "multipart/form-data;boundary=gc0p4Jq0M2Yt08jU534c0p");
                    settings.data = {name: "file", file: inputElement.files[0]};                    
                },*/
                success: function (result) {  
                	//alert(result);
                	console.log(result);
 					//alert("成功");
 					window.location.href='${pageContext.request.contextPath}/index';
                    return ;
                },
                error: function (result) {
                    console.log(result.responseText);
                    HideProgressBar(); 
                    return ;
                },
                xhr: function(){//進度表
                    var xhr = $.ajaxSettings.xhr();
                    if(onprogress && xhr.upload) {
                        xhr.upload.addEventListener("progress" , onprogress, false);
                        return xhr;
                    }
                    
                },complete:function(){
                	HideProgressBar(); 
                }, 
            });
			
		}
		/**
	     *    侦查附件上传情况    ,这个方法大概0.05-0.1秒执行一次
	     */
	     function onprogress(evt){
	        var loaded = evt.loaded;                  //已经上传大小情况 
	        var tot = evt.total;                      //附件总大小
	        var per = Math.floor(100*loaded/tot);      //已经上传的百分比          $("#son").html( per +"%" );
	        //$(".nav4").css("width" , per +"%");
	        if(per > 99){
				per = 99;
			}
	        $(".nav5").text(per +"%");
	        $(".nav5").css("width" , per +"%");
	        
	        /**
		     *    for(var i=value; value < per; value++){
		     *    		$(".nav5").css("width" , per +"%");
		     *    }
		     */
	      
	    }

		function uploadProgress(evt) {
			if (evt.lengthComputable) {
				var percentComplete = Math.round(evt.loaded * 100 / evt.total);
				document.getElementById('progressNumber').innerHTML = percentComplete
						.toString()
						+ '%';
			} else {
				document.getElementById('progressNumber').innerHTML = 'unable to compute';
			}
		}

		function uploadComplete(evt) {
			/* This event is raised when the server send back a response */
			//alert(evt.target.responseText);
		}

		function uploadFailed(evt) {
			alert("There was an error attempting to upload the file.");
		}

		function uploadCanceled(evt) {
			alert("The upload has been canceled by the user or the browser dropped the connection.");
		}
		
		
		// 顯示讀取遮罩
		function ShowProgressBar() {
		    displayProgress();
		    displayMaskFrame();
		}

		// 隱藏讀取遮罩
		function HideProgressBar() {
		    var progress = $('#divProgress');
		    var maskFrame = $("#divMaskFrame");
		    progress.hide();
		    maskFrame.hide();
		}
		// 顯示讀取畫面
		function displayProgress() {
		    var w = $(document).width();
		    var h = $(window).height();
		    var progress = $('#divProgress');
		    progress.css({ "z-index": 999999, "top": (h / 2) - (progress.height() / 2), "left": (w / 2) - (progress.width() / 2) });
		    progress.show();
		}
		// 顯示遮罩畫面
		function displayMaskFrame() {
		    var w = $(window).width();
		    var h = $(document).height();
		    var maskFrame = $("#divMaskFrame");
		    maskFrame.css({ "z-index": 999998, "opacity": 0.7, "width": w, "height": h });
		    maskFrame.show();
		}
		    
    </script>
<!--  
<button id="demo8">Run</button>
-->    
<div id="divProgress" style="text-align:center; display: none; position: fixed; top: 50%;  left: 50%;" >
    <img id="img-loading" src="${pageContext.request.contextPath}/resource/assets/images/loading.gif" />
    <br />
    <font color="#1B3563" size="2px">資料處理中</font>
    <div class="nav_rate">
		<div class="nav_rate2 nav5">&nbsp;</div>
	</div>
</div>
<div id="divMaskFrame" style="background-color: #F2F4F7; display: none; left: 0px;
    position: absolute; top: 0px;">
</div>
</body>
</html>