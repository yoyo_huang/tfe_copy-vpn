<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="Title" content="TFE"/>
    <meta name="Author" content="TFE"/>
    <meta name="Description" content="Description"/>
    <!-- iOS -->
    <!-- Android -->
    <meta property="og:title" content="Hconnect Mobile"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content=""/>
    <meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/share.jpg"/>
    <meta property="og:site_name" content="Hconnect"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:locality" content="Taipei"/>
    <meta property="og:country-name" content="Taiwan"/>
    <link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon"
          type="image/x-icon"/>
    <title>TFE</title>
    <script type="text/javascript">
        document.createElement('header');
        document.createElement('nav');
        document.createElement('menu');
        document.createElement('section');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('footer');
    </script>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
          media="screen" rel="stylesheet"
          type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/member.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <style>
        .thumb {
        / / height: 110 px;
            width: 180px;
            border: 1px solid #000;
            margin: 10px 5px 0 0;
        }

        #cboxTitle {
            bottom: -19px;
            color: white;
        }
    </style>
    <style>
        select {
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            padding: 2px 30px 2px 2px;
            border: none;
        }

        .status_brank select {
            width: auto;
        }

        #cboxContent {
            padding-top: 0;
        }

        #cboxClose {
            display: none;
        }

        .status_btn2 {
            text-align: center;
        }

        .status_photo {
            width: 80%;
            margin: 0 auto;
            margin-top: 30px;
        }

        /* line 694, ../../sass/units/member.scss */
        .status_photo .p1 {
            border-right: 1px solid #ddd;
            width: 151px;
            color: #4d4d4d;
        }

        .status_photo .p2 {
            float: left;
            margin-left: 5%;
            width: 230px;
            color: #4d4d4d;
        }

        /* line 707, ../../sass/units/member.scss */
        .status_photo .p3 {
            border-right: 1px solid #ddd;
            width: 130px;
            color: #4d4d4d;
        }

        .status_photo .p4 {
            float: left;
            width: 15%;
            margin-left: 2%;
            color: #4d4d4d;
        }

        .email2_button {
            text-align: center;
            display: inline-block;
            line-height: 50px;
            padding-left: 1.5em;
            padding-right: 1.5em;
            color: #fff;
            background-color: #ed842f;
            cursor: pointer;
            border-radius: 25px;
            background-repeat: repeat-x;

            font-family: "Helvetica Neue", Helvetica, Arial, Chinese, "微軟正黑體", sans-serif;
        }


    </style>

    <script type="text/javascript">

        function dump_props(obj, objName) {
            var result = "";

            for (var i in obj) {
                try {
                    result += "<tr><td>" + objName + "." + i + "</td><td>" + obj[i] + "</td></tr>";
                }
                catch (err) {
                    result += "<tr><td>" + objName + "." + i + "</td><td>" + err + "</td></tr>";
                }
            }

            return result;
        }

        var GO_DEBUG = false;
        function openDebug(obj, objName) {
            if (GO_DEBUG) {
                debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
                debugWindow.document.write("<HEAD><TITLE>Message window</TITLE></HEAD>");
                debugWindow.document.write("<Body><Table border='1'>" + dump_props(obj, objName) + "</Table></Body>");
            }
        }

        function openDebugText(_text) {
            if (GO_DEBUG) { //alert("debug");
                debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
                debugWindow.document.write(_text);
            }
        }


        function openDebugTextNewLine(_text) {
            if (GO_DEBUG) { //alert("debug");
                debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
                var re = /\&/g;
                var result = _text.replace(re, '<br/>&');
                debugWindow.document.write(result + '<br/>');
            }
        }


        var submitNotStart = true;

        /********************************progress bar start*******************************************/
        // 顯示讀取遮罩
        function ShowProgressBar() {
            displayProgress();
            displayMaskFrame();
        }

        // 隱藏讀取遮罩
        function HideProgressBar() {
            var progress = $('#divProgress');
            var maskFrame = $("#divMaskFrame");
            progress.hide();
            maskFrame.hide();
        }
        // 顯示讀取畫面
        function displayProgress() {
            var w = $(document).width();
            var h = $(window).height();
            var progress = $('#divProgress');
            progress.css({
                "z-index": 999999,
                "top": (h / 2) - (progress.height() / 2),
                "left": (w / 2) - (progress.width() / 2)
            });
            progress.show();
        }
        // 顯示遮罩畫面
        function displayMaskFrame() {
            var w = $(window).width();
            var h = $(document).height();
            var maskFrame = $("#divMaskFrame");
            maskFrame.css({"z-index": 999998, "opacity": 0.7, "width": w, "height": h});
            maskFrame.show();
        }
        /********************************progress bar end*********************************************/
        function doSubmit() {
//            alert("test==>"+submitNotStart);
            if (submitNotStart) {
                submitNotStart = false;
                var question1Answer = $("#question1Answer").val();
                var question2Answer = $("#question2Answer").val();
                var question3Answer = $("#question3Answer").val();
               
                console.log('question1 answer:' + $("#question1Answer").val());
                console.log('question2 answer:' + $("#question2Answer").val());
                console.log('question3 answer:' + $("#question3Answer").val());
                console.log('workingYear answer:' + $("#workingYear").val());
                console.log("depositBookInnerPageDeleteFileArray size:" + depositBookInnerPageDeleteFileArray.length);
                console.log("idCardFrontDeleteFileArray size:" + idCardFrontDeleteFileArray.length);// jim
                console.log("idCardBackDeleteFileArray size:" + idCardBackDeleteFileArray.length);// jim
                console.log("id2ndCardDeleteFileArray size:" + id2ndCardDeleteFileArray.length);// jim
                console.log("bankBookFrontDeleteFileArray size:" + bankBookFrontDeleteFileArray.length);// jim
                console.log("houseHoldregistrationTranscriptImageDeleteFileArray size:" + houseHoldregistrationTranscriptImageDeleteFileArray.length);
                console.log("personalPropertyDeleteFileArray size:" + personalPropertyDeleteFileArray.length);
                console.log("realPropertyDeleteFileArray size:" + realPropertyDeleteFileArray.length);
                console.log("anotherIdentifyDocumentDeleteFileArray size:" + anotherIdentifyDocumentDeleteFileArray.length);
                console.log("studentIdentificationImageDeleteFileArray size:" + studentIdentificationImageDeleteFileArray.length);
                console.log("scoreImageDeleteFileArray size:" + scoreImageDeleteFileArray.length);

                $('#' + 'depositBookInnerPage' + 'DeleteFileArray').val(depositBookInnerPageDeleteFileArray);
                $('#' + 'idCardFront' + 'DeleteFileArray').val(idCardFrontDeleteFileArray);//jim
                $('#' + 'idCardBack' + 'DeleteFileArray').val(idCardBackDeleteFileArray);//jim
                $('#' + 'id2ndCard' + 'DeleteFileArray').val(id2ndCardDeleteFileArray);//jim
                $('#' + 'bankBookFront' + 'DeleteFileArray').val(bankBookFrontDeleteFileArray);//jim
                $('#' + 'houseHoldregistrationTranscriptImage' + 'DeleteFileArray').val(houseHoldregistrationTranscriptImageDeleteFileArray);
                $('#' + 'personalProperty' + 'DeleteFileArray').val(personalPropertyDeleteFileArray);
                $('#' + 'realProperty' + 'DeleteFileArray').val(realPropertyDeleteFileArray);
                $('#' + 'anotherIdentifyDocument' + 'DeleteFileArray').val(anotherIdentifyDocumentDeleteFileArray);
                $('#' + 'studentIdentificationImage' + 'DeleteFileArray').val(studentIdentificationImageDeleteFileArray);
                $('#' + 'scoreImage' + 'DeleteFileArray').val(scoreImageDeleteFileArray);
                console.log("$('#'+'depositBookInnerPage'+'DeleteFileArray').val():" + $('#' + 'depositBookInnerPage' + 'DeleteFileArray').val());
                console.log("$('#'+'idCardFront'+'DeleteFileArray').val():" + $('#' + 'idCardFront' + 'DeleteFileArray').val());//jim
                console.log("$('#'+'idCardBack'+'DeleteFileArray').val():" + $('#' + 'idCardBack' + 'DeleteFileArray').val());//jim
                console.log("$('#'+'id2ndCard'+'DeleteFileArray').val():" + $('#' + 'id2ndCard' + 'DeleteFileArray').val());//jim
                console.log("$('#'+'bankBookFront'+'DeleteFileArray').val():" + $('#' + 'bankBookFront' + 'DeleteFileArray').val());//jim
                console.log("$('#'+'houseHoldregistrationTranscriptImage'+'DeleteFileArray').val():" + $('#' + 'houseHoldregistrationTranscriptImage' + 'DeleteFileArray').val());
                console.log("$('#'+'personalProperty'+'DeleteFileArray').val():" + $('#' + 'personalProperty' + 'DeleteFileArray').val());
                console.log("$('#'+'realProperty'+'DeleteFileArray').val():" + $('#' + 'realProperty' + 'DeleteFileArray').val());
                console.log("$('#'+'anotherIdentifyDocument'+'DeleteFileArray').val():" + $('#' + 'anotherIdentifyDocument' + 'DeleteFileArray').val());
                console.log("$('#'+'studentIdentificationImage'+'DeleteFileArray').val():" + $('#' + 'studentIdentificationImage' + 'DeleteFileArray').val());
                console.log("$('#'+'scoreImage'+'DeleteFileArray').val():" + $('#' + 'scoreImage' + 'DeleteFileArray').val());

                if (checkSubmitValue()) {
                    ShowProgressBar();
                    $('#file-form').submit();
                } else {
                    submitNotStart = true;
                    return false;
                }
            }
            return false;
        }

        function checkSubmitValue() {
//            openDebug($('#file-form'), "formObj");
//            openDebug($('#contactName'), "contactName");
//            alert("checkSubmitValue in");
//            openDebug($('#file-form').validationEngine(),"validationEngine");

            //請選擇必要上傳檔案
            var question1Answer = $("#question1Answer").val().trim();
            var question2Answer = $("#question2Answer").val().trim();
            var question3Answer = $("#question3Answer").val().trim();
			var workingYearAnswer = $("#workingYear").val().trim();
			var HasNotAnswer = false;
            if (question1Answer == '1' || question1Answer == '3') 
            {               	         
				if (question2Answer == '' || question3Answer == '') 
				{				    
				    HasNotAnswer = true;				    
				}
            } 
        	else if (question1Answer == '' || question2Answer == '')
            {                 
				HasNotAnswer = true;                
            }
            
            if(!HasNotAnswer && 
            	(question1Answer == '1' || question1Answer == '2') && workingYearAnswer == '')
           	{
            	HasNotAnswer = true;            	
            }
        	
            if(HasNotAnswer)
           	{
            	alert("請選擇需要回答的問題");
                return false;           
           	}


            var idCardFront = $('#' + 'idCardFront' + 'UploadButtonId').val();//jim
            var idCardBack = $('#' + 'idCardBack' + 'UploadButtonId').val();//jim
            var id2ndCard = $('#' + 'id2ndCard' + 'UploadButtonId').val();//jim
            var bankBookFront = $('#' + 'bankBookFront' + 'UploadButtonId').val();//jim

            console.log("1 idCardFront=\n" + idCardFront);
            console.log("2 idCardFront=\n" + $('#' + 'idCardFront' + 'Orignal').val());
            idCardFront = (idCardFront != '') ? idCardFront : $('#' + 'idCardFront' + 'Orignal').val();//jim
            idCardBack = (idCardBack != '') ? idCardBack : $('#' + 'idCardBack' + 'Orignal').val();//jim
            id2ndCard = (id2ndCard != '') ? id2ndCard : $('#' + 'id2ndCard' + 'Orignal').val();//jim
            bankBookFront = (bankBookFront != '') ? bankBookFront : $('#' + 'bankBookFront' + 'Orignal').val();//jim

            var depositBookInnerPage = $('#' + 'depositBookInnerPage' + 'UploadButtonId').val();
            var houseHoldregistrationTranscriptImage = $('#' + 'houseHoldregistrationTranscriptImage' + 'UploadButtonId').val();
            var personalProperty = $('#' + 'personalProperty' + 'UploadButtonId').val();
            var realProperty = $('#' + 'realProperty' + 'UploadButtonId').val();
            var anotherIdentifyDocument = $('#' + 'anotherIdentifyDocument' + 'UploadButtonId').val();
            var studentIdentificationImage = $('#' + 'studentIdentificationImage' + 'UploadButtonId').val();
            var scoreImage = $('#' + 'scoreImage' + 'UploadButtonId').val();

            console.log("1 depositBookInnerPage=\n" + depositBookInnerPage);
            console.log("2 depositBookInnerPage=\n" + $('#' + 'depositBookInnerPage' + 'Orignal').val());
            depositBookInnerPage = (depositBookInnerPage != '') ? depositBookInnerPage : $('#' + 'depositBookInnerPage' + 'Orignal').val();//jim
            houseHoldregistrationTranscriptImage = (houseHoldregistrationTranscriptImage != '') ? houseHoldregistrationTranscriptImage : $('#' + 'houseHoldregistrationTranscriptImage' + 'Orignal').val();//jim
            personalProperty = (personalProperty != '') ? personalProperty : $('#' + 'personalProperty' + 'Orignal').val();//jim
            realProperty = (realProperty != '') ? realProperty : $('#' + 'realProperty' + 'Orignal').val();//jim
            anotherIdentifyDocument = (anotherIdentifyDocument != '') ? anotherIdentifyDocument : $('#' + 'anotherIdentifyDocument' + 'Orignal').val();//jim
            studentIdentificationImage = (studentIdentificationImage != '') ? studentIdentificationImage : $('#' + 'studentIdentificationImage' + 'Orignal').val();//jim
            scoreImage = (scoreImage != '') ? scoreImage : $('#' + 'scoreImage' + 'Orignal').val();//jim

            console.log("question1Answer=" + question1Answer);
            console.log("idCardFront=\n" + idCardFront);//jim
            console.log("id2ndCard=\n" + id2ndCard);//jim
            console.log("bankBookFront=\n" + bankBookFront);//jim
            console.log("idCardBack=\n" + idCardBack);//jim
            console.log("depositBookInnerPage=\n" + depositBookInnerPage);
            console.log("houseHoldregistrationTranscriptImage=\n" + houseHoldregistrationTranscriptImage);
            console.log("personalProperty=\n" + personalProperty);
            console.log("realProperty=\n" + realProperty);
            console.log("anotherIdentifyDocument=\n" + anotherIdentifyDocument);
            console.log("studentIdentificationImage=\n" + studentIdentificationImage);
            console.log("scoreImage=\n" + scoreImage);


            if (question1Answer == '1') {
                if (!checkImageRequired(idCardFront, "身份證正面"))return false;//jim
                if (!checkImageRequired(idCardBack, "身份證反面"))return false;//jim
                if (!checkImageRequired(id2ndCard, "第二證件"))return false;//jim
                if (!checkImageRequired(bankBookFront, "存摺封面或提款卡"))return false;//jim
                if (!checkImageRequired(depositBookInnerPage, "存摺內頁"))return false;
                if (!checkImageRequired(anotherIdentifyDocument, "扣繳憑單"))return false;
//                if (depositBookInnerPage == '') {
//                    alert('請選擇必要上傳檔案,存摺內頁');
//                    return false;
//                }
            } else if (question1Answer == '2') {
                if (!checkImageRequired(idCardFront, "身份證正面"))return false;//jim
                if (!checkImageRequired(idCardBack, "身份證反面"))return false;//jim
                if (!checkImageRequired(id2ndCard, "第二證件"))return false;//jim
                if (!checkImageRequired(bankBookFront, "存摺封面或提款卡"))return false;//jim
                if (!checkImageRequired(depositBookInnerPage, "存摺內頁"))return false;
//                if (depositBookInnerPage == '') {
//                    alert('請選擇必要上傳檔案,存摺內頁');
//                    return false;
//                }
            } else if (question1Answer == '3') {
                if (!checkImageRequired(idCardFront, "身份證正面"))return false;//jim
                if (!checkImageRequired(idCardBack, "身份證反面"))return false;//jim
                if (!checkImageRequired(id2ndCard, "第二證件"))return false;//jim
                if (!checkImageRequired(bankBookFront, "存摺封面或提款卡"))return false;//jim
                if (!checkImageRequired(depositBookInnerPage, "存摺內頁"))return false;
                if (!checkImageRequired(houseHoldregistrationTranscriptImage, "戶口名簿影像"))return false;
//                if (depositBookInnerPage == '') {
//                    alert('請選擇必要上傳檔案,存摺內頁');
//                    return false;
//                }
//                if (houseHoldregistrationTranscriptImage == '') {
//                    alert('請選擇必要上傳檔案,戶口名簿影像');
//                    return false;
//                }
            } else if (question1Answer == '4') {
                if (!checkImageRequired(idCardFront, "身份證正面"))return false;//jim
                if (!checkImageRequired(idCardBack, "身份證反面"))return false;//jim
                if (!checkImageRequired(id2ndCard, "第二證件"))return false;//jim
                if (!checkImageRequired(bankBookFront, "存摺封面或提款卡"))return false;//jim
                if (!checkImageRequired(depositBookInnerPage, "存摺內頁"))return false;
                if (!checkImageRequired(studentIdentificationImage, "學生證影像"))return false;
                if (!checkImageRequired(scoreImage, "成績單影像"))return false;
//                if (depositBookInnerPage == '') {
//                    alert('請選擇必要上傳檔案,存摺內頁');
//                    return false;
//                }
//                if (studentIdentificationImage == '') {
//                    alert('請選擇必要上傳檔案,學生證影像');
//                    return false;
//                }
//                if (scoreImage == '') {
//                    alert('請選擇必要上傳檔案,成績單影像');
//                    return false;
//                }
            }

            return true;

        }

        function checkImageRequired(fieldsValue, displayName) {
            openDebugTextNewLine(displayName + " : [" + fieldsValue + "]")
            if (fieldsValue == '') {
                alert('請選擇必要上傳檔案,' + displayName);
                return false;
            }
            return true;
        }

        // 	function getFileExtension(filename) {
        // 		return filename.split('.').pop();
        // 	}
    </script>

    <fmt:parseNumber var="credStatus" type="number" value="${TFEACCCD.credStatus}"/>
    <fmt:parseNumber var="id1Status" type="number" value="${TFEACCCD.id1Status}"/>
    <fmt:parseNumber var="id2Status" type="number" value="${TFEACCCD.id2Status}"/>
    <fmt:parseNumber var="cardStatus" type="number" value="${TFEACCCD.cardStatus}"/>
    <fmt:parseNumber var="poscrStatus" type="number" value="${TFEACCCD.poscrStatus}"/>

    <%--<fmt:parseNumber var="credStatus" type="number" value="${TFEACCCD.credStatus}"/>--%>
    <fmt:parseNumber var="whvrStatus" type="number" value="${TFEACCCD.whvrStatus}"/>
    <fmt:parseNumber var="crrpeStatus" type="number" value="${TFEACCCD.crrpeStatus}"/>
    <fmt:parseNumber var="whvreStatus" type="number" value="${TFEACCCD.whvreStatus}"/>
    <fmt:parseNumber var="posinStatus" type="number" value="${TFEACCCD.posinStatus}"/>
    <fmt:parseNumber var="mopStatus" type="number" value="${TFEACCCD.mopStatus}"/>
    <fmt:parseNumber var="otpStatus" type="number" value="${TFEACCCD.otpStatus}"/>

</head>
<body>
<!-- 201*110 -->
<div class="layout-view">
    <section class="section-banner" style="height: 0;"></section>
    <!--subsection start-->
    <!--subsection end-->
    <section class="section-content">
        <div class="layout-mw-wrapper">
            <div class="register-content">
                <center>
                    <table width="0" border="0" align="center">
                        <tr>
                            <td><img src="${pageContext.request.contextPath}/resource/assets/images/submit_01.gif"
                                     alt="" width="500" height="100"></td>
                        </tr>
                    </table>
                </center>

                <form method="POST" action="${pageContext.request.contextPath}/credit/uploadCreditFiles?id=${id}"
                      id="file-form" name="FileForm" enctype="multipart/form-data">
                    <input id="idCardFrontOrignal" type="hidden" name="idCardFrontFileOrignal"
                           value="${account.adImg7!= null? account.adImg7:""}">
                    <input id="idCardBackOrignal" type="hidden" name="idCardBackFileOrignal"
                           value="${account.adImg8!= null? account.adImg8:""}">
                    <input id="id2ndCardOrignal" type="hidden" name="id2ndCardFileOrignal"
                           value="${account.adImg9!= null? account.adImg9:""}">
                    <input id="bankBookFrontOrignal" type="hidden" name="bankBookFrontFileOrignal"
                           value="${account.adImg3!= null? account.adImg3:""}">
                    <input id="depositBookInnerPageOrignal" type="hidden" name="depositBookInnerPageFileOrignal"
                           value="${account.adImg4!= null? account.adImg4:""}">
                    <input id="houseHoldregistrationTranscriptImageOrignal" type="hidden"
                           name="houseHoldregistrationTranscriptImageFileOrignal"
                           value="${account.adImg10!= null? account.adImg10:""}">
                    <input id="personalPropertyOrignal" type="hidden" name="personalPropertyFileOrignal"
                           value="${account.adImg5!= null? account.adImg5:""}">
                    <input id="realPropertyOrignal" type="hidden" name="realPropertyFileOrignal"
                           value="${account.adImg13!= null? account.adImg13:""}">
                    <input id="studentIdentificationImageOrignal" type="hidden"
                           name="studentIdentificationImageFileOrignal"
                           value="${account.adImg11!= null? account.adImg11:""}">
                    <input id="scoreImageOrignal" type="hidden" name="scoreImageFileOrignal"
                           value="${account.adImg12!= null? account.adImg12:""}">
                    <input id="anotherIdentifyDocumentOrignal" type="hidden" name="anotherIdentifyDocumentFileOrignal"
                           value="${account.adImg6!= null? account.adImg6:(account.adImg1!= null? account.adImg1:"")}">


                    <div class="register">
                        <%--<div class="register-nav Menber_nav">--%>
                        <%--<ul class="clearfix">--%>
                        <%--<li class="current_btn current">信用額度資料建立</li>--%>
                        <%--<li class="hidd">申請完成</li>--%>
                        <%--</ul>--%>
                        <%--<div class="nav_rate">--%>
                        <%--<div class="nav_rate2 nav3">&nbsp;</div>--%>
                        <%--</div>--%>
                        <%--</div>--%>
                        <div class="re_status">
                            <div class="status_t1">請根據收入狀況選擇您的身份</div>
                            <div class="status_brank">
                                <div class="clearfix b4_form">
                                    <div class="module-customselect b4_select inp01 ">
											<span class="module-customselect-typename inp_left01">
												<font style="color: red;">*</font>
												第一個問題												
											</span>
                                        <span class="module-customselect-selected inp_right01">-你的身分-</span>
                                        <input id="question1Answer" name="question1Answer" type="hidden" value="">
                                        <ul id="question1ul">
                                            <li id="question1ul1" value="1">受薪</li>
                                            <li id="question1ul2" value="2">非受薪(攤商.自營工作室及店面等)</li>
                                            <li id="question1ul3" value="3">其他投資人(股票族.房東或家庭主婦等)</li>
                                            <li id="question1ul4" value="4">學生</li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="question2div" class="clearfix b4_form" style="display: none;">
                                    <div class="module-customselect b4_select inp01">
											<span id="question2titlespan" class="module-customselect-typename inp_left01">
											    <font style="color: red;">*</font>
												第二個問題												
											</span>
                                        <span id="question2selectText" class="module-customselect-selected inp_right01"
                                              style="background: none;">-請選擇-</span>
                                        <input id="question2Answer" style="font-size:15px" name="question2Answer"
                                               type="hidden" value="">
                                        <ul id="question2ul" style="overflow: auto" />                                        
                                    </div>
                                </div>
                                <div id="question3div" class="clearfix b4_form" style="display: none;">
                                    <div class="module-customselect b4_select inp01">
											<span id="question3titlespan" class="module-customselect-typename inp_left01">
												<font style="color: red;">*</font>
												第三個問題												
											</span>
                                        <span id="question3selectText" class="module-customselect-selected inp_right01">-請選擇-</span>
                                        <input id="question3Answer" name="question3Answer" type="hidden" value="">
                                        <ul id="question3ul" style="overflow: auto" />                                        
                                    </div>
                                </div>
                                <div id="workingYeardiv" class="clearfix b4_form" style="display: none;">
                                    <div class="module-customselect b4_select inp01">
											<span id="workingYeartitlespan" class="module-customselect-typename inp_left01">
												<font style="color: red;">*</font>
												  年資												
											</span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <input type="text" id="workingYear" name="workingYear"
                                                   maxlength="3" placeholder="年資" style="font-size:15px"
                                                   class="text-input validate[required]"
                                                   value='${account.workingYear != null?account.workingYear:""}'/>
                                        </span>
                                    </div>
                                </div>
                                <div class="clear"></div>

                                <div class="register_status2" style="display: none;">
                                    <div class="clearfix b4_form">
                                        <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            	市內電話</span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <input type="text" id="tel" name="tel"
                                                   maxlength="15" placeholder="市內電話" style="font-size:15px"
                                                   value='${account.tel != null?account.tel:""}'/>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="clearfix b4_form">
                                        <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>聯絡人</span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <input class="text-input validate[required,custom[chinese]]"
                                                   placeholder="聯絡人"
                                                   type="text" id="contactName" name="contactName" maxlength="20"
                                                   style="font-size:15px"
                                                   value='${account.contactName != null?account.contactName:""}'/>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="clearfix b4_form">
                                        <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>關係
                                        </span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <select id="realation" name="realation" class="validate[required]"
                                                    style="width: 380px; font-size:15px">
                                                <option value="" ${account.realation == null?"selected":""}>請選擇</option>
                                                <option value="1" ${account.realation==1?"selected":""}>親戚</option>
                                                <option value="2" ${account.realation==2?"selected":""}>同事</option>
                                                <option value="3" ${account.realation==3?"selected":""}>朋友</option>
                                                <option value="4" ${account.realation==4?"selected":""}>其他</option>
                                            </select>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="clearfix b4_form">
                                        <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>聯絡人電話</span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <input type="text" id="contactTel" name="contactTel" maxlength="15"
                                                   class="text-input validate[required]" placeholder="聯絡人電話"
                                                   style="font-size:15px"
                                                   value='${account.contactTel != null?account.contactTel:""}'/>
                                        </span>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <%--***********************************New Add 出金 Start **************************************************--%>

                            <div class="register_status" style="border-top:0;display:none;">
                                <dl class="clearfix status_txt">
                                    <dt>
                                        <img src="${pageContext.request.contextPath}/resource/assets/images/s3.png">
                                    </dt>
                                    <dd>
                                        <h1>使用者身份的真實性可確保大家的資金安全，因此我們需要您上傳雙證件電子檔，作為您本人意願及線上身份確認之用，請注意：</h1>
                                        <h2>
                                            <span class="gray">●</span> 第二證件可以是健保卡、駕照、護照等附照片之證件，
                                            <span class="gray">（上傳檔案可以是手機拍攝的影像檔或掃描檔）</span>
                                        </h2>
                                    </dd>
                                </dl>
                            </div>
                            <div class="register_status" style="display:none;">
                                <ul class="clearfix status_photo">
                                    <li class="p1"><font color="#CC0000">*</font>身份證正面
                                        <div class="line W-hide"></div>
                                        <br/><font color="#CC0000">影像需清晰</font>
                                    </li>
                                    <li class="p2">
                                        <output id="idCardFrontImageList"></output>
                                        <%--'${account.adImg7}'--%>
                                    </li>
                                    <li class="p3">
                                        <div class="button">
                                            <div class="email_button status_button">
                                                <input id="idCardFrontUploadButtonId" type="file" style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;"
                                                       name="idCardFrontFile" accept="image/jpeg|image/png">
                                                <a onclick="$('input[id=idCardFrontUploadButtonId]').click();">選擇檔案</a>
                                            </div>
                                        </div>
                                        <div class="button">
                                            <div class="email_button status_button  ">
                                                <a id="idCardFrontPreviewId">編輯檔案 </a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="p4"><font color="red">${id1Status!=2?TFEACCCD.id1Remark:""}</font>
                                    </li>
                                </ul>
                                <ul class="clearfix status_photo">
                                    <li class="p1"><font color="#CC0000">*</font>身份證反面
                                        <div class="line W-hide"></div>
                                        <br/><font color="#CC0000">影像需清晰</font>
                                    </li>
                                    <li class="p2">
                                        <div id="files2" class="files2"></div>
                                        <output id="idCardBackImageList"></output>
                                        <%--'${account.adImg8}'--%>
                                    </li>
                                    <li class="p3">
                                        <div class="button">
                                            <div class="email_button status_button">
                                                <input id="idCardBackUploadButtonId" type="file" style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;"
                                                       name="idCardBackFile" accept="image/jpeg|image/png">
                                                <a onclick="$('input[id=idCardBackUploadButtonId]').click();">選擇檔案</a>
                                            </div>
                                        </div>
                                        <div class="button">
                                            <div class="email_button status_button  ">
                                                <a id="idCardBackPreviewId">編輯檔案 </a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="p4"><font color="red">${id2Status!=2?TFEACCCD.id2Remark:""}</font></li>
                                </ul>

                                <ul class="clearfix status_photo">
                                    <li class="p1"><font color="#CC0000">*</font>第二證件
                                        <div class="line W-hide"></div>
                                        <br/><font color="#CC0000">影像需清晰</font>
                                    </li>
                                    <li class="p2">
                                        <div id="files3" class="files3"></div>
                                        <output id="id2ndCardImageList"></output>
                                        <%--'${account.adImg9}'--%>
                                    </li>
                                    <li class="p3">
                                        <div class="button">
                                            <div class="email_button status_button">
                                                <input id="id2ndCardUploadButtonId" type="file" style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;"
                                                       name="id2ndCardFile" accept="image/jpeg|image/png">
                                                <a onclick="$('input[id=id2ndCardUploadButtonId]').click();">選擇檔案</a>
                                            </div>
                                        </div>
                                        <div class="button">
                                            <div class="email_button status_button  ">
                                                <a id="id2ndCardPreviewId">編輯檔案 </a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="p4"><font color="red">${cardStatus!=2?TFEACCCD.cardRemark:""}</font>
                                    </li>
                                </ul>
                            </div>

                            <div class="register_status" style="display: none;">
                                <dl class="clearfix status_txt">
                                    <dt>
                                        <img src="${pageContext.request.contextPath}/resource/assets/images/s2.png">
                                    </dt>
                                    <dd>
                                        <h1>我們需要您提供銀行帳戶資料及存摺封面電子檔做平台資金確認之用， 請務必填寫本人帳戶。</h1>
                                        <h2><span class="gray">●</span> 銀行帳戶資料及存摺封面電子檔 </h2>
                                    </dd>
                                </dl>
                            </div>

                            <div class="register_status" style="display: none;">
                                <div class="status_brank">

                                    <div class="clearfix b4_form">
                                        <div class="module-customselect b4_select inp01 ">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>金融機構類別
                                        </span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background:none;">
                                            <select id="bidNodeKind" name="bidNodeKind"
                                                    style="width: 380px;font-size:15px"
                                                    class="validate[required]">
                                                <option value="" ${account.bidNodeKind == null?"selected":""}>請選擇
                                                </option>
                                                <option value="A" ${account.bidNodeKind == "A"?"selected":""}>商業銀行
                                                </option>
                                                <option value="B" ${account.bidNodeKind == "B"?"selected":""}>信用合作社
                                                </option>
                                                <option value="C" ${account.bidNodeKind == "C"?"selected":""}>農漁會
                                                </option>
                                            </select>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="clearfix b4_form">

                                        <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>銀行名稱
                                        </span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <select id="bidNodeID" name="bidNodeID" style="width: 380px;font-size:15px"
                                                    class="validate[required]">
                                                <option value="" ${account.bidNodeID == null?"selected":""}>請選擇
                                                </option>
                                                <c:forEach var="codeVar" items="${code}" varStatus="loop">
                                                    <option value="${codeVar.id.code}" ${account.bidNodeID == codeVar.id.code?"selected":""}>${codeVar.description1}</option>
                                                </c:forEach>
                                            </select>
                                        </span>
                                        </div>
                                    </div>

                                    <div class="clearfix b4_form">
                                        <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>銀行帳號</span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
                                            <input type="text" id="bidNodeAccount" name="bidNodeAccount" maxlength="20"
                                                   class="text-input validate[required,custom[number],minSize[6],maxSize[14]]"
                                                   style="font-size:15px"
                                                   value='${account.bidNodeAccount != null?account.bidNodeAccount:""}'/>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="clearfix b4_form">
                                        <div class="module-customselect b4_select inp01">
                                        <span class="module-customselect-typename inp_left01">
                                            <font color="#CC0000">*</font>銀行帳戶名稱
                                        </span>
                                        <span class="module-customselect-selected inp_right01"
                                              style="background: none;">
											<input type="text" id="bidNodeUserName" name="bidNodeUserName"
                                                   maxlength="10" readonly="true" style="font-size:15px"
                                                   value='${(not empty account.bidNodeUserName)? account.bidNodeUserName:account.chineseName}'/>
                                        </span>
                                        </div>
                                    </div>

                                    <div class="clear"></div>
                                </div>


                                <ul class="clearfix status_photo">
                                    <li class="p1"><font color="#CC0000">*</font>存摺封面或提款卡
                                        <div class="line W-hide"></div>
                                        <br/><font color="#CC0000">影像需清晰</font>
                                    </li>
                                    <li class="p2">
                                        <div id="files4" class="files4"></div>
                                        <output id="bankBookFrontImageList"></output>
                                        <%--'${account.adImg3}'--%>
                                    <li class="p3">
                                        <div class="button">
                                            <div class="email_button status_button">
                                                <input id="bankBookFrontUploadButtonId" type="file"
                                                       style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;"
                                                       name="bankBookFrontFile" accept="image/jpeg|image/png">
                                                <a onclick="$('input[id=bankBookFrontUploadButtonId]').click();">選擇檔案</a>
                                            </div>
                                        </div>
                                        <div class="button">
                                            <div class="email_button status_button  ">
                                                <a id="bankBookFrontPreviewId">編輯檔案 </a>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="p4"><font color="red">${poscrStatus!=2?TFEACCCD.poscrRemark:""}</font>
                                    </li>
                                </ul>

                                <c:if test="${account.accountPurpose==2}">
                                    <div class="register_status">
                                        <ul class="clearfix status_photo">
                                            <li class="p1"><font color="red">備註說明</font>
                                                <div class="line W-hide"></div>
                                            </li>
                                            <li class="p4">
                                                <font color="red">${TFEACCCD.checkRemark2}</font>
                                            </li>
                                        </ul>
                                    </div>
                                </c:if>


                            </div>


                            <%--***********************************New Add 出金 END **************************************************--%>

                            <div class="register_status" style="display: none;">
                                <dl class="clearfix status_txt">
                                    <dt>
                                        <img src="${pageContext.request.contextPath}/resource/assets/images/s2.png">
                                    </dt>
                                    <dd>
                                        <h1>請上傳以下文件電子檔以及任何足資證明您財務狀況的資料，檔案可以是手機拍攝的影像檔或掃描檔</h1>
                                        <h2>
                                            <span class="gray">● </span>
                                            <span class="c2">※ 以下欄位資訊皆可批次上傳多張圖片 </span>
                                        </h2>
                                    </dd>
                                </dl>
                            </div>
                            <!-- 以下為上傳區   開始 -->
                            <div class="register_status" style="display: none;">
                                <!-- 									 		var depositBookInnerPageDeleteFileArray=[]; -->
                                <!-- 									 		var houseHoldregistrationTranscriptImageDeleteFileArray=[]; -->
                                <!-- 									 		var personalPropertyDeleteFileArray=[]; -->
                                <!-- 									 		var realPropertyDeleteFileArray=[]; -->
                                <!-- 									 		var anotherIdentifyDocumentDeleteFileArray=[]; -->
                                <!-- 									 		var studentIdentificationImageDeleteFileArray=[]; -->
                                <!-- 									 		var scoreImageDeleteFileArray=[];	 -->
                                <input type="hidden" id="depositBookInnerPageDeleteFileArray"
                                       name="depositBookInnerPageDeleteFileArray" value=""/>
                                <input type="hidden" id="idCardFrontDeleteFileArray"
                                       name="idCardFrontDeleteFileArray" value=""/> <%--jim--%>
                                <input type="hidden" id="idCardBackDeleteFileArray"
                                       name="idCardBackDeleteFileArray" value=""/> <%--jim--%>
                                <input type="hidden" id="id2ndCardDeleteFileArray"
                                       name="id2ndCardDeleteFileArray" value=""/> <%--jim--%>
                                <input type="hidden" id="bankBookFrontDeleteFileArray"
                                       name="bankBookFrontDeleteFileArray" value=""/> <%--jim--%>
                                <input type="hidden" id="houseHoldregistrationTranscriptImageDeleteFileArray"
                                       name="houseHoldregistrationTranscriptImageDeleteFileArray"
                                       value=""/>
                                <input type="hidden" id="personalPropertyDeleteFileArray"
                                       name="personalPropertyDeleteFileArray" value=""/>
                                <input type="hidden" id="realPropertyDeleteFileArray" name="realPropertyDeleteFileArray"
                                       value=""/>
                                <input type="hidden" id="anotherIdentifyDocumentDeleteFileArray"
                                       name="anotherIdentifyDocumentDeleteFileArray" value=""/>
                                <input type="hidden" id="studentIdentificationImageDeleteFileArray"
                                       name="studentIdentificationImageDeleteFileArray" value=""/>
                                <input type="hidden" id="scoreImageDeleteFileArray" name="scoreImageDeleteFileArray"
                                       value=""/>
                                <div id="depositBookInnerPageDiv">
                                    <ul class="clearfix status_photo">
                                        <li class="p1">
                                        	<font style="color: red;">*</font>
                                            	存摺內頁                                            
                                            <div class="line W-hide"></div>
                                            <br><font style="color: red;" size="2px">(過去三個月資料或銀行對帳單)</font>
                                        </li>
                                        <li class="p2 ">
                                            <output id="depositBookInnerPageImageList"></output>
                                        </li>
                                        <li class="p3">
                                            <div class="button">
                                                <div class="email_button status_button">
                                                    <input type="file" id="depositBookInnerPageUploadButtonId"
                                                           accept="image/jpeg|image/png"
                                                           name="depositBookInnerPageUploadFiles" multiple
                                                           style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;"/>
                                                    <a onclick="goChangeFileObj('depositBookInnerPageUploadButtonId'); $('input[id=depositBookInnerPageUploadButtonId]').click();">選擇檔案</a>
                                                </div>
                                            </div>
                                            <div class="button">
                                                <div class="email_button status_button  ">
                                                    <a id="depositBookInnerPagePreviewId">編輯檔案 </a>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="p4"><font
                                                color="red">${posinStatus!=2?TFEACCCD.posinRemark:""}</font></li>
                                        <div class="clear"></div>
                                    </ul>
                                    <div id="houseHoldregistrationTranscriptImageDiv">
                                        <ul class="clearfix status_photo">
                                            <li class="p1">
                                            	<font style="color: red;">*</font>
                                                	戶口名簿影像                                                
                                                <div class="line W-hide"></div>
                                            </li>
                                            <li class="p2 ">
                                                <output id="houseHoldregistrationTranscriptImageImageList"></output>
                                            </li>
                                            <li class="p3">
                                                <div class="button">
                                                    <div class="email_button status_button">
                                                        <input type="file"
                                                               id="houseHoldregistrationTranscriptImageUploadButtonId"
                                                               accept="image/jpeg|image/png"
                                                               name="houseHoldregistrationTranscriptImageUploadFiles"
                                                               multiple style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;"/>
                                                        <a onclick="goChangeFileObj('houseHoldregistrationTranscriptImageUploadButtonId'); $('input[id=houseHoldregistrationTranscriptImageUploadButtonId]').click();">選擇檔案</a>
                                                    </div>
                                                </div>
                                                <div class="button">
                                                    <div class="email_button status_button  ">
                                                        <a id="houseHoldregistrationTranscriptImagePreviewId">編輯檔案 </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <div class="clear"></div>
                                        </ul>
                                    </div>
                                    <div id="personalPropertyDiv">
                                        <ul class="clearfix status_photo">
                                            <li class="p1">
                                                動產證明
                                                <div class="line W-hide"></div>
                                            </li>
                                            <li class="p2 ">
                                                <output id="personalPropertyImageList"></output>
                                            </li>
                                            <li class="p3">
                                                <div class="button">
                                                    <div class="email_button status_button">
                                                        <input type="file" id="personalPropertyUploadButtonId"
                                                               accept="image/jpeg|image/png"
                                                               name="personalPropertyUploadFiles" multiple
                                                               style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;"/>
                                                        <a onclick="goChangeFileObj('personalPropertyUploadButtonId'); $('input[id=personalPropertyUploadButtonId]').click();">選擇檔案</a>
                                                    </div>
                                                </div>
                                                <div class="button">
                                                    <div class="email_button status_button  ">
                                                        <a id="personalPropertyPreviewId">編輯檔案 </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="p4">
                                                <font color="red">${mopStatus!=2?TFEACCCD.mopRemark:""}</font>
                                            </li>
                                            <div class="clear"></div>
                                        </ul>
                                    </div>
                                    <div id="realPropertyDiv">
                                        <ul class="clearfix status_photo">
                                            <li class="p1">
                                                不動產證明
                                                <div class="line W-hide"></div>
                                            </li>
                                            <li class="p2 ">
                                                <output id="realPropertyImageList"></output>
                                            </li>
                                            <li class="p3">
                                                <div class="button">
                                                    <div class="email_button status_button">
                                                        <input type="file" id="realPropertyUploadButtonId"
                                                               accept="image/jpeg|image/png"
                                                               name="realPropertyUploadFiles" multiple
                                                               style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;"/>
                                                        <a onclick="goChangeFileObj('realPropertyUploadButtonId'); $('input[id=realPropertyUploadButtonId]').click();">選擇檔案</a>
                                                    </div>
                                                </div>
                                                <div class="button">
                                                    <div class="email_button status_button  ">
                                                        <a id="realPropertyPreviewId">編輯檔案 </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="p4">
                                                <font color="red">${mopStatus!=2?TFEACCCD.mopRemark:""}</font>
                                            </li>
                                            <div class="clear"></div>
                                        </ul>
                                    </div>
                                    <div id="studentIdentificationImageDiv">
                                        <ul class="clearfix status_photo">
                                            <li class="p1">
                                            	<font style="color: red;">*</font>
                                                	學生證影像                                                
                                                <div class="line W-hide"></div>
                                            </li>
                                            <li class="p2 ">
                                                <output id="studentIdentificationImageImageList"></output>
                                            </li>
                                            <li class="p3">
                                                <div class="button">
                                                    <div class="email_button status_button">
                                                        <input type="file" id="studentIdentificationImageUploadButtonId"
                                                               accept="image/jpeg|image/png"
                                                               name="studentIdentificationImageUploadFiles"
                                                               multiple style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;"/>
                                                        <a onclick="goChangeFileObj('studentIdentificationImageUploadButtonId'); $('input[id=studentIdentificationImageUploadButtonId]').click();">選擇檔案</a>
                                                    </div>
                                                </div>
                                                <div class="button">
                                                    <div class="email_button status_button  ">
                                                        <a id="studentIdentificationImagePreviewId">編輯檔案 </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <div class="clear"></div>
                                        </ul>
                                    </div>
                                    <div id="scoreImageDiv">
                                        <ul class="clearfix status_photo">
                                            <li class="p1">
                                            	<font style="color: red;">*</font>
                                                	成績單影像                                                
                                                <div class="line W-hide"></div>
                                            </li>
                                            <li class="p2 ">
                                                <output id="scoreImageImageList"></output>
                                            </li>
                                            <li class="p3">
                                                <div class="button">
                                                    <div class="email_button status_button">
                                                        <input type="file" id="scoreImageUploadButtonId"
                                                               accept="image/jpeg|image/png"
                                                               name="scoreImageUploadFiles" multiple
                                                               style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;"/>
                                                        <a onclick="goChangeFileObj('scoreImageUploadButtonId'); $('input[id=scoreImageUploadButtonId]').click();">選擇檔案</a>
                                                    </div>
                                                </div>
                                                <div class="button">
                                                    <div class="email_button status_button  ">
                                                        <a id="scoreImagePreviewId">編輯檔案 </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <div class="clear"></div>
                                        </ul>
                                    </div>
                                    <div id="anotherIdentifyDocumentDiv">
                                        <ul class="clearfix status_photo">
                                            <li class="p1" id="otherId">
                                                其他證明文件
                                                <div class="line W-hide"></div>
                                                <br/><font style="color:red;" size="2px">(財產或所得清單)</font>
                                            </li>
                                            <li class="p2 ">
                                                <output id="anotherIdentifyDocumentImageList"></output>
                                            </li>
                                            <li class="p3">
                                                <div class="button">
                                                    <div class="email_button status_button">
                                                        <input type="file" id="anotherIdentifyDocumentUploadButtonId"
                                                               accept="image/jpeg|image/png"
                                                               name="anotherIdentifyDocumentUploadFiles" multiple
                                                               style="opacity: 0;width: 100%;left: 0;right: 0;top: 0;bottom: 0;position: absolute;"/>
                                                        <a onclick="goChangeFileObj('anotherIdentifyDocumentUploadButtonId'); $('input[id=anotherIdentifyDocumentUploadButtonId]').click();">選擇檔案</a>
                                                    </div>
                                                </div>
                                                <div class="button">
                                                    <div class="email_button status_button  ">
                                                        <a id="anotherIdentifyDocumentPreviewId">編輯檔案 </a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="p4">
                                                <font color="red">${otpStatus!=2?TFEACCCD.otpRemark:""}</font>
                                            </li>
                                            <div class="clear"></div>
                                        </ul>
                                    </div>
                                    <!-- 以上為上傳區    結束 -->
                                    <!-- 以下為宣導區    開始 -->                                   
                                    <div id="jcicReportDiv">
                                        <ul class="clearfix status_photo">
                                            <li class="p1">
                                            	<font style="color: red;">*</font>
                                                	聯徵報告                                                
                                                <div class="line W-hide"></div>
                                            </li>
                                            <li class="p2">
                                                <p>請準備正本文件寄送至台灣資金交易所<a href="${pageContext.request.contextPath}/doc"
                                                                       target="_blank">申請方式說明</a></p>

                                            </li>
                                            <li class="p3">
                                                <div class="button">
                                                    <div class="email3_button status3_button">
                                                        <a>&nbsp;</a>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="p4">
                                                <font color="red">${crrpeStatus!=2?TFEACCCD.crrpeRemark:""}</font>
                                            </li>
                                        </ul>
                                    </div>                                   
                                    <!-- 以下為宣傳區    結束 -->
                                    <div class="status_btn b139">
                                        <div class="button">
                                            <%--<div class="news_button btn_w2">--%>
                                            <%--<a onClick="doSubmit()">送出申請 </a>--%>
                                            <%-- 											<a href="${pageContext.request.contextPath}/credit/complete">送出註冊 </a> --%>
                                            <%--</div>--%>
                                            <input id="sendButton" class="news_button" type="submit" value="確定"/>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--PAGE END-->
        </div>


    </section>
    <img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%"
         style="margin-top: -1px;" alt="">
</div>
<div id="divProgressLink" style="text-align: center; display: none; position: fixed; top: 50%; left: 50%;"></div>
<div id="divMaskFrameLink"
     style="background-color: #F2F4F7; display: none; left: 0px; position: absolute; top: 0px;"></div>
<div id="divProgress" style="text-align: center; display: none; position: fixed; top: 50%; left: 50%;">
    <img id="img-loading" src="${pageContext.request.contextPath}/resource/assets/images/loading.gif"/>
    <br/>
    <font color="#1B3563" size="2px">資料處理中</font>
    <div class="nav_rate">
        <div class="nav_rate2 nav5">&nbsp;</div>
    </div>
</div>
<div id="divMaskFrame" style="background-color: #F2F4F7; display: none; left: 0px; position: absolute; top: 0px;"></div>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js"
        type="text/javascript"></script>

<script type="text/javascript"
        src="${pageContext.request.contextPath}/resource/common/js/jquery-1.11.3.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/common/css/validationEngine.jquery.css"/>
<script src="${pageContext.request.contextPath}/resource/common/js/jquery.validationEngine-zh_CN.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/common/js/jquery.validationEngine.js"></script>

<script>
    $(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());

    $('#file-form').validationEngine({
        validationEventTriggers: "blur", //触发的事件  validationEventTriggers:"keyup blur",
        inlineValidation: true,//是否即时验证，false为提交表单时验证,默认true
        success: false,//为true时即使有不符合的也提交表单,false表示只有全部通过验证了才能提交表单,默认false
        promptPosition: "topRight",//提示所在的位置，topLeft, topRight, bottomLeft,  centerRight, bottomRight
        scroll: false,
        //ajax
        ajaxFormValidation: true,
        ajaxFormValidationMethod: 'post',
        onAjaxFormComplete: ajaxValidationCallback,
        onBeforeAjaxFormValidation: beforeCall,
        custom_error_messages: {
            // Custom Error Messages for Validation Types
            '#contactName': {
                'custom[chinese]': {
                    'message': '姓名格式錯誤'
                },
            },
//            '#checkbox1': {
//                'required': {
//                    'message': '選項服務條款及隱私權政策未同意無法進行後續作業 '
//                }
//            },
        },

    });
    //This method is called right before the ajax form validation request
    //it is typically used to setup some visuals ("Please wait...");
    //you may return a false to stop the request
    function beforeCall(form, options) {
        if (!doSubmit()) {
            return false;
        }
//        alert("aaaa");
        ShowProgressBar();
        if (window.console) {
            //console.log('Right before the AJAX form validation call');
        }
        ;
        return true;
    }
    ;

    //Called once the server replies to the ajax form validation request
    function ajaxValidationCallback(status, form, json, options) {
        //console.log(status);
        //console.log(form);
        //console.log(json);
        //console.log(options);
        //if (window.console) {
        //console.log(status);
        //}
        //;

        if (status === true) {
            //alert('the form is valid!');
            if (json.rs == true) {
                //alert("成功");
                //$.post("${pageContext.request.contextPath}"+"/account/register/Register_B2",function(data){

                //$(".layout-container").html(data);

                //alert("a........href:"+data);
                //});
                if (json.mail == true) {
                    alert("系統已發送認證信至您修改後信箱,請您至信箱收取認證信進行確認,信箱將在您確認後進行更新");
                }
                HideProgressBar();
                alert("${pageContext.request.contextPath}" + json.view);
                window.location.replace("${pageContext.request.contextPath}" + json.view);
            } else {
                alert("失敗");
            }
        } else {
            alert("error");
        }
        HideProgressBar();

    }
    ;
</script>
<script>
    var uploadStatus = "${uploadStatus}";
    if (uploadStatus == 'false') {
        alert("影像檔案上傳失敗，請重新操作");
        console.log("message:" + "${message}");
    }
    var depositBookInnerPageDeleteFileArray = [];
    var idCardFrontDeleteFileArray = []; //jim
    var idCardBackDeleteFileArray = []; //jim
    var id2ndCardDeleteFileArray = []; //jim
    var bankBookFrontDeleteFileArray = []; //jim
    var houseHoldregistrationTranscriptImageDeleteFileArray = [];
    var personalPropertyDeleteFileArray = [];
    var realPropertyDeleteFileArray = [];
    var anotherIdentifyDocumentDeleteFileArray = [];
    var studentIdentificationImageDeleteFileArray = [];
    var scoreImageDeleteFileArray = [];

    $('.testFileUpload').each(function (index) {
        $(this).bind('change', function () {
            alert('This file size is: ' + this.files[0].size / 1024 / 1024 + "MB");
            if ($(this).val() != "") {
                var uploadFileId = $(this).attr("id");
                var uploadedFile = document.getElementById(uploadFileId);
                var fileSize = uploadedFile.files[0].size;
                if ((fileSize / 1024 / 1024) > 5) {
                    alert("檔案大小超過5MB，請重新選擇");
                    $(this).val("");
                }
            }

        });
        //console.log("index is " + index);

    });

    $('#clear').click(function () {
        $('.testFileUpload').each(function (index) {
            if ($(this).val() != "") {
                //var fileSize = $(this).files[0].size;

                var uploadFileId = $(this).attr("id");
                console.log("id:" + uploadFileId);

                var uploadedFile = document.getElementById(uploadFileId);

                var fileSize = uploadedFile.files[0].size;

                alert((fileSize / 1024 / 1024) + "MB");

                $(this).val("");
            }

        });
    });

    function goChangeFileObj(fileID) {
        console.log("in goChangeFileObj : " + fileID);
        var fileObj = document.getElementById(fileID);
        openDebug(fileObj, fileID);
        var files = fileObj.files;
        openDebug(files, "files");
        if (files.length > 0) {
            // Loop through the FileList and render image files as thumbnails.
            for (i = 0; f = files[i]; i++) {
                for (key in files[i]) {
                    try {
                        console.log("files[" + i + "]." + key + "=" + files[i][key]);
                    }
                    catch (err) {
                        console.log("files[" + i + "]." + key + "= Access Error!!");
                    }
                }
            }
            var fileObjName = fileID.replace("UploadButtonId", "UploadFiles");
            var fileObjs = document.getElementsByName(fileObjName);
            fileObj.setAttribute("id", fileID + fileObjs.length);
            var newFileObj = document.createElement("input");
            newFileObj.setAttribute("id", fileID);
            newFileObj.setAttribute("type", "file");
            newFileObj.setAttribute("accept", "image/jpeg|image/png");
            newFileObj.setAttribute("name", fileObjName);
            newFileObj.setAttribute("style", "display: none;");
            newFileObj.setAttribute("multiple", "true");
            newFileObj.addEventListener('change', handleFileSelect2, false);
            var parentDiv = document.getElementById("depositBookInnerPageDiv");
            parentDiv.appendChild(newFileObj);
        }
        console.log("out goChangeFileObj : " + fileObj);
    }

    function handleFileSelect2(evt) {
        console.log("in handleFileSelect2 : " + evt.target.id);
        // for (key in evt.target){
        //     console.log("evt["+ key +"]="+ evt.target[key]);
        // }
        var functionName = evt.target.id.replace("UploadButtonId", "");
        var activelist = evt.target.id.replace("UploadButtonId", "") + 'ImageList';
        var activelistGroup = evt.target.id.replace("UploadButtonId", "") + 'UploadGroup';
        console.log('functionName:' + functionName);
        console.log('activelist:' + activelist);
        console.log('activelistGroup:' + activelistGroup);

        if(evt.target.multiple==false){
            $('#' + activelist).html("");
        }
//        console.log("in handleFileSelect2 : " + evt.target);

//        clearDeleteArrayRecord(functionName);

        var files = evt.target.files; // FileList object
        console.log("files is " + files.length);
        var isCorrectSize = true;

        if (files.length > 10) {
            alert("檔案上傳數量為10，請重新選擇");
            $('#' + evt.target.id).val("");
            return;
        }

        /********************************************************************************/
        for (i = 0; f = files[i]; i++) {
            console.log("files[" + i + "].name=" + files[i].name);
            var re = /\.(jpg|jPg|jPG|JPG|Jpg|JpG)$/i;

            console.log("files[" + i + "].type=" + f.type);
            if (!(f.type.indexOf("image/jpeg") == 0) && !(f.type.indexOf("image/png") == 0)) {
//				if (!re.test(files[i].name)) {
                fileExtensionFail = true;
                alert("只允許上傳 JPG / PNG 影像檔");
                $('#' + evt.target.id).val("");
                return;
            }
            console.log('This file size is: ' + files[i].size / 1024 / 1024 + "MB");

            if ((f.size / 1024 / 1024) > 5) {
                alert(files[i].name + ":檔案大小超過5MB，請重新選擇");
                $('#' + evt.target.id).val("");
                isCorrectSize = false;
                break;
            }

        }
        /********************************************************************************/

        if (files.length > 0) {
            // Loop through the FileList and render image files as thumbnails.
            for (i = 0; f = files[i]; i++) {
                //console.log("files[i]" + files[i] );
                // 					console.log("i" + i );
                for (key in files[i]) {
                    console.log("files[" + i + "]." + key + "=" + files[i][key]);
                }
                // Only process image files.
                //if (!f.type.match('image.*')) {

                /* if (!f.type.match('image/jpeg')) { 移除重覆檢查 Jim 2016/07/05
                 if (!f.type.match('image/png')) {
                 continue;
                 }
                 }*/



                var reader = new FileReader();

                // Closure to capture the file information.
                reader.onload = (function (theFile, i) {
                    return function (e) {
                        // Render thumbnail.
                        // console.log("i:"+i);
                        // console.log("theFile:"+theFile.name);
                        if (isCorrectSize) {

                            var span = document.createElement('span');
                            span.id = [functionName + '_' + i + '_' + theFile.name + '_' + 'span'].join('');
                            span.innerHTML = ['<img name="', theFile.name, '" id="', functionName + '_' + i + '_' + theFile.name, '" class="', activelistGroup, ' thumb " href="', e.target.result, '" src="', e.target.result, '" title="', escape(theFile.name), '"/>', '<div id="', functionName + '_' + i + '_' + theFile.name, '_text"><br><font>', escape(theFile.name), '</font><br><div>'].join('');
//                            openDebugText(span.innerHTML, "test2");
                            document.getElementById(activelist).insertBefore(span, null);
                        }

                    };
                })(f, i);
                // Read in the image file as a data URL.
//                alert(f);
//                openDebug(f,"file");
                reader.readAsDataURL(f);
            }
            console.log("load image done");

        }
    }

    document.getElementById('depositBookInnerPageUploadButtonId').addEventListener('change', handleFileSelect2, false);
    document.getElementById('idCardFrontUploadButtonId').addEventListener('change', handleFileSelect2, false);//jim
    document.getElementById('idCardBackUploadButtonId').addEventListener('change', handleFileSelect2, false);//jim
    document.getElementById('id2ndCardUploadButtonId').addEventListener('change', handleFileSelect2, false);//jim
    document.getElementById('bankBookFrontUploadButtonId').addEventListener('change', handleFileSelect2, false);//jim
    document.getElementById('houseHoldregistrationTranscriptImageUploadButtonId').addEventListener('change', handleFileSelect2, false);
    document.getElementById('personalPropertyUploadButtonId').addEventListener('change', handleFileSelect2, false);
    document.getElementById('realPropertyUploadButtonId').addEventListener('change', handleFileSelect2, false);
    document.getElementById('anotherIdentifyDocumentUploadButtonId').addEventListener('change', handleFileSelect2, false);
    document.getElementById('studentIdentificationImageUploadButtonId').addEventListener('change', handleFileSelect2, false);
    document.getElementById('scoreImageUploadButtonId').addEventListener('change', handleFileSelect2, false);

    // 		存摺內頁           depositBookInnerPage
    // 		身份證正面           idCardFront //jim
    // 		第二證件           idCardBack //jim
    // 		存摺封面或提款卡           id2ndCard //jim
    // 		身份證反面           bankBookFront //jim
    // 		戶口名簿影像      houseHoldregistrationTranscriptImage
    // 		動產                 personalProperty
    // 		不動產              realProperty
    // 		其他證明文件      anotherIdentifyDocument
    // 		學生證影像         studentIdentificationImage
    // 		成績單影像         scoreImage
    // 		聯徵報告           jcicReport
    // 		扣繳憑單           theIncomeWithholdsTheVoucher
    // 		報稅單              taxDeclaration
    function clearDeleteArrayRecord(functionName) {
        console.log("enter function name");
        console.log("function name:" + functionName);

        if (functionName.match("^depositBookInnerPage")) {
            depositBookInnerPageDeleteFileArray = [];
        }
        if (functionName.match("^idCardFront")) { //jim
            idCardFrontDeleteFileArray = [];
        }
        if (functionName.match("^idCardBack")) { //jim
            idCardBackDeleteFileArray = [];
        }
        if (functionName.match("^id2ndCard")) { //jim
            id2ndCardDeleteFileArray = [];
        }
        if (functionName.match("^bankBookFront")) { //jim
            bankBookFrontDeleteFileArray = [];
        }
        if (functionName.match("^houseHoldregistrationTranscriptImage")) {
            houseHoldregistrationTranscriptImageDeleteFileArray = [];
        }
        if (functionName.match("^personalProperty")) {
            personalPropertyDeleteFileArray = [];
        }
        if (functionName.match("^realProperty")) {
            realPropertyDeleteFileArray = [];
        }
        if (functionName.match("^anotherIdentifyDocument")) {
            anotherIdentifyDocumentDeleteFileArray = [];
        }
        if (functionName.match("^studentIdentificationImage")) {
            studentIdentificationImageDeleteFileArray = [];
        }
        if (functionName.match("^scoreImage")) {
            scoreImageDeleteFileArray = [];
        }

    }

    function rebindImgePreview(rebindGroup, deleteFileArray) {
        console.log("enter rebindImagePreview , rebindGroup is " + rebindGroup);
        $('.' + rebindGroup + 'UploadGroup').unbind();
        $.colorbox.close();
        $('.' + rebindGroup + 'UploadGroup').colorbox({
            rel: rebindGroup + 'UploadGroup',
            //current : "<a class='popup-btn'>檔案刪除</a> <a id='closeSelected' class='popup-btn'>結束預覽</a>",
            title: function () {
                return this.name+"<br><br><a id='removeSelected' class='popup-btn'>檔案刪除</a> <a id='closeSelected' class='popup-btn' onclick='$.colorbox.close();'>結束預覽</a>";
            },
            //transition:"fade",
            photo: 'true',
            loop: false,
            arrowKey: 'true',
//            innerWidth: '430px',
            //innerHeight : '200px',
            open: true,
            onClosed: function () {
                console.log('enter onClose : function');
                for (var key in deleteFileArray) {
                    console.log("delete date:" + deleteFileArray[key]);
                }
            },
            onComplete: function () {
                console.log('enter onComplete : function');
                var $element = $.colorbox.element();
//                console.log('this Image id:' + this.id);
//                console.log('this Image name' + this.name);
                var theImageId = this.id;
                var theImageName = this.name;
                var _w = this.naturalWidth ;
                var _winW = window.innerWidth ;
                // console.log(this.width);
                // alert("theImageId = "+theImageId);
                // alert("_winW*0.8 = "+_winW*0.8);
                // alert("_w = "+_w);
                if ( parseInt(_winW*0.9 ,10 ) < parseInt(_w ,10 ) ){

                    // alert("_winW*0.9 = "+_winW*0.9);
                    $.colorbox({ width:_winW*0.9, });
                    $(".cboxPhoto").css("max-width","100%").css("height","initial");
                    $("#cboxContent").css("background","transparent");
                    $.colorbox.resize();

                }else{

                    // alert("_w = "+_w);
                    $.colorbox({ width:_winw });
                    $(".cboxPhoto").css("max-width","100%").css("height","initial");
                    $.colorbox.resize();
                }

                $("#closeSelected").click(function () {
                    console.log("click the close");
                    //$('#cboxClose').colorbox.close();
                    jQuery('#cboxClose').click();
                });
                $("#removeSelected").click(function () {
                    toremove = $.colorbox.element();//$('.'+rebindGroup+'UploadGroup');
                    toremove.remove();
                    target = (toremove.next().length) ? toremove.next() : ((toremove.prev().length) ? toremove.prev() : false);

                    console.log("toremove.next().length" + toremove.next().length);
                    console.log("toremove length" + toremove.length);

                    console.log("remove id: " + theImageId);
                    $('#' + theImageId + '_span').html('');
                    $('#' + theImageId + '_span').remove();
                    console.log("remove Text : " + theImageId + '_text');
                    var tempText = theImageId + '_text';
                    removeElement(document.getElementById(tempText));

                    deleteFileArray.push(theImageName);

                    //$('#' + thisShowId).remove();
                    console.log("theImageId:" + theImageId);
                    var validatedFiles = [];
                    var rebindGroupName = rebindGroup + 'UploadButtonId';
                    var oldfiles = document.getElementById(rebindGroupName).files;
                    console.log("oldfiles.length" + oldfiles.length);
                    if (oldfiles.length > 0) {
                        for (var y = 0; y < oldfiles.length; y++) {
                            console.log("oldFiles:" + oldfiles[y].name);
                            if (oldfiles[y].name != theImageName) {
                                console.log("Yes, delete one");
                                validatedFiles.push(oldfiles[y]);
                            }
                        }
                    }
                    console.log("validatedFiles.length" + validatedFiles.length);
                    $(rebindGroupName).attr("value", validatedFiles);
                    var newOldfiles = document.getElementById(rebindGroupName).files;
                    console.log("newOldfiles.length" + newOldfiles.length);
                    if (newOldfiles.length > 0) {
                        for (var y = 0; y < newOldfiles.length; y++) {
                            console.log("newFiles:" + newOldfiles[y].name);
                        }
                    }
                    console.log(toremove.next().length);

// 						for (key in $.colorbox) {
// 							console.log("$.colorbox.next()[" + key + "]=" + $.colorbox[key]);
// 						}

                    $.colorbox.next();
                    $.colorbox.prev();
                    $.colorbox.groupControls.show();
                });
            }
        });
    }

    $("body").on('click', '#colorbox', function(event) {
        // event.preventDefault();
        /* Act on the event */
        console.log( event.target );
        if( event.target.id == "colorbox" && window.innerWidth<=1024 ){
            $.colorbox.close();
        }
    });

    //移除在項目
    function removeElement(element) {
        element && element.parentNode && element.parentNode.removeChild(element);
    }

    // 		var depositBookInnerPageDeleteFileArray=[];
    // 		var houseHoldregistrationTranscriptImageDeleteFileArray=[];
    // 		var personalPropertyDeleteFileArray=[];
    // 		var realPropertyDeleteFileArray=[];
    // 		var anotherIdentifyDocumentDeleteFileArray=[];
    // 		var studentIdentificationImageDeleteFileArray=[];
    // 		var scoreImageDeleteFileArray=[];

    $('#' + 'depositBookInnerPage' + 'PreviewId').click(function () {
        rebindImgePreview('depositBookInnerPage', depositBookInnerPageDeleteFileArray);
    });
    $('#' + 'idCardFront' + 'PreviewId').click(function () { //jim
        rebindImgePreview('idCardFront', idCardFrontDeleteFileArray);
    });
    $('#' + 'idCardBack' + 'PreviewId').click(function () { //jim
        rebindImgePreview('idCardBack', idCardBackDeleteFileArray);
    });
    $('#' + 'id2ndCard' + 'PreviewId').click(function () { //jim
        rebindImgePreview('id2ndCard', id2ndCardDeleteFileArray);
    });
    $('#' + 'bankBookFront' + 'PreviewId').click(function () { //jim
        rebindImgePreview('bankBookFront', bankBookFrontDeleteFileArray);
    });
    $('#' + 'houseHoldregistrationTranscriptImage' + 'PreviewId').click(function () {
        rebindImgePreview('houseHoldregistrationTranscriptImage', houseHoldregistrationTranscriptImageDeleteFileArray);
    });
    $('#' + 'personalProperty' + 'PreviewId').click(function () {
        rebindImgePreview('personalProperty', personalPropertyDeleteFileArray);
    });
    $('#' + 'realProperty' + 'PreviewId').click(function () {
        rebindImgePreview('realProperty', realPropertyDeleteFileArray);
    });
    $('#' + 'anotherIdentifyDocument' + 'PreviewId').click(function () {
        rebindImgePreview('anotherIdentifyDocument', anotherIdentifyDocumentDeleteFileArray);
    });
    $('#' + 'studentIdentificationImage' + 'PreviewId').click(function () {
        rebindImgePreview('studentIdentificationImage', studentIdentificationImageDeleteFileArray);
    });
    $('#' + 'scoreImage' + 'PreviewId').click(function () {
        rebindImgePreview('scoreImage', scoreImageDeleteFileArray);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#img1').attr('src', e.target.result).attr('href', e.target.result).width(200).height(110);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function showImg(v) {
        document.getElementById("img1").src = v;
        document.getElementById("img1").href = v;
    }
    $(document).ready(function () {
        //console.log($("#question2div").val());
        //alert();

        $("#question1ul").change(function () {
            console.log($("#question1ul li").val());
        });

        $("#question1ul li").each(function () {
            //console.log($(this).val());
            $(this).click(function () {
                console.log($(this).val());
                $("#question1Answer").val($(this).val());
                resetAnswerDeleteArrayAndUploadValue();
                refreshQustion2li($(this).val());

                if ($(this).val() == 1) {
                    $('#otherId').html('<font style="color:red;">*</font>扣繳憑單<div class="line W-hide"></div><br/><font style="color:red;" size="2px">(請提供近一年度扣繳憑單)</font>')
                } else if ($(this).val() == 2) {
                    $('#otherId').html('<font style="color:red;"></font>報稅單<div class="line W-hide"></div><br/><font style="color:red;" size="2px">(須提供近半年資料)</font>')
                } else {
                    $('#otherId').html('其他證明文件<br/><div class="line W-hide"></div><font style="color:red;" size="2px">(財產或所得清單)</font>')
                }
            });
            //console.log($("#question1ul li").val());
        });

        $("#workingYeardiv").keydown(function (e) {
        	var keyCode = (window.event) ? e.which : e.keyCode;	        
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A, Command+A
                (keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                 // Allow: home, end, left, right, down, up
                (keyCode >= 35 && keyCode <= 40)) {
                     // let it happen, don't do anything
                     return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (keyCode < 48 || keyCode > 57)) && (keyCode < 96 || keyCode > 105)) {
                e.preventDefault();
            }
        });
        
        function resetAnswerDeleteArrayAndUploadValue() {

            $("#question1Answer").val('');
            $("#question2Answer").val('');
            $("#question3Answer").val('');            

            depositBookInnerPageDeleteFileArray = [];
//            idCardFrontDeleteFileArray = []; //jim
//            idCardBackDeleteFileArray = []; //jim
//            id2ndCardDeleteFileArray = []; //jim
//            bankBookFrontDeleteFileArray = []; //jim
            houseHoldregistrationTranscriptImageDeleteFileArray = [];
            personalPropertyDeleteFileArray = [];
            realPropertyDeleteFileArray = [];
            anotherIdentifyDocumentDeleteFileArray = [];
            studentIdentificationImageDeleteFileArray = [];
            scoreImageDeleteFileArray = [];

            $('#' + 'depositBookInnerPage' + 'UploadButtonId').val("");
//            $('#' + 'idCardFront' + 'UploadButtonId').val(""); //jim
//            $('#' + 'idCardBack' + 'UploadButtonId').val(""); //jim
//            $('#' + 'id2ndCard' + 'UploadButtonId').val(""); //jim
//            $('#' + 'bankBookFront' + 'UploadButtonId').val(""); //jim
            $('#' + 'houseHoldregistrationTranscriptImage' + 'UploadButtonId').val("");
            $('#' + 'personalProperty' + 'UploadButtonId').val("");
            $('#' + 'realProperty' + 'UploadButtonId').val("");
            $('#' + 'anotherIdentifyDocument' + 'UploadButtonId').val("");
            $('#' + 'studentIdentificationImage' + 'UploadButtonId').val("");
            $('#' + 'scoreImage' + 'UploadButtonId').val("");
//            $('#' + 'idCardFront' + 'UploadButtonId').val(""); //jim
//            $('#' + 'idCardBack' + 'UploadButtonId').val(""); //jim
//            $('#' + 'id2ndCard' + 'UploadButtonId').val(""); //jim
//            $('#' + 'bankBookFront' + 'UploadButtonId').val(""); //jim

            $('#' + 'depositBookInnerPage' + 'Orignal').val("");
//            $('#' + 'idCardFront' + 'UploadButtonId').val(""); //jim
//            $('#' + 'idCardBack' + 'UploadButtonId').val(""); //jim
//            $('#' + 'id2ndCard' + 'UploadButtonId').val(""); //jim
//            $('#' + 'bankBookFront' + 'UploadButtonId').val(""); //jim
            $('#' + 'houseHoldregistrationTranscriptImage' + 'Orignal').val("");
            $('#' + 'personalProperty' + 'Orignal').val("");
            $('#' + 'realProperty' + 'Orignal').val("");
            $('#' + 'anotherIdentifyDocument' + 'Orignal').val("");
            $('#' + 'studentIdentificationImage' + 'Orignal').val("");
            $('#' + 'scoreImage' + 'Orignal').val("");

            $('#' + 'depositBookInnerPage' + 'DeleteFileArray').val("");
//            $('#' + 'idCardFront' + 'DeleteFileArray').val("");//jim
//            $('#' + 'idCardBack' + 'DeleteFileArray').val("");//jim
//            $('#' + 'id2ndCard' + 'DeleteFileArray').val("");//jim
//            $('#' + 'bankBookFront' + 'DeleteFileArray').val("");//jim
            $('#' + 'houseHoldregistrationTranscriptImage' + 'DeleteFileArray').val("");
            $('#' + 'personalProperty' + 'DeleteFileArray').val("");
            $('#' + 'realProperty' + 'DeleteFileArray').val("");
            $('#' + 'anotherIdentifyDocument' + 'DeleteFileArray').val("");
            $('#' + 'studentIdentificationImage' + 'DeleteFileArray').val("");
            $('#' + 'scoreImage' + 'DeleteFileArray').val("");

            $('#' + 'depositBookInnerPage' + 'ImageList').html('');
//            $('#' + 'idCardFront' + 'ImageList').html('');//jim
//            $('#' + 'idCardBack' + 'ImageList').html('');//jim
//            $('#' + 'id2ndCard' + 'ImageList').html('');//jim
//            $('#' + 'bankBookFront' + 'ImageList').html('');//jim
            $('#' + 'houseHoldregistrationTranscriptImage' + 'ImageList').html('');
            $('#' + 'personalProperty' + 'ImageList').html('');
            $('#' + 'realProperty' + 'ImageList').html('');
            $('#' + 'anotherIdentifyDocument' + 'ImageList').html('');
            $('#' + 'studentIdentificationImage' + 'ImageList').html('');
            $('#' + 'scoreImage' + 'ImageList').html('');

        }

        function refreshQustion2li(question1Val) {			
            $('#question2selectText').css('background', 'url("${pageContext.request.contextPath}/resource/assets/images/icon-select.png") right center no-repeat white');

            $("#question2div").show();
            $("#question2ul").show();
            $("#question2ul").empty();
            $("#question3ul").empty();
            $("#question2selectText").text("-請選擇-");
            $("#question3selectText").text("-請選擇-");
            if (question1Val == 1) {
                <c:forEach items="${occupationType}" var="o" varStatus="count">                	              
                	$("#question2ul").append('<li id="question2ul${o.key}" value="${count.count+1}">${o.key}</li>');
                </c:forEach>

                $("#question2selectText").text("-職業類型-");
                $("#question2ul li").each(function () {                    
                    $(this).click(function () {
                        console.log(" question2ul li:" + $(this).text());
                        refreshQustion3li(question1Val, $(this).text());
                    });                    
                });
            } else if (question1Val == 2) {
            	<c:forEach items="${nonHireType}" var="o">                
                	$("#question2ul").append('<li id="question2ul${o.code}" value="${o.code}">${o.description1}</li>');
                </c:forEach>               
                $("#question2selectText").text("-現職行業-");
                $("#question2div").show();               
            } else if (question1Val == 3) {
                $("#question2ul").append('<li id="question2ul0" value="0">無</li>');
                $("#question2ul").append('<li id="question2ul1" value="1">一個</li>');
                $("#question2ul").append('<li id="question2ul2" value="2">兩個</li>');
                $("#question2ul").append('<li id="question2ul3" value="3">三個</li>');
                $("#question2ul").append('<li id="question2ul4" value="4">四個</li>');
                $("#question2ul").append('<li id="question2ul5" value="5">五個或五個以上</li>');
                $("#question2selectText").text("-小孩人數-");
            } else if (question1Val == 4) {
                <c:forEach items="${degreed}" var="o">
                	$("#question2ul").append('<li id="question2ul${o.code}" value="${o.code}">${o.description1}</li>');
                </c:forEach>              
                $("#question2selectText").text("-最高學歷-");
            }
            refreshQustion3li(question1Val, '');
        }

        function refreshQustion3li(question1Val, question2Val) {

            $('#depositBookInnerPage' + 'Div').show();//存摺內頁 todo : jim 不曉得要不要加Div
            $('#houseHoldregistrationTranscriptImage' + 'Div').show();//戶口名簿影像
            $('#personalProperty' + 'Div').show();//動產
            $('#realProperty' + 'Div').show();//不動產
            $('#anotherIdentifyDocument' + 'Div').show();//其他證明文件
            $('#studentIdentificationImage' + 'Div').show();//學生證影像
            $('#scoreImage' + 'Div').show();//成績單影像
            $('#jcicReport' + 'Div').show();//聯徵報告            
			$("#question3div").hide();			
            $("#question3ul").empty();
            $("#workingYeardiv").hide();
            
            if (question1Val == 1) {
            	var hasOccupationTypeDetail = false;
            	<c:forEach items="${occupationTypeDetail}" var="od">                	 
                	if(question2Val == '${od.description1}')
                	{         
                		if('${od.description2}' != '')
                		{
                			hasOccupationTypeDetail = true;
                			$("#question3ul").append('<li id="question3ul${od.code}" value="${od.code}">${od.description2}</li>');
                   		}
                   		else
                   		{                			
                			$("#question3ul").append('<li id="question3ul${od.code}" value="${od.code}">${od.description1}</li>');
                			$("#question3Answer").val('${od.code}');
                		}
                	};
    			</c:forEach>
                
                if(hasOccupationTypeDetail)
               	{
                	$("#question3selectText").text("-職業名稱-");
                	$("#question3div").show();
               	}               
               	$("#workingYear").attr("placeholder","請輸入現職年資");
                $("#workingYeardiv").show();
                    
                $('#houseHoldregistrationTranscriptImage' + 'Div').hide();                
                $('#studentIdentificationImage' + 'Div').hide();//學生證影像
                $('#scoreImage' + 'Div').hide();//成績單影像
            } else if (question1Val == 2) {
                $('#houseHoldregistrationTranscriptImage' + 'Div').hide();
                $("#workingYear").attr("placeholder","請輸入累計工作年資");
                $("#workingYeardiv").show();
                
                $('#studentIdentificationImage' + 'Div').hide();//學生證影像
                $('#scoreImage' + 'Div').hide();//成績單影像                
            } else if (question1Val == 3) {
            	<c:forEach items="${othersJobType}" var="ojt">                
                	$("#question3ul").append('<li id="question3ul${ojt.code}" value="${ojt.code}">${ojt.description1}</li>');
                </c:forEach>
            	
            	$("#question3selectText").text("-現職行業-");
                $("#question3div").show();
                $('#studentIdentificationImage' + 'Div').hide();//學生證影像
                $('#scoreImage' + 'Div').hide();//成績單影像               
            } else if (question1Val == 4) {
                $('#personalProperty' + 'Div').hide();//動產
                $('#realProperty' + 'Div').hide();//不動產
                $('#houseHoldregistrationTranscriptImage' + 'Div').hide();       
                $("#question3div").hide();
            }

            $(".register_status").each(function (index) {
                $(this).show();
            });

            $(".register_status2").each(function (index) {
                $(this).show();
            });

            
            $("#question2ul li").each(function () {
                $(this).click(function () {
                    console.log("question2ul li : " + $(this).val());
                    $("#question2Answer").val($(this).val());
                });
            });
           
            $("#question3ul li").each(function () {                
                $(this).click(function () {
                    console.log($(this).val());
                    $("#question3Answer").val($(this).val());
                });                
            });
            initData();
        }

        $("#bidNodeKind").on('change', function () {
            $.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/registerGetBankList",
                data: "type=" + $(this).val(),
                processData: false,
                cache: false,
                success: function (result) {
                    $("#bidNodeID option").remove();
                    $("#bidNodeID").append("<option value=''>請選擇 </option>");
                    for (var i = 0; i < result.length; i++) {
                        var monthStr = "<option value=" + result[i].id.code + ">" + result[i].description1 + "</option>";
                        $("#bidNodeID").append(monthStr);
                        //<select id="bidNodeID" name="bidNodeID" class="module-customselect-selected inp_right01">
                        //<option value="" ${account.bidNodeKind == null?"selected":""}>請選擇</option>
                    }
                    return;
                },
                error: function (result) {
                    //console.log(result);

                    return;
                }
            });
        });


        // 			$('#depositBookInnerPage'+'Div').hide();//存摺內頁
        // 			$('#houseHoldregistrationTranscriptImage'+'Div').hide();//戶口名簿影像
        // 			$('#personalProperty'+'Div').hide();//動產
        // 			$('#realProperty'+'Div').hide();//不動產
        // 			$('#anotherIdentifyDocument'+'Div').hide();//其他證明文件
        // 			$('#studentIdentificationImage'+'Div').hide();//學生證影像
        // 			$('#scoreImage'+'Div').hide();//成績單影像
        // 			$('#jcicReport'+'Div').hide();//聯徵報告
        // 			$('#theIncomeWithholdsTheVoucher'+'Div').hide();//扣繳憑單
        // 			$('#taxDeclaration'+'Div').hide();//報稅單

        // 			存摺封面 depositBookCover
        // 			存摺內頁 depositBookInnerPage
        // 			戶口名簿影像 houseHoldregistrationTranscriptImage
        // 			動產 personalProperty
        // 			不動產 realProperty
        // 			其他證明文件 anotherIdentifyDocument
        // 			學生證影像 studentIdentificationImage
        // 			成績單影像 scoreImage
        // 			聯徵報告 jcicReport
        // 			扣繳憑單theIncomeWithholdsTheVoucher
        // 			報稅單taxDeclaration


        function showImage(imageIdname, imageURL, fieldname) {
            var origFieldName = fieldname;
            console.log("Go! ==>imageURL" + imageURL);

            function findFilename(tmpImageURL) {
                var tmpStrArray = tmpImageURL.split('?');
                if (tmpStrArray != null) {
                    tmpStrArray = tmpStrArray[0].split('/');
                    return tmpStrArray[tmpStrArray.length - 1];
                }
            }

            if (imageURL != "") {
                console.log(imageURL.split('?')[0].split('/')[imageURL.split('?')[0].split('/').length - 1]);

                var functionName = imageIdname;
                var activelist = imageIdname + 'ImageList';
                var activelistGroup = imageIdname + 'UploadGroup';
                openDebugTextNewLine("0 " + activelist);

                $('#' + activelist).html("");

                clearDeleteArrayRecord(functionName);

//                openDebugTextNewLine("2");

                var urls = imageURL.split(";;");
                for (i = 0; tmpURL = urls[i]; i++) {
//                    openDebugTextNewLine(i + "-->" + tmpURL);

                    if (origFieldName == "fromURL") {
                        fieldname = findFilename(tmpURL);
                    }

                    var span = document.createElement('span');
                    span.id = [functionName + '_' + i + '_' + fieldname + '_' + 'span'].join('');
                    span.innerHTML = ['<img name="', fieldname, '" id="', functionName + '_' + i + '_' + fieldname, '" class="', activelistGroup, ' thumb " href="', tmpURL, '" src="', tmpURL, '" title="', escape(fieldname), '"/>', '<div id="', functionName + '_' + i + '_' + fieldname, '_text"><br><font>', escape(fieldname), '</font><br><div>'].join('');
//                    openDebugTextNewLine(span.innerHTML, "test3")
                    document.getElementById(activelist).insertBefore(span, null);
                }

            }

            console.log("showImage done");
            openDebugTextNewLine("showImage done");
        }


        function initData() {
//            alert("go initData");
            if (IS_LOAD_INIT_DONT == false) {
                <c:if test="${not empty account.adImg7}">
                showImage("idCardFront", "${account.adImg7}", "");
                </c:if>
                <c:if test="${empty account.adImg7}">
                showImage("idCardFront", "${pageContext.request.contextPath}/resource/assets/images/p200x110.jpg", "");
                </c:if>
                <%--showImage("idCardFront", "${account.adImg7}", "adImg7");--%>
                <c:if test="${not empty account.adImg8}">
                showImage("idCardBack", "${account.adImg8}", "");
                </c:if>
                <c:if test="${empty account.adImg8}">
                showImage("idCardBack", "${pageContext.request.contextPath}/resource/assets/images/p200x110_2.jpg", "");
                </c:if>
                <%--showImage("idCardBack", "${account.adImg8}", "adImg8");--%>
                <c:if test="${not empty account.adImg9}">
                showImage("id2ndCard", "${account.adImg9}", "");
                </c:if>
                <c:if test="${empty account.adImg9}">
                showImage("id2ndCard", "${pageContext.request.contextPath}/resource/assets/images/p3_200x110.jpg", "");
                </c:if>
                <%--showImage("id2ndCard", "${account.adImg9}", "adImg9");--%>
                <c:if test="${not empty account.adImg3}">
                showImage("bankBookFront", "${account.adImg3}", "");
                </c:if>
                <c:if test="${empty account.adImg3}">
                showImage("bankBookFront", "${pageContext.request.contextPath}/resource/assets/images/p7_200x110.jpg", "");
                </c:if>
                <%--showImage("bankBookFront", "${account.adImg3}", "adImg3");--%>

                showImage("depositBookInnerPage", "${account.adImg4}", "fromURL");
                showImage("houseHoldregistrationTranscriptImage", "${account.adImg10}", "fromURL");
                showImage("personalProperty", "${account.adImg5}", "fromURL");
                showImage("realProperty", "${account.adImg13}", "fromURL");
                showImage("studentIdentificationImage", "${account.adImg11}", "fromURL");
                showImage("scoreImage", "${account.adImg12}", "fromURL");
                var incomeSource = ${not empty account.incomeSource? account.incomeSource:-1};
                if (incomeSource != -1) {
                    if (incomeSource != 1) {
                        showImage("anotherIdentifyDocument", "${account.adImg6}", "fromURL");
                    } else {
                        showImage("anotherIdentifyDocument", "${account.adImg1}", "fromURL");
                    }
                }
                $('#' + 'depositBookInnerPage' + 'Orignal').val("${account.adImg4!= null? account.adImg4:""}");
                $('#' + 'idCardFront' + 'Orignal').val("${account.adImg7!= null? account.adImg7:""}");
                $('#' + 'idCardBack' + 'Orignal').val("${account.adImg8!= null? account.adImg8:""}");
                $('#' + 'id2ndCard' + 'Orignal').val("${account.adImg9!= null? account.adImg9:""}");
                $('#' + 'bankBookFront' + 'Orignal').val("${account.adImg3!= null? account.adImg3:""}");
                $('#' + 'houseHoldregistrationTranscriptImage' + 'Orignal').val("${account.adImg10!= null? account.adImg10:""}");
                $('#' + 'personalProperty' + 'Orignal').val("${account.adImg5!= null? account.adImg5:""}");
                $('#' + 'realProperty' + 'Orignal').val("${account.adImg13!= null? account.adImg13:""}");
                $('#' + 'anotherIdentifyDocument' + 'Orignal').val("${account.adImg6!= null? account.adImg6:(account.adImg1!= null? account.adImg1:"")}");
                $('#' + 'studentIdentificationImage' + 'Orignal').val("${account.adImg11!= null? account.adImg11:""}");
                $('#' + 'scoreImage' + 'Orignal').val("${account.adImg12!= null? account.adImg12:""}");

            }

            IS_LOAD_INIT_DONT = true;
        }


    });

    jQuery(window).load(function () {
//        alert("Go Load!");
		<c:if test="${account!=null && account.incomeSource!=null}">				
	        var incomeSource = ${account.incomeSource};        
	        $("#question1ul" + incomeSource).trigger('click');
			
	        switch (incomeSource) {
	            case 1:
	            	<c:forEach items="${occupationTypeDetail}" var="od">    
	            		<c:if test="${account.jobType==od.code}">          	                	        
	                		$("#question2ul li:contains('${od.description1}')").trigger('click');                	
	                	</c:if>
	    			</c:forEach>
	    			                
	                $("#question3ul" + "${account.jobType}").trigger('click');
	                console.log("account.jobType" + "${account.jobType}");
	                //console.log("account.workLevel" + "${account.workLevel}");
	                break;
	            case 2:                
	                $("#question2ul" + "${account.workKind}").trigger('click');
	                console.log("account.workingYear" + "${account.workingYear}");
	                console.log("account.workKind" + "${account.workKind}");
	                break;
	            case 3:
	                $("#question2ul" + "${account.children}").trigger('click');
	                $("#question3ul" + "${account.jobType}").trigger('click');
	                console.log("account.children" + "${account.children}");
	                console.log("account.jobType" + "${account.jobType}");
	                break;
	            case 4:
	                $("#question2ul" + "${account.educationLevel}").trigger('click');
	                console.log("account.educationLevel" + "${account.educationLevel}");
	                break;
	
	        }        
	        jQuery("body").trigger("click");
        </c:if>
    });

    var IS_LOAD_INIT_DONT = false;

</script>
</body>
</html>