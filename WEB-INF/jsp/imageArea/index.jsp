<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<!DOCTYPE html>
<html lang="zh-Hant">


<head>
  
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"  />
  <meta name="Title" content="TFE" />
  <meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
  <meta name="Description" content="Description" />

<!-- iOS -->

<!-- Android -->

  <meta property="og:title" content="Hconnect Mobile" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="" />
  <meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/share.jpg" />
  <meta property="og:site_name" content="Hconnect" />
  <meta property="og:description" content="Description" />
  <meta property="og:locality" content="Taipei" />
  <meta property="og:country-name" content="Taiwan" />
  
  <title>TFE</title>

  <link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
  
  <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /><link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />  <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/swiper.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/index.css" media="screen" rel="stylesheet" type="text/css" />

</head>

<body>



<div class="layout-container">
      <header id="header">
  <div class="layout-mw-wrapper">
    <div class="header-logo">
      <img src="${pageContext.request.contextPath}/resource/assets/images/tfe_logo.svg" alt="">
      <!-- <i class="fa fa-bars on-mobile btn-bars"></i> -->
      <!-- <i class="fa fa-times btn-bars"></i> -->
      <i class="icon-list on-mobile btn-bars"></i>
      <i class="icon-x on-mobile btn-bars"></i>
    </div>
  
    <div class="header-right">
      <i class="header-icon icon-user"></i>
      <a class="link-name" href="">登入</a>/<a class="link-name" href="">註冊</a>
      <i class="header-icon icon-set"></i>
      <a class="link-name" href="">競標組合</a>
    </div>
  </div>
</header>
<div class="on-mobile list-header-bg btn-bars">
</div>
<ul class="on-mobile list-header">
  <li class="list-header-item ">
    <i class="header-icon icon-user"></i>
    <a class="link-name" href="">登入</a>/<a class="link-name" href="">註冊</a>
  </li>
  <li class="list-header-item">
    <i class="header-icon icon-set"></i>
    <a class="link-name" href="">競標組合</a>
  </li>
</ul>


<div class="layout-view">


<section id="scene1">
  <h1>${imageUrl}</h1>
  
</section>

<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o.svg" width="100%" style="margin-top:-1px;" alt="">

</div>



<footer id="footer">
  <div class="layout-mw-wrapper">
    <div class="row row1">
      <div class="col-left">
        <a href="">
          <span class="btn-circle">
            <i class="icon-qna"></i>
            <p>會員須知</p>
          </span>
        </a>
        <a href="">
          <span class="btn-circle">
            <i class="icon-statistic"></i>
            <p>統計專區</p>
          </span>
        </a>
        <a href="">
          <span class="btn-circle">
            <i class="icon-blacklist"></i>
            <p>黑名單</p>
          </span>
        </a>
      </div>
      <div class="col-right">
        <p><a href="">最新消息</a>/<a href="">媒體報導</a>/<a href="">部落格分享</a>/<a href="">案例介紹</a></p>
        <p><a href="">關於我們</a>/<a href="">加入我們</a>/<a href="">聯絡我們</a></p>        
      </div>
    </div>
    <div class="row row2">
      <p><a href="">服務條款</a>/<a href="">隱私權政策</a>/<a href="">專利聲明</a></p>
      <p class="text-copycight">2015© Taiwan Fund Exchange.  All rights reserved.</p>
    </div>
  </div>
</footer>
      
      <script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script><script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js" type="text/javascript"></script><script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js" type="text/javascript"></script><script src="${pageContext.request.contextPath}/resource/assets/javascripts/initialization.js" type="text/javascript"></script><script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/swiper.js" type="text/javascript"></script><script>
    var swiperBanner = new Swiper('.bannerswiper-container', {
        pagination: '.bannerswiper-pagination',
        paginationClickable: true,
        speed: 1000,
        spaceBetween: 0,
        centeredSlides: true,
        autoplay: false,
        autoplayDisableOnInteraction: false,
        autoplay: 3000,
        effect: 'fade'
    });
    var swiperDesk = new Swiper('.swiper-container.on-desk', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        speed: 1000,
        spaceBetween: 0,
        centeredSlides: true,
        autoplay: false,
        autoplayDisableOnInteraction: false
    });
    var swiperMobile = new Swiper('.swiper-container.on-mobile', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        speed: 1000,
        spaceBetween: 0,
        centeredSlides: true,
        autoplay: false,
        autoplayDisableOnInteraction: false
    });
    //galleryTop.params.control = swiperDesk;
    galleryThumbs.params.control = swiperMobile;


</script>

    </div>


</body>
</html>