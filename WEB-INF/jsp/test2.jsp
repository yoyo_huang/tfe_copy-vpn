<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-Hant">


<head>
  
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"  />
  <meta name="Title" content="TFE" />
  <meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
  <meta name="Description" content="Description" />

<!-- iOS -->

<!-- Android -->

  <meta property="og:title" content="Hconnect Mobile" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="" />
  <meta property="og:image" content="resource/assets/images/share.jpg" />
  <meta property="og:site_name" content="Hconnect" />
  <meta property="og:description" content="Description" />
  <meta property="og:locality" content="Taipei" />
  <meta property="og:country-name" content="Taiwan" />

  <link href="resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
  <title>TFE</title>
  <link href="resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /><link href="resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />  
  <link href="resource/assets/stylesheets/vendors/swiper.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="resource/assets/stylesheets/units/index.css" media="screen" rel="stylesheet" type="text/css" />

</head>

<body>
      

  
      
<section id="scene1" style="background-image: url('');">

<div class="bannerswiper-outer">    
    <div class="bannerswiper-container">
        <div class="swiper-wrapper">
          
          Hello!!!
            
        </div>
    </div>
</div>


<section id="scene1">

    <div class="swiper-outer">    
        <div class="swiper-container on-desk">
            <div class="swiper-wrapper">
            
                <c:forEach items="${p2pList}" var="eachP2P">            
		          <c:forEach items="${eachP2P.advertisementManges}" var="adver">
		            
		            <div class="swiper-slide">		                            
		              <img width="768" height="500" src="${adver.imagePath}" />
		            </div>
		            
		          </c:forEach>			
				</c:forEach>
            <!--     
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/768x500/cccccc/ffffff');"></div>
            -->    
            </div>
            <div class="swiper-pagination"></div>
        </div>

        <div class="swiper-container on-mobile">
            <div class="swiper-wrapper">
            
           		<c:forEach items="${p2pList}" var="eachP2P">            
		          <c:forEach items="${eachP2P.advertisementManges}" var="adver">
		            
		            <div class="swiper-slide">		                            
		              <img width="320" height="340" src="${adver.imagePath}" />
		            </div>
		            
		          </c:forEach>			
				</c:forEach>
                
                
             <!--   
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
                
                <div class="swiper-slide" style="background-image: url('http://placehold.it/320x340/cccccc/ffffff');"></div>
            -->    
            </div>
            <div class="swiper-pagination"></div>
        </div>

        <div class="swiper-button-prev"></div>
        <div class="swiper-button-next"></div>
    </div>
    <!-- END OF Swiper -->
</section>

<section id="scene3">
    <h2 class="scene-title">模擬體驗</h2>
    <i class="icon-underline"></i>
    <h3 class="scene-subtitle">請讓TFE了解你的金融期望</h3>
    <div class="choose-chara">
    <div class="choose-chara-center">
        <img src="resource/assets/images/icon-longarrow-left.png" class="arrow-horizonal" alt="">
        <img src="resource/assets/images/icon-chara-choose.svg" class="choose-chara-center-img btn-lr" alt="">
        <img src="resource/assets/images/icon-longarrow-right.png" class="arrow-horizonal" alt="">
        <p>請選擇金融角色</p>
    </div>
        <div class="choose-chara-circle circle-right">
            <div class="choose-chara-innercircle"><img class="circleimg" src="resource/assets/images/icon-chara2.svg" alt=""><span class="circletxt">我是貸款者</span></div>
            <div class="choose-chara-innercircle-hover"><img class="circleimg" src="resource/assets/images/icon-chara2-hover.svg" alt=""><span class="circletxt">我是貸款者</span></div>
        </div>
        <div class="choose-chara-circle circle-left">
            <div class="choose-chara-innercircle"><img class="circleimg" src="resource/assets/images/icon-chara1.svg" alt=""><span class="circletxt">我是投資者</span></div>            
            <div class="choose-chara-innercircle-hover"><img class="circleimg" src="resource/assets/images/icon-chara1-hover.svg" alt=""><span class="circletxt">我是投資者</span></div>
        </div>
    </div>
</section>
<section id="scene4">
    <%-- --%>
    <c:forEach items="${storyList}" var="eachStory">            
      <c:forEach items="${eachStory.informationManages}" var="manage">
           
        <c:if test='${manage.showOrder eq 1}'>
        
          <div class="col-left" style="background-image: url('${manage.imageUrl}');">
          </div>
          <div class="col-right">
            <h2 class="scene-title">${manage.infoTitle}</h2>
            <i class="icon-underline"></i>
            <p class="story-content">${manage.infoContent}</p>
            <div class="btn-rounded" onclick="location.href='${manage.urlWay}'">了解我的投資故事</div>
          </div>
        
        </c:if>
           
      </c:forEach>			
	</c:forEach>
	  
	<!--   
        <div class="col-left" style="background-image: url('resource/assets/images/sample-story.jpg');">
        </div>
        <div class="col-right">
            <h2 class="scene-title">二次創業咖啡店</h2>
            <i class="icon-underline"></i>
            <p class="story-content">店裡溫馨的擺設、甜點蛋糕香氣的瀰漫，吸引著喜愛甜點的人們，在平常閒暇之餘來店裡坐坐，品嚐著我們親手做的甜品以及香濃的咖啡，人們透露出來的滿足就是</p>
            <div class="btn-rounded">了解我的投資故事</div>
        </div>
     -->  
</section>

<section id="scene5">
    <div class="layout-mw-wrapper">
    
      <c:forEach items="${infoList}" var="eachInfo">            
        <c:forEach items="${eachInfo.informationManages}" var="manage">
           
          <c:if test='${manage.showOrder eq 1}'>
            <div class="module-news-block">
                <div onclick="location.href='${manage.urlWay}'" class="module-news-img" style="background-image: url('${manage.imageUrl}');"></div>
                <div class="module-news-text">
                    <h3>${manage.infoTitle}</h3>
                    <p>${manage.infoContent}</p>
                </div>
            </div> 
          </c:if>
           
        </c:forEach>			
	  </c:forEach>
        
        
      <%--    
            <div class="module-news-block">
                <div class="module-news-img" style="background-image: url('resource/assets/images/sample-newsimg.png');"><!--img src="resource/assets/images/sample-newsimg.png" width="100%" alt=""--></div>
                <div class="module-news-text">
                    <h3>關注的應用程序改善市場時機？</h3>
                    <p>最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些</p>
                </div>
            </div>
        
            <div class="module-news-block">
                <div class="module-news-img" style="background-image: url('resource/assets/images/sample-newsimg.png');"><!--img src="resource/assets/images/sample-newsimg.png" width="100%" alt=""--></div>
                <div class="module-news-text">
                    <h3>關注的應用程序改善市場時機？</h3>
                    <p>最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些</p>
                </div>
            </div>
        
            <div class="module-news-block">
                <div class="module-news-img" style="background-image: url('resource/assets/images/sample-newsimg.png');"><!--img src="resource/assets/images/sample-newsimg.png" width="100%" alt=""--></div>
                <div class="module-news-text">
                    <h3>關注的應用程序改善市場時機？</h3>
                    <p>最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些最優投資組合管理要求實時，包括稅費，再平衡，風險和現金流一樣間接變量管理的投資組合。這是我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些我們的工作是微調這些</p>
                </div>
            </div>
      --%>  
    </div>
    <div class="scene5-bottom">
        <div class="btn-readmore"><span>查看更多<!-- <br/><img src="resource/assets/images/icon-arrow-down-orange-s.png" alt=""> --></span></div>    
    </div>
</section>
      </div>

</body>
</html>