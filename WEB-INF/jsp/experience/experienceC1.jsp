<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="Title" content="TFE" />
<meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
<meta name="Description" content="Description" />
<!-- iOS -->
<!-- Android -->
<meta property="og:title" content="Hconnect Mobile" />
<meta property="og:type" content="website" />
<meta property="og:url" content="" />
<meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/share.jpg" />
<meta property="og:site_name" content="Hconnect" />
<meta property="og:description" content="Description" />
<meta property="og:locality" content="Taipei" />
<meta property="og:country-name" content="Taiwan" />
<link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
<title>TFE</title>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
	type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/swiper.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/product-list.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/experience.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="layout-container">
		<div class="layout-view">
			<section class="section-banner" style="background-image: url(${pageContext.request.contextPath}/resource/assets/images/sub-bbg.jpg)">
				<h2>模擬體驗</h2>
				<h3>找尋適合自己的競標組合</h3>
			</section>
			<!--subsection start-->
			<!--combination-menu start-->
			<section class="section-search">
				<div class="layout-mw-wrapper">
					<div class="experience_wp">
						<div class="experience_form">
							<div class="form_prompt">${seMessage.message1}</div>
							<div class="form_prompt_m">${seMessage.message1}</div>
							<!-- 							<form action="experienceC2"> -->
							<div class="module-customselect style-l input_color">
								<span class="module-customselect-selected input_color" style="background: none;">${semTypeWord}</span>
								<input type="hidden" id="semType" value="${semType}" name="semType">
							</div>
							<div class="module-customselect style-l input_color">
								<div></div>
								<span class="module-customselect-selected input_color">${question1source.get(0).question1}</span>
								<input type="hidden" value="" id="question1">
								<ul id="q1ul">
									<c:choose>
									<c:when test="${not empty question1source}">
										<c:forEach var="question1" items="${question1source}" varStatus="loop">
											<li value="${question1.seId}">${question1.answer1}</li>
										</c:forEach>
									</c:when>
									<c:otherwise>
										<li value="">查無資料</li>
									</c:otherwise>
									</c:choose>
								</ul>
							</div>
							<div class="module-customselect style-l input_color">
								<span class="module-customselect-selected input_color" id="q2ulText">第二個問題</span>
								<input type="hidden" value="" id="question2">
								<ul id="q2ul">
									<li value="">查無資料</li>
								</ul>
							</div>
							<button class="btn-selectlike style-l " onclick="checkValue();">開始配對組合</button>
							<!-- 							</form> -->
						</div>
						<div class="top20">&nbsp;</div>
						<div class="experience_pg">
							<div class="experience_progress ">
								<div class="experience_progress_in in01">&nbsp;</div>
							</div>
							<ul class="progress_list hidd">
								<li class="mfirst current">選擇產品</li>
								<li class="second hidd">加入</li>
								<li class="second hidd">下標</li>
								<li class="end hidd">得標</li>
							</ul>
						</div>
					</div>
				</div>
			</section>
			<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
		</div>
	</div>
	<script>
		function checkValue() {
			var semType = $("#semType").val();
			var question1 = $("#question1").val();
			var question2 = $("#question2").val();
			if (!question1.trim() || !question2.trim()) {
				alert("請選擇");
				return false;
			}
			window.open('${pageContext.request.contextPath}/experienceC2?semType=' + semType + '&seId=' + question2, '_self');
		}

		function submitComment(q1seId) {
			//console.log("q1seId:" + q1seId);
			$.ajax({
				type : "POST",
				url : "<c:url value='/experienceC1/getQuestion2'/>",
				data : JSON.stringify({
					semType : $("#semType").val(),
					seId : q1seId
				}),
				dataType : "json",
				contentType : "application/json; charset=UTF-8",
				cache : false,
				async : false,
				success : function(json) { //取得回傳訊息
					//console.log('leaveMessages size:' + json.question2source.length);
					if (json.question2source) {
						$("#q2ulText").text(json.question2source[0].question2);
						for ( var key in json.question2source) {
							var q2seId = json.question2source[key].seId;
							var q2answer2 = json.question2source[key].answer2;
							//console.log("q2seId:" + q2seId);
							//console.log("q2question2" + q2question2);
							$("#q2ul").append('<li value='+q2seId+'>' + q2answer2 + '</li>');
						}
					}
				},
				error : function() {
					console.log("error");
				}
			});
		}

		function refreshQustion2li(question1Val) {
			$("#q2ul").empty();
			$("#question2").val("");
			$("#q2ulText").text("第二個問題");
			submitComment(question1Val);
		}
		$(document).ready(function() {
			$("#question1").val("");
			$("#question2").val("");
			$("#q1ul li").each(function() {
				$(this).click(function() {
					//console.log($(this).val());
					refreshQustion2li($(this).val());
				});
			});

		});
	</script>
</body>
</html>