<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="Title" content="TFE" />
<meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
<meta name="Description" content="Description" />
<!-- iOS -->
<!-- Android -->
<meta property="og:title" content="Hconnect Mobile" />
<meta property="og:type" content="website" />
<meta property="og:url" content="" />
<meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/share.jpg" />
<meta property="og:site_name" content="Hconnect" />
<meta property="og:description" content="Description" />
<meta property="og:locality" content="Taipei" />
<meta property="og:country-name" content="Taiwan" />
<link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
<title>TFE</title>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
	type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/about.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/experience.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/bidShlvAndSell.js" type="text/javascript"></script>
<body>
	<div class="layout-container">
		<div class="layout-view">
			<section class="section-banner" style="height: 0;"></section>
			<section class="section-banner " style="background-image: url(${pageContext.request.contextPath}/resource/assets/images/sub-bbg.jpg)">
				<h2>模擬體驗</h2>
			</section>
			<!--subsection end-->
			<section class="section-content">
				<div class="layout-mw-wrapper">
					<!--PAGE CONTENT START-->
					<!---->
					<div class="product_wp">
						<div class="begin">已開始</div>
						<div class="cb-products">
							<div class="m_c3">
								<h2>
									${seMessage.message3}
<!-- 									您可在「會員專區」 找到已加入的競標組合。點選 -->
<%-- 									<img src="${pageContext.request.contextPath}/resource/assets/images/hammer.png"> --%>
<!-- 									進行下標。 -->
								</h2>
							</div>
							<ul class="products">
								<!-- PER ITEM -->
								<li class="m_interact" id="experienceC3_1" />
								<script type="text/javascript">
									showBidshlvForExperience("experienceC3_1","${question.productNo}", "${question.trustLevel}", "${question.payment}","${question.periodsOfTotal}","${question.numberOfNow}","${question.numberOfTotal}",1,true,"${question.periodsOfNow}","");
								</script>	
								<!--END OF PER ITEM -->
								<!-- PER ITEM -->								
								<li id="experienceC3_2" />								
								<script type="text/javascript">
									showBidshlvForExperience("experienceC3_2","H0123456789", "A4", 300000,12,400,999,1,false,"1","");
								</script>	
								<!--END OF PER ITEM -->
								<!-- PER ITEM -->
								<li id="experienceC3_3" />
								<script type="text/javascript">
									showBidshlvForExperience("experienceC3_3","H0123456789", "A4", 300000,12,400,999,1,false,"1","");
								</script>	
								<!--END OF PER ITEM -->								
								<!-- PER ITEM -->
								<li class="w_interact" id="experienceC3_4" />
								<script type="text/javascript">
									showBidshlvForExperience("experienceC3_4","${question.productNo}", "${question.trustLevel}", "${question.payment}","${question.periodsOfTotal}","${question.numberOfNow}","${question.numberOfTotal}",1,true,"${question.periodsOfNow}","");
								</script>									
								<!--END OF PER ITEM -->								
								<li class="learn_more">
									<div class="c3">
										<h2>
											${seMessage.message3}
<!-- 											您可在「會員專區」 -->
<!-- 											<br> -->
<!-- 											找到已加入的競標組合。 -->
											<br>
											點選
											<img src="${pageContext.request.contextPath}/resource/assets/images/hammer.png">
											進行下標。
										</h2>
									</div>
								</li>
							</ul>
						</div>
						<div class="experience_pg">
							<div class="experience_progress">
								<div class="experience_progress_in in03">&nbsp;</div>
							</div>
							<ul class="progress_list hidd">
								<li class="mfirst hidd">選擇產品</li>
								<li class="second hidd">加入</li>
								<li class="second current">下標</li>
								<li class="end hidd">得標</li>
							</ul>
						</div>
					</div>
					<!---->
					<!--PAGE END-->
				</div>
			</section>
			<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
		</div>
	</div>
	<div style="display: none;">
		<div class="col_box " id="col_box">
			<section class="bidpopup" id="popup_content">
				<h3>下標金額拖拉</h3>
				<div class="m_tip">
<!-- 					<div class="c4_txt">您可使用左側拖曳，或是直接在輸入框輸入金額來下標，下標金額就是您期望的借款利息，輸入完畢請點選確認下標。</div> -->
					<div class="c4_txt">${seMessage.message4}</div>
				</div>
				<dl class="module-dl style-rounded on-mobile">
					<dt>下標金額</dt>
					<dd>
						<input type="text" class="js-amount-input-m" value="">
					</dd>
				</dl>
				<p class="num-max">Max</p>
				<div class="col-left">
					<div class="col-left-bar">
						<input type="range" data-orientation="vertical" value="0">
					</div>
					<div class="col-left-bidder" style="margin-top: 150px">
						<div class="per-bidder">
							<span class="bidder-price">1200</span>
							<span class="bidder-profile" data-price=1200>
								信用金1200
								<i>/</i>
								tempora
							</span>
						</div>
						<div class="per-bidder" style="padding-top: 30px;">
							<span class="bidder-price">792</span>
							<span class="bidder-profile" data-price=792>
								信用金792
								<i>/</i>
								accusantium
							</span>
						</div>
						<div class="per-bidder" style="padding-top: 75px;">
							<span class="bidder-price">123</span>
							<span class="bidder-profile" data-price=123>
								信用金123
								<i>/</i>
								dolores
							</span>
						</div>
					</div>
					<p class="num-min">min</p>
				</div>
				<div class="col-right">
					<div class="w_tip">
<!-- 						<div class="c4_txt">您可使用左側拖曳，或是直接在輸入框輸入金額來下標，下標金額就是您期望的借款利息，輸入完畢請點選確認下標。</div> -->
						<div class="c4_txt">${seMessage.message4}</div>
					</div>
					<dl class="module-dl style-rounded on-desk">
						<dt>下標金額</dt>
						<dd>
							<input type="text" class="js-amount-input" id="bidPayment" name="bidPayment" value="0">
						</dd>
					</dl>
					<dl class="module-dl">
						<dt>
							<span>現在期數/總期數</span>
						</dt>
						<dd>
							<span>${question.periodsOfNow}&nbsp;/&nbsp;${question.periodsOfTotal}</span>
						</dd>
						<dt>
							<span>可得標人數</span>
						</dt>
						<dd>
							<span>${question.biddedCnt}</span>
						</dd>
						<dt>
							<span>剩餘額度</span>
						</dt>
						<dd>
							<span><fmt:formatNumber value="${question.workSelfAccuCredit}" type="number"/></span>
						</dd>
						<dt>
							<span>得標總資金</span>
						</dt>
						<dd>
							<span><fmt:formatNumber value="${question.wonBidPrice}" type="number"/></span>
						</dd>
						<dt>
							<span>信用金</span>
						</dt>
						<dd>
							<span><fmt:formatNumber value="${question.guranCreditFee}" type="number"/></span>
						</dd>
						<dt>
							<span>管理費</span>
						</dt>
						<dd>
							<span><fmt:formatNumber value="${question.sysUseFee}" type="number"/></span>
						</dd>
					</dl>
					<div class="btn-wrap">
						<div class="btn-rounded style-gray" id="bidCancel" >取消下標</div>
						<!--
						<div class="btn-rounded" onclick='window.open("${pageContext.request.contextPath}/experienceC4?semType=${question.semType}&seId=${question.seId}","_self")'>確認下標</div>
						-->
						<div class="btn-rounded" onclick='doSubmit()'>確認下標</div>
					</div>
					<input type="hidden" name="min" value="0">
					<br />
					<input type="hidden" name="max" value="${question.payment}">
					<br />
					<input type="hidden" name="step" value="1">
				</div>
			</section>
		</div>
	</div>
</body>
<script>
	function formatNumber(str, glue) {
		if (isNaN(str)) {
			return NaN;
		}
		var glue = (typeof glue == 'string') ? glue : ',';
		var digits = str.toString().split('.'); // 先分左邊跟小數點
		var integerDigits = digits[0].split(""); // 獎整數的部分切割成陣列
		var threeDigits = []; // 用來存放3個位數的陣列
		while (integerDigits.length > 3) {
			threeDigits.unshift(integerDigits.splice(integerDigits.length - 3, 3).join(""));
		}
		threeDigits.unshift(integerDigits.join(""));
		digits[0] = threeDigits.join(glue);
		return digits.join(".");
	}

	$(document).ready(function() {
		var toltalNumber = '${question.numberOfTotal}';
		var nowNumber = '${question.numberOfNow}';
		var intToltalNumber = Number(toltalNumber);
		var intNowNumber = Number(nowNumber);
		var percent = (intNowNumber / intToltalNumber);
		//console.log("percent:"+percent);
		var w = $("#percentBar").width();
		var w2 = $("#percentBarM").width();
		//console.log("w2:"+w2);
		$("#percentBar").width(w * percent);
		$("#percentBarM").width(w2 * percent);
		var paymentMoney = '${question.payment}';
		var intPaymentMoney = Number(paymentMoney);
		$("#payment").html(formatNumber(intPaymentMoney));
		$("#paymentM").html(formatNumber(intPaymentMoney));
		
		$("#bidCancel").click(function(){
			// $.fn.colorbox.close();
			window.open("${pageContext.request.contextPath}/experienceC4?semType=${question.semType}&seId=${question.seId}&bidPayment=0","_self");
		});
		
	});
	
	function doSubmit(){
		var bidPayment = $("#bidPayment").val();
		window.open("${pageContext.request.contextPath}/experienceC4?semType=${question.semType}&seId=${question.seId}&bidPayment=" + bidPayment,"_self");
	}
</script>
</html>