<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="Title" content="TFE" />
<meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
<meta name="Description" content="Description" />
<!-- iOS -->
<!-- Android -->
<meta property="og:title" content="Hconnect Mobile" />
<meta property="og:type" content="website" />
<meta property="og:url" content="" />
<meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/share.jpg" />
<meta property="og:site_name" content="Hconnect" />
<meta property="og:description" content="Description" />
<meta property="og:locality" content="Taipei" />
<meta property="og:country-name" content="Taiwan" />
<link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
<title>TFE</title>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
	type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/swiper.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/product-list.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/experience.css" media="screen" rel="stylesheet" type="text/css" />
<style>
	#cboxContent{
		padding-bottom: 20px;
	}
	@media only screen and (max-width: 1024px){	
		#cboxLoadedContent,#cboxContent,#cboxWrapper{
				height: initial !important;
		}
	}
</style>
</head>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/bidShlvAndSell.js" type="text/javascript"></script>
<body>
	<div class="layout-container">
		<div class="layout-view">
			<div class="layout-view">
				<section class="section-banner" style="height: 0;"></section>
				<section class="section-banner " style="background-image: url(${pageContext.request.contextPath}/resource/assets/images/sub-bbg.jpg)">
					<h2>模擬體驗</h2>
					<h3>找尋適合自己的競標組合</h3>
				</section>
				<!--subsection start-->
				<!--combination-menu start-->
				<section class="section-search">
					<div class="layout-mw-wrapper">
						<div class="experience_wp top20">
							<div class="cb-products">
								<div class="m_c2 clearfix">
									<h2>
										${seMessage.message2}
										<!-- 										這些圓圈是可能適合您的競標組合。 -->
										<br>
										<font color="red"><b>請點選 (
										<img src="${pageContext.request.contextPath}/resource/assets/images/add_product.png">
										) 來加入。</b></font>
									</h2>
									<div class="txt2">若您想再了解圈內訊息，請點選</div>
									<div class="ex-btn-wrap-w">
										<a href="#col_box" class="do-popup-login cboxElement">
											<div class="ex-rounded style-white">了解更多</div>
										</a>
									</div>
								</div>
								<ul class="products clearfix ">									
									<!-- PER ITEM -->
									<li class="m_interact" id="experienceC2_1" />
									<script type="text/javascript">
										showBidshlvForExperience("experienceC2_1","${question.productNo}", "${question.trustLevel}", "${question.payment}","${question.periodsOfTotal}","${question.numberOfNow}","${question.numberOfTotal}",0,true,"","${pageContext.request.contextPath}/experienceC3?semType=${question.semType}&seId=${question.seId}");
									</script>	
									<!--END OF PER ITEM -->
									<!-- PER ITEM -->									
									<li id="experienceC2_2" />
									<script type="text/javascript">
										showBidshlvForExperience("experienceC2_2","H0123456789", "D3", 300000,36,0,36,0,false,"","");
									</script>											
									<!--END OF PER ITEM -->
									<!-- PER ITEM -->									
									<li id="experienceC2_3" />
									<script type="text/javascript">
										showBidshlvForExperience("experienceC2_3", "I0123456789", "A4", 300000,36,400,999,0,false,"","");
									</script>
									<!--END OF PER ITEM -->
									<!-- PER ITEM -->																		
									<li class="w_interact" id="experienceC2_4" />
									<script type="text/javascript">
										showBidshlvForExperience("experienceC2_4","${question.productNo}", "${question.trustLevel}", "${question.payment}","${question.periodsOfTotal}","${question.numberOfNow}","${question.numberOfTotal}",0,true, "","${pageContext.request.contextPath}/experienceC3?semType=${question.semType}&seId=${question.seId}");
									</script>
									<!--END OF PER ITEM -->																	
									<li class="learn_more">
										<div class="c2 clearfix">
											<h2>
												${seMessage.message2}
												<!-- 												這些圓圈是可能適合您的競標組合。 -->
												<br>
												<font color="red"><b>請點選 (
												<img src="${pageContext.request.contextPath}/resource/assets/images/add_product.png">
												) 來加入。</b></font>
											</h2>
											<div class="txt2">若您想再了解圈內訊息，請點選</div>
											<div class="ex-btn-wrap-w">
												<a href="#col_box" class="do-popup-login cboxElement">
													<div class="ex-rounded style-white">了解更多</div>
												</a>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<div class="experience_pg">
								<div class="experience_progress">
									<div class="experience_progress_in in02">&nbsp;</div>
								</div>
								<ul class="progress_list hidd">
									<li class="mfirst hidd">選擇產品</li>
									<li class="second current">加入</li>
									<li class="second hidd">下標</li>
									<li class="end hidd">得標</li>
								</ul>
							</div>
						</div>
					</div>
				</section>
				<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
			</div>
		</div>
		
		<div style="display: none;">
			<div id="col_box" class="col_box">
				<img id="img-loading" src="${pageContext.request.contextPath}/resource/assets/images/01_bid_illustrate.jpg" />
			</div>
		</div>		
</body>
<script>
	function formatNumber(str, glue) {
		if (isNaN(str)) {
			return NaN;
		}
		var glue = (typeof glue == 'string') ? glue : ',';
		var digits = str.toString().split('.'); // 先分左邊跟小數點
		var integerDigits = digits[0].split(""); // 獎整數的部分切割成陣列
		var threeDigits = []; // 用來存放3個位數的陣列
		while (integerDigits.length > 3) {
			threeDigits.unshift(integerDigits.splice(integerDigits.length - 3, 3).join(""));
		}
		threeDigits.unshift(integerDigits.join(""));
		digits[0] = threeDigits.join(glue);
		return digits.join(".");
	}

	$(document).ready(function() {
		var toltalNumber = '${question.numberOfTotal}';
		var nowNumber = '${question.numberOfNow}';
		var intToltalNumber = Number(toltalNumber);
		var intNowNumber = Number(nowNumber);
		var percent = (intNowNumber / intToltalNumber);
		//console.log("percent:"+percent);
		var w = $("#percentBar").width();
		var w2 = $("#percentBarM").width();
		//console.log("w2:"+w2);
		$("#percentBar").width(w * percent);
		$("#percentBarM").width(w2 * percent);
		var paymentMoney = '${question.payment}';
		var intPaymentMoney = Number(paymentMoney);
		$("#payment").html(formatNumber(intPaymentMoney));
		$("#paymentM").html(formatNumber(intPaymentMoney));
		
		$.colorbox({ inline: true, href: "#col_box" });
		$.colorbox.resize({width:"900px" , height:"520px"})
	});
</script>
</html>