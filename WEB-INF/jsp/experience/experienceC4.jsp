<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="Title" content="TFE" />
<meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
<meta name="Description" content="Description" />
<!-- iOS -->
<!-- Android -->
<meta property="og:title" content="Hconnect Mobile" />
<meta property="og:type" content="website" />
<meta property="og:url" content="" />
<meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/share.jpg" />
<meta property="og:site_name" content="Hconnect" />
<meta property="og:description" content="Description" />
<meta property="og:locality" content="Taipei" />
<meta property="og:country-name" content="Taiwan" />
<link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
<title>TFE</title>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
	type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/about.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/experience.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/bidShlvAndSell.js" type="text/javascript"></script>
<body>
	<div class="layout-container">
		<div class="layout-view">
			<section class="section-banner" style="height: 0;"></section>
			<section class="section-banner " style="background-image: url(${pageContext.request.contextPath}/resource/assets/images/sub-bbg.jpg)">
				<h2>模擬體驗</h2>
			</section>
			<!--subsection end-->
			<section class="section-content">
				<div class="layout-mw-wrapper">
					<!--PAGE CONTENT START-->
					<!---->
					<div class="product_wp">
						<div class="begin">已開始</div>
						<div class="cb-products">
							<div class="m_c5">
								<h2>
									${seMessage.message5}
									<!-- 									恭喜您得標了！ -->
									<!-- 									<br> -->
									<!-- 									根據每期得標人數，您的下標標金高低將決定您得標與否。若您得標即可取得借款，並逐期還款已累積信用，有借有還，再借不難！ -->
								</h2>
								<div class="ex-btn-wrap-m">
									<div class="ex-rounded style-white" onclick='window.open("${pageContext.request.contextPath}/qa","_self");'>了解更多</div>
									<div class="ex-rounded  style-white" onclick='window.open("${pageContext.request.contextPath}/experienceC1?semType=${question.semType}","_self");'>再次體驗</div>
									<div class="ex-rounded " onclick='window.open("${pageContext.request.contextPath}/register","_self");'>註冊會員</div>
								</div>
							</div>
							<ul class="products">
								<!-- PER ITEM -->
								<li id="experienceC4_1" />
								<script type="text/javascript">
									showBidshlvForExperience("experienceC4_1","${question.productNo}", "${question.trustLevel}", "${question.payment}","${question.periodsOfTotal}","${question.numberOfNow}","${question.numberOfTotal}",2,false,"${question.periodsOfNow}","");
								</script>	
								<!--END OF PER ITEM -->
								<!-- PER ITEM -->								
								<li id="experienceC4_2" />
								<script type="text/javascript">
									showBidshlvForExperience("experienceC4_2","H0123456789", "A4", 300000,12,400,999,2,false,"1","");
								</script>	
								<!--END OF PER ITEM -->
								<!-- PER ITEM -->								
								<li id="experienceC4_3" />
								<script type="text/javascript">
									showBidshlvForExperience("experienceC4_3","${question.productNo}", "${question.trustLevel}", "${question.payment}","${question.periodsOfTotal}","${question.numberOfNow}","${question.numberOfTotal}",3,true,"${question.periodsOfNow}","");
								</script>	
								<!--END OF PER ITEM -->															
								<li class="learn_more">
									<div class="c5">
										<h2>
											${seMessage.message5}
											<!-- 											恭喜您得標了！ -->
											<!-- 											<br> -->
											<!-- 											根據每期得標人數，您的下標標金高低將決定您得標與否。若您得標即可取得借款，並逐期還款已累積信用，有借有還，再借不難！ -->
										</h2>
										<div class="ex-btn-wrap-w">
											<div class="ex-rounded style-white" onclick='window.open("${pageContext.request.contextPath}/qa","_self");'>了解更多</div>
											<div class="ex-rounded  style-white" onclick='window.open("${pageContext.request.contextPath}/experienceC1?semType=${question.semType}","_self");'>再次體驗</div>
											<div class="ex-rounded " onclick='window.open("${pageContext.request.contextPath}/register","_self");'>註冊會員</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="experience_pg">
							<div class="experience_progress">
								<div class="experience_progress_in in04">&nbsp;</div>
							</div>
							<ul class="progress_list hidd">
								<li class="mfirst hidd">選擇產品</li>
								<li class="second hidd">加入</li>
								<li class="second hidd">下標</li>
								<li class="end current">得標</li>
							</ul>
						</div>
					</div>
					<!---->
					<!--PAGE END-->
				</div>
			</section>
			<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
		</div>
	</div>
</body>
<script>
	function formatNumber(str, glue) {
		if (isNaN(str)) {
			return NaN;
		}
		var glue = (typeof glue == 'string') ? glue : ',';
		var digits = str.toString().split('.'); // 先分左邊跟小數點
		var integerDigits = digits[0].split(""); // 獎整數的部分切割成陣列
		var threeDigits = []; // 用來存放3個位數的陣列
		while (integerDigits.length > 3) {
			threeDigits.unshift(integerDigits.splice(integerDigits.length - 3, 3).join(""));
		}
		threeDigits.unshift(integerDigits.join(""));
		digits[0] = threeDigits.join(glue);
		return digits.join(".");
	}

	$(document).ready(function() {
		var toltalNumber = '${question.numberOfTotal}';
		var nowNumber = '${question.numberOfNow}';
		var intToltalNumber = Number(toltalNumber);
		var intNowNumber = Number(nowNumber);
		var percent = (intNowNumber / intToltalNumber);
		//console.log("percent:"+percent);
		var w = $("#percentBar").width();
		var w2 = $("#percentBarM").width();
		//console.log("w2:"+w2);
		$("#percentBar").width(w * percent);
		$("#percentBarM").width(w2 * percent);
		var paymentMoney = '${question.payment}';
		var intPaymentMoney = Number(paymentMoney);
		$("#payment").html(formatNumber(intPaymentMoney));
		$("#paymentM").html(formatNumber(intPaymentMoney));
	});
</script>
</html>