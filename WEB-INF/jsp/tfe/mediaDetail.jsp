<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="zh_TW" />
<html lang="zh-tw">
<head>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
	type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/report.css" media="screen" rel="stylesheet" type="text/css" />
<style>
.shortTitle {
	width: 100%;
	white-space: nowrap;
	text-overflow: ellipsis;
	-o-text-overflow: ellipsis;
	overflow: hidden;
}

.shortTitle1 {
	width: 100%;
	white-space: nowrap;
	text-overflow: ellipsis;
	-o-text-overflow: ellipsis;
	overflow: hidden;
}

.shortContent {
	width: 100%;
	white-space: nowrap;
	text-overflow: ellipsis;
	-o-text-overflow: ellipsis;
	overflow: hidden;
}

.shortContentLineWrap {
	width: 100%;
	height: 140px; //
	white-space: nowrap;
	text-overflow: ellipsis;
	-o-text-overflow: ellipsis;
	overflow: hidden;
}

.explicit .txt18 {
	font-size: 1.8em;
	line-height: 28px;
	margin-top: 29px;
	color: #a0a0a0
}
.corner1 {
	margin: 20px 0px 10px 0px;
	border-radius: 10px;
	background-color: #f1f1f1;
	text-align: center;
	padding: 10px 20px 20px 20px;
}
</style>
</head>
<body>
	<div class="layout-container">
		<div class="layout-view">
			<section class="section-banner" style="height: 0;"></section>
			<!--subsection start-->
			<section class="section-proflie-banner accordion-menu">
				<!--laptop start-->
				<div class="layout-mw-wrapper">
					<div class="sbheader-center">
						<div class="cb-list3">
							<ul>
								<li class="on-mobile">媒體報導</li>
								<li>
									<a href="news">最新消息</a>
								</li>
								<li>
									<a class="is-active" href="media">媒體報導</a>
								</li>
								<li>
									<a href="story">案例介紹</a>
								</li>
								<li>
									<a href="https://www.youtube.com/channel/UCk0CzSupZWrcnaSXRmNXFyA/videos?view=0&sort=dd&shelf_id=0" target="_blank">影片分享</a>
								</li>
							</ul>
						</div>
					</div>
					<!--laptop end-->
				</div>
			</section>
			<!--subsection end-->
			<section class="section-content">
				<div class="layout-mw-wrapper">
					<!--PAGE CONTENT START-->
					<jsp:include page="/WEB-INF/jsp/tfe/informationAndStoryDetail.jsp"></jsp:include>
					<!--PAGE END-->
				</div>
			</section>
			<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
		</div>
	</div>
	<script>
		$(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
		var pageLocation = 'media';

		function jumpAnotherPage(id) {
			//alert(id);
			window.open(pageLocation + 'Detail?id=' + id, '_self');
		}
		
		function jumpToList() {
			//alert(pageIndex);
			window.open(pageLocation + '?page=' + ${page}, '_self');
		}	

		function replaceHtmlCode(ilmAccID,ilmDate,ilmMessage,ilmMAccID1,ilmMDate,ilmResponse1){
			var replaceHtml = "";
			console.log("replaceHtml:"+replaceHtml);
			replaceHtml +=  '	<div class="corner1 table-wrap">           '
			+'		<table width="100%" class="pure-table">                '
			+'			<thead>                                            '
			+'				<tr>                                           '
			+'					<th align="left" width="100px">發問人</th>   '
			+'					<th align="left" width="150px">時間</th>    '
			+'					<th align="left">問題內容</th>               '
			+'				</tr>                                          '
			+'			</thead>                                           '
			+'			<tbody>                                            '
			+'				<tr>                                           '
			+'					<td align="left">'+ilmAccID+'</td>         '
			+'					<td align="left">'+ilmDate+'</td>          '
			+'					<td align="left">'+ilmMessage+'</td>       '
			+'				</tr>                                          '
			+'			</tbody>                                           '
			
			if(ilmMAccID1.length>0&&ilmMDate.length>0&&ilmResponse1.length>0){
				replaceHtml +=''
				+'<thead>                                                      '
				+'				<tr>                                           '
				+'					<th align="left" width="100px">解答人</th>   '
				+'					<th align="left" width="150px">時間</th>    '
				+'					<th align="left" align="left">解答內容</th>  '
				+'				</tr>                                          '
				+'			</thead>                                           '
				+'			<tbody>                                            '
				+'				<tr>                                           '
				+'					<td align="left">'+ilmMAccID1+'</td>       '
				+'					<td align="left">'+ilmMDate+'</td>         '
				+'					<td align="left">'+ilmResponse1+'</td>     '
				+'				</tr>                                          '
				+'			</tbody>                                           '

			}
			replaceHtml+=''
			+'		</table>                                               '
			+'	</div>			                                           ';
			
			console.log("replaceHtml:"+replaceHtml);
			return replaceHtml;
		}
		function addHtmlPagger(stringPage,stringTotal){
			
			var page = Number(stringPage);
			var total = Number(stringTotal);
			
			var addHtmlPagger = "";
			console.log("addHtmlPagger:"+addHtmlPagger);
			
			addHtmlPagger += ' <div class="page_no"> ';
			
			if(total>1&&page!=1){
				addHtmlPagger += ' <a href="javascript: jumpPage('+(page-1)+');">&lt;</a> ';
			}
			for (var i = 1; i <= total; i++) {
				addHtmlPagger += ' <a href="javascript: jumpPage('+i+');" ';
				if(i==page){
					addHtmlPagger +=' class="cass" ';
				}
				addHtmlPagger += ' >' + i + '</a> ';
			}
			if(total>1&&total!=page){
				addHtmlPagger += ' <a href="javascript: jumpPage('+(page+1)+');">&gt;</a> ';
			}
			addHtmlPagger += ' </div>	';
			console.log("addHtmlPagger:"+addHtmlPagger);
			return addHtmlPagger;
		}
		
		function submitComment(){
			$.ajax({
				type:"POST",
				dataType : "json",
				url: "<c:url value='/informationAndStory/sendComment'/>",
				data:{
					imId:"${detail.id.imid}",
					ilmMessage:$("#comment").val()
				},
				cache:false, 
				async:false,
				success: function(json){
					console.log('page:'+json.page);
					console.log('total:'+json.total);
					console.log('leaveMessages size:'+json.leaveMessages.length);
					$('#leaveMessgeArea').html("");
					var insertHtml = $('#leaveMessgeArea').html();
					if(json.leaveMessages.length>0){
						for(var key in json.leaveMessages){
							console.log(  json.leaveMessages[key].ilmAccID        );
							console.log(  json.leaveMessages[key].ilmDate         );
							console.log(  json.leaveMessages[key].ilmMessage      );
							console.log(  json.leaveMessages[key].ilmMAccID1      );
							console.log(  json.leaveMessages[key].ilmMDate        );
							console.log(  json.leaveMessages[key].ilmResponse1    );
							
							insertHtml += replaceHtmlCode( json.leaveMessages[key].ilmAccID, json.leaveMessages[key].ilmDate, json.leaveMessages[key].ilmMessage,json.leaveMessages[key].ilmMAccID1, json.leaveMessages[key].ilmMDate, json.leaveMessages[key].ilmResponse1	);
						}
						insertHtml += addHtmlPagger(json.page,json.total);
						$('#leaveMessgeArea').html(insertHtml);
					}
				}, 
				error: function(message){
					console.log("error:"+message);
				}
			});
		}	
		
		
		function submitComment(){
			$.ajax({
				type:"POST",
				dataType : "json",
				url: "<c:url value='/informationAndStory/sendComment'/>",
				data:{
					imId:"${detail.id.imid}",
					ilmMessage:$("#comment").val()
				},
				cache:false, 
				async:false,
				success: function(json){
					console.log('page:'+json.page);
					console.log('total:'+json.total);
					console.log('leaveMessages size:'+json.leaveMessages.length);
					$('#leaveMessgeArea').html("");
					var insertHtml = $('#leaveMessgeArea').html();
					if(json.leaveMessages.length>0){
						for(var key in json.leaveMessages){
							console.log(  json.leaveMessages[key].ilmAccID        );
							console.log(  json.leaveMessages[key].ilmDate         );
							console.log(  json.leaveMessages[key].ilmMessage      );
							console.log(  json.leaveMessages[key].ilmMAccID1      );
							console.log(  json.leaveMessages[key].ilmMDate        );
							console.log(  json.leaveMessages[key].ilmResponse1    );
							
							insertHtml += replaceHtmlCode( json.leaveMessages[key].ilmAccID, json.leaveMessages[key].ilmDate, json.leaveMessages[key].ilmMessage,json.leaveMessages[key].ilmMAccID1, json.leaveMessages[key].ilmMDate, json.leaveMessages[key].ilmResponse1	);
						}
						insertHtml += addHtmlPagger(json.page,json.total);
						$('#leaveMessgeArea').html(insertHtml);
					}
				}, 
				error: function(message){
					console.log("error:"+message);
				}
			});
		}	
		
		
		function jumpPage(page){
			$.ajax({
				type:"POST",
				dataType : "json",
				url: "<c:url value='/informationAndStory/findAllComment'/>",
				data:{
					imId : "${detail.id.imid}",
					page : page
				},
				cache:false, 
				async:false,
				success: function(json){
					console.log('page:'+json.page);
					console.log('total:'+json.total);
					console.log('leaveMessages size:'+json.leaveMessages.length);
					$('#leaveMessgeArea').html("");
					var insertHtml = $('#leaveMessgeArea').html();
					if(json.leaveMessages.length>0){
						for(var key in json.leaveMessages){
							console.log(  json.leaveMessages[key].ilmAccID        );
							console.log(  json.leaveMessages[key].ilmDate         );
							console.log(  json.leaveMessages[key].ilmMessage      );
							console.log(  json.leaveMessages[key].ilmMAccID1      );
							console.log(  json.leaveMessages[key].ilmMDate        );
							console.log(  json.leaveMessages[key].ilmResponse1    );
							
							insertHtml += replaceHtmlCode( json.leaveMessages[key].ilmAccID, json.leaveMessages[key].ilmDate, json.leaveMessages[key].ilmMessage,json.leaveMessages[key].ilmMAccID1, json.leaveMessages[key].ilmMDate, json.leaveMessages[key].ilmResponse1	);
						}
						insertHtml += addHtmlPagger(json.page,json.total);
						$('#leaveMessgeArea').html(insertHtml);
					}
				}, 
				error: function(message){
					console.log("error:"+message);
				}
			});
		}			
	</script>
</body>
</html>
