<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="zh-tw">
<head>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
	type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/report.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="layout-container">
		<div class="layout-view">
			<section class="section-banner" style="height: 0;"></section>
			<!--subsection start-->
			<section class="section-proflie-banner accordion-menu">
				<!--laptop start-->
				<div class="layout-mw-wrapper">
					<div class="sbheader-center">
						<div class="cb-list3">
							<ul>
								<li class="on-mobile">媒體報導</li>
								<li>
									<a href="news">最新消息</a>
								</li>
								<li>
									<a href="media">媒體報導</a>
								</li>
								<li>
									<a href="story">案例介紹</a>
								</li>
								<li>
									<a href="https://www.youtube.com/channel/UCk0CzSupZWrcnaSXRmNXFyA/videos?view=0&sort=dd&shelf_id=0" target="_blank">影片分享</a>
								</li>
							</ul>
						</div>
					</div>
					<!--laptop end-->
				</div>
			</section>
			<!--subsection end-->
			<section class="section-content">
				<div class="layout-mw-wrapper">
					<!--PAGE CONTENT START-->
					<jsp:include page="/WEB-INF/jsp/tfe/informationAndStoryList.jsp"></jsp:include>
					<!--PAGE END-->
				</div>
			</section>
			<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
		</div>
	</div>
	<script>
		$(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
		var pageLocation = 'story';
		function jumpPage(pageIndex) {
			//alert(pageIndex);
			window.open(pageLocation + '?page=' + pageIndex, '_self');
		}
		function jumpAnotherPage(id) {
			//alert(id);
			window.open(pageLocation + 'Detail?id=' + id + '&page=' + '${contents.page}', '_self');
		}
	</script>
</body>
</html>
