<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zh-tw">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="Title" content="TFE"/>

    <!-- iOS -->

    <!-- Android -->

    <meta property="og:title" content="Hconnect Mobile"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content=""/>
    <meta property="og:image" content="${pageContext.request.contextPath}/resource/assets/images/share.jpg"/>
    <meta property="og:site_name" content="Hconnect"/>
    <meta property="og:description" content="Description"/>
    <meta property="og:locality" content="Taipei"/>
    <meta property="og:country-name" content="Taiwan"/>
    <link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon"
          type="image/x-icon"/>
    <title>TFE</title>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
          media="screen" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/member.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/style.css" media="screen"
          rel="stylesheet" type="text/css"/>
    <style type="text/css">
        a:link {
            color: #267CAB;
        }
    </style>
</head>
<body>
<div class="layout-container">
    <div class="on-mobile list-header-bg btn-bars">
    </div>
    <div class="layout-view">
        <section class="section-banner" style="height:0;">
        </section>
        <!--subsection start-->
        <!--subsection end-->

        <section class="section-content">
            <div class="layout-mw-wrapper">
                <!--PAGE CONTENT START-->
                <div class="register-content">
                    <div class="register">
                        <div class="re_status">
                            <div class="status_t1">申請信用報告的方式與說明</div>
                            <div class="register_status">
                                <dl class="clearfix status_txt">
                                    <dt><img src="resource/assets/images/s2.png">
                                    </dt>
                                    <dd><h1>前往全國各地郵局儲匯窗口辦理
                                        ［<a href="https://tfe-operation.s3.amazonaws.com/file/doc/%E8%81%AF%E5%BE%B5%E7%94%B3%E8%AB%8B%E6%9B%B8_20160910.pdf"
                                            title="聯徵信用報告申請書" target="_blank">下載申請書</a>］</h1>
                                        <p>&nbsp;</p>
                                    </dd>
                                    <dt>&nbsp;</dt>
                                    <dd>
                                        <p>&nbsp;</p>
                                        <table width="0" border="0">
                                            <tr>
                                                <td height="25"><p>注意事項： <br>
                                                    申請時，文件寄發地址欄位填寫<span class="form-help-text"><strong>台灣資金交易所(11575
                                                        台北市忠孝東路六段21號12樓-7)</strong></span></p>
                                                    <p>另於下方勾選加查其他信用資料並填入<strong>：S11、B68、K22、J10(信用評分)</strong>項目。</p>
                                                    <p>&nbsp;</p></td>
                                            </tr>
                                            <tr>
                                                <td><img src="resource/assets/images/credit02.gif" width="800"
                                                         height="500"></td>
                                            </tr>
                                            <tr>
                                                <td align="center">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </dd>
                                    <dt>&nbsp;</dt>
                                    <dt>&nbsp;</dt>
                                    <dt>&nbsp;</dt>
                                    <dd>
                                        <p>&nbsp;</p>
                                    </dd>
                                    <dt>&nbsp;</dt>
                                    <dd>
                                        <p><img src="resource/assets/images/more.png" width="15" height="15"> <a
                                                href="http://www.jcic.org.tw/main_ch/docDetail.aspx?uid=525&pid=93&docid=364"
                                                target="_blank">前往財團法人金融聯合徵信中心官網</a></p>
                                    </dd>
                                    <dt>&nbsp;</dt>
                                    <dd>
                                        <p>&nbsp;</p>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <!--PAGE END-->
            </div>
        </section>
        <img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%"
             style="margin-top:-1px;" alt="">
    </div>
</div>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/initialization.js"
        type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js"
        type="text/javascript"></script>
<script>
    $(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
</script>

</body>
</html>