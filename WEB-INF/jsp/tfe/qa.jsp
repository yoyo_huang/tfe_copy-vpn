<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<head>

    <!-- iOS -->

    <!-- Android -->

    <script type="text/javascript">
        function dump_props(obj, objName) {
            var result = "";

            for (var i in obj) {
                try {
                    result += "<tr><td>" + objName + "." + i + "</td><td>" + obj[i] + "</td></tr>";
                }
                catch (err) {
                    result += "<tr><td>" + objName + "." + i + "</td><td>" + err + "</td></tr>";
                }
            }

            return result;
        }

        var GO_DEBUG = true;
        function openDebug(obj, objName) {
            if (GO_DEBUG) {
                debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
                debugWindow.document.write("<HEAD><TITLE>Message window</TITLE></HEAD>");
                debugWindow.document.write("<Body><Table border='1'>" + dump_props(obj, objName) + "</Table></Body>");
            }
        }
        function openDebugText(_text) {
            if (GO_DEBUG) { //alert("debug");
                debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
                debugWindow.document.write(_text);
            }
        }
        function openDebugTextNewLine(_text) {
            if (GO_DEBUG) { //alert("debug");
                debugWindow = window.open("", "debugWindow", "menubar=yes,scrollbars=yes,resizable=yes");
                var re = /\&/g;
                var result = _text.replace(re, '<br/>&');
                debugWindow.document.write(result + '<br/>');
            }
        }

        function MM_jumpMenu(targ, selObj, restore) { //v3.0
            eval(targ + ".location='" + selObj.options[selObj.selectedIndex].value + "'");
            if (restore) selObj.selectedIndex = 0;
        }
    </script>
    <link href="resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
          type="text/css"/>
    <link href="resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css"/>
    <link href="resource/assets/stylesheets/units/qa.css" media="screen" rel="stylesheet" type="text/css"/>
    <style>
        .highlight {
            background: rgb(255, 255, 100);
        }
    </style>

</head>

<div class="layout-container">
    <div class="layout-view">

        <section class="section-banner" style="height:0;">

        </section>
        <!--subsection start-->
        <section class="section-proflie-banner accordion-menu">
            <!--laptop start-->
            <div class="layout-mw-wrapper">

                <div class="sbheader-center">

                    <div class="cb-list3">
                        <div class="top_qa">Q&amp;A</div>
                    </div>

                </div>
                <!--laptop end-->
            </div>
        </section>
        <!--subsection end-->

        <section class="section-content">
            <div class="qa_header">
                <div class="search clearfix">
                    <input id="queryText" type="text" name="" class="input-search" autocomplete="off"
                           placeholder="輸入您的問題或關鍵字" onkeydown="javascript:goKeyDown(event);">
                    <input name="search" type="image" class="search_btn" id="search" value=""
                           onclick="javascript:goShowData(-1);"
                           src="resource/assets/images/search.png"/>
                </div>
            </div>
            <div class="layout-mw-wrapper">

                <!--PAGE CONTENT START-->
                <!--開始-->

                <div class="qa_wp">
                    <div class="qa_content clearfix">
                        <div class="qa_left qa_left_w" id="qa_menu_w">
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#" class="current">ac參加前注意事項</a></p>
                                </dt>
                            </dl>
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#">a額度與信用評等</a></p>
                                </dt>
                            </dl>
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#">競標組合開始</a></p>
                                </dt>
                            </dl>
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#">競標</a></p>
                                </dt>
                            </dl>
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#">開標</a></p>
                                </dt>
                            </dl>
                        </div>
                        <div class="qa_left qa_left_m">
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#">參加前注意事項</a></p>
                                <div class="title_arow"><img src="resource/assets/images/b_title_arow.png"></div>
                                </dt>
                                <dd style="display:none">
                                    <ul class="qa_list">
                                        <h3><span><img src="resource/assets/images/qa_arow02.png"></span><a
                                                href="javascript:"
                                                class="h ">a誰可以參加台灣資金交易所?</a>
                                            <a href="javascript:" class="s">b誰可以參加台灣資金交易所?</a>
                                        </h3>
                                        <li>法定年齡：年滿二十歲且具有完全行為能力。</li>
                                        <li>本國國民：持有中華民國核發之國民身分證之本國國民。</li>
                                    </ul>
                                    <ul class="qa_list">
                                        <h3><span><img src="resource/assets/images/qa_arow02.png"></span><a
                                                href="javascript:"
                                                class="h">參加前須完成哪些準備?</a>
                                            <a href="javascript:" class="s">參加前須完成哪些準備?</a></h3>
                                        <li>mbbbb法定年齡：年滿二十歲且具有完全行為能力。</li>
                                        <li>mbbb本國國民：持有中華民國核發之國民身分證之本國國民。</li>
                                    </ul>
                                </dd>
                            </dl>
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#">額度與信用評等</a></p>
                                <div class="title_arow"><img src="resource/assets/images/b_title_arow.png"></div>
                                </dt></dl>
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#">競標組合開始</a></p>
                                <div class="title_arow"><img src="resource/assets/images/b_title_arow.png"></div>
                                </dt></dl>
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#">競標</a></p>
                                <div class="title_arow"><img src="resource/assets/images/b_title_arow.png"></div>
                                </dt></dl>
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#">開標</a></p>
                                <div class="title_arow"><img src="resource/assets/images/b_title_arow.png"></div>
                                </dt></dl>
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#">得標</a></p>
                                <div class="title_arow"><img src="resource/assets/images/b_title_arow.png"></div>
                                </dt></dl>
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#">未得標</a></p>
                                <div class="title_arow"><img src="resource/assets/images/b_title_arow.png"></div>
                                </dt></dl>
                            <dl>
                                <dt>
                                <p class="b_title"><a href="#">競標組合拍賣</a></p>
                                <div class="title_arow"><img src="resource/assets/images/b_title_arow.png"></div>
                                </dt></dl>
                        </div>

                        <div class="qa_right" id="qa_data_w">
                            <ul class="qa_list">
                                <h3><span><img src="resource/assets/images/qa_arow02.png"></span>
                                    <a href="javascript:" class="h ">c誰可以參加台灣資金交易所?</a>
                                    <a href="javascript:" class="s">d誰可以參加台灣資金交易所?</a>
                                </h3>
                                <li>kkk法定年齡：年滿二十歲且具有完全行為能力。</li>
                                <li>kkkk本國國民：持有中華民國核發之國民身分證之本國國民。</li>
                            </ul>
                            <ul class="qa_list">
                                <h3><span><img src="resource/assets/images/qa_arow02.png"></span>
                                    <a href="javascript:" class="h">e參加前須完成哪些準備?</a>
                                    <a href="javascript:" class="s">f參加前須完成哪些準備?</a></h3>
                                <li>bbbb法定年齡：年滿二十歲且具有完全行為能力。</li>
                                <li>bbb本國國民：持有中華民國核發之國民身分證之本國國民。</li>
                            </ul>

                        </div>
                    </div>
                </div>
                <!--結束-->
                <!--PAGE END-->
            </div>
        </section>

        <img src="resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top:-1px;" alt="">
    </div>
</div>

<script src="resource/assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script>
<script src="resource/assets/javascripts/vendors/fastclick.js" type="text/javascript"></script>
<script src="resource/assets/javascripts/vendors/svg4everybody.min.js" type="text/javascript"></script>
<script src="resource/assets/javascripts/vendors/jquery.colorbox.js" type="text/javascript"></script>
<%--<script src="resource/assets/javascripts/initialization.js" type="text/javascript"></script>--%>
<script src="resource/assets/javascripts/vendors/accordion.js" type="text/javascript"></script>
<script>
    $(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
</script>
<script type="TEXT/JAVASCRIPT">
    var n = 0;
    $(document).ready(function () {
        function initData() {
//            alert("aa");
//            openDebug($('#qa_menu_w'),"#qa_menu_w");
            $('#qa_menu_w').html("");
            $('#qa_data_w').html("");
//            openDebug($('#qa_menu_w'),"After:S#qa_menu_w");
//            alert("bb");
            <c:forEach items="${qaData}" var="qaMenu" varStatus="loopIndex">
            	$("#qa_menu_w").append('<dl><dt><p class="b_title"><a href="javascript:goShowData(${qaMenu.titleNo});" class="${loopIndex.index == 0 ? 'current' : ''}">${qaMenu.title}</a></p></dt></dl>');
            	<c:if test="${loopIndex.index == 0 && (queryText==null || queryText=='')}">goShowData('${qaMenu.titleNo}');
            	</c:if>
            </c:forEach>
            <c:if test="${queryText!=null && queryText!=''}">
            	$("#queryText").val('${queryText}');
            	goShowData(-1);
            </c:if>
            addClickEvent2ManageData();
        }

        initData();


    });
    function goKeyDown(e) {
//        console.log(e.keyCode);
//        console.log(e.keyCode == 13);
        if (e.keyCode == 13)
            goShowData(-1);
    }
    function goShowData(titleNo) {
//        console.log(titleNo);
        var queryText = "%20";
        if (titleNo == -1) {
            if ($("#queryText").val() == null || $("#queryText").val().trim() == "") {
                alert("請輸入您的問題或關鍵字");
                return;
            } else {
                queryText = $("#queryText").val().trim();
                $("#qa_menu_w .b_title .current").removeClass("current");
            }
        }
        if (1 == 1) {
            $.ajax({
                type: "POST",
                url: "${pageContext.request.contextPath}/qaQuery",
                data: "titleNo=" + titleNo + "&queryText=" + queryText,
                processData: false,
                cache: false,
                success: function (result) {
//                    console.log(result);
//                    console.log("qaDatalist:" + result.qaDataList);
                    $('#qa_data_w ul').remove();

                    var tmpDataStr = '';
                    if (result.result != "No Data Found") {
                        for (var i = 0; i < result.qaDataList.length; i++) {
                            tmpDataStr += '<ul class="qa_list">';
                            tmpDataStr += '<h3><span><img class="dataImg" src="resource/assets/images/qa_arow02.png"></span>';
                            tmpDataStr += '<a href="javascript:" class="dataH">' + result.qaDataList[i].topic + '</a>  ';
                            tmpDataStr += '<a href="javascript:" class="dataS">' + result.qaDataList[i].topic + '</a>   ';
                            tmpDataStr += '</h3>';
//                            tmpDataStr+='<div>'+result.qaDataList[i].content;+'</div>';
                            tmpDataStr += '<output style="height:auto; max-width:500px;">' + result.qaDataList[i].content;
                            +'</output>';
                            tmpDataStr += '</ul>';
                            $('#qa_data_w').append(tmpDataStr);
                            tmpDataStr = '';
                        }
                        addClickEvent2ManageData();
                        if (queryText != "%20")
                            $('#qa_data_w').highlight(queryText);
                    } else {
                        tmpDataStr += '<ul class="qa_list">';
                        tmpDataStr += '<h3>';
                        tmpDataStr += '查無資料';
                        tmpDataStr += '</h3>';
                        tmpDataStr += '</ul>';

                        $('#qa_data_w').append(tmpDataStr);
//                        $('#qa_data_w').append("<ul></ul><p><span style='font-size:20px;'>查無資料</span></p></ul>");
                    }
                    return;
                },
                error: function (result) {
                    //console.log(result);

                    return;
                }
            });
        }
    }
    $(function () {
        $("#qa_menu_w .b_title a").click(function () {
//            console.log($("#qa_menu_w .b_title .current"));
            $("#qa_menu_w .b_title .current").removeClass("current");
//            console.log(this);
            $(this).addClass("current");
            if( window.innerWidth < 1024 ){
                var nextscroll = $('.qa_right').offset().top;
                $('html,body').animate({ scrollTop:( parseInt(nextscroll)-55 )}, 800);
            }
        });

        $(".qa_left_m .b_title a").click(function () {
//            alert("c");
            if (n++ % 2 == 0) {
//                alert("c1");
                $(".qa_left_m dl").hide();
                $(this).parents("dl").show();
                $(".qa_left_m .b_title a").removeClass("current");
                $(this).addClass("current");
                //alert($(this).parents("dl").html());
                $(this).parents("dt").find(".title_arow").show();
                $(this).parents("dl").find("dd").show();
//                console.log(0);
            } else {
                // console.log(1);
//                alert("c2");
                $(".qa_left_m dl").show();

                $(".qa_left_m .b_title a").removeClass("current");

                //alert($(this).parents("dl").html());
                $(".qa_left_m dl .title_arow").hide();
                $(".qa_left_m dl dd").hide();

            }
        });

        jQuery.extend({
            highlight: function (node, re, nodeName, className) {
//                console.log(node.nodeName + " : " + node.nodeType);
                if (node.nodeType === 3) {
                    var match = node.data.match(re);
                    if (match) {
                        var highlight = document.createElement(nodeName || 'font');
                        highlight.className = className || 'highlight';
                        var wordNode = node.splitText(match.index);
                        wordNode.splitText(match[0].length);
                        var wordClone = wordNode.cloneNode(true);
                        highlight.appendChild(wordClone);
                        wordNode.parentNode.replaceChild(highlight, wordNode);
                        return 1; //skip added node in parent
                    }
                } else if ((node.nodeType === 1 && node.childNodes) && // only element nodes that have children
                        !/(script|style)/i.test(node.tagName) && // ignore script and style nodes
                        !(node.tagName === nodeName.toUpperCase() && node.className === className)) { // skip if already highlighted
                    for (var i = 0; i < node.childNodes.length; i++) {
                        i += jQuery.highlight(node.childNodes[i], re, nodeName, className);
                    }
                }

                return 0;
            }
        });

        jQuery.fn.unhighlight = function (options) {
            var settings = {className: 'highlight', element: 'font'};
            jQuery.extend(settings, options);

            return this.find(settings.element + "." + settings.className).each(function () {
                var parent = this.parentNode;
                parent.replaceChild(this.firstChild, this);
                parent.normalize();
            }).end();
        };

        jQuery.fn.highlight = function (words, options) {
//            alert("b");
            var settings = {className: 'highlight', element: 'font', caseSensitive: false, wordsOnly: false};
            jQuery.extend(settings, options);

            if (words.constructor === String) {
                words = [words];
            }
            words = jQuery.grep(words, function (word, i) {
                return word != '';
            });
            words = jQuery.map(words, function (word, i) {
                return word.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
            });
            if (words.length == 0) {
                return this;
            }
            ;

            var flag = settings.caseSensitive ? "" : "i";
            var pattern = "(" + words.join("|") + ")";
            if (settings.wordsOnly) {
                pattern = "\\b" + pattern + "\\b";
            }
            var re = new RegExp(pattern, flag);

            return this.each(function () {
                jQuery.highlight(this, re, settings.element, settings.className);
            });
        };

    })
</script>
<script type="TEXT/JAVASCRIPT">
    function addClickEvent2ManageData() {
        $(function () {
            $(".qa_list h3 .dataH").click(function () {
//                alert("a");
                $(this).closest('.qa_list').find("output").slideToggle(300);
                $(this).closest('.qa_list').find(".dataImg").toggleClass('style-rotate');
//                $(this).parent().nextAll().show("fast");
//                $(this).parent().nextAll().nextAll().show("fast");
//                $(this).parent().find(".dataImg").attr("src", "resource/assets/images/qa_arow01.png");
//                $(this).parent().find(".dataH").hide();
//                $(this).parent().find(".dataS").show()
            });

//            $(".qa_list h3 .dataS").click(function () {
////                alert("b");
//                $(this).parent().nextAll().hide();
//                $(this).parent().find(".dataS").hide();
//                $(this).parent().find(".dataH").show();
//                $(this).parent().find(".dataImg").attr("src", "resource/assets/images/qa_arow02.png");
//            });
        });

    }

</script>
