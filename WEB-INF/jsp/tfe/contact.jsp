<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="zh-tw">
<head>
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css"
	media="screen" rel="stylesheet" type="text/css" />
<link
	href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/about.css"
	media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="layout-container">
		<div class="layout-view">
			<section class="section-banner" style="height: 0;"></section>
			<!--subsection start-->
			<section class="section-proflie-banner accordion-menu">
				<!--laptop start-->
				<div class="layout-mw-wrapper">
					<div class="sbheader-center">
						<div class="cb-list3">
							<ul>
								<li class="on-mobile">聯絡我們</li>
								<li><a href="${pageContext.request.contextPath}/about">關於我們</a></li>
								<li><a href="${pageContext.request.contextPath}/join">加入我們</a></li>
								<li><a class="is-active" >聯絡我們</a></li>
							</ul>
						</div>
					</div>
					<!--laptop end-->
				</div>
			</section>
			<!--subsection end-->

			<section class="section-content">
				<div class="layout-mw-wrapper">
					<!--PAGE CONTENT START-->
					<!--媒體報導開始-->
					${content}
					<!--媒體報導結束-->
					<!--PAGE END-->
				</div>
			</section>
			<img
				src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg"
				width="100%" style="margin-top: -1px;" alt="">
		</div>

	</div>

	<script
		src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js"
		type="text/javascript"></script>
	<script
		src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js"
		type="text/javascript"></script>
	<script>
		$(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());

		$(".accordion-item").find(".accordion-item-title").on(
				'click',
				function() {
					$(this).parent().toggleClass('on').find(".fa").toggleClass(
							'fa-rotate-180');
				});

		$(".accordion-menu").on('click', function() {
			$(this).toggleClass('is-active');
		});
	</script>

</body>
</html>