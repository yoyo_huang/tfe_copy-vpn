<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="zh-tw">
<head>

    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <link
            href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/about.css"
            media="screen" rel="stylesheet" type="text/css"/>
    <style>
        #textview {
            text-align: center;
            font-size: 1.2cm;
        }

        #rs {
            text-align: center;
        }
    </style>
    <script type='text/javascript'>
        var totalTime = 10;
        //alert("UUUUUU");

        function startTime() {
            // add a zero in front of numbers<10
            setTimeout('redirectPage()', 5500);
        }
    </script>
</head>
<body onload="startTime()">
<div class="layout-container">
    <div class="layout-view">
        <section class="section-banner" style="height: 0;"></section>
        <!--subsection start-->
        <section class="section-proflie-banner accordion-menu">
            <!--laptop start-->
            <div class="layout-mw-wrapper">
                <div class="sbheader-center">
                    <div class="cb-list3">
                        <ul>
                            <li class="on-mobile">${msg}</li>
                            <li><a class="is-active">${msg}</a></li>
                        </ul>
                    </div>
                </div>
                <!--laptop end-->
            </div>
        </section>
        <!--subsection end-->

        <section class="section-content">
            <br>
            <div id="textview" class="layout-mw-wrapper">
                <br>
                <font color="red">${content}
                    <br>
                    <div class="profile-obtn" id="rs" sytle="text-align: center;"><a
                            href="${pageContext.request.contextPath}/index" class="style-white">回首頁</a></div>
            </div>
    </div>
</div>
</section>
<img
        src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg"
        width="100%" style="margin-top: -1px;" alt="">
</div>

</div>

<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js"
        type="text/javascript"></script>
<script
        src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js"
        type="text/javascript"></script>
<script>
    $(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());

    $(".accordion-item").find(".accordion-item-title").on(
            'click',
            function () {
                $(this).parent().toggleClass('on').find(".fa").toggleClass(
                        'fa-rotate-180');
            });

    $(".accordion-menu").on('click', function () {
        $(this).toggleClass('is-active');
    });

    function redirectPage() {
        var redirectURL = "${redirectURL}";
        var msg = "${msg}";

        if (msg !="ERROR") {
            if (redirectURL != "") {
                window.location.replace("${pageContext.request.contextPath}/" + redirectURL);
            } else {
                window.location.replace("${pageContext.request.contextPath}/index");
            }
        }
    }
</script>

</body>
</html>
