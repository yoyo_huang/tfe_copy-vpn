<!-- <link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" /> -->
<!-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /> -->
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/mypage.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/qa.css" media="screen" rel="stylesheet" type="text/css" />


	<div class="layout-container">
		<div class="on-mobile list-header-bg btn-bars"></div>

		<div class="layout-view">
			<section class="section-banner" style="height: 0;"></section>
			<!--subsection start-->
			<section class="section-proflie-banner accordion-menu">
				<!--laptop start-->
				<div class="layout-mw-wrapper">
					<div class="sbheader-center">
						<div class="layout-mw-wrapper">
							<div class="sbheader-center">
								<div class="cb-list3">
									<ul>
										<li class="on-mobile">服務條款</li>
										<li><a href="${pageContext.request.contextPath}/patentStatement" ${linkFrom eq 'patentStatement' ? 'class="is-active"': ''}>專利聲明</a></li>
										<li><a href="${pageContext.request.contextPath}/termsOfService" ${linkFrom eq 'termsOfService' ? 'class="is-active"': ''}>服務條款</a></li>
										<li><a href="${pageContext.request.contextPath}/policyOfPrivacy" ${linkFrom eq 'policyOfPrivacy' ? 'class="is-active"': ''}>隱私權政策</a></li>
									</ul>
								</div>
							</div>
							<!--laptop end-->
						</div>
					</div>
					<!--laptop end-->
				</div>
			</section>
			<!--subsection end-->

			<section class="section-content">
				<div class="layout-mw-wrapper">
					<!--PAGE CONTENT START-->
					<!--開始-->
					<div class="qa_wp">
						<div class="Statistics">
							<div class="Statistics-txt">
								${content}
								<%-- <img src="${pageContext.request.contextPath}/resource/assets/images/Statistics.jpg"> --%>
							</div>
						</div>
					</div>
					<!--結束-->
					<!--PAGE END-->
				</div>
			</section>
			<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
		</div>

	</div>

<!-- 	<script src="assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script> -->
<!-- 	<script src="assets/javascripts/vendors/fastclick.js" type="text/javascript"></script> -->
<!-- 	<script src="assets/javascripts/vendors/svg4everybody.min.js" type="text/javascript"></script> -->
<!-- 	<script src="assets/javascripts/vendors/jquery.colorbox.js" type="text/javascript"></script> -->
<!-- 	<script src="assets/javascripts/initialization.js" type="text/javascript"></script> -->
<!-- 	<script src="assets/javascripts/vendors/accordion.js" type="text/javascript"></script> -->
	<script>
		$(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
	</script>