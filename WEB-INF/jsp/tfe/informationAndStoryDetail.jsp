<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<fmt:setLocale value="zh_TW" />
<!--PAGE CONTENT START-->
<div class="report_wp">
	<div class="report_content">
		<div class="detailed">
			<div class="detailed_title clearfix">
				<div class="year_date">
					<div class="year">
						<c:choose>
							<c:when test="${not empty detail.lastUpdateTime}">
								<fmt:formatDate value="${detail.lastUpdateTime}" type="both" dateStyle="full" pattern="yyyy" />
							</c:when>
							<c:otherwise>
								<fmt:formatDate value="${detail.createTime}" type="both" dateStyle="full" pattern="yyyy" />
							</c:otherwise>
						</c:choose>
					</div>
					<div class="date_time">
						<div class="date">
							<c:choose>
								<c:when test="${not empty detail.lastUpdateTime}">
									<fmt:formatDate value="${detail.lastUpdateTime}" type="both" dateStyle="full" pattern="dd" />
								</c:when>
								<c:otherwise>
									<fmt:formatDate value="${detail.createTime}" type="both" dateStyle="full" pattern="dd" />
								</c:otherwise>
							</c:choose>
						</div>
						<div class="months">
							<c:choose>
								<c:when test="${not empty detail.lastUpdateTime}">
									<fmt:setLocale value="zh_TW" />
									<fmt:formatDate value="${detail.lastUpdateTime}" type="both" dateStyle="full" pattern="MMMM" />
								</c:when>
								<c:otherwise>
									<fmt:setLocale value="zh_TW" />
									<fmt:formatDate value="${detail.createTime}" type="both" dateStyle="full" pattern="MMMM" />
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
				<div class="title_txt">
					<h1>
						<font>${detail.infoTitle}&nbsp;</font>
					</h1>
					<h2>${detail.infoTitle1}</h2>
				</div>
			</div>
			<div class="detailed_content">
				<div class="share clearfix">
					<!-- <div class="line_icon"> -->
					<!-- 	<a href="#">&nbsp;</a> -->
					<!-- </div> -->
					<!-- <div class="share_icon"> -->
					<!-- 	<a hidden="#">&nbsp;</a> -->
					<!-- </div> -->
				</div>
				<div class="explicit">
					<span class="txt18" style="min-width: 50%; max-width: 100%">${detail.infoContent}</span>
				</div>
				<div class="buttom">
					<div class="btn-rounded">
						<a href="javascript: jumpToList();">返回列表</a>
					</div>
				</div>
				<div class="message">
					<!-- TODO:留問題區 -->
					<!-- <div class="corner1 table-wrap" style="margin-bottom: 0px;">
						<table width="100%" class="pure-table">
							<thead>
								<tr>
									<th rowspan="3" align="left" width="15%">
										<h1>留下問題</h1>
									</th>
									<th colspan="2" align="right">
										<textarea id="comment" style="width: 100%; height: 50px; resize: none;" maxlength="100"></textarea>
									</th>
								</tr>
								<tr>
									<th colspan="2" align="right">
										<input type="button" value="送出" onclick="submitComment()">
									</th>
								</tr>
							</thead>
						</table>
					</div>
					<div id="leaveMessgeArea">
						<c:if test='${ not empty leaveMessages.rows }'>
						</c:if>
						<c:forEach var="infoLeaveMessage" items="${leaveMessages.rows}" varStatus="loop">
							<%--  ${loop.index})  --%>
							<div class="corner1 table-wrap">
								<table width="100%" class="pure-table">
									<thead>
										<tr>
											<th align="left" width="100px">發問人</th>
											<th align="left" width="150px">時間</th>
											<th align="left">問題內容</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td align="left">${infoLeaveMessage.ilmAccID}</td>
											<td align="left">
												<fmt:formatDate pattern="yyyy/MM/dd hh:mm:ss" value="${infoLeaveMessage.ilmDate}" />
											</td>
											<td align="left">${infoLeaveMessage.ilmMessage}</td>
										</tr>
									</tbody>
									<c:if
										test='${ (not empty infoLeaveMessage.ilmMAccID1) && (not empty infoLeaveMessage.ilmMDate) && (not empty infoLeaveMessage.ilmResponse1) }'>
										<thead>
											<tr>
												<th align="left" width="100px">解答人</th>
												<th align="left" width="150px">時間</th>
												<th align="left" align="left">解答內容</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td align="left">${infoLeaveMessage.ilmMAccID1}</td>
												<td align="left">
													<fmt:formatDate pattern="yyyy/MM/dd hh:mm:ss" value="${infoLeaveMessage.ilmMDate}" />
												</td>
												<td align="left">${infoLeaveMessage.ilmResponse1}</td>
											</tr>
										</tbody>
									</c:if>
								</table>
							</div>
						</c:forEach>
						<div class="page_no">
							<fmt:parseNumber var="pageNumber" integerOnly="true" type="number" value="${leaveMessages.page}" />
							<fmt:parseNumber var="totalNumber" integerOnly="true" type="number" value="${leaveMessages.total}" />
							<c:if test="${(totalNumber > 1  ) && (pageNumber ne 1) }">
								<a href="javascript: jumpPage(${pageNumber-1});">&lt;</a>
							</c:if>
							<c:forEach var="i" begin="1" end="${leaveMessages.total}" step="1">
								<a href="javascript: jumpPage(${i});" <c:if test="${(pageNumber eq i) }">class="cass"</c:if>>${i}</a>
							</c:forEach>
							<c:if test="${(totalNumber > 1   ) && (totalNumber ne pageNumber)}">
								<a href="javascript: jumpPage(${pageNumber+1});">&gt;</a>
							</c:if>
						</div>
					</div> -->
					<!-- TODO:留問題區 -->
				</div>
			</div>
		</div>
		<div class="read_title">延伸閱讀</div>
		<ul class="news_list clearfix">
			<c:if test='${not empty contents.rows[0]}'>
				<li>
					<img src="${contents.rows[0].imageUrl}">
					<div class="upper">
						<div class="date_time">
							<div class="date">
										<fmt:formatDate value="${contents.rows[0].createTime}" type="both" dateStyle="full" pattern="dd" />
							</div>
							<div class="months">
								<fmt:setLocale value="zh_TW" />
								<fmt:formatDate value="${contents.rows[0].createTime}" type="both" dateStyle="full" pattern="MMMM" />
							</div>
						</div>
					</div>
					<h1 class="shortTitle">
						<a href="javascript: jumpAnotherPage(${contents.rows[0].id.imid});">${contents.rows[0].infoTitle}</a>
					</h1>
					<p class="shortTitle1">${contents.rows[0].infoTitle1}</p>
				</li>
			</c:if>
			<c:if test='${not empty contents.rows[1]}'>
				<li>
					<img src="${contents.rows[1].imageUrl}">
					<div class="upper">
						<div class="date_time">
							<div class="date">
								<fmt:formatDate value="${contents.rows[1].createTime}" type="both" dateStyle="full" pattern="dd" />
							</div>
							<div class="months">
								<fmt:setLocale value="zh_TW" />
								<fmt:formatDate value="${contents.rows[1].createTime}" type="both" dateStyle="full" pattern="MMMM" />
							</div>
						</div>
					</div>
					<h1 class="shortTitle">
						<a href="javascript: jumpAnotherPage(${contents.rows[1].id.imid});">${contents.rows[1].infoTitle}</a>
					</h1>
					<p class="shortTitle1">${contents.rows[1].infoTitle1}</p>
				</li>
			</c:if>
			<c:if test='${not empty contents.rows[2]}'>
				<li>
					<img src="${contents.rows[2].imageUrl}">
					<div class="upper">
						<div class="date_time">
							<div class="date">
								<fmt:formatDate value="${contents.rows[2].createTime}" type="both" dateStyle="full" pattern="dd" />
							</div>
							<div class="months">
								<fmt:setLocale value="zh_TW" />
								<fmt:formatDate value="${contents.rows[2].createTime}" type="both" dateStyle="full" pattern="MMMM" />
							</div>
						</div>
					</div>
					<h1 class="shortTitle">
						<a href="javascript: jumpAnotherPage(${contents.rows[2].id.imid});">${contents.rows[2].infoTitle}</a>
					</h1>
					<p class="shortTitle1">${contents.rows[2].infoTitle1}</p>
				</li>
			</c:if>
		</ul>
	</div>
</div>
<!--PAGE END-->
