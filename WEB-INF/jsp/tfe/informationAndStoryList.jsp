<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<fmt:setLocale value="zh_TW" />
<!--PAGE CONTENT START-->
<!--媒體報導開始-->
<div class="report_wp">
	<div class="report_content">
		<c:if test='${not empty contents.rows[0]}'>
			<dl class="headlines clearfix">
				<dt>
					<img src="${contents.rows[0].imageUrl}">
				</dt>
				<dd>
					<div class="clearfix">
						<div class="date_time">
							<div class="date">
								<c:choose>
									<c:when test="${not empty contents.rows[0].lastUpdateTime}">
										<fmt:formatDate value="${contents.rows[0].lastUpdateTime}" type="both" dateStyle="full" pattern="dd" />
									</c:when>
									<c:otherwise>
										<fmt:formatDate value="${contents.rows[0].createTime}" type="both" dateStyle="full" pattern="dd" />
									</c:otherwise>
								</c:choose>
							</div>
							<div class="months">
								<c:choose>
									<c:when test="${not empty contents.rows[0].lastUpdateTime}">
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[0].lastUpdateTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:when>
									<c:otherwise>
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[0].createTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="head_title">
							<h1 class="shortTitle">
								<a href="javascript: jumpAnotherPage(${contents.rows[0].id.imid});">${contents.rows[0].infoTitle}</a>
							</h1>
							<h2 class="shortTitle1">${contents.rows[0].infoTitle1}</h2>
						</div>
					</div>
					<div class="m_head_pic">
						<img src="${contents.rows[0].imageUrl}">
					</div>
					<div class="txt18 shortContentLineWrap">${contents.rows[0].infoContent}</div>
					<div class="more">
						<a href="javascript: jumpAnotherPage(${contents.rows[0].id.imid});">
							<img src="${pageContext.request.contextPath}/resource/assets/images/more.png">
						</a>
					</div>
				</dd>
			</dl>
		</c:if>
		<ul class="news_list clearfix">
			<c:if test='${not empty contents.rows[1]}'>
				<li>
					<img src="${contents.rows[1].imageUrl}">
					<div class="upper">
						<div class="date_time">
							<div class="date">
								<c:choose>
									<c:when test="${not empty contents.rows[1].lastUpdateTime}">
										<fmt:formatDate value="${contents.rows[1].lastUpdateTime}" type="both" dateStyle="full" pattern="dd" />
									</c:when>
									<c:otherwise>
										<fmt:formatDate value="${contents.rows[1].createTime}" type="both" dateStyle="full" pattern="dd" />
									</c:otherwise>
								</c:choose>
							</div>
							<div class="months">
								<c:choose>
									<c:when test="${not empty contents.rows[1].lastUpdateTime}">
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[1].lastUpdateTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:when>
									<c:otherwise>
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[1].createTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
					</div>
					<h1 class="shortTitle">
						<a href="javascript: jumpAnotherPage(${contents.rows[1].id.imid});">${contents.rows[1].infoTitle}</a>
					</h1>
					<p class="shortTitle1">${contents.rows[1].infoTitle1}</p>
				</li>
			</c:if>
			<c:if test='${not empty contents.rows[2]}'>
				<li>
					<img src="${contents.rows[2].imageUrl}">
					<div class="upper">
						<div class="date_time">
							<div class="date">
								<c:choose>
									<c:when test="${not empty contents.rows[2].lastUpdateTime}">
										<fmt:formatDate value="${contents.rows[2].lastUpdateTime}" type="both" dateStyle="full" pattern="dd" />
									</c:when>
									<c:otherwise>
										<fmt:formatDate value="${contents.rows[2].createTime}" type="both" dateStyle="full" pattern="dd" />
									</c:otherwise>
								</c:choose>
							</div>
							<div class="months">
								<c:choose>
									<c:when test="${not empty contents.rows[2].lastUpdateTime}">
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[2].lastUpdateTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:when>
									<c:otherwise>
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[2].createTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
					</div>
					<h1 class="shortTitle">
						<a href="javascript: jumpAnotherPage(${contents.rows[2].id.imid});">${contents.rows[2].infoTitle}</a>
					</h1>
					<p class="shortTitle1">${contents.rows[2].infoTitle1}</p>
				</li>
			</c:if>
			<c:if test='${not empty contents.rows[3]}'>
				<li>
					<img src="${contents.rows[3].imageUrl}">
					<div class="upper">
						<div class="date_time">
							<div class="date">
								<c:choose>
									<c:when test="${not empty contents.rows[3].lastUpdateTime}">
										<fmt:formatDate value="${contents.rows[3].lastUpdateTime}" type="both" dateStyle="full" pattern="dd" />
									</c:when>
									<c:otherwise>
										<fmt:formatDate value="${contents.rows[3].createTime}" type="both" dateStyle="full" pattern="dd" />
									</c:otherwise>
								</c:choose>
							</div>
							<div class="months">
								<c:choose>
									<c:when test="${not empty contents.rows[3].lastUpdateTime}">
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[3].lastUpdateTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:when>
									<c:otherwise>
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[3].createTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
					</div>
					<h1 class="shortTitle">
						<a href="javascript: jumpAnotherPage(${contents.rows[3].id.imid});">${contents.rows[3].infoTitle}</a>
					</h1>
					<p class="shortTitle1">${contents.rows[3].infoTitle1}</p>
				</li>
			</c:if>
		</ul>
		<div class="read_title">延伸閱讀</div>
		<ul class="read clearfix">
			<c:if test='${not empty contents.rows[4]}'>
				<li class="clearfix">
					<div class="read_pic">
						<img src="${contents.rows[4].imageUrl}">
					</div>
					<div class="read_txt">
						<div class="date_time">
							<div class="date">
								<c:choose>
									<c:when test="${not empty contents.rows[4].lastUpdateTime}">
										<fmt:formatDate value="${contents.rows[4].lastUpdateTime}" type="both" dateStyle="full" pattern="dd" />
									</c:when>
									<c:otherwise>
										<fmt:formatDate value="${contents.rows[4].createTime}" type="both" dateStyle="full" pattern="dd" />
									</c:otherwise>
								</c:choose>
							</div>
							<div class="months">
								<c:choose>
									<c:when test="${not empty contents.rows[4].lastUpdateTime}">
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[4].lastUpdateTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:when>
									<c:otherwise>
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[4].createTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="b_t1 shortTitle">
							<a href="javascript: jumpAnotherPage(${contents.rows[4].id.imid});">${contents.rows[4].infoTitle}</a>
							<span class="shortTitle1">&nbsp;&nbsp;&nbsp;${contents.rows[4].infoTitle1}</span>
						</div>
						<h2 class="shortContent">${contents.rows[4].infoContent}</h2>
					</div>
					<div class="more">
						<a href="javascript: jumpAnotherPage(${contents.rows[4].id.imid});">
							<img src="${pageContext.request.contextPath}/resource/assets/images/more.png">
						</a>
					</div>
				</li>
			</c:if>
			<c:if test='${not empty contents.rows[5]}'>
				<li class="clearfix">
					<div class="read_pic">
						<img src="${contents.rows[5].imageUrl}">
					</div>
					<div class="read_txt">
						<div class="date_time">
							<div class="date">
								<c:choose>
									<c:when test="${not empty contents.rows[5].lastUpdateTime}">
										<fmt:formatDate value="${contents.rows[5].lastUpdateTime}" type="both" dateStyle="full" pattern="dd" />
									</c:when>
									<c:otherwise>
										<fmt:formatDate value="${contents.rows[5].createTime}" type="both" dateStyle="full" pattern="dd" />
									</c:otherwise>
								</c:choose>
							</div>
							<div class="months">
								<c:choose>
									<c:when test="${not empty contents.rows[5].lastUpdateTime}">
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[5].lastUpdateTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:when>
									<c:otherwise>
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[5].createTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="b_t1 shortTitle">
							<a href="javascript: jumpAnotherPage(${contents.rows[5].id.imid});"">${contents.rows[5].infoTitle}</a>
							<span class="shortTitle1">&nbsp;&nbsp;&nbsp;${contents.rows[5].infoTitle1}</span>
						</div>
						<h2 class="shortContent">${contents.rows[5].infoContent}</h2>
					</div>
					<div class="more">
						<a href="javascript: jumpAnotherPage(${contents.rows[5].id.imid});">
							<img src="${pageContext.request.contextPath}/resource/assets/images/more.png">
						</a>
					</div>
				</li>
			</c:if>
			<c:if test='${not empty contents.rows[6]}'>
				<li class="clearfix">
					<div class="read_pic">
						<img src="${contents.rows[6].imageUrl}">
					</div>
					<div class="read_txt">
						<div class="date_time">
							<div class="date">
								<c:choose>
									<c:when test="${not empty contents.rows[6].lastUpdateTime}">
										<fmt:formatDate value="${contents.rows[6].lastUpdateTime}" type="both" dateStyle="full" pattern="dd" />
									</c:when>
									<c:otherwise>
										<fmt:formatDate value="${contents.rows[6].createTime}" type="both" dateStyle="full" pattern="dd" />
									</c:otherwise>
								</c:choose>
							</div>
							<div class="months">
								<c:choose>
									<c:when test="${not empty contents.rows[6].lastUpdateTime}">
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[6].lastUpdateTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:when>
									<c:otherwise>
										<fmt:setLocale value="zh_TW" />
										<fmt:formatDate value="${contents.rows[6].createTime}" type="both" dateStyle="full" pattern="MMMM" />
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						<div class="b_t1 shortTitle">
							<a href="javascript: jumpAnotherPage(${contents.rows[6].id.imid});">${contents.rows[6].infoTitle}</a>
							<span class="shortTitle1">&nbsp;&nbsp;&nbsp;${contents.rows[6].infoTitle1}</span>
						</div>
						<h2 class="shortContent">${contents.rows[6].infoContent}</h2>
					</div>
					<div class="more">
						<a href="javascript: jumpAnotherPage(${contents.rows[6].id.imid});">
							<img src="${pageContext.request.contextPath}/resource/assets/images/more.png">
						</a>
					</div>
				</li>
			</c:if>
		</ul>
		<div class="page_no">
			<fmt:parseNumber var="pageNumber" integerOnly="true" type="number" value="${contents.page}" />
			<fmt:parseNumber var="totalNumber" integerOnly="true" type="number" value="${contents.total}" />
			<c:if test="${(totalNumber > 1  ) && (pageNumber ne 1) }">
				<a href="javascript: jumpPage(${pageNumber-1});">&lt;</a>
			</c:if>
			<c:forEach var="i" begin="1" end="${contents.total}" step="1">
				<a href="javascript: jumpPage(${i});" <c:if test="${(pageNumber eq i) }">class="cass"</c:if>>${i}</a>
			</c:forEach>
			<c:if test="${(totalNumber > 1   ) && (totalNumber ne pageNumber)}">
				<a href="javascript: jumpPage(${pageNumber+1});">&gt;</a>
			</c:if>
		</div>
	</div>
</div>
<!--PAGE END-->
