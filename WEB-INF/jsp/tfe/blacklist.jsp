<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="zh-tw">
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1" />
<meta name="Title" content="TFE" />
<meta name="Author"
	content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
<meta name="Description" content="Description" />

<link href="resource/assets/images/favicon.png" rel="shortcut icon"
	type="image/x-icon" />
<title>TFE</title>

<link
	href="resource/assets/stylesheets/font-awesome/css/font-awesome.min.css"
	media="screen" rel="stylesheet" type="text/css" />
<link href="resource/assets/stylesheets/vendors/colorbox.css"
	media="screen" rel="stylesheet" type="text/css" />
<link href="resource/assets/stylesheets/global.css" media="screen"
	rel="stylesheet" type="text/css" />
<link href="resource/assets/stylesheets/units/mypage.css" media="screen"
	rel="stylesheet" type="text/css" />
<link href="resource/assets/stylesheets/units/qa.css" media="screen"
	rel="stylesheet" type="text/css" />
<style type="TEXT/CSS">
.cb-list3 a:hover, .cb-list3 .ml {
	display: block;
}

@media ( max-width : 768px) {
	.cb-list3 a:hover, .cb-list3 .ml {
		display: none;
	}
	.section-proflie-banner.is-active .cb-list3 li:first-child {
		background: none;
	}
	.cb-list3 li:first-child {
		display: block;
		background: none
	}
}
</style>
</head>
<body>
	<div class="layout-container">
		<div class="layout-view">
			${content}
			<img src="resource/assets/images/bg-tri-twin-o-white.svg"
				width="100%" style="margin-top: -1px;" alt="">
		</div>

	</div>
	<script src="resource/assets/javascripts/vendors/jquery.min.js"
		type="text/javascript"></script>
	<script src="resource/assets/javascripts/vendors/fastclick.js"
		type="text/javascript"></script>
	<script src="resource/assets/javascripts/vendors/svg4everybody.min.js"
		type="text/javascript"></script>
	<script src="resource/assets/javascripts/vendors/jquery.colorbox.js"
		type="text/javascript"></script>
	<script src="resource/assets/javascripts/vendors/accordion.js"
		type="text/javascript"></script>
	<script>
		$(".cb-list3 li").eq(0).text($(".cb-list3 li a.is-active").text());
	</script>

</body>
</html>