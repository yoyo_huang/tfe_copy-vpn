﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/bidShlvAndSell.js" type="text/javascript"></script>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/swiper.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/product-list.css" media="screen" rel="stylesheet" type="text/css" />

	<div class="layout-view">
		<section class="section-banner" style="background-image: url(../resource/assets/images/sub-bbg.jpg)">
			<h2>熱門排行榜</h2>
			<h3>找尋適合自己的競標組合</h3>
		</section>
		<!--subsection start-->
		<!--combination-menu start-->
		<section class="section-filter">
			<div class="layout-mw-wrapper">
				<span class="btn-selectlike style-white btn-accordion">篩選適合的競標組合</span>
				<form id="queryRanking" action="${pageContext.request.contextPath}/product/queryRankingList" class="module-accordion is-off-m" method="post">
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">總金額</span>
						<input type="hidden" value="${total == null ? 0 : total}" name="total" id="total" >
						<ul>
							<li id="totalValue0" value="0">總金額</li>
							<li id="totalValue1" value="1">10萬以下</li>
							<li id="totalValue2" value="2">10~30萬</li>
							<li id="totalValue3" value="3">30~60萬</li>
							<li id="totalValue4" value="4">60~90萬</li>
							<li id="totalValue5" value="5">90萬以上</li>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">期數</span>
						<input type="hidden" value="${period == null ? 0 : period}" name="period" id="period">
						<ul>
							<li id="totalPeriods0" value="0">期數</li>
							<c:forEach var="totalVar" items="${totalPeriods}" varStatus="loop">
								<li id="totalPeriods${totalVar}" value="${totalVar}">${totalVar}</li>
							</c:forEach>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">每期金額</span>
						<input type="hidden" value="${payment == null ? 0 : payment}" name="payment" id="payment">
						<ul>
							<li id="moneyPerPeriod0" value="0">每期金額</li>
							<c:forEach var="moneyVar" items="${moneyPerPeriod}" varStatus="loop">
								<li id="moneyPerPeriod${moneyVar}" value="${moneyVar}">${moneyVar}</li>
							</c:forEach>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">信用評等</span>
						<input type="hidden" value="${level == null ? 0 : level}" name="level" id="level">
						<ul>
							<li id="levelValue0" value="0">信用評等</li>
							<li id="levelValue1" value="1">A</li>
							<li id="levelValue2" value="2">B</li>
							<li id="levelValue3" value="3">C</li>
							<li id="levelValue4" value="4">D</li>
							<li id="levelValue5" value="5">E</li>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<%--<div class="module-customselect">
						<span class="module-customselect-selected">融資傾向</span>
						<input type="hidden" value="${index == null ? 0 : index}" name="index" id="index">
						<ul>
							<li id="indexValue0" value="0">融資傾向</li>
							<li id="indexValue1" value="1">25%以下</li>
							<li id="indexValue2" value="2">25~50%</li>
							<li id="indexValue3" value="3">50~75%</li>
							<li id="indexValue4" value="4">75%以上</li>
						</ul>
					</div>--%>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">完成度</span>
						<input type="hidden" value="${progress == null ? 0 : progress}" name="progress" id="progress">
						<ul>
							<li id="progressValue0" value="0">完成度</li>
							<li id="progressValue1" value="1">50%以下</li>
							<li id="progressValue2" value="2">50~80%</li>
							<li id="progressValue3" value="3">80%以上</li>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<button type="button" class="btn-selectlike" onclick="querySelect()">開始配對組合</button>
				</form>
			</div>
		</section>
		<!--combination-menu end-->
		<!--subsection end-->
		<section class="section-content">
			<div class="layout-mw-wrapper">
				<section class="section-btnwrap">
					<a href="${pageContext.request.contextPath}/product/listOverview">
						<span class="btn-rounded22">所有競標組合一覽表</span>
					</a>
				</section>
				<div class="cb-products">
					<ul class="products">
						<c:forEach var="topRank" items="${topRankList}" varStatus="status" end="30">
							<li id="rank${topRank.shelID}">
								<p class="ranking">
									<i class="icon-rise">${status.count}</i>
								</p>
								<script type="text/javascript">
									showBidshlv("rank${topRank.shelID}", "${topRank.shelID}", "${topRank.crClass}", "${topRank.bidTermM}","${topRank.bidTermC}","${topRank.bidIntendNumber}","${topRank.bidFulMember}", '', '', 0, "${pageContext.request.contextPath}/product/detail?from=hot&shelId=${topRank.shelID}", "addToCart('${topRank.shelID}')");									
        						</script>								
							</li>
						</c:forEach>
					</ul>
				</div>
			</div>
		</section>
		<section class="section-content3">
			<div class="btn-readmore">
				<a href="${pageContext.request.contextPath}/qa">
					看不懂競標組合嗎？
					<br />
					了解更多
				</a>
			</div>
		</section>
		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
	</div>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js" type="text/javascript"></script> --%>

<script>
	function addToCart(shelId){
		$.ajax({
			type:"POST",
			url: "<c:url value='/product/addBid'/>",
			data:{
				shelId: shelId,
			},
			cache:false, 
			async:false,
			success: function(backmsg){ //取得回傳訊息
				if (backmsg == "Success"){
					$("#rank"+shelId).hide();
					alert('您所選擇競標組合已成功加入追蹤清單,請您至追蹤清單查詢,若確定要加入此競標組合,請勾選確定加入,即可成功加入競標組合');
				}else{
					alert("追蹤清單筆數已超過上限,無法加入");
				}
			}, 
			error: function(){
			}
		});
	}
	
	function querySelect() {
		var total= $("#total").val();
		var period= $("#period").val();
		var payment= $("#payment").val();
		var level= $("#level").val();
		//var index= $("#index").val();
		var progress= $("#progress").val();

		if(total == 0 && period==0 && payment==0 && level==0 && /*index==0 &&*/ progress==0){
			alert("至少選擇一種條件");
		}else{
			$("#queryRanking").submit();
		}
	}
	
	$(document).ready(
		function() {
			$("#total").parent().find('.module-customselect-selected').text($("#totalValue${total == null ? 0 : total}").text());
			$("#period").parent().find('.module-customselect-selected').text($("#totalPeriods${period == null ? 0 : period}").text());
			$("#payment").parent().find('.module-customselect-selected').text($("#moneyPerPeriod${payment == null ? 0 : payment}").text());
 			$("#level").parent().find('.module-customselect-selected').text($("#levelValue${level == null ? 0 : level}").text());
 			//$("#index").parent().find('.module-customselect-selected').text($("#indexValue${index == null ? 0 : index}").text());
 			$("#progress").parent().find('.module-customselect-selected').text($("#progressValue${progress == null ? 0 : progress}").text());
		}
	);
</script>
