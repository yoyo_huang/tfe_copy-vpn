<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/bidShlvAndSell.js" type="text/javascript"></script>
<%-- <script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/swiper.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/product-list.css" media="screen" rel="stylesheet" type="text/css" />

	<div class="layout-view">
		<section class="section-banner" style="background-image: url(${pageContext.request.contextPath}/resource/assets/images/sub-bbg.jpg)">
			<h2>競標組合</h2>
			<h3>找尋適合自己的競標組合</h3>
		</section>
		<!--subsection start-->
		<!--combination-menu start-->
		<section class="section-search">
			<div class="layout-mw-wrapper">
				<form id="searchResult" action="<c:url value='/product/listSearchresult'/>" method="post">
					<div class="module-customselect style-l">
						<span class="module-customselect-selected">參與目的</span>
						<input type="hidden" id="question1" value="" name="question1">
						<ul>
							<li value="1" onclick="selQuestions(this.value)">投資</li>
							<li value="2" onclick="selQuestions(this.value)">貸款</li>
						</ul>
					</div>
					
					<div class="module-customselect style-l question23">
						<span class="module-customselect-selected question2-1">每個月想投資多少</span>
						<span class="module-customselect-selected question2-2">想貸多少</span>
						<input type="hidden" id="question2" value="0" name="question2">
						<ul>
							<li class="question2-1" value="5000">五千或以下</li>
							<li class="question2-1" value="10000">五千~一萬</li>
							<li class="question2-1" value="99999">超過一萬或以上</li>
							
							<li class="question2-2" value="50000">五萬或五萬以下</li>
							<li class="question2-2" value="150000">五萬~十五萬</li>
							<li class="question2-2" value="300000">十五萬~三十萬</li>
							<li class="question2-2" value="999999">超過三十萬</li>
						</ul>
					</div>
					
					<div class="module-customselect style-l question23">
						<span class="module-customselect-selected question3-1">想投資多久</span>
						<span class="module-customselect-selected question3-2">每個月能還多少</span>
						<input type="hidden" id="question3" value="0" name="question3">
						<ul>
							<li class="question3-1" value="12">一年或更短</li>
							<li class="question3-1" value="24">兩年</li>
							<li class="question3-1" value="36">三年</li>
							
							<li class="question3-2" value="5000">五千或以下</li>
							<li class="question3-2" value="10000">五千~一萬</li>
							<li class="question3-2" value="99999">超過一萬</li>
						</ul>
					</div>
				</form>
				<button class="btn-selectlike style-l" onclick="queryList()">確定</button>
			</div>
		</section>
		<!--combination-menu end-->
		<!--subsection end-->
		<section class="section-content">
			<div class="layout-mw-wrapper">
				<!--PAGE CONTENT START-->
				<!--combination-porduct-group start-->
				<div class="cb-view">
					<a href="${pageContext.request.contextPath}/product/bidraisedlist?rows=30">
						<span class="btn-rounded22">完成募集競標組合</span>
					</a>
					<a href="${pageContext.request.contextPath}/product/listOverview">
						<span class="btn-rounded22">募集中競標組合</span>
					</a>
				</div>
				<br>
				<br>
				<!--group1-->
				<div class="cb-view2">
					<i class="item-icon icon-crown"></i>
					<a class="link-name" href="${pageContext.request.contextPath}/product/listRanking">熱門排行榜</a>
				</div>
				<div class="cb-products">
					<ul class="products">
						<c:forEach var="topTen" items="${topTenList}" varStatus="status" end="10">
							<li id="rank${topTen.shelID}">
								<p class="ranking">
									<i class="icon-rise">${status.count}</i>
								</p>								
								<script type="text/javascript">
									showBidshlv("rank${topTen.shelID}", "${topTen.shelID}", "${topTen.crClass}", "${topTen.bidTermM}","${topTen.bidTermC}","${topTen.bidIntendNumber}","${topTen.bidFulMember}", '', '', 0, "${pageContext.request.contextPath}/product/detail?from=hot&shelId=${topTen.shelID}", "addToCart('rank','${topTen.shelID}')");									
        						</script>								
							</li>
						</c:forEach>
					</ul>
				</div>
				<!--group1 end-->
				<!--group2-->
				<div class="cb-view2">
					<i class="item-icon icon-time"></i>
					<a class="link-name" href="${pageContext.request.contextPath}/product/listComingfull">即將滿編</a>
				</div>
				<div class="cb-products">
					<ul class="products">
						<c:forEach var="comingFull" items="${comingFullList}" varStatus="status" end="10">
							<li id="cf${comingFull.shelID}">
								<!-- PER ITEM -->								
								<script type="text/javascript">
									showBidshlv("cf${comingFull.shelID}", "${comingFull.shelID}", "${comingFull.crClass}", "${comingFull.bidTermM}","${comingFull.bidTermC}","${comingFull.bidIntendNumber}","${comingFull.bidFulMember}", '', '', 0, "${pageContext.request.contextPath}/product/detail?from=comingFull&shelId=${comingFull.shelID}", "addToCart('comingFull','${comingFull.shelID}')");									
        						</script>														
								<!--END OF PER ITEM -->
							</li>
						</c:forEach>
					</ul>
				</div>
				<!--group2 end-->
				<!--group3-->
				<div class="cb-view2">
					<i class=" item-icon icon-bell"></i>
					<a class="link-name" href="${pageContext.request.contextPath}/product/listAuction">拍賣專區</a>													
					<input type="image" onclick="showquestion()"                     
                           src="${pageContext.request.contextPath}/resource/assets/images/icon_question.png"/>
                    </form>
				</div>
				<div class="cb-products">
					<ul class="products">
						<c:forEach var="auction" items="${auctionList}" varStatus="status" end="10">
							<li id="auction${status.count}" name="auction${auction.id.shelId}" >
								<!-- PER ITEM -->		
								<script type="text/javascript">
									showBidSell("auction${status.count}", '${auction.id.shelId}', '${auction.bidSellValue}', '${auction.payment}', '${auction.bidCurrentPeriod}', '${auction.periods}', '${auction.bidOrignValue}', '${auction.bidPrepayPeriod}', 0, "${pageContext.request.contextPath}/product/detail?from=auction&shelId=${auction.id.shelId}", "buyAuctionItem(${auction.id.shelId}, '${auction.id.identification}', ${auction.id.bidNoPayPeriod}, ${auction.bidSellValue})");
								</script>													
								<!--END OF PER ITEM -->
							</li>							
						</c:forEach>
					</ul>
				</div>
				<!--group3 end-->
				<!--PAGE END-->
			</div>
		</section>
		<section class="section-content3">
			<div class="btn-readmore">
				<a href="${pageContext.request.contextPath}/qa">
					看不懂競標組合嗎？ <br/> 了解更多
				</a>
			</div>
		</section>
		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
	</div>
	<form id="toQAForm" action="/TFEFrontend/qa" method="post" >
		<input id="queryText" type="hidden" name="queryText" value="${queryText}" />		
	</form>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js" type="text/javascript"></script> --%>
<script>
	function selQuestions(value){
		if(value == 1){
			$(".module-customselect-selected.question2-1").text("每個月想投資多少");
			$(".question2-2").hide();
			$(".question2-1").show();
			$(".module-customselect-selected.question3-1").text("想投資多久");
			$(".question3-2").hide();
			$(".question3-1").show();
		}else{
			$(".module-customselect-selected.question2-2").text("想貸多少");
			$(".question2-1").hide();
			$(".question2-2").show();
			$(".module-customselect-selected.question3-2").text("每個月能還多少");
			$(".question3-1").hide();
			$(".question3-2").show();
		}
		$("#question2").val(0);
		$("#question3").val(0);
		$(".question23").show();
	}
	
	function queryList(){
		var question1 = $("#question1").val();
		var question2 = $("#question2").val();
		var question3 = $("#question3").val();
		
		if(question1 == 0 || question2==0 || question3==0){
			alert("請選擇條件");
		}else{
			$("#searchResult").submit();
		}
	}
	
	function showquestion() {		
		$("#toQAForm").submit();		
	}
	
	function addToCart(from, shelId){
		$.ajax({
			type:"POST",
			url: "<c:url value='/product/addBid'/>",
			data:{
				shelId: shelId,
			},
			cache:false, 
			async:false,
			success: function(backmsg){ //取得回傳訊息
				if (backmsg == "Success"){
					$("#rank"+shelId).hide();
					$("#cf"+shelId).hide();
					alert('您所選擇競標組合已成功加入追蹤清單,請您至追蹤清單查詢,若確定要加入此競標組合,請勾選確定加入,即可成功加入競標組合');
				}else{
					alert("追蹤清單筆數已超過上限,無法加入");
				}
			}, 
			error: function(){
				alert("未預期錯誤");
			}
		});
	}
	
	function buyAuctionItem(shelId, sellIdentity, bidNoPayPeriod, bidSellValue){
		var r = confirm("是否確認購買商品");
		if (r == true) {

		} else {
			return;
		}

		if("${accountVO ne null}" == "true"){
			var credStatus1 = "${accountVO.credStatus1}";
			if(credStatus1 == 4){
				var buyIdentity = "${accountVO.identification}";
				$.ajax({
					type:"POST",
					url: "<c:url value='/product/buyAuction'/>",
					data:{
						buyIdentity: buyIdentity,
						shelId: shelId,
						sellIdentity: sellIdentity,
						bidNoPayPeriod: bidNoPayPeriod,
						bidSellValue: bidSellValue,
					},
					cache:false, 
					async:false,
					success: function(backmsg){ //取得回傳訊息
						if (backmsg.success == true){
//							$("#auction"+shelId).hide();
							alert("拍賣商品購買成功");
							$("li[name='auction"+shelId+"']").hide();
						}else{
							alert(backmsg.msg);
						}
					}, 
					error: function(){
						alert("購買失敗");
					}
				});
			}else{
				alert("非正式會員不得購買拍賣商品");
			}
		}else{
			alert("非正式會員不得購買拍賣商品");
		}
		
	}
	
	$(document).ready(
		function() {
			$(".question23").hide();
			$(".question2-2").hide();
			$(".question3-2").hide();
		}
	);
</script>