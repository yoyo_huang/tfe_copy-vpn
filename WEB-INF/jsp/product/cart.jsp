﻿﻿﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/bidShlvAndSell.js" type="text/javascript"></script>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/swiper.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/product-list.css" media="screen" rel="stylesheet" type="text/css" />
<%--<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/style.css" media="screen" rel="stylesheet" type="text/css" />--%>
<%--<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />--%>

	<div class="layout-view">
		<section class="section-banner" style="background-image: url(../resource/assets/images/sub-bbg.jpg)">
			<h2>追蹤清單</h2>
			<h3>找尋適合自己的競標組合</h3>
		</section>
		<!--subsection start-->
		<!--combination-menu start-->
		<!--subsection start-->
		<section class="section-banner2 style-cart">
			<!--laptop start-->
			<div class="layout-mw-wrapper">
				<span class="module-btn btn-cart-checkall">
					<img id="allClick" src="${pageContext.request.contextPath}/resource/assets/images/icon-check-wb.png" alt="">全部選取
				</span>
				<span class="module-btn btn-cart-discheckall">
					<img id="allCancel" src="${pageContext.request.contextPath}/resource/assets/images/icon-check-ob.png" alt="">全部取消
				</span>
				<span class="module-btn btn-orange">
					<a onclick="buyBidValid()">確定加入</a>
					<i id="createQuest" href="#popup-html" class="do-popup-login cboxElement" onclick="questionCreate()" style="display: none;"></i>
				</span>
			</div>
			<!--mobile end-->
		</section>
		<!--combination-menu end-->
		<!--subsection end-->
		<section class="section-content">
			<div class="layout-mw-wrapper">
				<!--PAGE CONTENT START-->
				<div class="cb-products">
					<ul class="products">
						<c:forEach var="cartBidShlv" items="${cartBidShlvList}" varStatus="status">
							<li id="bid${cartBidShlv.shelID}">								
        						<input type="hidden" name="tmpPayment" value="${cartBidShlv.bidTermM}">																					
								<input type="hidden" name="productName" value="${cartBidShlv.prodName}">
								<input type="hidden" name="productNo" value="${cartBidShlv.prodmID}">
								<input type="hidden" name="totalPeriods" value="${cartBidShlv.bidTermC}">
								<input type="hidden" name="biddedCnt" value="${cartBidShlv.biddedCnt}">
							</li>
							<script type="text/javascript">
								showBidshlv("bid${cartBidShlv.shelID}", "${cartBidShlv.shelID}", "${cartBidShlv.crClass}", "${cartBidShlv.bidTermM}","${cartBidShlv.bidTermC}","${cartBidShlv.bidIntendNumber}","${cartBidShlv.bidFulMember}", '', '', 1, "${pageContext.request.contextPath}/product/detail?from=cart&shelId=${cartBidShlv.shelID}", "validCount()");									
       						</script>
						</c:forEach>
					</ul>
				</div>
			</div>
			<div class="hidden-popup-contents" style="display: none;">
				<div id="popup-html" class="module-popup" style="min-width: 620px; max-width: 750px; padding-bottom: 2em;">
					<h3>參與目的</h3>
					<div class="cart_check">
					</div>
					<div class="cart_check_btn">
						<span class="module-btn btn-orange" onclick="buyBidList()">送出</span>
					</div>
				</div>
			</div>
		</section>
		<section class="section-content3">
			<div class="btn-readmore">
				<a href="${pageContext.request.contextPath}/qa">看不懂競標組合嗎？<br />了解更多</a>
			</div>
		</section>
		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
	</div>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js" type="text/javascript"></script> --%>
<script>
	function buyBidValid(){
		var totalCount = 0;
		var buyCount = 0;
		var bidList = $("input[name=buy]");
		bidList.each(function (index) {
			totalCount++;
			var ischecked = $(this).is(":checked");
			if (ischecked) {
				buyCount++;
			}
		});
		if(buyCount > 0){
			var isAccountExist = "${accountVO eq null}";
			console.log(isAccountExist);
			if(isAccountExist == "false"){
				var credStatus1 = "${accountVO.credStatus1}";
				if(credStatus1 == 4){
					jQuery("#createQuest").click();
				}else{
					alert("請完成會員審核後,再加入組合");
				}
			}else{
				alert("請註冊會員,並匯入首期會錢後,再進行加入組合動作");
			}
		}else{
			alert("請至少選取一筆組合加入");
		}
	}
	
	function validCount(){
		var totalCount = 0;
		var buyCount = 0;
		var bidList = $("input[name=buy]");
		bidList.each(function (index) {
			totalCount++;
			var ischecked = $(this).is(":checked");
			if (ischecked) {
				buyCount++;
			}
		});
		if(totalCount == buyCount){
			$("#allClick").attr("src","../resource/assets/images/icon-check-ob.png");
		}else{
			$("#allClick").attr("src","../resource/assets/images/icon-check-wb.png");
		}
		if(buyCount > 0){
			$("#allCancel").attr("src","../resource/assets/images/icon-check-wb.png");
		}else{
			$("#allCancel").attr("src","../resource/assets/images/icon-check-ob.png");
		}
	}
	
	function questionCreate(){
		$(".cart_check").html("");
		var bidList = $("input[name=buy]");
		var questionStr = "<table>";
		bidList.each(function (index) {
			var ischecked = $(this).is(":checked");
	        if (ischecked) {
	        	questionStr = questionStr + "<tr><td>" + $("#bid"+bidList.eq(index).val()).html() + "</td>" +
	        	'<td><div class="option" style="text-align: left;"><label class="module-checkbox" style="max-width: 600px;">'+
	        	'<span class="chooseitem"><input type="radio" name="whyJoinBid' + index + '" value="1"><i class="module-symbol"></i><span>投資理財</span></span>'+
	        	'<span class="chooseitem"><input type="radio" name="whyJoinBid' + index + '" value="2"><i class="module-symbol"></i><span>個人消費</span></span>'+
	        	'<span class="chooseitem"><input type="radio" name="whyJoinBid' + index + '" value="3"><i class="module-symbol"></i><span>居家修繕</span></span>'+
	        	'<span class="chooseitem"><input type="radio" name="whyJoinBid' + index + '" value="4"><i class="module-symbol"></i><span>債務整合</span></span>'+
	        	'<span class="chooseitem"><input type="radio" name="whyJoinBid' + index + '" value="5"><i class="module-symbol"></i><span>資金周轉</span></span>'+
	        	'<span class="chooseitem"><input type="radio" name="whyJoinBid' + index + '" value="6"><i class="module-symbol"></i><span>結婚</span></span>'+
	        	'<span class="chooseitem"><input type="radio" name="whyJoinBid' + index + '" value="7"><i class="module-symbol"></i><span>進修</span></span>'+
	        	'<span class="chooseitem"><input type="radio" name="whyJoinBid' + index + '" value="8"><i class="module-symbol"></i><span>休閒旅遊</span></span>'+
	        	'<span class="chooseitem"><input type="radio" name="whyJoinBid' + index + '" value="9"><i class="module-symbol"></i><span>買車</span></span>'+
	        	'<span class="chooseitem"><input type="radio" name="whyJoinBid' + index + '" value="10" checked="checked"><i class="module-symbol"></i><span>其他</span></span>'+
	        	'</label></div></td></tr>'
	        }
		});
		questionStr = questionStr + "</table>";
		$(".cart_check").append(questionStr);
	
	}
	
	function buyBidList(){
		$.colorbox.close();
		var bidList = $("input[name=buy]");
		var paymentList = $("span.dollarsign");
		var tmpPaymentList = $("input[name='tmpPayment']");
		var totalPayment = 0;
		bidShlvVOList = [];
		bidList.each(function (index) {
	        var ischecked = $(this).is(":checked");
	        if (ischecked) {
	            var shelId = bidList.eq(index).val();
//	            var payment = Number(paymentList.eq(index).parent().text().trim().substring(5));
	            var payment = Number(tmpPaymentList.eq(index).val());
//				console.log("11: "+paymentList.eq(index).parent().text().trim().substring(5));
//				console.log("22: "+paymentList.eq(index).parent().text().trim());
//				console.log("33: "+paymentList.eq(index).parent().text());
//				console.log("44: "+tmpPaymentList.eq(index).val());
//				console.log("shelId:" + shelId + "  payment:"+payment);

	            totalPayment = totalPayment + payment;
	            bidShlvVO = {};
	            bidShlvVO["shelId"] = shelId;
	            bidShlvVO["payment"] = payment;
	            bidShlvVO["productName"] = $("input[name=productName]").eq(index).val();
	            bidShlvVO["productNo"] = $("input[name=productNo]").eq(index).val();
	            bidShlvVO["period"] = $("input[name=totalPeriods]").eq(index).val();
	            bidShlvVO["biddedCnt"] = $("input[name=biddedCnt]").eq(index).val();
	            bidShlvVO["intendAsk"] = $("input[name=whyJoinBid" + index + "]:checked").val();
				bidShlvVOList.push(bidShlvVO);
	        }
	    });
		cartBidShlvVO = {};
		cartBidShlvVO['bidShlvVOList'] = bidShlvVOList;
		cartBidShlvVO['identification'] = "${accountVO.identification}";
		cartBidShlvVO['totalPayment'] = totalPayment;
//		console.log("cartBidShlvVO:"+cartBidShlvVO);
		$.ajax({
			type:"POST",
			headers : { 'Accept' : 'application/json', 'Content-Type' : 'application/json' },
			url: "<c:url value='/product/buyBidList'/>",
			data : JSON.stringify(cartBidShlvVO),
			cache:false,
			async:false,
			success: function(backmsg){ //取得回傳訊息
				if (backmsg.success){
					alert(backmsg.msg);
					var failShelIdList = backmsg.failShelIdList;
					bidList.each(function (index) {
				        if ($(this).is(":checked")) {
				        	var isSuccess = true;
				        	for(var i=0; i < failShelIdList.length; i++){
				        		if(bidList.eq(index).val() == failShelIdList[i]){
				        			isSuccess = false;
				        		}
				        	}
				        	if(isSuccess){
				        		$("#bid"+bidList.eq(index).val()).hide();
				        	}
				        }
					});
				}else{
					alert(backmsg.msg);
				}
			},
			error: function(){
				alert("未預期錯誤");
			}
		});
	}
	
</script>