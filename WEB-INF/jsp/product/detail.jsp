﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/swiper.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/product-detail.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/common/css/jquery.jqplot.css" rel="stylesheet" type="text/css"  />

<script>
	var jobDisplay1 = new Object();
	<c:forEach var="codeMapping" items="${jobDisplayFor1}">jobDisplay1["${codeMapping.code}"]= ["${codeMapping.description1}", "${codeMapping.description2}"]; </c:forEach>

	var jobDisplay2 = new Object();
	<c:forEach var="codeMapping" items="${jobDisplayFor2}">jobDisplay2["${codeMapping.code}"]= ["${codeMapping.description1}", ""]; </c:forEach>

	var jobDisplay3 = new Object();
	<c:forEach var="codeMapping" items="${jobDisplayFor3}">jobDisplay3["${codeMapping.code}"]= ["${codeMapping.description1}", ""]; </c:forEach>

	function getJobDisplay(incomeSource, jobType,workKind){
//		console.log("incomeSource=%s, jobType=%s,workKind=%s", incomeSource, jobType,workKind);
		var outStr ="";
		if(incomeSource=="" ||incomeSource=="0"){
			outStr="投資戶";
		}else if(incomeSource=="4"){
			outStr="學生";
		}else if(incomeSource=="1"){
			var displayValues = jobDisplay1[jobType];
			outStr = (displayValues[1]=="")? displayValues[0]:displayValues[1];
		}else if(incomeSource=="2"){
			var displayValues = jobDisplay2[workKind];
			outStr = (displayValues[1]=="")? displayValues[0]:displayValues[1];
		}else if(incomeSource=="3"){
			var displayValues = jobDisplay3[jobType];
			outStr = (displayValues[1]=="")? displayValues[0]:displayValues[1];
		}
//		console.log(outStr);
		document.write(outStr);

	}

	function getAge(dateString)
	{
		var today = new Date();
		var birthDate = new Date(dateString);
		var age = today.getFullYear() - birthDate.getFullYear();
//		console.log("dateString:%s, age:%s",dateString,age);
//		var m = today.getMonth() - birthDate.getMonth();
//		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate()))
//		{
//			age--;
//		}
		document.write(age);
	}
</script>

<body>
	<div class="layout-view">
		<section class="section-banner" style="height: 0;">
			<!--
    <h2>大標</h2>
    <h3>小標</h3> -->
		</section>
		<!--subsection start-->
		<section class="section-banner2">
			<!--laptop start-->
			<div class="layout-mw-wrapper">
				<div class="sbheader-right">
					<c:if test="${!cartList.contains(bidShlv.shelID)}">
						<span id="addcartList" class="btn-rounded2" onclick="addToCart(${bidShlv.shelID})">
							<i class="icon-cart"></i>
							<a class="link-name">加入追蹤清單</a>
						</span>
					</c:if>
					<span id="addTrackList" class="btn-rounded2" onclick="updateBidTrack('add')" style="display: none;">
						<i class="icon-follow"></i>
						<a class="link-name">加入追蹤清單</a>
					</span>
					<span id="removeTrackList" class="btn-rounded2" onclick="updateBidTrack('remove')" style="display: none;">
						<i class="icon-follow"></i>
						<a class="link-name">退出追蹤清單</a>
					</span>
					<span id="shareFBBtn" class="btn-circle3">
						<a class="link-name" href="javascript:void(0);" onclick="shareFB();return false;">
							<i class="icon-fb"></i>
						</a>
<!-- 						<div id="shareLink" class="fb-share-button" data-href="http://www.taiwanfundexchange.com.tw/TFEFrontend/product/detail?from=hot&amp;shelId=1605017811" data-layout="box_count" data-mobile-iframe="false" style="height: 15px"/> -->
					</span>
				</div>
				<!--laptop end-->
				<!-- mobile start-->
				<div class="sbheader-mobile">
					<div class="sbheader-box">
						<c:if test="${!cartList.contains(bidShlv.shelID)}">
							<span id="pAddcartList" class="btn-circle2" onclick="addToCart(${bidShlv.shelID})">
								<i class="icon-cart"></i>
							</span>
						</c:if>
						<span id="appAddTrackList" class="btn-circle2" onclick="updateBidTrack('add')" style="display: none;">
							<i class="icon-follow"></i>
						</span>
						<a class="link-name" href="javascript:void(0);" onclick="shareFB();return false;">
							<span class="btn-circle5">
								<i class="icon-fb2"></i>
							</span>
						</a>
					</div>
				</div>
			</div>
			<!--mobile end-->
		</section>
		<!--subsection end-->
		<section class="section-content">
			<div class="layout-mw-wrapper">
				<!--PAGE CONTENT START-->
				<!--走勢圖開始-->
				<section id="section-product-detail-n-chart">
					<div class="col-left">
						<h2 class="scene-title">資訊列表</h2>
						<i class="icon-underline"></i>
						<div id="column-detailtable">
							<div class="tr-rounded">
								<div class="tr-rounded-td">競標組合代號</div>
								<div class="tr-rounded-td">${bidShlv.shelID}</div>
							</div>
							<div class="tr-rounded">
								<div class="tr-rounded-td">總金額</div>
								<div class="tr-rounded-td">${bidShlv.bidCostTtl}</div>
							</div>
							<div class="tr-rounded">
								<div class="tr-rounded-td">每期組合金額</div>
								<div class="tr-rounded-td">${bidShlv.bidTermM}</div>
							</div>
							<div class="tr-rounded">
								<div class="tr-rounded-td">期數</div>
								<div class="tr-rounded-td">${bidShlv.bidTermC}</div>
							</div>
							<div class="tr-rounded">
								<div class="tr-rounded-td">總人數</div>
								<div class="tr-rounded-td">${bidShlv.bidTermC * bidShlv.biddedCnt}</div>
							</div>
							<div class="tr-rounded">
								<div class="tr-rounded-td">每期得標人數</div>
								<div class="tr-rounded-td">${bidShlv.biddedCnt}</div>
							</div>
							<div class="tr-rounded">
								<div class="tr-rounded-td">TFE評等</div>
								<div class="tr-rounded-td">${bidShlv.crClass}</div>
							</div>
							<div class="tr-rounded">
								<div class="tr-rounded-td">開標日</div>
								<div class="tr-rounded-td">
									<c:choose>
										<c:when test="${not empty currentBidSetDetailList}">
											${fn:substring(currentBidSetDetailList[0].bidDate, 0 , 4)}/${fn:substring(currentBidSetDetailList[0].bidDate, 4 , 6)}/${fn:substring(currentBidSetDetailList[0].bidDate, 6 , 8)}
										</c:when>
									</c:choose>
								</div>
							</div>
							<div class="tr-rounded">
								<div class="tr-rounded-td">開標時間</div>
								<div class="tr-rounded-td">${bidOpenTime}</div>
							</div>
							<div class="tr-rounded">
								<div class="tr-rounded-td">融資傾向</div>
								<div class="tr-rounded-td">${bidShlv.credit}%</div>
							</div>
						</div>
					</div>
					<div class="col-right">
						<h2 class="scene-title">走勢圖</h2>
						<i class="icon-underline"></i>
						<div class="trendchart">
							<div id="chartdiv" style="height:400px;width:100%; "></div>
						</div>
					</div>
					<div style="clear: both;"></div>
				</section>
				<!--走勢圖結束-->
				<!--逐期競標結果-->
				<section id="period-result">
					<h2 class="scene-title">逐期競標結果</h2>
					<i class="icon-underline"></i>
					<div class="corner1 table-wrap">
						<table width="100%" class="pure-table">
							<thead>
								<tr>
									<th>期數</th>
									<th>使用者代號</th>
									<th>得標金</th>
									<th>
										信用金
										<input type="image" onclick="showquestion('${queryText3}')" src="${pageContext.request.contextPath}/resource/assets/images/icon_question.png"/>
									</th>
									<th>金流</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${empty bidSetDetailList}">
										<tr><td colspan="5" align="right">查無資料</td></tr>
									</c:when>
									<c:otherwise>
										<c:forEach var="bidSetDetail" items="${bidSetDetailList}" varStatus="">
											<tr>
												<td>${bidSetDetail.id.periods}</td>
												<td>${wonIdMap.get(bidSetDetail.id.identification)}</td>
												<td>NT
													<c:choose>
														<c:when test="${bidSetDetail.isLuckyBid eq 'Y'}">
															<span style="color: red;">0(福氣出場)</span>
														</c:when>
														<c:otherwise>
															<span>${bidSetDetail.wonBidPrice}</span>
														</c:otherwise>
													</c:choose>
												</td>
												<td>NT<span>${bidSetDetail.guranCreditFee}</span></td>
												<td>
													<c:choose>
														<c:when test="${bidSetDetail.isLuckyBid eq 'Y'}">
															<span>0</span>
														</c:when>
														<c:otherwise>
															<span>${bidSetDetail.wonBidPrice + bidSetDetail.guranCreditFee}</span>
														</c:otherwise>
													</c:choose>
												</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
					<!--逐期競標結果手機版-->
					<div class="table-wrap-on-mobile">
						<div class="corner1">
							<div class="swiper-outer">
								<div class="swiper-container">
									<div class="swiper-wrapper">
										<c:forEach var="bidSetDetail" items="${bidSetDetailList}" >
											<div class="swiper-slide">
												<table width="100%" class="pure-table style-on-mobile">
													<tbody>
														<tr>
															<td>期數</td>
															<td>${bidSetDetail.id.periods}</td>
														</tr>
														<tr>
															<td>使用者代號</td>
															<td>${wonIdMap.get(bidSetDetail.id.identification)}</td>
														</tr>
														<tr>
															<td>得標金</td>
															<td>
																<c:choose>
																	<c:when test="${bidSetDetail.isLuckyBid eq 'Y'}">
																		<span style="color: red;">0(福氣出場)</span>
																	</c:when>
																	<c:otherwise>
																		<span>${bidSetDetail.wonBidPrice}</span>
																	</c:otherwise>
																</c:choose>
															</td>
														</tr>
														<tr>
															<td>
																信用金<input type="image" onclick="showquestion('${queryText3}')" src="${pageContext.request.contextPath}/resource/assets/images/icon_question.png"/>
															</td>
															<td>NT<span>${bidSetDetail.guranCreditFee}</span></td>
														</tr>
														<tr>
															<td>金流</td>
															<td>${bidSetDetail.wonBidPrice + bidSetDetail.guranCreditFee}</td>
														</tr>
													</tbody>
												</table>
											</div>
										</c:forEach>
									</div>
									<div class="swiper-pagination" style="display: none;"></div>
								</div>
							</div>
						</div>
						<div class="swipe-pager">
							<div class="swiper-button-prev"></div>
							<span class="nowpage">1</span>
							/
							<span class="totalpage">1</span>
							<div class="swiper-button-next"></div>
						</div>
					</div>
				</section>
				<!--逐期競標結果END-->
				<!--參與者詳細資料-->
				<section id="joiner-result" class="is-off">
					<div class="btn-rounded3 switch-joiner">查看參與者詳細資料</div>
					<div class="corner1 table-wrap">
						<table width="100%" class="pure-table">
							<thead>
								<tr>
									<th>代號</th>
									<th>
										TFE評等
										<input type="image" onclick="showquestion('${queryText1}')" src="${pageContext.request.contextPath}/resource/assets/images/icon_question.png"/>
									</th>
									<th>
										融資傾向
										<input type="image" onclick="showquestion('${queryText2}')" src="${pageContext.request.contextPath}/resource/assets/images/icon_question.png"/>
									</th>	
									<th>累積標會數</th>
									<th>性別</th>
									<th>年齡</th>
									<th>職業</th>
									<th>資產證明</th>
									<th>目的</th>
									<th>狀態</th>
								</tr>
							</thead>
							<tbody>
								<c:choose>
									<c:when test="${empty accountList}">
										<tr><td colspan="11" align="right">查無資料</td></tr>
									</c:when>
									<c:otherwise>
										<c:forEach var="account" items="${accountList}" >
											<tr>
												<td>${account.userId}</td>
												<td>${account.TFELevel}</td>
												<td>
													<c:forEach var="iofData" items="${iofDataList}">
														<c:if test="${account.identification eq iofData.IOFID}">
															${iofData.IOFValue}%
														</c:if>
													</c:forEach>
												</td>
												<td>
													<c:forEach var="bidCountMap" items="${bidCountMap}">
														<c:if test="${account.identification eq bidCountMap.key}">
															${bidCountMap.value}
														</c:if>	
													</c:forEach>												
												</td>
												<td>${account.gender == 1 ? "男" : "女"}</td>
												<td><script>getAge("${account.birthdate}");</script></td>
												<td><script>getJobDisplay("${account.incomeSource}", "${account.jobType}","${account.workKind}");</script></td>
												<td>${account.otpRemark}</td>
												<td>
													<c:forEach var="memberIntendBidding" items="${mibList}">
														<c:if test="${account.identification eq memberIntendBidding.identification}">
															<c:if test="${memberIntendBidding.intendAsk eq 1}">投資理財</c:if>
															<c:if test="${memberIntendBidding.intendAsk eq 2}">個人消費</c:if>
															<c:if test="${memberIntendBidding.intendAsk eq 3}">居家修繕</c:if>
															<c:if test="${memberIntendBidding.intendAsk eq 4}">債務整合</c:if>
															<c:if test="${memberIntendBidding.intendAsk eq 5}">資金周轉</c:if>
															<c:if test="${memberIntendBidding.intendAsk eq 6}">結婚</c:if>
															<c:if test="${memberIntendBidding.intendAsk eq 7}">進修</c:if>
															<c:if test="${memberIntendBidding.intendAsk eq 8}">休閒旅遊</c:if>
															<c:if test="${memberIntendBidding.intendAsk eq 9}">買車</c:if>
															<c:if test="${memberIntendBidding.intendAsk eq 10}">其他</c:if>
														</c:if>
													</c:forEach>
												</td>
												<td>
													<c:choose>
														<c:when test="${not empty currentBidSetDetailList}">
															<c:forEach var="currentBidSetDetail" items="${currentBidSetDetailList}">
																<c:if test="${account.identification eq currentBidSetDetail.id.identification}">
																	<c:if test="${currentBidSetDetail.bidPayStatus eq 0 || currentBidSetDetail.bidPayStatus eq 1 || currentBidSetDetail.bidPayStatus eq 2 || currentBidSetDetail.bidPayStatus eq 6 || currentBidSetDetail.bidPayStatus eq 7 || currentBidSetDetail.bidPayStatus eq 10}">正常</c:if>
																	<c:if test="${currentBidSetDetail.bidPayStatus eq 5 || currentBidSetDetail.bidPayStatus eq 8}">拍賣中</c:if>
																	<c:if test="${currentBidSetDetail.bidPayStatus eq 3 || currentBidSetDetail.bidPayStatus eq 11 || currentBidSetDetail.bidPayStatus eq 12}">逾期</c:if>
																	<c:if test="${currentBidSetDetail.bidPayStatus eq 4 || currentBidSetDetail.bidPayStatus eq 9}">遲繳</c:if>
																</c:if>
															</c:forEach>
														</c:when>
														<c:otherwise>正常</c:otherwise>
													</c:choose>
												</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
					<!--參與者詳細資料手機版-->
					<div class="table-wrap-on-mobile">
						<div class="corner1">
							<div class="swiper-outer">
								<div class="swiper-container">
									<div class="swiper-wrapper">
										<c:forEach var="account" items="${accountList}" >
											<div class="swiper-slide">
												<table width="100%" class="pure-table style-on-mobile">
													<tbody>
														<tr>
															<td>代號</td>
															<td>${account.userId}</td>
														</tr>
														<tr>
															<td>
																TFE評等
																<input type="image" onclick="showquestion('${queryText1}')" src="${pageContext.request.contextPath}/resource/assets/images/icon_question.png"/>
															</td>
															<td>${account.TFELevel}</td>
														</tr>
														<tr>
															<td>
																融資傾向
																<input type="image" onclick="showquestion('${queryText2}')" src="${pageContext.request.contextPath}/resource/assets/images/icon_question.png"/>	
															</td>
															<td>${account.bidIntIndex}</td>
														</tr>
														<tr>
															<td>TFE年資</td>
															<td>${account.TFEYear}</td>
														</tr>
														<tr>
															<td>性別</td>
															<td>${account.gender == 1 ? "男" : "女"}</td>
														</tr>
														<tr>
															<td>年齡</td>
															<td>${account.age}</td>
														</tr>
														<tr>
															<td>職業</td>
															<td>
																<c:if test="${account.incomeSource == 0}">投資戶</c:if>
																<c:if test="${account.incomeSource == 1}">
																	<c:if test="${account.jobType == 1}">專業人士</c:if>
																	<c:if test="${account.jobType == 2}">公教正式編制人員</c:if>
																	<c:if test="${account.jobType == 3}">績優公司主管</c:if>
																	<c:if test="${account.jobType == 4}">一般企業白領主管</c:if>
																	<c:if test="${account.jobType == 5}">軍警校級以上軍官</c:if>
																	<c:if test="${account.jobType == 6}">公教藍領</c:if>
																	<c:if test="${account.jobType == 7}">公教退休</c:if>
																	<c:if test="${account.jobType == 8}">績優公司白領員工</c:if>
																	<c:if test="${account.jobType == 9}">績優公司作業員</c:if>
																	<c:if test="${account.jobType == 10}">績優公司藍領</c:if>
																	<c:if test="${account.jobType == 11}">軍警普考以上警察</c:if>
																	<c:if test="${account.jobType == 12}">公教兼任人員</c:if>
																	<c:if test="${account.jobType == 13}">一般企業員工</c:if>
																	<c:if test="${account.jobType == 14}">軍警士官長</c:if>
																	<c:if test="${account.jobType == 15 || account.jobType == 16}">軍警基層員警</c:if>
																</c:if>
																<c:if test="${account.incomeSource == 2}">非受薪</c:if>
																<c:if test="${account.incomeSource == 3}">專業投資人</c:if>
																<c:if test="${account.incomeSource == 4}">學生</c:if>
															</td>
														</tr>
														<tr>
															<td>目的</td>
															<td>
																<c:forEach var="memberIntendBidding" items="${mibList}">
																	<c:if test="${account.identification eq memberIntendBidding.identification}">
																		<c:if test="${memberIntendBidding.intendAsk eq 1}">投資理財</c:if>
																		<c:if test="${memberIntendBidding.intendAsk eq 2}">個人消費</c:if>
																		<c:if test="${memberIntendBidding.intendAsk eq 3}">居家修繕</c:if>
																		<c:if test="${memberIntendBidding.intendAsk eq 4}">債務整合</c:if>
																		<c:if test="${memberIntendBidding.intendAsk eq 5}">資金周轉</c:if>
																		<c:if test="${memberIntendBidding.intendAsk eq 6}">結婚</c:if>
																		<c:if test="${memberIntendBidding.intendAsk eq 7}">進修</c:if>
																		<c:if test="${memberIntendBidding.intendAsk eq 8}">休閒旅遊</c:if>
																		<c:if test="${memberIntendBidding.intendAsk eq 9}">買車</c:if>
																		<c:if test="${memberIntendBidding.intendAsk eq 10}">其他</c:if>
																	</c:if>
																</c:forEach>
															</td>
														</tr>
														<tr>
															<td>狀態</td>
															<td>
																<c:choose>
																	<c:when test="${not empty currentBidSetDetailList}">
																		<c:forEach var="currentBidSetDetail" items="${currentBidSetDetailList}">
																			<c:if test="${account.identification eq currentBidSetDetail.id.identification}">
																				<c:if test="${currentBidSetDetail.bidPayStatus eq 0 || currentBidSetDetail.bidPayStatus eq 1 || currentBidSetDetail.bidPayStatus eq 2 || currentBidSetDetail.bidPayStatus eq 6 || currentBidSetDetail.bidPayStatus eq 7 || currentBidSetDetail.bidPayStatus eq 10}">正常</c:if>
																				<c:if test="${currentBidSetDetail.bidPayStatus eq 5 || currentBidSetDetail.bidPayStatus eq 8}">拍賣中</c:if>
																				<c:if test="${currentBidSetDetail.bidPayStatus eq 3 || currentBidSetDetail.bidPayStatus eq 11 || currentBidSetDetail.bidPayStatus eq 12}">逾期</c:if>
																				<c:if test="${currentBidSetDetail.bidPayStatus eq 4 || currentBidSetDetail.bidPayStatus eq 9}">遲繳</c:if>
																			</c:if>
																		</c:forEach>
																	</c:when>
																	<c:otherwise>正常</c:otherwise>
																</c:choose>
															</td>
														</tr>
													</tbody>
												</table>
											</div>
										</c:forEach>
									</div>
									<div class="swiper-pagination" style="display: none;"></div>
								</div>
							</div>
						</div>
						<div class="swipe-pager">
							<div class="swiper-button-prev"></div>
							<span class="nowpage">1</span>
							/
							<span class="totalpage">1</span>
							<div class="swiper-button-next"></div>
						</div>
					</div>
				</section>
				<!--參與者詳細資料END-->
				<section id="column-discuss">
					<div class="corner1 table-wrap" style="margin-bottom: 0px;">
						<table width="100%" class="pure-table">
							<thead>
								<tr>
									<th rowspan="3" align="left" width="15%"><h1>留下訊息</h1></th>
									<th colspan="2" align="right"><textarea id="comment" style="width:100%;height:50px;resize:none;"></textarea></th>
								</tr>
								<tr>
									<th colspan="2" align="right"><input type="button" value="送出" onclick="submitComment()"></th>
								</tr>
							</thead>
						</table>
					</div>
					<div class="corner1 table-wrap">
						<table width="100%" class="pure-table">
							<thead>
								<tr>
									<th width="10%">留言人</th>
									<th width="18%">時間</th>
									<th>留言</th>
								</tr>
							</thead>
							<tbody id="allComments">
								<c:choose>
									<c:when test="${empty bidShlvCommentList}">
										<tr class="noValue"><td colspan="3" align="right">查無資料</td></tr>
									</c:when>
									<c:otherwise>
										<c:forEach var="bidShlvComment" items="${bidShlvCommentList}">
											<tr>
												<td>${bidShlvComment.userId}</td>
												<td><fmt:formatDate pattern="yyyy/MM/dd hh:mm:ss" value="${bidShlvComment.createTime}" /></td>
												<td>${bidShlvComment.comment}</td>
											</tr>
										</c:forEach>
									</c:otherwise>
								</c:choose>
							</tbody>
						</table>
					</div>
				</section>
				<!--PAGE END-->
			</div>
		</section>
		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
	</div>
	<form id="toQAForm" action="/TFEFrontend/qa" method="post" >
		<input id="queryText" type="hidden" name="queryText" value="${queryText}" />		
	</form>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js" type="text/javascript"></script> --%>
	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/swiper.js" type="text/javascript"></script>
	<script>
		var swiperResult = new Swiper('#period-result .swiper-container', {
			pagination : '#period-result .swiper-pagination',
			nextButton : '#period-result .swiper-button-next',
			prevButton : '#period-result .swiper-button-prev',
			paginationClickable : true,
			speed : 1000,
			spaceBetween : 0,
			centeredSlides : true,
			autoplay : false,
			autoplayDisableOnInteraction : false,
			onSlideChangeStart : function(swiper) {
				// console.log(this);
				$("#period-result .nowpage").text(swiper.activeIndex + 1);
			},
			onInit : function(swiper) {
				var totalpagenum = $("#period-result .swiper-slide").size();
				$("#period-result .totalpage").text(totalpagenum);
			}
		});
		var swiperJoiner = new Swiper('#joiner-result .swiper-container', {
			pagination : '#joiner-result .swiper-pagination',
			nextButton : '#joiner-result .swiper-button-next',
			prevButton : '#joiner-result .swiper-button-prev',
			paginationClickable : true,
			speed : 1000,
			spaceBetween : 0,
			centeredSlides : true,
			autoplay : false,
			autoplayDisableOnInteraction : false,
			onSlideChangeStart : function(swiper) {
				// console.log(this);
				$("#joiner-result .nowpage").text(swiper.activeIndex + 1);
			},
			onInit : function(swiper) {
				var totalpagenum = $("#joiner-result .swiper-slide").size();
				$("#joiner-result .totalpage").text(totalpagenum);
			}
		});

		function addToCart(shelId){
			$.ajax({
				type:"POST",
				url: "<c:url value='/product/addBid'/>",
				data:{
					shelId: shelId,
				},
				cache:false,
				async:false,
				success: function(backmsg){ //取得回傳訊息
					if (backmsg == "Success"){
						$("#addcartList").hide();
						$("#pAddcartList").hide();
						alert('您所選擇競標組合已成功加入追蹤清單,請您至追蹤清單查詢,若確定要加入此競標組合,請勾選確定加入,即可成功加入競標組合');
					}else{
						alert("追蹤清單筆數已超過上限,無法加入");
					}
				},
				error: function(){
					alert("未預期錯誤");
				}
			});
		}

		function submitComment(){
			var isFormalUser = "${accountVO ne null}";
			if(isFormalUser == "true"){
				var shelId = ${bidShlv.shelID};
				var comment = $("#comment").val();
				var id = "${accountVO.identification}";
				var userId = "${accountVO.userId}";
				$.ajax({
					type:"POST",
					dataType : "json",
					url: "<c:url value='/product/sendComment'/>",
					data:{
						shelId: shelId,
						identification: id,
						userId: userId,
						comment: comment
					},
					cache:false,
					async:false,
					success: function(json){
						if (json.createTime != null){
							var targetTb = $("#allComments");
							var td = '<tr><td>' + userId + '</td><td>' + json.createTime + '</td><td>' + comment + '</td></tr>';
							if(targetTb.text().trim() == "查無資料"){
								targetTb.html(td);
							}else{
								targetTb.prepend(td);
							}
							$("#comment").val("");
						}
					},
					error: function(){
					}
				});
			}else{
				alert("請先登入會員");
			}

		}

		var trackTimes = parseInt("${bidTrack eq null ? 0: bidTrack.trackNumber}");
		function updateBidTrack(action){
			var shelId = ${bidShlv.shelID};
			var id = "${accountVO.identification}";
			var productNo = "${bidShlv.prodmID}";
			var productName = "${bidShlv.prodName}";
			var bidStatus = "${(from eq 'auction' || from eq 'join') ? 1 : 0}";
			$.ajax({
				type:"POST",
				url: "<c:url value='/product/updateBidTrack'/>",
				data:{
					action: action,
					shelID: shelId,
					identification: id,
					productNo: productNo,
					productName: productName,
					bidStatus: bidStatus
				},
				cache:false,
				async:false,
				success: function(result){
					if (result == "Success"){
						if(action == "add"){
							$("#addTrackList").hide();
							$("#appAddTrackList").hide();
							$("#removeTrackList").show();
						}else{
							trackTimes += 1;
							if( trackTimes < 3){
								$("#addTrackList").show();
								$("#appAddTrackList").show();
							}else{
								$("#addTrackList").hide();
								$("#appAddTrackList").hide();
							}
							$("#removeTrackList").hide();
						}
					}
				},
				error: function(){
					alert("error");
				}
			});
		}

		function getPictureData(){
			$.ajax({
				type:"POST",
				url: "<c:url value='/product/getPictureData'/>",
				data:{
					shelId: "${bidShlv.shelID}"
				},
				cache:false,
				async:false,
				success: function(result){
					if (result.success){
						createPic(result);
					}else{
						$("#chartdiv").html('<table style="height:400px;width:100%;"><tr><td>查無歷史資料</td></tr></table>');
					}
				},
				error: function(){
					console.log("getPictureData fail");
				}
			});
		}

		function createPic(result){
			var avgBreakArry = result.avgBreakArry;// [1, 4, 3, 1, 0, 1, 7];
			var minBidArry = result.minBidArry; //[10, 23, 4, 88, 9, 67, 99];
			var avgBidArry = result.avgBidArry; //[37, 25, 34, 99, 27, 151, 111];
			var maxBidArry = result.maxBidArry; //[66, 28, 64, 105, 44, 176, 128];
			var ohlcArray = result.ohlcArray;
			$.jqplot('chartdiv', [avgBreakArry, ohlcArray, minBidArry, avgBidArry, maxBidArry], {
				seriesDefaults: {
		            rendererOptions: {
		                smooth: false
		            }
		        },
				series:[
					{
						yaxis: 'y2axis',
						renderer:$.jqplot.BarRenderer,
						rendererOptions: { barWidth: 30 }
					},
					{
						renderer: jQuery.jqplot.OHLCRenderer,
		                rendererOptions: { candleStick: true }
					}
				],
				axesDefaults: {
					pad: 1.1,
				},
				axes: {
					xaxis: {
						min: 0,
						max: 10,
					},
					yaxis: {
						min: 0
	                },
	                y2axis: {
						min: 0,
						max: 50,
						ticks:[0, 50],
						showTicks: false
	                }
				},
		        fillBetween: {
		        	 series1: [2, 3],
		             series2: [4, 4],
		             color: "rgba(0, 0, 255, 0.1)",
		             baseSeries: 2,
		             fill: true
		        },
		        legend: {
		        	labels:[ '違約人數', '目前進行平均得標金', '最低得標金', '平均得標金', '最高得標金' ],
		        	show: true,
		        	placement: "outsideGrid",
		        	location: "se"
		        },
		        highlighter: {
		        	show: true,
		        	tooltipAxes: 'y',
		        	yvalues: 1,
		        }
			});
		}
		
		function showquestion(queryValue) {
			$("#queryText").val(queryValue);
			$("#toQAForm").submit();		
		}

		$(document).ready(
			function(){
				getPictureData();
				if("${from}" === "bidraised" || "${from}" === "join"){
					$("#addcartList").hide();
					$("#pAddcartList").hide();
				}else{
					if("${accountVO ne null && accountVO.formalUser}" == "true"){
						if("${bidTrack eq null || (bidTrack.trackStatus == 1 && bidTrack.trackNumber < 3)}" == "true"){
							$("#addTrackList").show();
							$("#appAddTrackList").show();
							$("#removeTrackList").hide();
						}else if("${bidTrack.trackStatus == 0 && bidTrack.trackNumber < 3}" == "true"){
							$("#addTrackList").hide();
							$("#appAddTrackList").hide();
							$("#removeTrackList").show();
						}else{
							$("#addTrackList").hide();
							$("#appAddTrackList").hide();
							$("#removeTrackList").hide();
						}
					}
				}
			}
		);

		function shareFB(){
			var from = "${from}";
			var shelId = "${bidShlv.shelID}";
			window.open('https://www.facebook.com/sharer/sharer.php?sdk=joey&u=http%3A%2F%2Fwww.taiwanfundexchange.com.tw%2FTFEFrontend%2Fproduct%2Fdetail%3Ffrom%3D' + from + '%26shelId%3D' + shelId + '&display=popup&ref=plugin&src=share_button','title','width=500,height=300');
		}

        <%--var jobDisplay1 = {"key1":{"value1","value1"}--%>
            <%--<c:forEach var="codeMapping" items="${jobDisplayFor1}">, "${codeMapping.code}":{"${codeMapping.description1}", "${codeMapping.description2}"}--%>
            <%--</c:forEach>--%>
        <%--}--%>



	</script>
</body>