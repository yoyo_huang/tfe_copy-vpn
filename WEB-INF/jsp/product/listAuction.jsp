﻿﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/bidShlvAndSell.js" type="text/javascript"></script>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/swiper.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/product-list.css" media="screen" rel="stylesheet" type="text/css" />

	<div class="layout-view">
		<section class="section-banner" style="background-image: url(../resource/assets/images/sub-bbg.jpg)">
			<h2>拍賣專區</h2>
			<h3>找尋適合自己的競標組合</h3>
		</section>
		<!--subsection start-->
		<!--combination-menu start-->
		<section class="section-filter">
			<div class="layout-mw-wrapper">
				<span class="btn-selectlike style-white btn-accordion">篩選適合的競標組合</span>
				<form id="queryAuction" action="${pageContext.request.contextPath}/product/queryAuctionList" class="module-accordion is-off-m" method="post">
					<input type="hidden" value="${accountVO.identification}" name="identification" id="identification" >
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">總金額</span>
						<input type="hidden" value="${total == null ? 0 : total}" name="total" id="total" >
						<ul>
							<li id="totalValue0" value="0">總金額</li>
							<li id="totalValue1" value="1">10萬以下</li>
							<li id="totalValue2" value="2">10~30萬</li>
							<li id="totalValue3" value="3">30~60萬</li>
							<li id="totalValue4" value="4">60~90萬</li>
							<li id="totalValue5" value="5">90萬以上</li>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">期數</span>
						<input type="hidden" value="${period == null ? 0 : period}" name="period" id="period">
						<ul>
							<li id="totalPeriods0" value="0">期數</li>
							<c:forEach var="totalVar" items="${totalPeriods}" varStatus="loop">
								<li id="totalPeriods${totalVar}" value="${totalVar}">${totalVar}</li>
							</c:forEach>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">每期金額</span>
						<input type="hidden" value="${payment == null ? 0 : payment}" name="payment" id="payment">
						<ul>
							<li id="moneyPerPeriod0" value="0">每期金額</li>
							<c:forEach var="moneyVar" items="${moneyPerPeriod}" varStatus="loop">
								<li id="moneyPerPeriod${moneyVar}" value="${moneyVar}">${moneyVar}</li>
							</c:forEach>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">信用評等</span>
						<input type="hidden" value="${level == null ? 0 : level}" name="level" id="level">
						<ul>
							<li id="levelValue0" value="0">信用評等</li>
							<li id="levelValue1" value="1">A</li>
							<li id="levelValue2" value="2">B</li>
							<li id="levelValue3" value="3">C</li>
							<li id="levelValue4" value="4">D</li>
							<li id="levelValue5" value="5">E</li>
							<li id="levelValue6" value="6">I</li>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<%--<div class="module-customselect">
						<span class="module-customselect-selected">融資傾向</span>
						<input type="hidden" value="${index == null ? 0 : index}" name="index" id="index">
						<ul>
							<li id="indexValue0" value="0">融資傾向</li>
							<li id="indexValue1" value="1">25%以下</li>
							<li id="indexValue2" value="2">25~50%</li>
							<li id="indexValue3" value="3">50~75%</li>
							<li id="indexValue4" value="4">75%以上</li>
						</ul>
					</div>--%>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
<!-- 					<div class="module-customselect"> -->
<!-- 						<span class="module-customselect-selected">完成度</span> -->
<%-- 						<input type="hidden" value="${progress == null ? 0 : progress}" name="progress" id="progress"> --%>
<!-- 						<ul> -->
<!-- 							<li id="progressValue0" value="0">完成度</li> -->
<!-- 							<li id="progressValue1" value="1">50%以下</li> -->
<!-- 							<li id="progressValue2" value="2">50~80%</li> -->
<!-- 							<li id="progressValue3" value="3">80%以上</li> -->
<!-- 						</ul> -->
<!-- 					</div> -->
					<!--END OF PER SELECT-->
					<button type="button" class="btn-selectlike" onclick="querySelect()">開始配對組合</button>
				</form>
			</div>
		</section>
		<!--combination-menu end-->
		<!--subsection end-->
		<section class="section-content">
			<div class="layout-mw-wrapper">
				<!--PAGE CONTENT START-->
				<section class="section-btnwrap">
					<a href="${pageContext.request.contextPath}/product/listOverview">
						<span class="btn-rounded22">所有競標組合一覽表</span>
					</a>
				</section>
				<div class="cb-products">
					<ul class="products">
						<c:forEach var="auction" items="${auctionList}" varStatus="status" end="30">
							<li id="auction${status.count}" name="auction${auction.id.shelId}" >
								<script type="text/javascript">
									showBidSell("auction${status.count}", '${auction.id.shelId}', '${auction.bidSellValue}', '${auction.payment}', '${auction.bidCurrentPeriod}', '${auction.periods}', '${auction.bidOrignValue}', '${auction.bidPrepayPeriod}', 0, "${pageContext.request.contextPath}/product/detail?from=auction&shelId=${auction.id.shelId}", "buyAuctionItem(${auction.id.shelId}, '${auction.id.identification}', ${auction.id.bidNoPayPeriod}, ${auction.bidSellValue})");
								</script>
								<!--END OF PER ITEM -->
							</li>
						</c:forEach>
					</ul>
				</div>
				<!--group1 end-->
				<!--PAGE END-->
			</div>
		</section>
		<section class="section-content3">
			<div class="btn-readmore">
				<a href="${pageContext.request.contextPath}/qa">
					看不懂競標組合嗎？ <br/> 了解更多
				</a>
			</div>
		</section>
		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
	</div>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js" type="text/javascript"></script> --%>
<%-- 	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js" type="text/javascript"></script> --%>

<script>
	function querySelect() {
		var total= $("#total").val();
		var period= $("#period").val();
		var payment= $("#payment").val();
		var level= $("#level").val();
		//var index= $("#index").val();
		if(total == 0 && period==0 && payment==0 && level==0 /*&& index==0*/){
			alert("至少選擇一種條件");
		}else{
			$("#queryAuction").submit();
		}
	}
	
	function buyAuctionItem(shelId, sellIdentity, bidNoPayPeriod, bidSellValue){
		var r = confirm("是否確認購買商品");
		if (r == true) {

		} else {
			return
		}

		if("${accountVO ne null}" == "true"){
			var credStatus1 = "${accountVO.credStatus1}";
			if(credStatus1 == 4){
				var buyIdentity = "${accountVO.identification}";
				$.ajax({
					type:"POST",
					url: "<c:url value='/product/buyAuction'/>",
					data:{
						buyIdentity: buyIdentity,
						shelId: shelId,
						sellIdentity: sellIdentity,
						bidNoPayPeriod: bidNoPayPeriod,
						bidSellValue: bidSellValue,
					},
					cache:false, 
					async:false,
					success: function(backmsg){ //取得回傳訊息
						if (backmsg.success == true){
							alert("拍賣商品購買成功");
//							$("#auction"+shelId).hide();
//							$("li[id='auction"+shelId+"']").hide();
							$("li[name='auction"+shelId+"']").hide();
						}else{
							alert(backmsg.msg);
						}
					}, 
					error: function(){
						alert("購買失敗");
					}
				});
			}else{
				alert("非正式會員不得購買拍賣商品");
			}
		}else{
			alert("非正式會員不得購買拍賣商品");
		}
	}
	
	$(document).ready(
		function() {
			$("#total").parent().find('.module-customselect-selected').text($("#totalValue${total == null ? 0 : total}").text());
			$("#period").parent().find('.module-customselect-selected').text($("#totalPeriods${period == null ? 0 : period}").text());
			$("#payment").parent().find('.module-customselect-selected').text($("#moneyPerPeriod${payment == null ? 0 : payment}").text());
 			$("#level").parent().find('.module-customselect-selected').text($("#levelValue${level == null ? 0 : level}").text());
 			//$("#index").parent().find('.module-customselect-selected').text($("#indexValue${index == null ? 0 : index}").text());
		}
	);
</script>