﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/bidShlvAndSell.js" type="text/javascript"></script>
<%-- <script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<%-- <link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/swiper.css" media="screen" rel="stylesheet" type="text/css" /> --%>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/product-list.css" media="screen" rel="stylesheet" type="text/css" />

	<div class="layout-view">
		<section class="section-banner" style="background-image: url(${pageContext.request.contextPath}/resource/assets/images/sub-bbg.jpg)">
			<h2>完成募集競標組合</h2>
		</section>
		<section class="section-content" style="padding-bottom: 0px; padding-top: 20px;">
			<div class="layout-mw-wrapper">
				<div class="cb-products">
					<ul class="products" style="padding-bottom: 20px;">
						<c:forEach var="bidRaised" items="${bidRaisedList}" varStatus="status">
							<li id="rank${bidRaised.shelId}" />
							<script type="text/javascript">
								showBidshlvForFullOrStart("rank${bidRaised.shelId}", "${bidRaised.shelId}", "${bidRaised.crClass}", "${bidRaised.bidTermM}","${bidRaised.bidTermC}","${bidRaised.bidIntendNumber}","${bidRaised.bidFulMember}","${bidRaised.isClosed}","${bidRaised.bidDate}", 4, "${bidRaised.currentPeriod}", "${pageContext.request.contextPath}/product/detail?from=bidraised&&shelId=${bidRaised.shelId}", "", "");
							</script>
						</c:forEach>
					</ul>
				</div>
			</div>
			<br>
			<div align="center">
				<c:if test="${pageSetting.page ne 1}">
					&nbsp;&nbsp;&nbsp;
					<a href="${pageContext.request.contextPath}/product/bidraisedlist?rows=2&page=${pageSetting.page - 1}">
						<span class="btn-rounded22">上一頁</span>
					</a>
					&nbsp;&nbsp;&nbsp;
				</c:if>
				<c:if test="${pageSetting.page ne pageSetting.totalPage}">
					&nbsp;&nbsp;&nbsp;
					<a href="${pageContext.request.contextPath}/product/bidraisedlist?rows=2&page=${pageSetting.page + 1}">
						<span class="btn-rounded22">下一頁</span>
					</a>
					&nbsp;&nbsp;&nbsp;
				</c:if>
			</div>
		</section>
		<section class="section-content3">
			<div class="btn-readmore">
				<a href="${pageContext.request.contextPath}/qa">
					看不懂競標組合嗎？ <br/> 了解更多
				</a>
			</div>
		</section>
		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
	</div>