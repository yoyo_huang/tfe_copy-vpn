﻿﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/bidShlvAndSell.js" type="text/javascript"></script>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="Title" content="TFE" />
<meta name="Author" content="Kani@Drama iMedia Limited, Yoyo@Drama iMedia Limited" />
<meta name="Description" content="Description" />

<link href="${pageContext.request.contextPath}/resource/assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
<title>TFE</title>
<script type="text/javascript">

	var Area1 = '${area1}';
	var Area2 = '${(area2 != null)?area2:0}';
	var Area3 = '${(area3 != null)?area3:0}';
	function querySelect() {
		
		var total= $("#total").val();
		var period= $("#period").val();
		var payment= $("#payment").val();
		var level= $("#level").val();
		//var index= $("#index").val();
		var progress= $("#progress").val();

		if(total == 0 && period==0 && payment==0 && level==0 && /*index==0 &&*/ progress==0){
			alert("至少選擇一種條件");
			return;
		}
		
		$.ajax({
			type : "POST",
			url : "${pageContext.request.contextPath}/product/productSearchResult",
			data: {
				total: total,
				period: period,
				payment: payment,
				level: level,
				//index: index,
				progress: progress,
				Area1: Area1,
				Area2: Area2,
				Area3: Area3,
			},
			cache : false,
			async : false,
			success : function(backmsg) { //取得回傳訊息
				//console.log(backmsg);
				//alert(backmsg);
				if (backmsg.rs == 1) {
					$("#products1").html("");
					jQuery.each(backmsg.queryData, function(index, value) {
						var obj = value;
						$(addContent(obj)).appendTo("#products1");
					       
					   });
					
					$("#products2").html("");
					jQuery.each(backmsg.queryData2, function(index, value) {
						var obj = value;
						$(addContent(obj)).appendTo("#products2");
					       
					   });
					
					$("#products3").html("");
					jQuery.each(backmsg.queryData3, function(index, value) {
						//console.log(this);
						var obj = value;
						$(addContent(obj)).appendTo("#products3");
					       
					   });
					//alert("新增成功" );
					//$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip_ok.jpg' >此帳號可使用!");

				} else {
					//alert("查詢失敗" );
				}
				//return;
				//$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");

			},
			error : function() {
				//alert("查詢失敗" );
				//$(".tip").html("<img src='${pageContext.request.contextPath}/resource/assets/images/tip.jpg' >此號已使用！請改另一個帳號!");
				//return;
			}
		});
	}
	
	function addCar(buyId) {
		buy(buyId);
	}
	
	function addContent(obj) {
		var herh = '${pageContext.request.contextPath}/product/detail?from=comingFull&shelId=' + obj.shelID;
		var clickFunc = "addCar('" + obj.shelID + "')";
		var content = 
			'	<li id='+obj.shelID+'li>'+	
			getShowBidshlvString(obj, 0, herh, clickFunc) +							
			'   </li>';
		return content;			
	}
	
	
	$(document).ready(function() {		
		
		$("#total").val('0');
		$("#period").val('0');
		$("#payment").val('0');
		$("#level").val('0');
		$("#index").val('0');
		$("#progress").val('0');
	});
	
	
	function buy(shelId) {
		
		$.ajax({
			type:"POST",
			url: "<c:url value='/product/addBid'/>",
			data:{
				shelId: shelId,
			},
			cache:false, 
			async:false,
			success: function(backmsg){ //取得回傳訊息
				if (backmsg == "Success"){
					$('#'+shelId+'li').remove();
					alert('您所選擇競標組合已成功加入追蹤清單,請您至追蹤清單查詢,若確定要加入此競標組合,請勾選確定加入,即可成功加入競標組合');
				}else{
					alert("追蹤清單筆數已超過上限,無法加入");
				}
			}, 
			error: function(){
			}
		});
	}
	
	
	
</script>
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/font-awesome/css/font-awesome.min.css" media="screen" rel="stylesheet"
	type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/colorbox.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/vendors/swiper.css" media="screen" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/resource/assets/stylesheets/units/product-list.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="layout-view">
		<section class="section-banner" style="background-image: url(${pageContext.request.contextPath}/resource/assets/images/sub-bbg.jpg)">
			<h2>配對組合推薦</h2>
			<h3>競標組合的信用評等分12等級，請選擇適合自己的組合</h3>
		</section>
		<!--subsection start-->
		<!--combination-menu start-->
		<section class="section-filter">
			<div class="layout-mw-wrapper">
				<span class="btn-selectlike style-white btn-accordion">篩選適合的競標組合</span>
				<!-- <form action="" class="module-accordion is-off-m"> -->
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">總金額</span> <input
							type="hidden" value="0" name="total" id="total" >
						<ul>
							<li value="0">總金額</li>
							<li value="1">10萬以下</li>
							<li value="2">10~30萬</li>
							<li value="3">30~60萬</li>
							<li value="4">60~90萬</li>
							<li value="5">90萬以上</li>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">期數</span> <input
							type="hidden" value="0" name="period" id="period">
						<ul>
							<li value="0">期數</li>
							<c:forEach var="totalVar" items="${total}" varStatus="loop">
								<li value="${totalVar}">${totalVar}</li>
							</c:forEach>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">每期金額</span> <input
							type="hidden" value="0" name="payment" id="payment">
						<ul>
							<li value="0">每期金額</li>
							<c:forEach var="moneyVar" items="${money}" varStatus="loop">
								<li value="${moneyVar}">${moneyVar}</li>
							</c:forEach>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">信用評等</span> <input
							type="hidden" value="0" name="level" id="level">
						<ul>
							<li value="0">信用評等</li>
							<li value="1">A</li>
							<li value="2">B</li>
							<li value="3">C</li>
							<li value="4">D</li>
							<li value="5">E</li>

						</ul>
					</div>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<%--<div class="module-customselect">
						<span class="module-customselect-selected">融資傾向</span> <input
							type="hidden" value="0" name="index" id="index">
						<ul>
							<li value="0">融資傾向</li>
							<li value="1">25%以下</li>
							<li value="2">25~50%</li>
							<li value="3">50~75%</li>
							<li value="4">75%以上</li>
						</ul>
					</div>--%>
					<!--END OF PER SELECT-->
					<!--PER SELECT-->
					<div class="module-customselect">
						<span class="module-customselect-selected">完成度</span> <input
							type="hidden" value="0" name="progress" id="progress">
						<ul>
							<li value="0">完成度</li>
							<li value="1">50%以下</li>
							<li value="2">50~80%</li>
							<li value="3">80%以上</li>
						</ul>
					</div>
					<!--END OF PER SELECT-->
					<button class="btn-selectlike" onclick="querySelect()">開始配對組合</button>
				<!--</form>-->
			</div>
		</section>
		<!--combination-menu end-->
		<!--subsection end-->
		<section class="section-content">
			<div class="layout-mw-wrapper">
				<!--PAGE CONTENT START-->
				<section class="section-btnwrap">
					<a href="${pageContext.request.contextPath}/product/listOverview">
						<span class="btn-rounded22">募集中競標組合</span>
					</a>
				</section>
				<!--group1-->
				<div class="cb-view2">
					<i class="item-icon icon-coin"></i>
					<a class="link-name">
						小於
						<span class="color-pink">${area1}</span>
						的組合
					</a>
				</div>
				<div class="cb-products">
					<ul class="products" id="products1">
						<c:forEach var="obj" items="${queryData}" varStatus="loop">
							<c:if test="${obj.bidTermM < area1}">
								<li id='${obj.shelID}li' />									
								<script type="text/javascript">
									showBidshlv("${obj.shelID}li", "${obj.shelID}", "${obj.crClass}", "${obj.bidTermM}","${obj.bidTermC}","${obj.bidIntendNumber}","${obj.bidFulMember}", '', '', 0, "${pageContext.request.contextPath}/product/detail?from=comingFull&shelId=${obj.shelID}", "addCar('${obj.shelID}')");									
        						</script>
							</c:if>							
						</c:forEach>
						<!-- more list items -->
					</ul>
				</div>
				<div class="cb-view2">
					<i class=" item-icon icon-coin"></i>
					<a class="link-name">
						${(area2 != null && area2 !=0 )?"":"大於"}
						<span class="color-pink">${area1} ${(area2 != null && area2 !=0 )?"-":""} ${(area2 != null && area2 !=0 )?area2:""}</span>
						組合
					</a>
				</div>
				<div class="cb-products">
					<ul class="products" id="products2">
						<c:forEach var="obj" items="${queryData2}" varStatus="loop">
							<li id='${obj.shelID}li' />								
							<script type="text/javascript">
								showBidshlv("${obj.shelID}li", "${obj.shelID}", "${obj.crClass}", "${obj.bidTermM}","${obj.bidTermC}","${obj.bidIntendNumber}","${obj.bidFulMember}", '', '', 0, "${pageContext.request.contextPath}/product/detail?from=comingFull&shelId=${obj.shelID}", "addCar('${obj.shelID}')");									
      						</script>	
						</c:forEach>
						<!-- more list items -->
					</ul>
				</div>
				<c:if test="${(area2 != null && area2 !=0 )}">
					<div class="cb-view2">
						<i class=" item-icon icon-coin"></i>
						<a class="link-name">
							${(area3 != null && area3 !=0 )?"":"大於"}
							<span class="color-pink">${area2} ${(area3 != null && area3 !=0 )?"-":""} ${(area3 != null && area3 !=0 )?area3:""}</span>
							組合
						</a>
					</div>
					<div class="cb-products">
						<ul class="products" id="products3">
							<c:forEach var="obj" items="${queryData3}" varStatus="loop">
								<li id='${obj.shelID}li' />									
								<script type="text/javascript">
									showBidshlv("${obj.shelID}li", "${obj.shelID}", "${obj.crClass}", "${obj.bidTermM}","${obj.bidTermC}","${obj.bidIntendNumber}","${obj.bidFulMember}", '', '', 0, "${pageContext.request.contextPath}/product/detail?from=comingFull&shelId=${obj.shelID}", "addCar('${obj.shelID}')");									
	       						</script>		
							</c:forEach>
							<!-- more list items -->
						</ul>
					</div>
				</c:if>
				<!--group1 end-->
				
				<!--PAGE END-->
			</div>
		</section>
		<section class="section-content3">
			<div class="btn-readmore">
				<a href="${pageContext.request.contextPath}/qa">
					看不懂競標組合嗎？
					<br />
					了解更多
				</a>
			</div>
		</section>
		<img src="${pageContext.request.contextPath}/resource/assets/images/bg-tri-twin-o-white.svg" width="100%" style="margin-top: -1px;" alt="">
	</div>
	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.min.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/fastclick.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/svg4everybody.min.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/jquery.colorbox.js" type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/resource/assets/javascripts/vendors/accordion.js" type="text/javascript"></script>	
</body>
</html>